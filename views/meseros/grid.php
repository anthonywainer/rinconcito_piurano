<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid meseros-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRES
			</th>
			<th class="ui-widget-header ">
					APELLIDOS
			</th>
			<th class="ui-widget-header ">
					SEXO
			</th>

			<th class="ui-widget-header ">
					SUELDO BÁSICO
			</th>


			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($meseros as $key => $meseros): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($meseros->getId()) ?>&nbsp;</td>
			<td><?php echo h($meseros->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($meseros->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($meseros->getSexo()) ?>&nbsp;</td>
			<td><?php echo h($meseros->getSueldoBasico()) ?>&nbsp;</td>
			<td>

                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('meseros/edit/' . $meseros->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('meseros/delete/' . $meseros->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>