<div id="usa">
<h6>
    <div align="center" >
    <?php echo $meseros->isNew() ? "NUEVO" : "EDITAR" ?> MESEROS</h6>
<form method="post" action="<?php echo site_url('meseros/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($meseros->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="meseros_nombres">Nombres</label>
			<input required class="form-control" id="meseros_nombres" type="text" name="nombres" value="<?php echo h($meseros->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="meseros_apellidos">Apellidos</label>
			<input required class="form-control" id="meseros_apellidos" type="text" name="apellidos" value="<?php echo h($meseros->getApellidos()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="meseros_sexo">Sexo</label>
			<input required class="form-control" id="meseros_sexo" type="text" name="sexo" value="<?php echo h($meseros->getSexo()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="meseros_sueldo_basico">Sueldo Básico</label>
			<input required class="form-control" id="meseros_sueldo_basico" type="number" name="sueldo_basico" value="<?php echo h($meseros->getSueldoBasico()) ?>" />
		</div>
<hr>
	</div>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $meseros->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>