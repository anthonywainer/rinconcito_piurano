<h1>View Meseros</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('meseros/edit/' . $meseros->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Meseros">
		Edit	</a>
	<a href="<?php echo site_url('meseros/delete/' . $meseros->getId()) ?>"
		class="button" data-icon="trash" title="Delete Meseros"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('ventas?mesero_id=' . $meseros->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Meseros">
		Ventas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombres</span>
		<?php echo h($meseros->getNombres()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Apellidos</span>
		<?php echo h($meseros->getApellidos()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Sexo</span>
		<?php echo h($meseros->getSexo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Ingreso</span>
		<?php echo h($meseros->getFechaIngreso(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Sueldo Basico</span>
		<?php echo h($meseros->getSueldoBasico()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Imagen</span>
		<?php echo h($meseros->getImagen()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($meseros->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($meseros->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($meseros->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>