<div id="usa">
<h6>
    <div align="center" >
    <?php echo $grupos->isNew() ? "NUEVO" : "EDITAR" ?> GRUPO</h6>
<form method="post" action="<?php echo site_url('grupos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($grupos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="grupos_descripcion">Descripción</label>
			<input required class="form-control"  id="grupos_descripcion" type="text" name="descripcion" value="<?php echo h($grupos->getDescripcion()) ?>" />
		</div>
        <br>
	</div>
    <h3>Permisos</h3>
    <?php foreach ($modulos as $m ):?>
        <ul>
            <?= $m->nombre ?>
            <?php $perM= Permisos::getAll("WHERE idmodulo= ".$m->id); if($perM){   ?>
                <input type="checkbox" <?php if ($permisos) if(in_array($perM[0]->id,$permisos)==1){echo "checked";}?> name="permisos[]" value="<?= $perM[0]->id ?>">
            <?php } ?>

            <?php foreach (Submodulo::getAll('WHERE idmodulo= '.$m->id) as $sm){?>
                <li><?= $sm->nombre ?>
                    <?php $persM= Permisos::getAll("WHERE idsubmodulo= ".$sm->id); if($persM){
                        foreach ($persM as $pm){
                        ?>
                            <label for=""><?= $pm->nombre ?></label>
                        <input type="checkbox" <?php if ($permisos) if(in_array($pm->id,$permisos)==1){echo "checked"; }?> name="permisos[]" value="<?= $pm->id ?>">
                    <?php } }?>
                </li>
            <?php } ?>

        </ul>
    <?php endforeach; ?>
	<div class="form-action-buttons ui-helper-clearfix" align="right">

		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-secondary"  data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
        <span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $grupos->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
	</div>

</form>
    </div>