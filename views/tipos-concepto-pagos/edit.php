<div id="usa" >
<h6>
    <div align="center" >
    <?php echo $tipos_concepto_pago->isNew() ? "NUEVO" : "EDITAR" ?> TIPOS DE CONCEPTO DE PAGO</h6>
<form method="post" action="<?php echo site_url('tipos-concepto-pagos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($tipos_concepto_pago->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_concepto_pago_descripcion">Descripcion</label>
			<input required class="form-control" id="tipos_concepto_pago_descripcion" type="text" name="descripcion" value="<?php echo h($tipos_concepto_pago->getDescripcion()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
        <span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $tipos_concepto_pago->isNew() ? "Guardar" : "Guardar cambios" ?>" />
		</span>
	</div>
</form>