<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none h-100 b-r-1 px-3" type="button">☰</button>

    <ul class="nav navbar-nav ml-auto">
        <div class="dropdown">
            <a class="dropdown-toggle"
               href="#" id="dropdownMenuLink"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="icon-settings"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="<?php echo site_url('logout') ?>">Cerrar Sesión</a>
            </div>
        </div>

    </ul>
</header>
<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="divider"></li>
                <li class="nav-item" align="center">
                    <img src="<?php echo site_url('imagenes/8.jpg') ?>" class="img-avatar">
                </li>
                <li class="nav-title text-center">
                    <span><?php echo $_SESSION['user']->correo ?></span>
                </li>
                <li class="divider"></li>
                <?php  foreach ($modulosPrincipal as $m): ?>
                    <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="<?php echo $m["icon"] ?>"></i> <?php echo $m["nombre"] ?></a>
                    <ul class="nav-dropdown-items">
                        <?php foreach ($SubmodulosPrincipal as $sm){ if($sm['idmodulo']==$m["id"]){?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url($sm["url"]) ?>">
                                <i class="<?php echo $sm["icon"] ?>"></i> <?php echo $sm["nombre"] ?>
                            </a>
                        </li>
                        <?php }} ?>
                    </ul>
                </li>
                <?php endforeach; ?>
        </nav>

        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <!-- Main content -->
    <main class="main">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <?php echo $params['u'] ?>
            </li>
        </ol>
        <br>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 bod">
                                <?php View::load($params['u'], $params) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>
