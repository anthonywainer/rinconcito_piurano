<h1>View Mesas</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('mesas/edit/' . $mesas->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Mesas">
		Edit	</a>
	<a href="<?php echo site_url('mesas/delete/' . $mesas->getId()) ?>"
		class="button" data-icon="trash" title="Delete Mesas"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('ventas-mesas?mesa_id=' . $mesas->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Mesas Mesas">
		Ventas Mesas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Numero</span>
		<?php echo h($mesas->getNumero()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado Mesa</span>
		<?php echo h($mesas->getEstadoMesa()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Mesa</span>
		<?php echo h($mesas->getMesa()) ?>
	</div>
</div>