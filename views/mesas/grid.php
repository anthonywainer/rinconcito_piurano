<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid mesas-grid  table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NÚMERO
			</th>
			<th class="ui-widget-header ">
					ESTADO DE MESA
			</th>
			<th class="ui-widget-header ">
					MESA
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($mesas as $key => $mesas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($mesas->getId()) ?>&nbsp;</td>
			<td><?php echo h($mesas->getNumero()) ?>&nbsp;</td>
			<td><?php echo h($mesas->getEstadoMesa()) ?>&nbsp;</td>
			<td><?php echo h($mesas->getMesa()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('mesas/edit/' . $mesas->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('mesas/delete/' . $mesas->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>