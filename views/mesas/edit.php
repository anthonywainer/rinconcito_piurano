<div id="usa">
<h6>
    <div align="center" >
    <?php echo $mesas->isNew() ? "NUEVA" : "EDITAR" ?> MESA</h6>
<form method="post" action="<?php echo site_url('mesas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($mesas->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="mesas_numero">Número</label>
			<input required class="form-control" id="mesas_numero" type="text" name="numero" value="<?php echo h($mesas->getNumero()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="mesas_estado_mesa">Estado Mesa</label>
			<input required class="form-control" id="mesas_estado_mesa" type="text" name="estado_mesa" value="<?php echo h($mesas->getEstadoMesa()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="mesas_mesa">Mesa</label>
			<input required class="form-control" id="mesas_mesa" type="text" name="mesa" value="<?php echo h($mesas->getMesa()) ?>" />
		</div>
        <hr>
	</div>
	<div class="form-action-buttons ui-helper-clearfix" align="right" >
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input  class="btn btn-primary" type="submit" value="<?php echo $mesas->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>