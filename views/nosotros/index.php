<nav class="navbar navbar-toggleable-md navbar-light bg-faded" style="background-color: white;">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="">
        <img src="imagenes/logo.png" alt="" class="logo">
    </a>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/rinconcito_piurano/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active" >
                <a class="nav-link" href="nosotros">Nosotros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="carta">Carta</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="ubicacion">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="login">Iniciar Sesión</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
	<div class="nosotros">
		<div class="row">
			<div class="col-12 title">
				<span>Nosotros</span> <i></i>
			</div>
			<div class="col-md-7 historia">
				<h2 for="">Historia</h2>
				<p>La Cevicheria Restaurant "RINCONCITO PIURANO" fue creado
                    el 15 de marzo del 2010 (6 años aproximadamente
                    de creación).Don Otilano y su familia vinieron desde Piura con la mentalidad de formar un negocio familiar en la ciudad de Tarapoto, lo cual pusieron un restaurant Cevicheria, cuenta que en los primeros meses vendía poco , entonces decidieron hacer publicidad de su Cevicheria en los diferentes medios de la ciudad e Tarapoto hasta que se hizo muy conocida y hasta hoy tiene una gran acogida en su Cevicheria.</p>
			</div>
			<div class="col-md-5 res">
				<img src="imagenes/res.png" alt="">
                <img src="imagenes/.png" alt="">
			</div>

		</div>
	</div>	
</div>