<h1>View Permisos Usuario</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('permisos-usuarios/edit/' . $permisos_usuario->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Permisos_usuario">
		Edit	</a>
	<a href="<?php echo site_url('permisos-usuarios/delete/' . $permisos_usuario->getId()) ?>"
		class="button" data-icon="trash" title="Delete Permisos_usuario"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Idusuario</span>
		<?php echo h($permisos_usuario->getUsuariosRelatedByIdusuario()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Idpermiso</span>
		<?php echo h($permisos_usuario->getPermisosRelatedByIdpermiso()) ?>
	</div>
</div>