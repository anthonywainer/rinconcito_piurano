<h1>
	<a href="<?php echo site_url('permisos-usuarios/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Permisos usuario">
		New Permisos usuario
	</a>
	Permisos Usuarios
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('permisos-usuarios/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>