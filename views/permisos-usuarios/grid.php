<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid permisos_usuario-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PermisosUsuario::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PermisosUsuario::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PermisosUsuario::IDUSUARIO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PermisosUsuario::IDUSUARIO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Idusuario
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PermisosUsuario::IDPERMISO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PermisosUsuario::IDPERMISO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Idpermiso
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($permisos_usuarios as $key => $permisos_usuario): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos_usuario->getId()) ?>&nbsp;</td>
			<td><?php echo h($permisos_usuario->getUsuariosRelatedByIdusuario()) ?>&nbsp;</td>
			<td><?php echo h($permisos_usuario->getPermisosRelatedByIdpermiso()) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Permisos_usuario"
					href="<?php echo site_url('permisos-usuarios/show/' . $permisos_usuario->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Permisos_usuario"
					href="<?php echo site_url('permisos-usuarios/edit/' . $permisos_usuario->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Permisos_usuario"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('permisos-usuarios/delete/' . $permisos_usuario->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>