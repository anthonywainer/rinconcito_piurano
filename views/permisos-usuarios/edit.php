<h1><?php echo $permisos_usuario->isNew() ? "New" : "Edit" ?> Permisos Usuario</h1>
<form method="post" action="<?php echo site_url('permisos-usuarios/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<div class="form-field-wrapper">
			<input id="permisos_usuario_id" type="hidden" name="id" value="<?php echo h($permisos_usuario->getId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_usuario_idusuario">Usuarios</label>
			<select id="permisos_usuario_idusuario" name="idusuario">
			<?php foreach (Usuarios::getAll() as $usuarios): ?>
				<option <?php if ($permisos_usuario->getIdusuario() === $usuarios->id ) echo 'selected="selected"' ?> value="<?php echo $usuarios->id ?>"><?php echo $usuarios->nombres ?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_usuario_idpermiso">Permisos</label>
			<select id="permisos_usuario_idpermiso" name="idpermiso">
			<?php foreach (Permisos::doSelect() as $permisos): ?>
				<option <?php if ($permisos_usuario->getIdpermiso() === $permisos->id) echo 'selected="selected"' ?> value="<?php echo $permisos->id ?>"><?php echo $permisos->nombre ?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $permisos_usuario->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>