<div class="listaped">
<table class="table">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Monto</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody >
        <?php  $pp = PedidoProducto::getAll('Where venta_id='.$id);  if (count($pp)>0) foreach ( $pp as $p): ?>
        <tr>
            <?php $pro=Productos::getAll('where id='.$p->getProducto_id()); ?>
            <td width="150">
                <?php  echo $pro[0]->getNombre()?>
            </td>
            <td width="100">
                <?php if($p->getCreatedAt()): ?>
                    <?= $p->getPrecio() ?>
                <?php else: ?>
                <input type="number"
                       min="1"
                       onkeydown="savePrecio(this,'i<?=$p->id?>','<?= site_url('pedido-productos/editarprecio/'.$p->id) ?>',<?=$id?>);"
                       onchange="savePrecio(this,'i<?=$p->id?>','<?= site_url('pedido-productos/editarprecio/'.$p->id) ?>',<?=$id?>);"
                       class="form-control-sm" style="width:100%;"
                       id="i<?=$p->id?>p" value="<?= $p->getPrecio() ?>">
                <?php endif; ?>
            </td>
            <td width="300">
                <div class="form-inline">
                    <div class="form-group">

                        <?php if($p->getCreatedAt()): ?>
                            <?= $p->getCantidad() ?>
                        <?php else: ?>
                            <button class="btn" onclick="document.getElementById('i<?=$p->id?>').stepDown(1); saveP('i<?=$p->id?>',<?=$p->id?>,<?=$id?>); return false;">-</button>
                            <input  href="<?= site_url('pedido-productos/save/') ?>"
                                    id="i<?=$p->id?>" style="width: 30%; text-align: center"
                                    class="form-control" type="number"
                                    min="1"
                                    value="<?= $p->getCantidad() ?>">
                            <button class="btn" onclick="document.getElementById('i<?=$p->id?>').stepUp(1); saveP('i<?=$p->id?>',<?=$p->id?>,<?=$id?>); return false;">+</button>
                        <?php endif; ?>


                    </div>
                </div>

            </td>
            <td>
                <?= $p->getCantidad()*$p->getPrecio() ?>
            </td>


            <td>
                <?php if($p->getCreatedAt()): ?>
                    Emitido
                <a   href="<?= site_url('pedido-productos/delete/'.$p->id) ?>">Cancelar
                    <?php else: ?>
                <a  class="btn btn-danger" href="<?= site_url('pedido-productos/delete/'.$p->id) ?>"><i class="fa fa-trash-o "></i>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>

<?php $vv= Ventas::getAll('where id='.$id)[0]; $tpp = PedidoProducto::getConnection()->query('SELECT
Sum(pedido_producto.total) as total
FROM
pedido_producto
WHERE
pedido_producto.venta_id ='.$id)->fetchAll() ?>

<div class="form-inline">
</div>
    <h1 align="right" >
        <input type="hidden" id="totalP" value="<?= $tpp[0]['total'] ?>">
        S/. <span id="totalF"><?= number_format($tpp[0]['total'],2,'.',',') ?></span>
    </h1>
<div align="right">
    <button class="btn btn-default" onclick="cancelar_pedido(<?=$id?>)" >
        cancelar pedido
    </button>

    <?php

    $vvv= PedidoProducto::getAll('where venta_id='.$id.' and created_at is null');
    if($vvv): ?>
        <i class="btn btn-info" onclick="confirmar(this)"
           href="<?= site_url('ventas/save/'.$id.'/?up=h') ?>" >
            Generar comanda
        </i>
        <?php else:?>
        <?php if($vv->getUpdated_at()){ ?>
        <a class="btn btn-info" href="<?= site_url('movimientos-de-dineros/index/'.$id) ?>" >
            Pagar
        </a>
        <?php } ?>
    <?php endif; ?>



</div>

<script>
    function extra(t) {

        $('#totalF').html((parseFloat($('#totalP').val())+parseFloat($(t).val())).toFixed(2))
    }
</script>
