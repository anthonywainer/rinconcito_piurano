<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid pedido_producto-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::PRODUCTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::PRODUCTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Producto
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::VENTA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::VENTA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Venta
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::CANTIDAD))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::CANTIDAD): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cantidad
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::IMPORTE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::IMPORTE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Importe
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::TOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::TOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Total
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::TIEMPO_ESPERA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::TIEMPO_ESPERA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Tiempo Espera
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => PedidoProducto::ESTADO_PEDIDO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == PedidoProducto::ESTADO_PEDIDO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Estado Pedido
				</a>
			</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($pedido_productos as $key => $pedido_producto): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($pedido_producto->getId()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getProductosRelatedByProductoId()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getVentasRelatedByVentaId()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getCantidad()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getImporte()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getTotal()) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($pedido_producto->getTiempoEspera()) ?>&nbsp;</td>
			<td><?php if ($pedido_producto->getEstadoPedido() === 1) echo 'Yes'; elseif ($pedido_producto->getEstadoPedido() === 0) echo 'No' ?>&nbsp;</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>