<h1>
	<a href="<?php echo site_url('pedido-productos/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Pedido producto">
		New Pedido producto
	</a>
	Pedido Productos
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('pedido-productos/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>