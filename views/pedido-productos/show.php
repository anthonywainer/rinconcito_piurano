<?php $vm= VentasMesas::getAll('where venta_id='.$id) ?>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 comanda">
            <h2 align="center">"rinconcito Piurano"</h2>
            <h4 align="center"> Detalle venta MESA N° <?php
                if (count($vm)>1){
                    $mmm = '';
                    foreach ($vm as $v){
                        $mmm.=Mesas::getAll('where id='.$v->mesa_id)[0]->mesa;
                    }
                    echo $mmm;
                }else{
                    echo Mesas::getAll('where id='.$vm[0]->mesa_id)[0]->mesa;
                }
                ?>   </h4>
            Cliente=
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th>Cant.</th>
                    <th>Descripción</th>
                    <th>S/. Total</th>
                </tr>
                </thead>
                <tbody>
                <?php $tt=0; $pp = PedidoProducto::getAll('Where venta_id='.$id);
                if (count($pp)>0) foreach ( $pp as $p): ?>
                    <tr>
                        <td>
                            <?= $p->getCantidad() ?>
                        </td>
                        <td>
                            <?php $pro=Productos::getAll('where id='.$p->getProducto_id());
                            echo $pro[0]->getNombre()?>
                        </td>
                        <td>
                            <?php echo 'S/. '.$p->getTotal(); $tt+=$p->getTotal(); ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
            <div align="right">
                Total: S/. <?= number_format($tt,2,'.',',') ?>
            </div>
            <hr>
            <div>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Forma de pago</th>
                            <th>Cliente</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach(VentasMovimiento::getAll('where venta_id='.$id) as $vm): ?>
                        <tr>
                            <td>
                                <?= $vm->getMovimiento()->getFormaPago()->descripcion ?>
                            </td>
                            <td><?= $vm->getCliente()->nombres." ".$vm->getCliente()->apellidos ?></td>
                            <td><?= $vm->getCuota() ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>