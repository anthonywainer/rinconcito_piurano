<style>
    .panel-title{
        background: #024C51;
        color: white;
    }
    .proP{
        height: 120px;
        padding: 20px;
    }
    .producto{
        height: 120px;
        padding: 20px;
        text-align: center;
        cursor: pointer;
        border: 2px dashed skyblue;
        border-radius: 5px;
    }
    .no-se{
        cursor: no-drop;
    }
    .no-pro{
        cursor: no-drop;
        border: 2px dashed red;
    }
    .lprod{
        height: 400px;
        overflow-x: hidden;
    }
    .listaped{
        height: 290px;
        overflow-x: hidden;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
            <?php if(PedidoProducto::getAll('where venta_id='.$_GET['id'])){ ?>
                <a class="btn btn-info" href="<?= site_url('ventas/index/')?>" >Atrás</a> <hr>
            <?php }else{ ?>
                <a class="btn btn-info" href="<?= site_url('ventas/delete/'.$_GET['id'])?>" >Atrás</a> <hr>
            <?php } ?>
        </div>
        <input type="search" onkeyup="buscar_prod(this)" class="form-control"
               placeholder="Buscar Producto...">
        <div class="row lprod" >
                <?php  foreach (Productos::getAll() as $p): ?>
                    <div idd="<?= $p->getId() ?>" class="col-md-4 proP <?php if(in_array($p->id,$pp)){ echo ' no-se "'; }else{ echo ' " onclick="add_producto(this,'.$_GET['id'].')" '; }?>
                         href="<?= site_url('pedido-productos/save/') ?>" >
                        <div class="producto  <?php if(in_array($p->id,$pp)){ echo ' no-pro'; } ?>"  >
                            <h5>s/.<?= $p->getPrecio() ?></h5>
                            <strong style="color: #00a67c"><?= $p->nombre ?></strong>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>


    </div>
    <div class="col-md-6">

        <div class="panel-title" >
            <img src="<?= site_url('imagenes/fork-knife.png', true)?>" alt="tenedor,cuchillo" height="70" width="70">
            <h2 style="margin-top: -60px;padding-bottom: 25px" align="center">Mesa: <?php $vm= VentasMesas::getAll('where venta_id='.$_GET['id']);
                if (count($vm)>1){
                    $mmm = '';
                    foreach ($vm as $v){
                        $mmm.=Mesas::getAll('where id='.$v->mesa_id)[0]->mesa;
                    }
                    echo $mmm;
                }else{
                    echo Mesas::getAll('where id='.$vm[0]->mesa_id)[0]->mesa;
                }
                ?>
            </h2>
        </div>
        <br>

            <div id="form-ped">

                <?php  View::load('pedido-productos/edit_prod',['id'=>$_GET['id']]) ?>
            </div>




    </div>
</div>
<script>
    function confirmar(t) {
        $.post($(t).attr('href'),{'extra':$("#extra").val(),'observacion':$("#observacion").val()}).done(
            function (data) {
                window.history.pushState('', '', '<?= site_url("ventas/show/".$_GET['id'])?>');
                $(".container-fluid").empty().html(data);
            }
        );
    }
    function cancelar_pedido(id) {
        if (confirm('¿Está seguro de cancelar el pedido sin antes guardar?')) {
            window.location.href = "<?php echo site_url('ventas/delete/') ?>/"+id;
        }
        return false;
    }
    function add_producto(t,idv) {
            $.post($(t).attr('href'), {'venta_id': idv, 'producto_id': $(t).attr('idd')}).done(
                function (data) {
                    $('#form-ped').html(data);
                    $(t).removeAttr('onclick').addClass('no-se');
                    $(t).children('.producto').addClass('no-pro');

                }
            );
    }
 function saveP(id,t,idv) {
     $.post($('#'+id).attr('href')+'/'+t,
         {'cantidad':$('#'+id).val(),
             'precio': $('#'+id+'p').val(),
             'total':$('#'+id).val()*$('#'+id+'p').val(),
             'venta_id': idv,
         } ).done(
             function (data) {
                 $('#form-ped').html(data);
             }
     )
 }
    function savePrecio(t,id,url,idv) {
        $.post(url,
            {'cantidad':$('#'+id).val(),
                'precio': $('#'+id+'p').val(),
                'total':$('#'+id).val()*$('#'+id+'p').val(),
                'venta_id': idv,
            } ).fail(function() {
            $(t).prop('disabled',true);
            toastr.error('usted no tiene permisos!')
        }).done(
            function (data) {
                $('#form-ped').html(data);
            }
        )
    }
 function buscar_prod(t) {
         var rex = new RegExp($(t).val(), 'i');
         $('.proP').hide();
         $('.proP').filter(function () {
             return rex.test($(this).text());
         }).show();
 }
</script>
