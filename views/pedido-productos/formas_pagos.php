<div class="col-md-6" align="left">
    PAGOS
</div>
<?php $q = "SELECT SUM(cuota) - total as vuelto
                            FROM
                            ventas_movimiento
                            WHERE
                            ventas_movimiento.venta_id =  
                           ".$idv;
$v = VentasMovimiento::getConnection()->query($q)->fetchAll();
?>
<div class="col-md-6" align="right">
    <?php  if($v[0]['vuelto']<0 or  $v[0]['vuelto']==''): ?>
        <button href="<?=site_url('ventas-movimientos/save/')?>" onclick="agregar_pago(this,<?= $idv ?>);
        return false;" class="btn btn-info">Agregar</button>
    <?php endif; ?>
</div>

<div class="col-md-12" style="height: 250px">

    <table class="table table-sm">
        <thead>
        <tr>
            <th>Forma de Pago</th>
            <th>Monto</th>
            <th>Acción</th>
        </  tr>
        </thead>
        <tbody >
        <?php foreach (VentasMovimiento::getAll('where venta_id='.$idv) as $vm){
            $m= MovimientosDeDinero::getAll('where id='.$vm->getMovimiento_id());
            ?>
            <tr>
                <td>
                    <select required href="<?= site_url('movimientos-de-dineros/save/'.$vm->getMovimiento_id()) ?>"
                            onchange="update_mov(this,<?=$idv?>)"
                            class="form-control" name="forma_pago_id" >
                        <option value="">Seleccionar...</option>
                        <?php foreach (FormasPago::getAll() as $fp): ?>
                            <option <?php if($m[0]->getForma_pago_id()==$fp->id){echo 'selected'; }?>  value="<?= $fp->id ?>"><?= $fp->descripcion ?></option>
                        <?php endforeach; ?>
                    </select>

                </td>
                <td>
                    <div class="form-inline">
                        <div class="form-group">
                            <button class="btn btn-default">S/.</button>
                            <input href="<?= site_url('ventas-movimientos/save/'.$vm->id) ?>"
                                   onblur="update_monto(this,<?=$vm->getCuota()?>,
                                           '<?php if($m[0]->getForma_pago_id()==12){echo 'yes';}else{echo 'no';}?>'
                                           )"
                                   onkeyup="update_monto(this,<?=$vm->getCuota()?>,
                                           '<?php if($m[0]->getForma_pago_id()==12){echo 'yes';}else{echo 'no';}?>'
                                           )"
                                   step="any" id="efec"
                                   name="efectivo" style="width: 60%"
                                   value="<?php if($vm->getEfectivo()==0){ echo $vm->getCuota();}else{echo $vm->getEfectivo();} ?>"
                                   class="form-control input-sm"
                                   type="number" min="0">
                            <input type="hidden" id="totv" value="<?=$vm->getTotal()?>">
                        </div>
                    </div>
                </td>
                <td>
                    <a class="btn btn-danger"  href="<?= site_url('ventas-movimientos/delete/'.$vm->id.'/?venta_id='.$idv) ?>"> <i class="fa fa-trash-o "></i></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>


</div>

<br>
<div class="col-md-12" align="right" >
    <div class="panel-title row" align="center">


        <?php if($v[0]['vuelto']>0): ?>
            <div class="col-md-6">
                <strong >Vuelto: </strong>
            </div>
            <div class="col-md-6">
                <span class="dinero"  id="vuelto"> S/. <?=$v[0]['vuelto']?></span>
            </div>
        <?php else: ?>
            <div class="col-md-6">
                <strong >Falta pagar: </strong>
            </div>
            <div class="col-md-6">
                <span class="dinero"  id="vuelto"> S/. <?=$v[0]['vuelto']*-1?></span>
            </div>
        <?php endif; ?>
    </div>
    <a class="btn btn-error" href="<?= site_url('pedido-productos/?id='.$idv )?>">Cancelar</a>
    <?php if($v[0]['vuelto']!='') if($v[0]['vuelto']>=0): ?>
        <button  class="btn btn-info"
                 onclick="movimiento('<?= site_url('movimientos-de-dineros/show/'.$idv)?>')"
            >Pagar</button>
    <?php endif; ?>
</div>
<script>
    $("input[type=number]").keypress(function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });
</script>