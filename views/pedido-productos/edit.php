<h1><?php echo $pedido_producto->isNew() ? "New" : "Edit" ?> Pedido Producto</h1>
<form method="post" action="<?php echo site_url('pedido-productos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_id">Id</label>
			<input id="pedido_producto_id" type="text" name="id" value="<?php echo h($pedido_producto->getId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_producto_id">Productos</label>
			<select id="pedido_producto_producto_id" name="producto_id">
			<?php foreach (Productos::doSelect() as $productos): ?>
				<option <?php if ($pedido_producto->getProductoId() === $productos->getId()) echo 'selected="selected"' ?> value="<?php echo $productos->getId() ?>"><?php echo $productos?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_venta_id">Ventas</label>
			<select id="pedido_producto_venta_id" name="venta_id">
			<?php foreach (Ventas::doSelect() as $ventas): ?>
				<option <?php if ($pedido_producto->getVentaId() === $ventas->getId()) echo 'selected="selected"' ?> value="<?php echo $ventas->getId() ?>"><?php echo $ventas?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_cantidad">Cantidad</label>
			<input id="pedido_producto_cantidad" type="text" name="cantidad" value="<?php echo h($pedido_producto->getCantidad()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_importe">Importe</label>
			<input id="pedido_producto_importe" type="text" name="importe" value="<?php echo h($pedido_producto->getImporte()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_total">Total</label>
			<input id="pedido_producto_total" type="text" name="total" value="<?php echo h($pedido_producto->getTotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_created_at">Created At</label>
			<input id="pedido_producto_created_at" type="text" name="created_at" value="<?php echo h($pedido_producto->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_updated_at">Updated At</label>
			<input id="pedido_producto_updated_at" type="text" name="updated_at" value="<?php echo h($pedido_producto->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_deleted_at">Deleted At</label>
			<input id="pedido_producto_deleted_at" type="text" name="deleted_at" value="<?php echo h($pedido_producto->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_tiempo_espera">Tiempo Espera</label>
			<input id="pedido_producto_tiempo_espera" type="text" name="tiempo_espera" value="<?php echo h($pedido_producto->getTiempoEspera()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="pedido_producto_estado_pedido">Estado Pedido</label>
			<label>
				<input <?php if ($pedido_producto->getEstadoPedido() === 1) echo 'checked="checked"' ?> name="estado_pedido" type="radio" value="1" />
				Yes
			</label>

			<label>
				<input <?php if ($pedido_producto->getEstadoPedido() === 0) echo 'checked="checked"' ?> name="estado_pedido" type="radio" value="0" />
				No
			</label>
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $pedido_producto->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>