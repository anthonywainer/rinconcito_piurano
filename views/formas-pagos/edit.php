<div id="usa">
<h6>
    <div align="center" >
    <?php echo $formas_pago->isNew() ? "NUEVA" : "EDITAR" ?> FORMA DE PAGO</h6>
<form method="post" action="<?php echo site_url('formas-pagos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($formas_pago->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="formas_pago_descripcion">Descripción</label>
			<input required class="form-control" id="formas_pago_descripcion" type="text" name="descripcion" value="<?php echo h($formas_pago->getDescripcion()) ?>" />
		</div>

	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix"align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary"  data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
        <span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $formas_pago->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
	</div>
</form>
</div>