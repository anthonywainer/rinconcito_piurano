<h1>View Formas Pago</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('formas-pagos/edit/' . $formas_pago->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Formas_pago">
		Edit	</a>
	<a href="<?php echo site_url('formas-pagos/delete/' . $formas_pago->getId()) ?>"
		class="button" data-icon="trash" title="Delete Formas_pago"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('movimientos-de-dineros?forma_pago_id=' . $formas_pago->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Movimientos De Dineros Formas_pago">
		Movimientos De Dineros	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($formas_pago->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($formas_pago->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($formas_pago->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($formas_pago->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>