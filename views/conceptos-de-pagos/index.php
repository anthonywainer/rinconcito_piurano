<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" href="<?= site_url('cajas/show') ?>">Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('cajas') ?>">Historial Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="<?= site_url('conceptos-de-pagos') ?>">Operaciones</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="<?= site_url('movimientos-de-dineros') ?>">Transacciones</a>
    </li>

</ul>
<div class="row">
    <div class="col-md-12">
        <br>
	<i href="<?php echo site_url('conceptos-de-pagos/edit') ?>" onclick="openmodal(this)"
	   class="button"
	   data-icon="plusthick"
	   title="New Conceptos de pago">
        <button class="btn btn-default" style="background: #024c51; color: white">
            +1   Nueva Operación
        </button>
	</i>
    <hr>

    <div class="row">
        <div class="form-group col-md-4">
            <div class="input-group">
                <input id="filtrar" type="search" onkeyup="buscar_tabla(this)" class="form-control" placeholder="Buscar Conceptos de Pago">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
    </div>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('conceptos-de-pagos/grid', $params) ?>
</div>
<?php View::load('pager', compact('pager')) ?>
    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #024C51">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
        <script>
            function check_max(t,c) {
                if ($('.conpag:checked').val()==2){
                    if ($(t).val()>c){
                        $(t).val('');
                        toastr.error("Monto Insuficiente en caja");

                    }
                }
            }
        </script>