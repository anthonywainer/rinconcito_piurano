<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid conceptos_de_pago-grid  table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					DESCRIPCIÓN
			</th>
			<th class="ui-widget-header ">
					TIPO DE PAGO
			</th>
            <th>
                    Monto
            </th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($conceptos_de_pagos as $key => $conceptos_de_pago): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($conceptos_de_pago->getId()) ?>&nbsp;</td>
			<td><?php echo h($conceptos_de_pago->getDescripcion()) ?>&nbsp;</td>
			<td><?= TiposConceptoPago::getAll('where id='.$conceptos_de_pago->getMovimiento()->concepto_de_pago_id)[0]->getDescripcion() ?>&nbsp;</td>
            <th>
                <?php echo h($conceptos_de_pago->getMonto()) ?>&nbsp;
            </th>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('conceptos-de-pagos/edit/' . $conceptos_de_pago->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('conceptos-de-pagos/delete/' . $conceptos_de_pago->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>