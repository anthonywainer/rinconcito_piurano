<h1>View Conceptos De Pago</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('conceptos-de-pagos/edit/' . $conceptos_de_pago->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Conceptos_de_pago">
		Edit	</a>
	<a href="<?php echo site_url('conceptos-de-pagos/delete/' . $conceptos_de_pago->getId()) ?>"
		class="button" data-icon="trash" title="Delete Conceptos_de_pago"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('movimientos-de-dineros?concepto_de_pago_id=' . $conceptos_de_pago->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Movimientos De Dineros Conceptos_de_pago">
		Movimientos De Dineros	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($conceptos_de_pago->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Tipo De Pago</span>
		<?php echo h($conceptos_de_pago->getTiposConceptoPagoRelatedByTipoDePagoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($conceptos_de_pago->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($conceptos_de_pago->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($conceptos_de_pago->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>