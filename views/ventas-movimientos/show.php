<h1>View Ventas Movimiento</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('ventas-movimientos/edit/' . $ventas_movimiento->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Ventas_movimiento">
		Edit	</a>
	<a href="<?php echo site_url('ventas-movimientos/delete/' . $ventas_movimiento->getId()) ?>"
		class="button" data-icon="trash" title="Delete Ventas_movimiento"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Venta</span>
		<?php echo h($ventas_movimiento->getVentasRelatedByVentaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Subtotal</span>
		<?php echo h($ventas_movimiento->getSubtotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Movimiento</span>
		<?php echo h($ventas_movimiento->getMovimientosDeDineroRelatedByMovimientoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha</span>
		<?php echo h($ventas_movimiento->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cliente</span>
		<?php echo h($ventas_movimiento->getClientesRelatedByClienteId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descuento</span>
		<?php echo h($ventas_movimiento->getDescuento()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Porcentaje</span>
		<?php echo h($ventas_movimiento->getPorcentaje()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Total</span>
		<?php echo h($ventas_movimiento->getTotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Efectivo</span>
		<?php echo h($ventas_movimiento->getEfectivo()) ?>
	</div>
</div>