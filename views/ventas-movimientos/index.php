<h1>
	<a href="<?php echo site_url('ventas-movimientos/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Ventas movimiento">
		New Ventas movimiento
	</a>
	Ventas Movimientos
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('ventas-movimientos/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>