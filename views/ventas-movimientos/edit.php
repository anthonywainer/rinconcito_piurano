<h1><?php echo $ventas_movimiento->isNew() ? "New" : "Edit" ?> Ventas Movimiento</h1>
<form method="post" action="<?php echo site_url('ventas-movimientos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas_movimiento->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_venta_id">Ventas</label>
			<select id="ventas_movimiento_venta_id" name="venta_id">
			<?php foreach (Ventas::doSelect() as $ventas): ?>
				<option <?php if ($ventas_movimiento->getVentaId() === $ventas->getId()) echo 'selected="selected"' ?> value="<?php echo $ventas->getId() ?>"><?php echo $ventas?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_subtotal">Subtotal</label>
			<input id="ventas_movimiento_subtotal" type="text" name="subtotal" value="<?php echo h($ventas_movimiento->getSubtotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_movimiento_id">Movimientos De Dinero</label>
			<select id="ventas_movimiento_movimiento_id" name="movimiento_id">
			<?php foreach (MovimientosDeDinero::doSelect() as $movimientos_de_dinero): ?>
				<option <?php if ($ventas_movimiento->getMovimientoId() === $movimientos_de_dinero->getId()) echo 'selected="selected"' ?> value="<?php echo $movimientos_de_dinero->getId() ?>"><?php echo $movimientos_de_dinero?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_fecha">Fecha</label>
			<input id="ventas_movimiento_fecha" type="text" name="fecha" value="<?php echo h($ventas_movimiento->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_cliente_id">Clientes</label>
			<select id="ventas_movimiento_cliente_id" name="cliente_id">
			<?php foreach (Clientes::doSelect() as $clientes): ?>
				<option <?php if ($ventas_movimiento->getClienteId() === $clientes->getId()) echo 'selected="selected"' ?> value="<?php echo $clientes->getId() ?>"><?php echo $clientes?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_descuento">Descuento</label>
			<input id="ventas_movimiento_descuento" type="text" name="descuento" value="<?php echo h($ventas_movimiento->getDescuento()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_porcentaje">Porcentaje</label>
			<input id="ventas_movimiento_porcentaje" type="text" name="porcentaje" value="<?php echo h($ventas_movimiento->getPorcentaje()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_total">Total</label>
			<input id="ventas_movimiento_total" type="text" name="total" value="<?php echo h($ventas_movimiento->getTotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_efectivo">Efectivo</label>
			<input id="ventas_movimiento_efectivo" type="text" name="efectivo" value="<?php echo h($ventas_movimiento->getEfectivo()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $ventas_movimiento->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>