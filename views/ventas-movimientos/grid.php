<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid ventas_movimiento-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::VENTA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::VENTA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Venta
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::SUBTOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::SUBTOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Subtotal
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::MOVIMIENTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::MOVIMIENTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Movimiento
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::FECHA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::FECHA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::CLIENTE_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::CLIENTE_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cliente
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::DESCUENTO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::DESCUENTO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descuento
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::PORCENTAJE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::PORCENTAJE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Porcentaje
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::TOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::TOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Total
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMovimiento::EFECTIVO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMovimiento::EFECTIVO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Efectivo
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ventas_movimientos as $key => $ventas_movimiento): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ventas_movimiento->getId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getVentasRelatedByVentaId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getSubtotal()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getMovimientosDeDineroRelatedByMovimientoId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getClientesRelatedByClienteId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getDescuento()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getPorcentaje()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getTotal()) ?>&nbsp;</td>
			<td><?php echo h($ventas_movimiento->getEfectivo()) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Ventas_movimiento"
					href="<?php echo site_url('ventas-movimientos/show/' . $ventas_movimiento->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Ventas_movimiento"
					href="<?php echo site_url('ventas-movimientos/edit/' . $ventas_movimiento->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Ventas_movimiento"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('ventas-movimientos/delete/' . $ventas_movimiento->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>