<h1>View Ventas Delivery</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('ventas-deliveries/edit/' . $ventas_delivery->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Ventas_delivery">
		Edit	</a>
	<a href="<?php echo site_url('ventas-deliveries/delete/' . $ventas_delivery->getId()) ?>"
		class="button" data-icon="trash" title="Delete Ventas_delivery"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Movimiento</span>
		<?php echo h($ventas_delivery->getMovimientosDeDineroRelatedByMovimientoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Delivery</span>
		<?php echo h($ventas_delivery->getDeliveriesRelatedByDeliveryId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Subtotal</span>
		<?php echo h($ventas_delivery->getSubtotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha</span>
		<?php echo h($ventas_delivery->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descuento</span>
		<?php echo h($ventas_delivery->getDescuento()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Porcentaje</span>
		<?php echo h($ventas_delivery->getPorcentaje()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Total</span>
		<?php echo h($ventas_delivery->getTotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Efectivo</span>
		<?php echo h($ventas_delivery->getEfectivo()) ?>
	</div>
</div>