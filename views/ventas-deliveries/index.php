<h1>
	<a href="<?php echo site_url('ventas-deliveries/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Ventas delivery">
		New Ventas delivery
	</a>
	Ventas Deliveries
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('ventas-deliveries/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>