<h1><?php echo $ventas_delivery->isNew() ? "New" : "Edit" ?> Ventas Delivery</h1>
<form method="post" action="<?php echo site_url('ventas-deliveries/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas_delivery->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_movimiento_id">Movimientos De Dinero</label>
			<select id="ventas_delivery_movimiento_id" name="movimiento_id">
			<?php foreach (MovimientosDeDinero::doSelect() as $movimientos_de_dinero): ?>
				<option <?php if ($ventas_delivery->getMovimientoId() === $movimientos_de_dinero->getId()) echo 'selected="selected"' ?> value="<?php echo $movimientos_de_dinero->getId() ?>"><?php echo $movimientos_de_dinero?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_delivery_id">Deliveries</label>
			<select id="ventas_delivery_delivery_id" name="delivery_id">
			<?php foreach (Deliveries::doSelect() as $deliveries): ?>
				<option <?php if ($ventas_delivery->getDeliveryId() === $deliveries->getId()) echo 'selected="selected"' ?> value="<?php echo $deliveries->getId() ?>"><?php echo $deliveries?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_subtotal">Subtotal</label>
			<input id="ventas_delivery_subtotal" type="text" name="subtotal" value="<?php echo h($ventas_delivery->getSubtotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_fecha">Fecha</label>
			<input id="ventas_delivery_fecha" type="text" name="fecha" value="<?php echo h($ventas_delivery->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_descuento">Descuento</label>
			<input id="ventas_delivery_descuento" type="text" name="descuento" value="<?php echo h($ventas_delivery->getDescuento()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_porcentaje">Porcentaje</label>
			<input id="ventas_delivery_porcentaje" type="text" name="porcentaje" value="<?php echo h($ventas_delivery->getPorcentaje()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_total">Total</label>
			<input id="ventas_delivery_total" type="text" name="total" value="<?php echo h($ventas_delivery->getTotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_delivery_efectivo">Efectivo</label>
			<input id="ventas_delivery_efectivo" type="text" name="efectivo" value="<?php echo h($ventas_delivery->getEfectivo()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $ventas_delivery->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>