<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid ventas_delivery-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::MOVIMIENTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::MOVIMIENTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Movimiento
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::DELIVERY_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::DELIVERY_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Delivery
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::SUBTOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::SUBTOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Subtotal
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::FECHA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::FECHA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::DESCUENTO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::DESCUENTO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descuento
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::PORCENTAJE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::PORCENTAJE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Porcentaje
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::TOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::TOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Total
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasDelivery::EFECTIVO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasDelivery::EFECTIVO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Efectivo
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ventas_deliveries as $key => $ventas_delivery): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ventas_delivery->getId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getMovimientosDeDineroRelatedByMovimientoId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getDeliveriesRelatedByDeliveryId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getSubtotal()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getDescuento()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getPorcentaje()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getTotal()) ?>&nbsp;</td>
			<td><?php echo h($ventas_delivery->getEfectivo()) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Ventas_delivery"
					href="<?php echo site_url('ventas-deliveries/show/' . $ventas_delivery->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Ventas_delivery"
					href="<?php echo site_url('ventas-deliveries/edit/' . $ventas_delivery->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Ventas_delivery"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('ventas-deliveries/delete/' . $ventas_delivery->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>