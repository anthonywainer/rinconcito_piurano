<h1>View Ventas Mesas</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('ventas-mesas/edit/' . $ventas_mesas->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Ventas_mesas">
		Edit	</a>
	<a href="<?php echo site_url('ventas-mesas/delete/' . $ventas_mesas->getId()) ?>"
		class="button" data-icon="trash" title="Delete Ventas_mesas"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Venta</span>
		<?php echo h($ventas_mesas->getVentasRelatedByVentaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Mesa</span>
		<?php echo h($ventas_mesas->getMesasRelatedByMesaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado Venta Mesa</span>
		<?php if ($ventas_mesas->getEstadoVentaMesa() === 1) echo 'Yes'; elseif ($ventas_mesas->getEstadoVentaMesa() === 0) echo 'No' ?>
	</div>
</div>