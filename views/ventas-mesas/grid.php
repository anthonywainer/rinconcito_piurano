<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid ventas_mesas-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMesas::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMesas::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMesas::VENTA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMesas::VENTA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Venta
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMesas::MESA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMesas::MESA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Mesa
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasMesas::ESTADO_VENTA_MESA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasMesas::ESTADO_VENTA_MESA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Estado Venta Mesa
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ventas_mesas as $key => $ventas_mesas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ventas_mesas->getId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_mesas->getVentasRelatedByVentaId()) ?>&nbsp;</td>
			<td><?php echo h($ventas_mesas->getMesasRelatedByMesaId()) ?>&nbsp;</td>
			<td><?php if ($ventas_mesas->getEstadoVentaMesa() === 1) echo 'Yes'; elseif ($ventas_mesas->getEstadoVentaMesa() === 0) echo 'No' ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Ventas_mesas"
					href="<?php echo site_url('ventas-mesas/show/' . $ventas_mesas->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Ventas_mesas"
					href="<?php echo site_url('ventas-mesas/edit/' . $ventas_mesas->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Ventas_mesas"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('ventas-mesas/delete/' . $ventas_mesas->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>