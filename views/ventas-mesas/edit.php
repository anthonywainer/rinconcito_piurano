<h1><?php echo $ventas_mesas->isNew() ? "New" : "Edit" ?> Ventas Mesas</h1>
<form method="post" action="<?php echo site_url('ventas-mesas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas_mesas->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_mesas_venta_id">Ventas</label>
			<select id="ventas_mesas_venta_id" name="venta_id">
			<?php foreach (Ventas::doSelect() as $ventas): ?>
				<option <?php if ($ventas_mesas->getVentaId() === $ventas->getId()) echo 'selected="selected"' ?> value="<?php echo $ventas->getId() ?>"><?php echo $ventas?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_mesas_mesa_id">Mesas</label>
			<select id="ventas_mesas_mesa_id" name="mesa_id">
			<?php foreach (Mesas::doSelect() as $mesas): ?>
				<option <?php if ($ventas_mesas->getMesaId() === $mesas->getId()) echo 'selected="selected"' ?> value="<?php echo $mesas->getId() ?>"><?php echo $mesas?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_mesas_estado_venta_mesa">Estado Venta Mesa</label>
			<label>
				<input <?php if ($ventas_mesas->getEstadoVentaMesa() === 1) echo 'checked="checked"' ?> name="estado_venta_mesa" type="radio" value="1" />
				Yes
			</label>

			<label>
				<input <?php if ($ventas_mesas->getEstadoVentaMesa() === 0) echo 'checked="checked"' ?> name="estado_venta_mesa" type="radio" value="0" />
				No
			</label>
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $ventas_mesas->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>