<h1>
	<a href="<?php echo site_url('ventas-mesas/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Ventas mesas">
		New Ventas mesas
	</a>
	Ventas Mesas
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('ventas-mesas/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>