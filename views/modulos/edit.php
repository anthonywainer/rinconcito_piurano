<div id="usa">
<h6><div align="center" >
    <?php echo $modulos->isNew() ? "NUEVO" : "EDITAR" ?> MODULOS</h6>
<form method="post" action="<?php echo site_url('modulos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($modulos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="modulos_nombre">Nombre</label>
			<input required class="form-control" id="modulos_nombre" type="text" name="nombre" value="<?php echo h($modulos->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="modulos_url">Url</label>
			<input required class="form-control" id="modulos_url" type="text" name="url" value="<?php echo h($modulos->getUrl()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="modulos_icon">Icon</label>
			<input required class="form-control" id="modulos_icon" type="text" name="icon" value="<?php echo h($modulos->getIcon()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $modulos->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
	</div>
</form>