<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid modulos-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
            <th class="ui-widget-header ">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					URL
			</th>
			<th class="ui-widget-header ">
					ICON
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIOES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($modulos as $key => $modulos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($modulos->getId()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getUrl()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getIcon()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('modulos/edit/' . $modulos->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('modulos/delete/' . $modulos->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
				<a class="btn btn-warning" title="ver modulos"href="<?php echo site_url('submodulos?idmodulo=' . $modulos->getId()) ?>">
                    <i class="fa fa-search "></i> Submódulos
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>