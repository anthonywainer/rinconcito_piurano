<h1>View Modulos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('modulos/edit/' . $modulos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Modulos">
		Edit	</a>
	<a href="<?php echo site_url('modulos/delete/' . $modulos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Modulos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('submodulos?idmodulo=' . $modulos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Submodulos Modulos">
		Submodulos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($modulos->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Url</span>
		<?php echo h($modulos->getUrl()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Icon</span>
		<?php echo h($modulos->getIcon()) ?>
	</div>
</div>