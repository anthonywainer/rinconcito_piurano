<h1>
	<a href="<?php echo site_url('urls-permitidas/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Urls permitidas">
		New Urls permitidas
	</a>
	Urls Permitidas
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('urls-permitidas/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>