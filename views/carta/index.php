<nav class="navbar navbar-toggleable-md navbar-light bg-faded" style="background-color: white;">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="">
        <img src="imagenes/logo.png" alt="" class="logo">
    </a>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/rinconcito_piurano/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item" >
                <a class="nav-link" href="nosotros">Nosotros</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="carta">Carta</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="ubicacion">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="login">Iniciar Sesión</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
	<div class="carta">
		<div class="row">
			<div class="col-md-12 title">
				<span>Platos Típicos</span> <i></i>
			</div>
			<div class="col-md-12 tipicos">
                <div class="row">
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/arrozpato.png" alt="">
                            <span>Arroz con Pato</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/lomito.png" alt="">
                            <span>Lomito Saltado</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/cabrito.png" alt="">
                            <span>Cabrito</span>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-12 title">
				<span>Pescados y Mariscos</span> <i></i>
			</div>
			<div class="col-md-12 tipicos">
                <div class="row">
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/chicharron.png" alt="">
                            <span>Chicharrón de Pescado</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/mixto.png" alt="">
                            <span>Ceviche Mixto</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/chaufa.png" alt="">
                            <span>Chaufa con Langostinos</span>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-12 title">
				<span>Bebidas y Licores</span> <i></i>
			</div>
			<div class="col-md-12 tipicos">
                <div class="row">
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/arrozpato.png" alt="">
                            <span>Refrescos</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/jora.png" alt="">
                            <span>Chica de Jora</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tipico-plato">
                            <img src="imagenes/cabrito.png" alt="">
                            <span>Cervezas</span>
                        </div>
                    </div>
                </div>
			</div>

		</div>
	</div>



</div>