<h1>
	<a href="<?php echo site_url('usuarios-grupos/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Usuarios grupo">
		New Usuarios grupo
	</a>
	Usuarios Grupos
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('usuarios-grupos/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>