<h1>View Usuarios Grupo</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('usuarios-grupos/edit/' . $usuarios_grupo->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Usuarios_grupo">
		Edit	</a>
	<a href="<?php echo site_url('usuarios-grupos/delete/' . $usuarios_grupo->getId()) ?>"
		class="button" data-icon="trash" title="Delete Usuarios_grupo"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Usuario</span>
		<?php echo h($usuarios_grupo->getUsuariosRelatedByUsuarioId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Grupo</span>
		<?php echo h($usuarios_grupo->getGruposRelatedByGrupoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($usuarios_grupo->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($usuarios_grupo->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($usuarios_grupo->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>