<h1>View Vouchers</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('vouchers/edit/' . $vouchers->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Vouchers">
		Edit	</a>
	<a href="<?php echo site_url('vouchers/delete/' . $vouchers->getId()) ?>"
		class="button" data-icon="trash" title="Delete Vouchers"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('movimientos-de-dineros?voucher_id=' . $vouchers->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Movimientos De Dineros Vouchers">
		Movimientos De Dineros	</a>
	<a href="<?php echo site_url('sery?voucher_id=' . $vouchers->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Sery Vouchers">
		Sery	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Tipo Comprobante</span>
		<?php echo h($vouchers->getTipoComprobante()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha</span>
		<?php echo h($vouchers->getFecha(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($vouchers->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($vouchers->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($vouchers->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>