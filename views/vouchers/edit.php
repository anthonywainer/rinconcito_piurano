<div id="usa">
<h6>
    <div align="center" >
    <?php echo $vouchers->isNew() ? "NUEVO" : "EDITAR"?> VOUCHER</h6>
<form method="post" action="<?php echo site_url('vouchers/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($vouchers->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="vouchers_tipo_comprobante">Tipo Comprobante</label>
			<input required class="form-control" id="vouchers_tipo_comprobante" type="text" name="tipo_comprobante" value="<?php echo h($vouchers->getTipoComprobante()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix"align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $vouchers->isNew() ? "Guardar" : "Guardar Cambio" ?>" />
		</span>

	</div>
</form>
</div>