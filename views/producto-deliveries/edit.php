<h1><?php echo $producto_delivery->isNew() ? "New" : "Edit" ?> Producto Delivery</h1>
<form method="post" action="<?php echo site_url('producto-deliveries/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_delivery_id">Id</label>
			<input id="producto_delivery_id" type="hidden" name="id" value="<?php echo h($producto_delivery->getId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_delivery_producto_id">Productos</label>
			<select id="producto_delivery_producto_id" name="producto_id">
			<?php foreach (Productos::doSelect() as $productos): ?>
				<option <?php if ($producto_delivery->getProductoId() === $productos->getId()) echo 'selected="selected"' ?> value="<?php echo $productos->getId() ?>"><?php echo $productos?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_delivery_delivery_id">Deliveries</label>
			<select id="producto_delivery_delivery_id" name="delivery_id">
			<?php foreach (Deliveries::doSelect() as $deliveries): ?>
				<option <?php if ($producto_delivery->getDeliveryId() === $deliveries->getId()) echo 'selected="selected"' ?> value="<?php echo $deliveries->getId() ?>"><?php echo $deliveries?></option>
			<?php endforeach ?>
			</select>
		</div>

	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $producto_delivery->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>