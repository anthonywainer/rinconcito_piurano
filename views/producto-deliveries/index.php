<h1>
	<a href="<?php echo site_url('producto-deliveries/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Producto delivery">
		New Producto delivery
	</a>
	Producto Deliveries
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('producto-deliveries/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>