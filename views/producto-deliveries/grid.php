<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid producto_delivery-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::PRODUCTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::PRODUCTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Producto
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::DELIVERY_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::DELIVERY_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Delivery
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::CANTIDAD))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::CANTIDAD): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cantidad
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::TOTAL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::TOTAL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Total
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoDelivery::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoDelivery::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($producto_deliveries as $key => $producto_delivery): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($producto_delivery->getId()) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getProductosRelatedByProductoId()) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getDeliveriesRelatedByDeliveryId()) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getCantidad()) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getTotal()) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($producto_delivery->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>