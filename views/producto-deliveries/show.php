<h1>View Producto Delivery</h1>
<div class="action-buttons ui-helper-clearfix">
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Id</span>
		<?php echo h($producto_delivery->getId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Producto</span>
		<?php echo h($producto_delivery->getProductosRelatedByProductoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Delivery</span>
		<?php echo h($producto_delivery->getDeliveriesRelatedByDeliveryId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cantidad</span>
		<?php echo h($producto_delivery->getCantidad()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Total</span>
		<?php echo h($producto_delivery->getTotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($producto_delivery->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($producto_delivery->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($producto_delivery->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>