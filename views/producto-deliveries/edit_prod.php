<div class="listaped">
    <table class="table">
        <thead>
        <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Monto</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody >
            <?php  $pp = ProductoDelivery::getAll('Where delivery_id='.$id);  if (count($pp)>0) foreach ( $pp as $p): ?>
<tr>
    <?php $pro=Productos::getAll('where id='.$p->getProducto_id()); ?>
    <td width="150">
        <?php  echo $pro[0]->getNombre()?>
    </td>

    <td width="300">
        <div class="form-inline">
            <div class="form-group">
                <button class="btn" onclick="document.getElementById('i<?=$p->id?>').stepDown(1); savePD('i<?=$p->id?>',<?=$p->id?>,<?= $id ?>); return false;">-</button>
                <input precio="<?= $pro[0]->getPrecio() ?>"
                       href="<?= site_url('producto-deliveries/save/') ?>"
                       id="i<?=$p->id?>" style="width: 30%; text-align: center" class="form-control"
                       type="number" min="1"
                       value="<?= $p->getCantidad() ?>">
                <button class="btn" onclick="document.getElementById('i<?=$p->id?>').stepUp(1);
                savePD('i<?=$p->id?>',<?=$p->id?>,<?= $id ?>); return false; ">+</button>
            </div>
        </div>

    </td>
    <td width="160">
        <input type="number"
               name="precio"
               id="i<?=$p->id?>p"
               onkeydown="savePD('i<?=$p->id?>',<?=$p->id?>,<?=$id?>);"
               onchange="savePD('i<?=$p->id?>',<?=$p->id?>,<?=$id?>);"
               style="width:100%;" value="<?= $p->getPrecio() ?>" min="1" class="form-control-sm">
    </td>
    <td>
        <?= $p->getCantidad()*$pro[0]->getPrecio() ?>
    </td>

    <td>
        <a class="btn btn-danger" href="<?= site_url('producto-deliveries/delete/'.$p->id) ?>">
            <i class="fa fa-trash-o"></i>
    </td>
</tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php $tpp = ProductoDelivery::getConnection()->query('
                  SELECT
                    Sum(producto_delivery.total) as total
                    FROM
                    producto_delivery
                    WHERE
                    producto_delivery.delivery_id ='.$id)->fetchAll() ?>
<h1 align="right" >
    S/. <span id="totalF"><?= number_format($tpp[0]['total'],2,'.',',') ?></span>
</h1>
