<style>
    .comanda{
        border-radius: 5px;
        border: 1px solid skyblue;
        padding: 50px;
        background: white;
    }
</style>
<?php $vm= VentasMovimiento::getAll('where venta_id='.$id) ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 comanda">
        <h2 align="center">Cevichería Restaurant "Rinconcito Piurano"</h2>
            <h5 align="center">RUC: 10027156261 </h5>
            <h5 align="center">Jirón: Sinchi Roca N° 465 </h5>
        <table class="table  table-sm">
            <tbody>
                <tr>
                    <td>
                        <?= $vm[0]->getMovimiento()->getVoucher()->tipo_comprobante ?>
                        N°</td>
                    <td><?= '00000'.$id ?></td>
                </tr>
            <tr>
                <td>Fecha:</td>
                <td><?=date('d/m/Y H:m:s')?></td>
            </tr>
            <tr>
                <td>
                    Usuario:
                </td>
                <td>
                    <?= $vm[0]->getMovimiento()->getUsuario()->nombres.' '.$vm[0]->getMovimiento()->getUsuario()->apellidos ?>
                </td>
            </tr>
            <tr>
                <td>Venta al:</td>
                <td><?= $vm[0]->getMovimiento()->getFormaPago()->descripcion?></td>
            </tr>

            </tbody>
        </table>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th>Cant.</th>
                <th>Descripción</th>
                <th>Precio/U</th>
                <th>Sub</th>
            </tr>
            </thead>
            <tbody>
            <?php $tt=0; $pp = PedidoProducto::getAll('Where venta_id='.$id);  if (count($pp)>0) foreach ( $pp as $p): ?>
                <tr>
                    <td>
                        <?= $p->getCantidad() ?>
                    </td>
                    <td>
                        <?php $pro=Productos::getAll('where id='.$p->getProducto_id()); echo $pro[0]->getNombre()?>
                    </td>
                    <td>
                        <?= $p->getPrecio() ?>
                    </td>
                    <td>
                        <?php echo 'S/. '.$p->getTotal(); $tt+=$p->getTotal(); ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>

        </table>
        <div align="right">
            <?php $q = "SELECT SUM(cuota) - total as vuelto, SUM(cuota) as tot
                            FROM
                            ventas_movimiento
                            WHERE
                            ventas_movimiento.venta_id =  
                           ".$id;
            $v = VentasMovimiento::getConnection()->query($q)->fetchAll(); ?>
            Descuento: S/. <?= $vm[0]->getDescuento() ?><br>
            Efectivo: S/.    <?= number_format($v[0]['tot'],2,'.',',') ?> <br>
            Total: S/.  <?= $tt-$vm[0]->getDescuento() ?> <br>
            Vuelto: S/. <?php $vu = $v[0]['vuelto']; echo $vu;?><br>
            <hr>
            <?php if ($vm[0]->getMovimiento()->getVoucher_id() == 1):  ?>
                Cliente: <?= $vm[0]->getCliente()->nombres.' '.$vm[0]->getCliente()->apellidos ?><br>
                DNI:     <?= $vm[0]->getCliente()->dni ?><br>
                Dirección:     <?= $vm[0]->getCliente()->direccion ?><br>
                <?php else: ?>
                Razón Social: <?= $vm[0]->getCliente()->nombres ?><br>
                RUC:          <?= $vm[0]->getCliente()->ruc ?><br>
                Dirección:    <?= $vm[0]->getCliente()->direccion ?><br>
            <?php endif; ?>
            <h5 align="center">¡Gracias por su preferencia!</h5>
        </div>
    </div>
    </div>
</div>
