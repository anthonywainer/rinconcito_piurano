<style>
    .panel-title{
        background: #024C51;
        color: white;
        height: 50px;
        line-height: 50px;
    }
    .tbody {
        display:block;
        height:280px;
        overflow:auto;
    }
    .thead, .tbody tr {
        display:table;
        width:100%;
        table-layout:fixed;/* even columns width , fix width of table too*/
    }
    .thead {
        width: calc( 100% )/* scrollbar is average 1em/16px width, remove it from thead width */
    }
    .oculto{
        display: none;
    }
    .dinero{
        font-size: 1.7em;
    }
</style>
    <div class="row">
        <div class="col-md-6" style="padding-right: 50px">

            <table class="table table-bordered table-sm">
                <thead class="thead">
                <tr>
                    <th colspan="4" style="background: #024C51; color: white">
                        LISTA DE PEDIDOS
                    </th>
                </tr>
                <tr>
                    <th>Producto</th>
                    <th>Cant.</th>
                    <th>Precio</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody class="tbody">
                <?php  View::load('pedido-productos/list_prod',['id'=>$idv]) ?>
                </tbody>
            </table>
            <?php $vm = VentasMovimiento::getAll('where venta_id='.$idv); $tpp = PedidoProducto::getConnection()->query('SELECT
    Sum(pedido_producto.total) as total
    FROM
    pedido_producto
    WHERE
    pedido_producto.venta_id ='.$idv)->fetchAll(); $tttp  = number_format($tpp[0]['total'],2,'.',',');
    if ($vm){
        $tttp -= $vm[0]->descuento;
    }
    ?>
        <table class="table  table-borderd table-sm">
            <tbody>
            <tr>
                <td colspan="3" >SubTotal</td>
                <td align="left">S/.<?= $tttp ?>
                    <input type="hidden" name="venta_id" value="<?= $idv ?>">
                    <input name="subtotal" type="hidden" value="<?= $tttp ?>"
                           id="total_b" >
                </td>
            </tr>
            <tr>
                <td></td>
                <td>Descuento:</td>
                <td>
                    <div class="form-inline">
                        <div class="form-group">
                    <button class="btn btn-default">S/.</button>
                    <input
                        <?php  if ($vm){echo "disabled";}?>
                            onkeyup="calcular_precio(this)" step="any" style="width: 60%"
                           name="descuento" value="<?php if ($vm){echo $vm[0]->getDescuento();}else{echo 0;}  ?>"
                           class="form-control input-sm" type="number"
                           min="0" id="descuento" onclick="calcular_precio(this)" >

                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
            <tfoot class="panel-title">
                <tr>
                    <td align="right">TOTAL</td>
                    <td colspan="3" align="right" id="totalh" style="font-size: 1.9em">
                        S/.<?= $tttp?>
                    </td>
                    <input step="any" type="hidden" name="total" id="total" value="<?= $tttp?>">
                </tr>
            </tfoot>
        </table>
        </div>
        <div class="col-md-6">
            <div class="panel-title" align="center">
                <h5>PAGOS:</h5>
            </div>
            <br>
            <div class="row">
            <div class="col-md-5">
                Tipo de Comprobante: <br>
                <select required class="form-control"
                        onchange="comprobante(this)"
                        id="comprobante" name="voucher_id">
                                <?php foreach (Vouchers::getAll() as $v): ?>
                                    <option value="<?= $v->id ?>"><?= $v->getTipo_comprobante() ?></option>
                                <?php endforeach; ?>
                </select>

            </div>
            <div class="col-md-7">
                Cliente:
                <div class="form-inline" id="c_dni">
                     <select required class="form-control"  name="cliente_id"
                             id="mo_cliente_dni">
                         <option value="">Buscar cliente</option>
                                <?php foreach (Clientes::getAll('where dni is not null') as $fp): ?>
                                    <option value="<?= $fp->id ?>"><?= $fp->dni.' --> '.$fp->getNombres().' '.$fp->getApellidos() ?></option>
                                <?php endforeach; ?>
                     </select>
                    <i onclick="openmodal(this)"
                       href="<?= site_url('clientes/edit?tipo=12') ?>"
                       class="btn btn-info">+</i>
                </div>
                <div class="form-inline oculto" id="c_ruc">
                                <select required  disabled class="form-control"
                                        name="cliente_id" id="mo_cliente_ruc">
                                    <option value="">Buscar cliente</option>
                                    <?php foreach (Clientes::getAll('where ruc is not null') as $fp): ?>
                                        <option value="<?= $fp->id ?>"><?= $fp->ruc.' --> '.$fp->getNombres() ?></option>
                                    <?php endforeach; ?>
                                </select>
                    <i onclick="openmodal(this)"
                       href="<?= site_url('clientes/edit?tipo=13') ?>"
                       class="btn btn-info">+</i>
                            </div>
                        </div>
            </div>
                <br>
                <div class="col-md-12">
                    <div id="form-pago" class="row">
                        <?php  View::load('pedido-productos/formas_pagos',['idv'=>$idv]) ?>
                    </div>
                </div>

            </div>


        </div>

    </div>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function movimiento(url) {
        $('#primaryModal').modal('show');
        $('#primaryModal .modal-footer').html('' +
            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>'+
            '<button class="btn btn-info" onclick="movimientog(\''+url+'\')">Listo</button>' +
            '');
        $('#primaryModal .modal-body').load(url+'?tip=1');
    }

    function movimientog(url) {
        $('#primaryModal').modal('hide');
        $('#primaryModal').on('hidden.bs.modal', function () {
            $('.container-fluid').load(url,function () {
                $("input[type=number]").keypress(function (evt) {
                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
                    {
                        evt.preventDefault();
                    }
                });
                $("select").select2();
                window.open(
                    url+'?tip=1',
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        });

    }
    function comprobante(t) {
        if ($(t).val()==1){
            $('#c_dni').removeClass('oculto');
            $("#mo_cliente_dni").prop('disabled',false);
            $("#c_ruc").addClass('oculto');
            $("#mo_cliente_ruc").prop('disabled',true);
        }else{
            $('#c_ruc').removeClass('oculto');
            $("#mo_cliente_ruc").prop('disabled',false);
            $("#c_dni").addClass('oculto');
            $("#mo_cliente_dni").prop('disabled',true);
        }
    }
    function update_mov(t,idv) {
        $.get($(t).attr('href'),{'forma_pago_id':$(t).val(),'venta_id':idv}).done(
            function (da) {
                $("#form-pago").html(da);
            }
        );
    }
    function update_monto(t,cuota,b) {
        moc = $(t).val();
        if ( moc == '') {
            toastr.error('no puede poner nulo');
        }else{
            if (b == 'yes') {
                v = ($(t).val() - cuota);
                $('#vuelto').html('S/.' + v);
                param = {'efectivo': $(t).val(), 'cuota': $(t).val(), 'resta': cuota - $(t).val()};
            } else {

                param = {'cuota': $(t).val(), 'resta': cuota - $(t).val()}
            }


            $.get($(t).attr('href'), param).done(
                function (da) {
                    $("#form-pago").html(da);
                }
            );
        }

    }

    function agregar_pago(t,idv) {
        if ($('#comprobante').val()==1){
            cliente = $("#mo_cliente_dni").val();
        }else{
            cliente= $("#mo_cliente_ruc").val();
        }
        if (cliente != "") {
            param = {
                total: $('#total').val(),
                cuota: $('#total').val(),
                cliente_id: cliente,
                voucher_id: $("#comprobante").val(),
                descuento: $("#descuento").val(),
                concepto_de_pago_id: 1,
                subtotal: $('#total_b').val(),
                venta_id: idv
            };
            url = $(t).attr('href');
            $.get(url, param).done(function (data) {
                $("#form-pago").html(data);
                $('#descuento').prop('disabled', true);
            });
        }else{
            alert("seleccione cliente");
        }



    }
    
    function calcular_precio(t) {
        desc =  $(t).val();
        base_b = $("#total_b").val();
        $("#totalh").html("S/. "+(base_b-desc));
        $("#total").val(base_b-desc)
    }
    function vuelto(t) {
        v = ($(t).val()-$('#total').val());

        $('#vuelto').html('S/.'+v)
    }
    function registrar_dni(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#clientes_dni").val()+","+$("#clientes_nombres").val()+" "+$("#clientes_apellidos").val();
            var option = new Option(nom, id);
            $("#mo_cliente_dni").append(option);
            $("#mo_cliente_dni").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function registrar_ruc(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#clientes_ruc").val()+", "+$("#clientes_nombres").val();
            var option = new Option(nom, id);
            $("#mo_cliente_ruc").append(option);
            $("#mo_cliente_ruc").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function bloquear(t) {
        if ($(t).val()==13){
            $('#efec').prop('disabled',true);
        }else{
            $('#efec').prop('disabled',false);
        }
    }
</script>