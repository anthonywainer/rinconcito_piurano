<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" href="<?= site_url('cajas/show') ?>">Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('cajas') ?>">Historial Caja</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link " href="<?= site_url('conceptos-de-pagos') ?>">Operaciones</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link active" href="<?= site_url('movimientos-de-dineros') ?>">Transacciones</a>
    </li>

</ul>

<div class="ui-widget-content ui-corner-all">
	<?php View::load('movimientos-de-dineros/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>