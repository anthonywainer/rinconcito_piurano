<style>
    .panel-title{
        background: #024C51;
        color: white;
        height: 50px;
    }
    .oculto{
        display: none;
    }
    #vuelto{
        font-size: 2em;
    }
</style>
<form onsubmit="guardarmD(this,'<?= site_url('movimientos-de-dineros/save/') ?>','<?= site_url('delivery')?>'); return false;" method="post">
    <div class="row">
        <div class="col-md-6" style="padding-right: 50px">
            <input type="hidden" name="del">
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th colspan="4" style="background: #024C51; color: white">
                        LISTA DE PEDIDOS
                    </th>
                </tr>
                <tr>
                    <th>Producto</th>
                    <th>Cant.</th>
                    <th>P.u</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <?php  View::load('producto-deliveries/list_prod',['id'=>$idv]) ?>
                </tbody>
            </table>
            <?php $tpp = PedidoProducto::getConnection()->query('SELECT
                        Sum(producto_delivery.total) as total
                        FROM
                        producto_delivery
                        WHERE
                        producto_delivery.delivery_id ='.$idv)->fetchAll() ?>
        <table class="table table-condensed">
            <tbody>
            <tr>
                <td colspan="2">SubTotal</td>
                <td>S/.<?= $tpp[0]['total'] ?>
                    <input type="hidden" id="delideli" name="delivery_id" value="<?= $idv ?>">
                    <input name="subtotal" type="hidden" value="<?= $tpp[0]['total'] ?>" id="total_b" >
                </td>
            </tr>
            <tr>
                <td>Precio Adicional:</td>
                <td>

                </td>
                <td>
                    <div class="form-inline">
                        <div class="form-group">
                    <button class="btn btn-default">S/.</button>
                    <input step="any" style="width: 60%"
                           onchange="calcular_precio()"
                           onkeyup="calcular_precio()"
                           name="descuento" value="0"
                           class="form-control input-sm" type="number" min="0" id="descuento">
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
            <tfoot class="panel-title">
                <tr>
                    <td align="right">TOTAL</td>
                    <td></td>
                    <td align="left" id="totalh">
                        <h3>S/.<?= $tpp[0]['total']?></h3>
                    </td>
                    <input step="any" type="hidden" name="total" id="total"
                           value="<?= $tpp[0]['total']?>">
                </tr>
            </tfoot>
        </table>
        </div>
        <div class="col-md-6">
            <div class="panel-title" align="center">
                <h5>PAGOS:</h5>
            </div>
            <br>
            <div class="row">
                    <input type="hidden" value="12" name="forma_pago_id" />
                <div class="col-md-4">
                    Tipo de Comprobante: <br>
                    <select required class="form-control"
                            onchange="comprobanteD(this)"
                            id="comprobante" name="voucher_id">
                        <?php foreach (Vouchers::getAll() as $v): ?>
                            <option value="<?= $v->id ?>"><?= $v->getTipo_comprobante() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-8">
                    Cliente:
                    <div class="form-inline" id="c_dni">
                        <select required class="form-control"  name="cliente_id"
                                id="mo_cliente_dni">
                            <option value="">Buscar cliente</option>
                            <?php foreach (Clientes::getAll('where dni is not null') as $fp): ?>
                                <option value="<?= $fp->id ?>"><?= $fp->dni.' --> '.$fp->getNombres().' '.$fp->getApellidos() ?></option>
                            <?php endforeach; ?>
                        </select>
                        <i onclick="openmodal(this)"
                           href="<?= site_url('clientes/edit?tipo=12') ?>"
                           class="btn btn-info">+</i>
                    </div>
                    <div class="form-inline oculto" id="c_ruc">
                        <select required  disabled class="form-control"
                                name="cliente_id" id="mo_cliente_ruc">
                            <option value="">Buscar cliente</option>
                            <?php foreach (Clientes::getAll('where ruc is not null') as $fp): ?>
                                <option value="<?= $fp->id ?>"><?= $fp->ruc.' --> '.$fp->getNombres() ?></option>
                            <?php endforeach; ?>
                        </select>
                        <i onclick="openmodal(this)"
                           href="<?= site_url('clientes/edit?tipo=13') ?>"
                           class="btn btn-info">+</i>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-inline">
                        <label for="">Efectivo: </label>
                        <div class="form-group">
                            <button class="btn btn-default">S/.</button>
                            <input step="any" name="efectivo" style="width: 60%"
                                   onkeyup="vuelto(this)"
                                   onchange="vuelto(this)"
                                   min="<?=$tpp[0]['total']?>"
                                   value="<?=$tpp[0]['total']?>" class="form-control input-sm" type="number">
                        </div>
                    </div>
                </div>
                <br><br>

            </div>
            <br>
            <div class="panel-title">
                <strong >Vuelto: </strong>
                <span  id="vuelto"></span>
            </div>
            <br>
            <div align="right">
                <input type="hidden" value="1" name="concepto_de_pago_id">
                <a class="btn btn-danger" href="<?= site_url('delivery/edit/?id='.$idv )?>">Cancelar</a>
                <button  type="submit" class="btn btn-info">Aceptar</button>
            </div>
        </div>

    </div>
</form>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    function movimientod(u1,u2) {
        deli = $("#delideli").val();
        $('#primaryModal').modal('hide');

        $('#primaryModal').on('hidden.bs.modal', function () {
            $('.container-fluid').load(u2+' .container-fluid',function () {
                $("input[type=number]").keypress(function (evt) {
                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
                    {
                        evt.preventDefault();
                    }
                });
                $("select").select2();
                window.open(
                    u1+'/'+deli+'/?delicia=1',
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        });
    }

    function guardarmD(t,url,u2) {
        $('#primaryModal').modal('show');
        $('#primaryModal .modal-footer').html('' +
            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>'+
            '<button class="btn btn-info" onclick="movimientod(\''+url+'\',\''+u2+'\')">Listo</button>' +
            '');
        $('#primaryModal .modal-body').load(url+'?'+$(t).serialize());
    }


    function comprobanteD(t) {
        if ($(t).val()==1){
            $('#c_dni').removeClass('oculto');
            $("#mo_cliente_dni").prop('disabled',false);
            $("#c_ruc").addClass('oculto');
            $("#mo_cliente_ruc").prop('disabled',true);
        }else{
            $('#c_ruc').removeClass('oculto');
            $("#mo_cliente_ruc").prop('disabled',false);
            $("#c_dni").addClass('oculto');
            $("#mo_cliente_dni").prop('disabled',true);
        }
    }
    function calcular_precio(t) {
        base_b = parseFloat($("#total_b").val());
        desc= parseFloat($("#descuento").val());
        $("#totalh").html("S/. "+(base_b+desc));
        $("#total").val(base_b+desc)
    }
    function vuelto(t) {
        to = $('#total').val();
        console.log(to+$(t).val());
        if ($(t).val()>=parseFloat(to)) {
            v = ($(t).val() - to);
            $('#vuelto').html('S/.' + v)
        }else{
            $(t).val('');
            toastr.error('monto incorrecto');
        }
    }
    function registrar_dni(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#clientes_dni").val()+","+$("#clientes_nombres").val()+" "+$("#clientes_apellidos").val();
            var option = new Option(nom, id);
            $("#mo_cliente_dni").append(option);
            $("#mo_cliente_dni").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function registrar_ruc(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#clientes_ruc").val()+", "+$("#clientes_nombres").val();
            var option = new Option(nom, id);
            $("#mo_cliente_ruc").append(option);
            $("#mo_cliente_ruc").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
</script>