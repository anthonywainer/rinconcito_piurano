<h1>View Movimientos De Dinero</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('movimientos-de-dineros/edit/' . $movimientos_de_dinero->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Movimientos_de_dinero">
		Edit	</a>
	<a href="<?php echo site_url('movimientos-de-dineros/delete/' . $movimientos_de_dinero->getId()) ?>"
		class="button" data-icon="trash" title="Delete Movimientos_de_dinero"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('amortizaciones?movimiento_de_dinero_id=' . $movimientos_de_dinero->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Amortizaciones Movimientos_de_dinero">
		Amortizaciones	</a>
	<a href="<?php echo site_url('ventas-movimientos?movimiento_de_dinero_id=' . $movimientos_de_dinero->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Movimientos Movimientos_de_dinero">
		Ventas Movimientos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Numero Voucher</span>
		<?php echo h($movimientos_de_dinero->getNumeroVoucher()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Voucher</span>
		<?php echo h($movimientos_de_dinero->getVouchersRelatedByVoucherId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Concepto De Pago</span>
		<?php echo h($movimientos_de_dinero->getConceptosDePagoRelatedByConceptoDePagoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Caja</span>
		<?php echo h($movimientos_de_dinero->getCajasRelatedByCajaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Usuario</span>
		<?php echo h($movimientos_de_dinero->getUsuariosRelatedByUsuarioId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Forma Pago</span>
		<?php echo h($movimientos_de_dinero->getFormasPagoRelatedByFormaPagoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($movimientos_de_dinero->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($movimientos_de_dinero->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($movimientos_de_dinero->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>