<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<br>
<table class="table table-sm table-hover" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					Comprobante
			</th>
			<th class="ui-widget-header ">
					USUARIOS
			</th>
			<th class="ui-widget-header ">
					FORMAS DE PAGO
			</th>
			<th class="ui-widget-header ">
					FECHA
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($movimientos_de_dineros as $key => $movimientos_de_dinero): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($movimientos_de_dinero->getId()) ?>&nbsp;</td>
			<td><?php echo Vouchers::getAll('where id='.$movimientos_de_dinero->getVoucher_id())[0]->getTipo_comprobante() ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getUsuariosRelatedByUsuarioId()->nombres) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getFormasPagoRelatedByFormaPagoId()->descripcion) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
                <a class="btn btn-info" title=""
                   target="_blank"
                   href="<?php echo site_url('movimientos-de-dineros/transaccion/' . $movimientos_de_dinero->getId()) ?>">
                    <i class="fa fa-search "></i>
                </a>


			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>