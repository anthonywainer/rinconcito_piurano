<h1>View Permisos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('permisos/edit/' . $permisos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Permisos">
		Edit	</a>
	<a href="<?php echo site_url('permisos/delete/' . $permisos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Permisos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Idaccion</span>
		<?php echo h($permisos->getAccionesRelatedByIdaccion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Idsubmodulo</span>
		<?php echo h($permisos->getSubmoduloRelatedByIdsubmodulo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($permisos->getNombre()) ?>
	</div>
</div>