<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid permisos-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					ID ACCIÓN
			</th>
            <th class="ui-widget-header ">
                    MÓDULO
            </th>
			<th class="ui-widget-header ">
					ID SUBMÓDULO
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($permisos as $key => $permisos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos->getId()) ?>&nbsp;</td>
			<td><?php echo $permisos->getAccion($permisos->getIdaccion())[0]->accion ?>&nbsp;</td>
            <td><?php if($permisos->getIdmodulo()) echo $permisos->getModulos($permisos->getIdmodulo())[0]->nombre ?>&nbsp;</td>
			<td><?php if($permisos->getIdSubmodulo()) echo $permisos->getSubModulos($permisos->getIdSubmodulo())[0]->nombre ?>&nbsp;</td>
			<td><?php echo h($permisos->getNombre()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('permisos/edit/' . $permisos->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('permisos/delete/' . $permisos->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>