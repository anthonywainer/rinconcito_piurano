<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/login.css', true) ?>">
	<header>
		<h2 class="titulo">BIENVENIDOS</h2>
	</header>
	<div class="container">
		<div class="row">
			<div class="col-md-8 login-logo">
				<img src="<?php echo site_url('imagenes/logo.png') ?>" alt="">
			</div>

			<div class="col-md-4 formulario">

				<form action="/rinconcito_piurano/login/ingresar" method="post">

					<span class="sesion">INICIAR SESIÓN</span>
					<label for="">USUARIO</label>
					<input type="email" name="correo" class="input-log">
					<label for="">CONTRASEÑA</label>
					<input type="password" name="clave" class="input-log">
                    <span style="color: red; font-size: 14px"><?php if(isset($_GET["error"])) {
                            echo "<p class='error'>".$_GET['error']."</p>";
                        }
                        ?></span>
					<input type="checkbox" class="chec">
					<span class="recor">Recordarme</span>
					<input type="submit" value="INICIAR" class="boton">
				</form>
			</div>
		</div>
	</div>
