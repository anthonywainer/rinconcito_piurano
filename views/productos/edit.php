<div id="usa">
<h6 align="center">

    <?php echo $productos->isNew() ? "NUEVO" : "EDITAR" ?> PRODUCTOS</h6>
<form method="post" action="<?php echo site_url('productos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
        <input type="hidden" name="id" value="<?php echo h($productos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_nombre">Nombre</label>
			<input  required class="form-control"  id="productos_nombre" type="text" name="nombre" value="<?php echo h($productos->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_precio">Precio</label>
			<input  required class="form-control"  id="productos_precio" type="number" name="precio" value="<?php echo h($productos->getPrecio()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_tipo_producto_id">Tipos Producto</label>
			<select  required class="form-control"  id="productos_tipo_producto_id" name="tipo_producto_id">
			<?php foreach (TiposProducto::getAll() as $tp): ?>
				<option  <?php if ($productos->getTipoProductoId()== $tp->id){echo "selected"; }  ?>
                        value="<?php echo $tp->id ?>"><?php echo $tp->nombre?></option>
			<?php endforeach ?>
			</select>
		</div>
		</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel"  href= "<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $productos->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>

	</div>
</form>
</div>