<h1>View Productos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('productos/edit/' . $productos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Productos">
		Edit	</a>
	<a href="<?php echo site_url('productos/delete/' . $productos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Productos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('detalles-venta-productos?producto_id=' . $productos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Detalles Venta Productos Productos">
		Detalles Venta Productos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($productos->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio</span>
		<?php echo h($productos->getPrecio()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Tipo Producto</span>
		<?php echo h($productos->getTiposProductoRelatedByTipoProductoId()) ?>
	</div>
    <div class="field-wrapper">
        <span class="field-label">Created At</span>
        <?php echo h($productos->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
    </div>
    <div class="field-wrapper">
        <span class="field-label">Updated At</span>
        <?php echo h($productos->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
    </div>
    <div class="field-wrapper">
        <span class="field-label">Deleted At</span>
        <?php echo h($productos->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
    </div>
</div>
