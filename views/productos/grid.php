<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid productos-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					PRECIO
			</th>
			<th class="ui-widget-header ">
					TIPO PRODUCTO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($productos as $key => $productos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($productos->getId()) ?>&nbsp;</td>
			<td><?php echo h($productos->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($productos->getPrecio()) ?>&nbsp;</td>
			<td>
                <?= TiposProducto::getAll('where id='.$productos->tipo_producto_id )[0]->nombre ?>&nbsp;
            </td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('productos/edit/' . $productos->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('productos/delete/' . $productos->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>