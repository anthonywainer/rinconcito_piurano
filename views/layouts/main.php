<!DOCTYPE html>
<html lang="es">
	<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo site_url('css/font-awesome.css', true) ?>">
        <link rel="stylesheet" href="<?php echo site_url('css/simple-line-icons.min.css', true) ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/bootstrap/css/bootstrap.css', true) ?>">
        <link href="<?php echo site_url('css/toastr.min.css', true) ?>" rel="stylesheet">
        <?php         if ($_SESSION) { ?>
            <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/style-admin.css', true) ?>">
            <link href="<?php echo site_url('css/bootstrap-theme.min.css', true) ?>" rel="stylesheet">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
            <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
            <link href="<?php echo site_url('css/tableexport.min.css', true) ?>" rel="stylesheet">

        <?php }else{ ?>
            <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/style.css', true) ?>">

        <?php } ?>


	</head>
	<body>


		<div class="content-wrapper ui-widget">


			<div class="content">
				<?php View::load($content_view, $params) ?>
			</div>
		</div>


        <?php if ($_SESSION) { ?>
            <script src="<?php echo site_url('js/jquery.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/moment.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/popper.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/bootstrap.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/toastr.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/app.js', true) ?>"></script>
            <script src="<?php echo site_url('js/select2.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/daterangepicker.js', true) ?>"></script>
            <script src="<?php echo site_url('js/xlsx.core.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/Blob.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/FileSaver.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/tableexport.min.js', true) ?>"></script>
            <script src="<?php echo site_url('js/jspdf.min.js', true) ?>"></script>
            <script>
                <?php if(isset($errors)): ?>
                toastr.error("<?php View::load('errors', compact('errors')) ?>", 'Eliminado', {
                    closeButton: true,
                    progressBar: true,
                });
                <?php endif ?>
                <?php if(isset($messages)): ?>
                toastr.info("<?php View::load('messages', compact('messages')) ?>", 'Guardado', {
                    closeButton: true,
                    progressBar: true,
                });
                <?php endif ?>
            </script>
        <?php }else{ ?>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

        <?php } ?>



	</body>
</html>