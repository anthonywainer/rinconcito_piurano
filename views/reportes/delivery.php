
<div id="ventasG">
    <h1 align="center">Reportes de Delivery</h1>
    <div class="col-md-12" align="right">
        <div class="form-inline" align="right" >
            <i class="fa fa-calendar" align="right"></i>
            <input type='text' align="right" style="width:30%;" href="<?=site_url('reportedelivery')?>"
                   value="<?= $fechai ?> - <?= $fechae ?>"
                   class="form-control datepicker" />

        </div>
    </div>

    <hr>

<table class="table table-sm table-hover"  id="reporte">
    <thead>
    <tr>
        <th>N°Delivery</th>
        <th>Fecha</th>
        <th>deliverista</th>
        <th>producto</th>
        <th>cantidad</th>
        <th>precio</th>
        <th>total</th>
    </tr>
    <tbody>
    <?php foreach ($delivery as $e):?>
        <tr>
            <td><?= 'D000'.$e['id'] ?></td>
            <td><?= $e['fecha'] ?></td>
            <td><?= $e['deliverista'] ?></td>
            <td><?= $e['nombre'] ?></td>
            <td><?= $e['cantidad'] ?></td>
            <td><?= $e['precio'] ?></td>
            <td><?= $e['total'] ?></td>

        </tr>
    <?php endforeach; ?>
    </tbody>
    </thead>
</table>
</div>

<script>
    function demoFromHTML() {
        var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#ventasG')[0];

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function (element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
                // dispose: object with X, Y of the last line add to the PDF
                //          this allow the insertion of new lines after html
                pdf.save('reporte_ventas.pdf');
            }, margins);
    }
</script>