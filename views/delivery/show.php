<h1>View Deliveries</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('delivery/edit/' . $deliveries->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Deliveries">
		Edit	</a>
	<a href="<?php echo site_url('delivery/delete/' . $deliveries->getId()) ?>"
		class="button" data-icon="trash" title="Delete Deliveries"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('producto-deliveries?delivery_id=' . $deliveries->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Producto Deliveries Deliveries">
		Producto Deliveries	</a>
	<a href="<?php echo site_url('ventas-deliveries?delivery_id=' . $deliveries->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Deliveries Deliveries">
		Ventas Deliveries	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Fecha</span>
		<?php echo h($deliveries->getFecha(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Direccion</span>
		<?php echo h($deliveries->getDireccion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Referencia</span>
		<?php echo h($deliveries->getReferencia()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deliverista</span>
		<?php echo h($deliveries->getDeliveristaRelatedByDeliveristaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($deliveries->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($deliveries->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($deliveries->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cliente</span>
		<?php echo h($deliveries->getClientesRelatedByClienteId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Tiempo Demora</span>
		<?php echo h($deliveries->getTiempoDemora()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado Delivery</span>
		<?php if ($deliveries->getEstadoDelivery() === 1) echo 'Yes'; elseif ($deliveries->getEstadoDelivery() === 0) echo 'No' ?>
	</div>
</div>