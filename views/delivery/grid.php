<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table  table-condensed  table" cellspacing="0" >
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					FECHA
			</th>
			<th class="ui-widget-header ">
					DIRECCIÓN
			</th>
			<th class="ui-widget-header ">
					DELIVERISTA
			</th>

			<th class="ui-widget-header ">
					ESTADO DELIVERY
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($delivery as $key => $deliveries): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($deliveries->getId()) ?>&nbsp;</td>
			<td><?php echo h($deliveries->getFecha(VIEW_DATE_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($deliveries->getDireccion()).' '.h($deliveries->getReferencia()) ?>&nbsp;</td>
			<td>
                <?php if($deliveries->getDeliverista()){
                    echo h($deliveries->getDeliverista()->nombres);
                    }else{
                    echo "No asignado Deliverista";
                } ?>&nbsp;
            </td>
			<td><?php if ($deliveries->getEstadoDelivery() == 2) echo 'Pagado';
			elseif ($deliveries->getEstadoDelivery() == 1) echo 'En proceso... <a href="'.site_url('delivery/save/').'/'.$deliveries->getId().'?estado_delivery=2" class="btn btn-info">Recibir</a>'; else echo "No pagado"?>&nbsp;</td>
			<td>
                <?php if ($deliveries->getEstadoDelivery() == 0) : ?>
                    <a
                            class="button"
                            data-icon="trash"
                            title="Delete Deliveries"
                            href="#"
                            onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('delivery/delete/' . $deliveries->getId()) ?>'; } return false">
                        Eliminar
                    </a>
                    <?php elseif ($deliveries->getEstadoDelivery() == 1) : ?>
                    <a class="btn btn-danger" href="<?php echo site_url('delivery/delete/' . $deliveries->getId()) ?>">
                        Cancelar
                    </a>

                <?php endif;  ?>
                <a class="btn btn-info"
                   target="_blank"
                   href="<?= site_url('movimientos-de-dineros/transaccion/'.VentasDelivery::getAll('where delivery_id='.$deliveries->id)[0]->getMovimiento_id()) ?>">
                    <i class="fa fa-search "></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>