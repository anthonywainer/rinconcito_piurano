<style>
    .comanda{
        border-radius: 5px;
        border: 1px solid skyblue;
        padding: 50px;
        background: white;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 comanda">
        <h2 align="center">"rinconcito Piurano"</h2>
        <h5 align="center"> COMANDA N° <?= '0'.$id ?> - DELIVERY N° <?=$id?>  </h5>
            Deliverista: <?= Deliveries::getAll('where id='.$id)[0]->getDeliverista()->nombres ?>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th>Cant.</th>
                <th>Descripción</th>
                <th>S/. Total</th>
            </tr>
            </thead>
            <tbody>
            <?php $tt=0; $pp = ProductoDelivery::getAll('Where delivery_id='.$id);  if (count($pp)>0) foreach ( $pp as $p): ?>
                <tr>
                    <td>
                        <?= $p->getCantidad() ?>
                    </td>
                    <td>
                        <?php $pro=Productos::getAll('where id='.$p->getProducto_id()); echo $pro[0]->getNombre()?>
                    </td>
                    <td>
                        <?php echo 'S/. '.$p->getTotal(); $tt+=$p->getTotal(); ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div align="right">
            Total: S/. <?= number_format($tt,2,'.',',') ?>
        </div>
    </div>
        <div class="col-md-3"></div>
    </div>

</div>

