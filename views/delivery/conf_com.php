<?php View::load('delivery/comanda', ['id'=> $id]) ?>
<div class="col-md-12" align="center">
    <button class="btn btn-info"
       onclick="comm('<?= site_url('movimientos-de-dineros/index/'.$id.'/?del=') ?>',
           '<?= site_url('delivery/show/'.$id.'/?com=') ?>'
           )" >
        Guardar Comanda
    </button>
</div>

<script>
    function comm(url,url2) {
        $('.container-fluid').load(url,function () {
            $("input[type=number]").keypress(function (evt) {
                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
                {
                    evt.preventDefault();
                }
            });
            $("select").select2();
            window.open(
                url2,
                '_blank' // <- This is what makes it open in a new window.
            );
        });
    }
</script>