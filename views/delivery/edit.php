
<style>
    .panel-title{
        background: #024C51;
        color: white;
    }
    .proP{
        height: 120px;
        padding: 20px;
    }
    .producto{
        height: 120px;
        padding: 20px;
        text-align: center;
        cursor: pointer;
        border: 2px dashed skyblue;
        border-radius: 5px;
    }
    .no-se{
        cursor: no-drop;
    }
    .no-pro{
        cursor: no-drop;
        border: 2px dashed red;
    }
    .lprod{
        height: 400px;
        overflow-x: hidden;
    }
    .listaped{
        height: 290px;
        overflow-x: hidden;
    }
    span.select2-container {
        z-index:10050;
    }
    .coman{
        display: none;
    }
</style>
<a class="btn btn-default" href="<?= site_url('delivery/index/')?>" ></a>
<form action="<?= site_url('delivery/save/'.$deliveries->getId())?>" method="post">
<div class="row">
     <div class="col-md-6">
         <div class="panel">
             <?php if(ProductoDelivery::getAll('where delivery_id='.$deliveries->getId())){ ?>
                 <a class="btn btn-info" href="<?= site_url('delivery/index/')?>" >Atrás</a> <hr>
             <?php }else{ ?>
                 <a class="btn btn-info" href="<?= site_url('delivery/delete/'.$deliveries->getId())?>" >Atrás</a> <hr>
             <?php } ?>
         </div>
            <div class="panel">
            <div class="row">
                <div class="col-md-12">
                    <input type="search" onkeyup="buscar_prod(this)" class="form-control"
                           placeholder="Buscar Producto...">
                    <div class="row lprod" >

                        <?php  foreach (Productos::getAll() as $p): ?>
                        <div idd="<?= $p->getId() ?>" class="col-md-4 proP <?php if(in_array($p->id,$pp)){ echo ' no-se "'; }else{ echo ' " onclick="add_producto(this,'.$deliveries->getId().')" '; }?>
                         href="<?= site_url('producto-deliveries/save/') ?>" >
                        <div class="producto  <?php if(in_array($p->id,$pp)){ echo ' no-pro'; } ?>"  >
                            <h5>s/.<?= $p->getPrecio() ?></h5>
                            <strong style="color: #00a67c"><?= $p->nombre ?></strong>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                </div>

            </div>

            <hr>




        </div>

    </div>

    <div class="col-md-6">
        <div class="panel-title" >
            <img src="<?= site_url('imagenes/michi.jpg', true)?>" alt="delivery" height="70" width="70">
            <h2 style="margin-top: -60px;padding-bottom: 25px" align="center">Delivery: 00<?php echo h($deliveries->getId()) ?> </h2>
        </div>
        <div id="form-ped">
            <?php  View::load('producto-deliveries/edit_prod',['id'=>$deliveries->getId()]) ?>
        </div>
        <div align="right">
            <a  class="btn btn-danger" href="<?= site_url('delivery/delete/'.$deliveries->getId()) ?>">
                Cancelar
            </a>
            <a  class="btn btn-info" id="add_data" data-toggle="modal" data-target="#primaryC">
                Agregar Datos
            </a>

        <?php if ($deliveries->getUpdated_at()): ?>
            <a class="btn btn-info"
               href="<?= site_url('movimientos-de-dineros/index/'.$deliveries->getId().'/?del=') ?>" >
                Pagar
            </a>
        <?php else: ?>
            <button type="submit"  class="btn btn-info coman" href="<?= site_url('delivery/edit/'.$deliveries->getId()) ?>">
                Generar Comanda
            </button>
        <?php endif; ?>


        </div>
    </div>
</div>
    <div class="modal fade" id="primaryC" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #024C51">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label class="form-field-label" for="deliveries_deliverista_id">Deliverista:</label>
                        <br>
                        <select style="width: 100%" required id="deliveries_deliverista_id" class="form-control" name="deliverista_id">
                            <option value="">Seleccione un Deliverista...</option>
                            <?php foreach (Deliverista::doSelect() as $deliverista): ?>
                                <option <?php if ($deliveries->getDeliveristaId() === $deliverista->getId()) echo 'selected="selected"' ?> value="<?php echo $deliverista->getId() ?>">
                                    <?= $deliverista->getNombres().' '.$deliverista->getApellidos()?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>


                    <div class="col-md-12">
                        <div class="form-field-wrapper">
                            <br>
                            <label class="form-field-label"  for="deliveries_direccion">Dirección:</label>
                            <input required placeholder="ingrese dirección" id="deliveries_direccion" class="form-control" type="text" name="direccion" value="<?php echo h($deliveries->getDireccion()) ?>" />
                        </div>
                        <br>
                    </div>
                    <div class="col-md-12">
                        <div class="form-field-wrapper">
                            <label  class="form-field-label" for="deliveries_referencia">Referencia: </label>
                            <input required placeholder="ingrese referencia" id="deliveries_referencia" class="form-control" type="text" name="referencia" value="<?php echo h($deliveries->getReferencia()) ?>" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-field-wrapper">
                            <label class="form-field-label"  for="deliveries_tiempo_demora">Tiempo Demora: </label>
                            <input required id="deliveries_tiempo_demora"
                                   placeholder="ingrese tiempo" class="form-control" type="number"
                                   min="1"
                                   name="tiempo_demora" value="<?php echo h($deliveries->getTiempoDemora()) ?>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="$('.coman').removeClass('coman'); $('#add_data').addClass('coman');" class="btn btn-primary"data-dismiss="modal">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancerlar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</form>


<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

    function add_producto(t,idv) {
            $.post($(t).attr('href'), {'delivery_id': idv, 'producto_id': $(t).attr('idd')}).done(
                function (data) {
                    $('#form-ped').html(data);
                    $(t).removeAttr('onclick').addClass('no-se');
                    $(t).children('.producto').addClass('no-pro');
                }
            )
    }
    function savePD(id,t,idv) {
        $.post($('#'+id).attr('href')+'/'+t,
            {'cantidad':$('#'+id).val(),
                'precio': $('#'+id+'p').val(),
                'total':$('#'+id).val()*$('#'+id+'p').val(),
                'delivery_id': idv
            }
                ).done(
            function (data) {
                $('#form-ped').html(data);
                return false;
            }
        );
        return false;
    }

    function registrar(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param+'&json=1').done(function (id) {
            alert("Guardado Correctamente");
            nom = $("#clientes_dni").val()+","+$("#clientes_ruc").val()+", "+$("#clientes_nombres").val()+" "+$("#clientes_apellidos").val();
            var option = new Option(nom, id);
            $("#deliveries_cliente_id").append(option);
            $("#deliveries_cliente_id").val(id).trigger('change');
            $("#deliveries_direccion").val($("#clientes_direccion").val());
            $("#primaryModal").modal("hide");
        });
    }
    function buscar_prod(t) {
        var rex = new RegExp($(t).val(), 'i');
        $('.proP').hide();
        $('.proP').filter(function () {
            return rex.test($(this).text());
        }).show();
    }
</script>
