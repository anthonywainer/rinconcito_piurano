<?php
if ($estado) {
    $q = "
                                select sum(monto) as total, sum(cant) as cant
                                from(
                                SELECT
                                sum(ventas_movimiento.cuota) as monto, 
		                        count(ventas_movimiento.cuota) as cant
                                FROM
                                movimientos_de_dinero
                                INNER JOIN ventas_movimiento ON ventas_movimiento.movimiento_id = movimientos_de_dinero.id
                                WHERE
                                movimientos_de_dinero.caja_id = " . $estado[0]->id . " and
                                concepto_de_pago_id = 1 and movimientos_de_dinero.forma_pago_id = 12
                                UNION ALL
                                SELECT
                                sum(ventas_delivery.efectivo) as monto, 
		                        count(ventas_delivery.efectivo) as cant
                                FROM
                                movimientos_de_dinero
                                INNER JOIN ventas_delivery ON ventas_delivery.movimiento_id = movimientos_de_dinero.id
                                WHERE
                                movimientos_de_dinero.caja_id = " . $estado[0]->id . "
                                and
                                concepto_de_pago_id = 1 and movimientos_de_dinero.forma_pago_id = 12
                                ) as T                        
                        
                        ";
    $in = Cajas::getConnection()->query($q)->fetch();


    $q = "
SELECT
	sum(conceptos_de_pago.monto) AS monto
FROM
	movimientos_de_dinero
INNER JOIN conceptos_de_pago ON conceptos_de_pago.movimiento_id = movimientos_de_dinero.id
WHERE
	movimientos_de_dinero.caja_id = " . $estado[0]->id . "
AND concepto_de_pago_id = ";
    $eg = Cajas::getConnection()->query($q.'2')->fetch();

    $nieg = Cajas::getConnection()->query($q.'1')->fetch();

    $in[0]+=$nieg[0];
}
?>

<style>
    .card_caja{
        height: 200px;
    }
    .card-body{
        text-align: center;
    }
    .caja_center{
        line-height: 70px;
    }

</style>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" href="<?= site_url('cajas/show') ?>">Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('cajas') ?>">Historial Caja</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link " href="<?= site_url('conceptos-de-pagos') ?>">Operaciones</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="<?= site_url('movimientos-de-dineros') ?>">Transacciones</a>
    </li>

</ul>
<br><br>
<?php if(isset($estado)) if (empty($estado)): ?>
    <i href="<?php echo site_url('cajas/aperturar') ?>" onclick="openmodal(this)"
       class="button"
       data-icon="plusthick"
       title="Aperturar Cajas">
        <button class="btn btn-default" style="background: #024c51; color: white">
            Aperturar Caja
        </button>
        <hr>
    </i>

    <?php else: ?>
        <a
                class="btn btn-danger"
                data-icon="carat-1-e"
                href="<?php echo site_url('cajas/cerrar/' . $estado[0]->getId().'?monto_cerrado='.($estado[0]->monto_abierto+$in[0]-$eg[0])) ?>">
            Cerrar Caja
        </a>
    <?php endif; ?>
<?php if($estado): ?>
<div class="row">
    <div class="col-sm-6 col-md-4 card_caja">
        <div class="card card-accent-info card_caja">
            <div class="card-header">
                       <strong>SALDO ACTUAL:</strong> <?= $estado[0]->fecha_abierta ?>
             </div>
            <div class="card-body">
                <strong>
                    <h1 class="caja_center">
                      S/.  <?= $estado[0]->monto_abierto+$in[0]-$eg[0] ?>
                    </h1>
                </strong>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 card_caja">
        <div class="card card-accent-info card_caja">
            <div class="card-header" align="center">
                <strong>  MONTO TOTAL DE INGRESOS </strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        Efectivo:
                        <strong>

                            <?php
                            if ($in[0]){
                                echo "<h3 class=\"caja_center\"> S/. ".$in[0]."</h3>";
                            }else{
                                echo "<p class=\"caja_center\" style='font-size: 0.5em; color: red;'>No tiene ninguna venta en efectivo</h5>";
                            }
                            ?>
                        </strong>
                    </div>
                    <div class="col-md-6">
                        Tarjeta:
                        <?php $nq = "
                    SELECT
                        sum(ventas_movimiento.cuota) AS monto, count(ventas_movimiento.cuota) cant
                    FROM
                        movimientos_de_dinero
                    INNER JOIN ventas_movimiento ON ventas_movimiento.movimiento_id = movimientos_de_dinero.id
                    WHERE
                    movimientos_de_dinero.caja_id = ".$estado[0]->id." AND
                    movimientos_de_dinero.concepto_de_pago_id = 1 AND
                    movimientos_de_dinero.forma_pago_id = 13                
                        ";
                        $inT = Cajas::getConnection()->query($nq)->fetch();
                        if ($inT[0]){
                            echo '<h3 class="caja_center"> S/. '.$inT[0].'</h3>';
                        }else{
                            echo '<p class="caja_center" style="font-size:0.5em; color: red;">No tiene ninguna venta con Tarjeta</p>';
                        }
                        ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 card_caja">
        <div class="card card-accent-info card_caja">
            <div class="card-header" align="center">
                <strong> MONTO TOTAL DE EGRESOS </strong>
            </div>
            <div class="card-body">
                <strong>

                         <?php

                        if ($eg[0]){
                            echo '<h1 class="caja_center"> S/. '.$eg[0].'</h1>';
                        }else{
                            echo '<h4 class="caja_center" style="color: red;">No tiene ningún egreso</h4>';
                        }
                        ?>

                </strong>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <br><br>
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th colspan="2">
                        Monto Aperturado:
                    </th>
                    <th>
                        S/.  <?= $estado[0]->monto_abierto ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        Forma de Pago
                    </th>
                    <th>
                        Cant. Transacciones
                    </th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>Contado</td>
                <td>
                    <?php
                    if ($in[1]){
                        echo $in[1];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($in[0]){
                        echo "S/. ".$in[0];
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Tarjeta</td>
                <td>
                    <?php
                    if ($inT[1]){
                        echo $inT[1];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($inT[0]){
                        echo "S/. ".$inT[0];
                    }
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
