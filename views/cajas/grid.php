<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid cajas-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
                ID
			</th>
			<th class="ui-widget-header ">
                MONTO ABIERTO
			</th>
			<th class="ui-widget-header ">
				MONTO CERRADO
			</th>
			<th class="ui-widget-header ">
                FECHA CERRADA
			</th>
			<th class="ui-widget-header ">
                FECHA ABIERTA
			</th>
			<th class="ui-widget-header ">
                USUARIO
			</th>
			<th class="ui-widget-header ">

			</th>

		</tr>
	</thead>
	<tbody>
<?php foreach ($cajas as $key => $cajas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($cajas->getId()) ?>&nbsp;</td>
			<td><?php echo h($cajas->getMontoAbierto()) ?>&nbsp;</td>
			<td><?php echo h($cajas->getMontoCerrado()) ?>&nbsp;</td>
			<td><?php echo h($cajas->getFechaCerrada()) ?>&nbsp;</td>
			<td><?php echo h($cajas->getFechaAbierta()) ?>&nbsp;</td>
			<td><?php $idu=$cajas->getUsuarioId(); $u = Usuarios::getAll('WHERE id='.$idu)[0]; echo($u->nombres.' '.$u->apellidos);?>&nbsp;</td>
			<td>
                <?php if($cajas->estado): ?>


                <?php endif; ?>

		</tr>
<?php endforeach ?>
	</tbody>
</table>