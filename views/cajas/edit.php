<div id="usa">
<h6>
    <div align="center" >
    <?php echo $estado ? "APERTURAR" : "CERRAR" ?> CAJA</h6>

<form method="post" action="<?php echo site_url('cajas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($cajas->getId()) ?>" />
        <?php if ($estado): ?>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="cajas_monto_abierto">Monto Abierto</label>
			<input required class="form-control"  id="cajas_monto_abierto"
                   min="1"
                   type="number" name="monto_abierto" value="<?php echo $monto_ap ?>" />
            <input type="hidden" name="monto_cerrado" value="<?php echo $monto_ap ?>" />
		</div>
        <input type="hidden" name="fecha_abierta" value="<?= date("Y-m-d h:i:s") ?>" />
        <?php else: ?>
            <div class="form-field-wrapper">
                <label class="form-field-label" for="cajas_monto_cerrado">Monto Cerrado</label>
                <input required class="form-control"  id="cajas_monto_cerrado"
                       min="1"
                       type="number" name="monto_cerrado"
                       value="<?php echo h($cajas->getMontoAbierto()) ?>" />
            </div>
            <input type="hidden" name="estado" value="0">
            <input type="hidden" name="fecha_cerrada" value="<?= date("Y-m-d h:i:s") ?>" />
        <?php endif; ?>
		<input type="hidden" value="<?= $_SESSION['user']->id ?>" name="usuario_id">
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $cajas->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>
