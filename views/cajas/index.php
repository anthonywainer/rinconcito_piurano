<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" href="<?= site_url('cajas/show') ?>">Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="<?= site_url('cajas') ?>">Historial Caja</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('conceptos-de-pagos') ?>">Operaciones</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="<?= site_url('movimientos-de-dineros') ?>">Transacciones</a>
    </li>

</ul>
<br>


<h2 align="center">
	Historial de Cajas
</h2>
<div class="ui-widget-content ui-corner-all">

	<?php View::load('cajas/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
