<h1>View Clientes</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('clientes/edit/' . $clientes->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Clientes">
		Edit	</a>
	<a href="<?php echo site_url('clientes/delete/' . $clientes->getId()) ?>"
		class="button" data-icon="trash" title="Delete Clientes"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('ventas?cliente_id=' . $clientes->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Clientes">
		Ventas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombres</span>
		<?php echo h($clientes->getNombres()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Apellidos</span>
		<?php echo h($clientes->getApellidos()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Telefono</span>
		<?php echo h($clientes->getTelefono()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Direccion</span>
		<?php echo h($clientes->getDireccion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Correo</span>
		<?php echo h($clientes->getCorreo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($clientes->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($clientes->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($clientes->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>