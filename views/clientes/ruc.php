<div id="usa">
<h1><?php echo $clientes->isNew() ? "Nuevo" : "Editar" ?> Cliente</h1>
<form method="post" onsubmit="registrar_ruc(this); return false;" action="<?php echo site_url('clientes/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($clientes->getId()) ?>" />
        <div class="form-field-wrapper">
            <label class="form-field-label" for="clientes_ruc">RUC</label>
            <input id="clientes_ruc" class="form-control" type="number" name="ruc"  maxlength="11" minlength="0" value="<?php echo h($clientes->getRuc()) ?>" />
        </div>
        <div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_nombres">Razón Social</label>
			<input id="clientes_nombres" class="form-control" type="text" name="nombres" value="<?php echo h($clientes->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_direccion">Direccion</label>
			<input id="clientes_direccion" class="form-control" type="text" name="direccion" value="<?php echo h($clientes->getDireccion()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span  data-icon="disk">
			<input class="button btn btn-info" type="submit" value="<?php echo $clientes->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button btn" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>
