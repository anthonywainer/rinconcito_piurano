<div id="usa">
<h1><?php echo $clientes->isNew() ? "Nuevo" : "Editar" ?> Cliente</h1>
<form method="post" onsubmit="registrar_dni(this); return false;" action="<?php echo site_url('clientes/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($clientes->getId()) ?>" />
        <div class="form-field-wrapper">
            <label class="form-field-label" for="clientes_dni">DNI</label>
            <input id="clientes_dni" class="form-control" type="number" name="dni" value="<?php echo h($clientes->getDni()) ?>" />
        </div>
        <div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_nombres">Nombres</label>
			<input id="clientes_nombres" class="form-control" type="text" name="nombres" value="<?php echo h($clientes->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_apellidos">Apellidos</label>
			<input id="clientes_apellidos" class="form-control" type="text" name="apellidos" value="<?php echo h($clientes->getApellidos()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_telefono">Telefono</label>
			<input id="clientes_telefono" class="form-control" type="text" name="telefono" value="<?php echo h($clientes->getTelefono()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_direccion">Direccion</label>
			<input id="clientes_direccion" class="form-control" type="text" name="direccion" value="<?php echo h($clientes->getDireccion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_correo">Correo</label>
			<input id="clientes_correo" class="form-control" type="email" name="correo" value="<?php echo h($clientes->getCorreo()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button btn" data-icon="disk">
			<input type="submit" value="<?php echo $clientes->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button btn" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>
