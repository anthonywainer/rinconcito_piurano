<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid clientes-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRES
			</th>
			<th class="ui-widget-header ">
					APELLIDOS
			</th>
			<th class="ui-widget-header ">
					TELÉFONO
			</th>
			<th class="ui-widget-header ">
					DIRECCIÓN
			</th>
			<th class="ui-widget-header ">
					CORREO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($clientes as $key => $clientes): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($clientes->getId()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getDireccion()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getCorreo()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('clientes/edit/' . $clientes->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('clientes/delete/' . $clientes->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>