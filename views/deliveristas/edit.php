<div id="usa">
<h1><?php echo $deliverista->isNew() ? "New" : "Edit" ?> Deliverista</h1>
<form method="post" action="<?php echo site_url('deliveristas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($deliverista->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="deliverista_nombres">Nombres</label>
			<input id="deliverista_nombres" type="text" name="nombres" value="<?php echo h($deliverista->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="deliverista_apellidos">Apellidos</label>
			<input id="deliverista_apellidos" type="text" name="apellidos" value="<?php echo h($deliverista->getApellidos()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="deliverista_telefono">Telefono</label>
			<input id="deliverista_telefono" type="text" name="telefono" maxlength="9" value="<?php echo h($deliverista->getTelefono()) ?>" />
		</div>

	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $deliverista->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>
</div>