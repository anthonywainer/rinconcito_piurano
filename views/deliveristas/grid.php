<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid deliverista-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRES
			</th>
			<th class="ui-widget-header ">
					APELLIDO
			</th>
			<th class="ui-widget-header ">
					TELÉFONO
			</th>
            <th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($deliveristas as $key => $deliverista): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($deliverista->getId()) ?>&nbsp;</td>
			<td><?php echo h($deliverista->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($deliverista->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($deliverista->getTelefono()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('deliverista/' . $deliverista->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('deliverista/delete/' . $deliverista->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('delivery?deliverista_id=' . $deliverista->getId()) ?>">
					Delivery
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>