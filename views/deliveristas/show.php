<h1>View Deliverista</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('deliveristas/edit/' . $deliverista->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Deliverista">
		Edit	</a>
	<a href="<?php echo site_url('deliveristas/delete/' . $deliverista->getId()) ?>"
		class="button" data-icon="trash" title="Delete Deliverista"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('delivery?deliverista_id=' . $deliverista->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Delivery Deliverista">
		Delivery	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombres</span>
		<?php echo h($deliverista->getNombres()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Apellidos</span>
		<?php echo h($deliverista->getApellidos()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Telefono</span>
		<?php echo h($deliverista->getTelefono()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($deliverista->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($deliverista->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($deliverista->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>