<?php include 'layouts/header.php' ?>

<div class="container">
    <div class="home">
        <div class="row">
            <div class="col-sm-6">
                <div class="direccion">
                    <i class="fa fa-map-marker ubi" aria-hidden="true" style="color: red"></i>
                    <span style="text-align: center;margin-left: 120px;color: blue">Jr. Sinchi Roca N°465, Tarapoto</span>
                    <i class="fa fa-facebook-official face" aria-hidden="true"></i>
                </div>
                <img src="imagenes/ceviche.png" class="ceviche" alt="">
                <div class="telefono">
                    <i class="fa fa-phone-square" aria-hidden="true"></i>
                    <span>(042) 530222</span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="variedad">
                    <span style="color: white">variedad</span>
                </div>
                <img src="imagenes/pez.png" class="pez" alt="">
                <div class="disfruta">
                    <span>Disfruta de una deliciosa comida</span>
                </div>
            </div>

            <div class="col">
                <div class="footer">
                    <ul>
                        <li><a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                        <li><a href="">Home</a></li>
                        <li><a href="">Nosotros</a></li>
                        <li><a href="">Carta</a></li>
                        <li><a href="">Ubicación</a></li>
                        <li><a href="">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>