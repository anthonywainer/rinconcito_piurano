<h1><?php echo $ventas->isNew() ? "New" : "Edit" ?> Ventas</h1>
<form method="post" action="<?php echo site_url('ventas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_mesero_id">Meseros</label>
			<select id="ventas_mesero_id" name="mesero_id">
			<?php foreach (Meseros::doSelect() as $meseros): ?>
				<option <?php if ($ventas->getMeseroId() === $meseros->getId()) echo 'selected="selected"' ?> value="<?php echo $meseros->getId() ?>"><?php echo $meseros?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_cliente_id">Clientes</label>
			<select id="ventas_cliente_id" name="cliente_id">
			<?php foreach (Clientes::doSelect() as $clientes): ?>
				<option <?php if ($ventas->getClienteId() === $clientes->getId()) echo 'selected="selected"' ?> value="<?php echo $clientes->getId() ?>"><?php echo $clientes?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_created_at">Created At</label>
			<input id="ventas_created_at" type="text" name="created_at" value="<?php echo h($ventas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_updated_at">Updated At</label>
			<input id="ventas_updated_at" type="text" name="updated_at" value="<?php echo h($ventas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_deleted_at">Deleted At</label>
			<input id="ventas_deleted_at" type="text" name="deleted_at" value="<?php echo h($ventas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_estado_venta">Estado Venta</label>
			<label>
				<input <?php if ($ventas->getEstadoVenta() === 1) echo 'checked="checked"' ?> name="estado_venta" type="radio" value="1" />
				Yes
			</label>

			<label>
				<input <?php if ($ventas->getEstadoVenta() === 0) echo 'checked="checked"' ?> name="estado_venta" type="radio" value="0" />
				No
			</label>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_fecha">Fecha</label>
			<input id="ventas_fecha" type="text" name="fecha" value="<?php echo h($ventas->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $ventas->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>