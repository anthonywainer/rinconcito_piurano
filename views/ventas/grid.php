<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm table-hover" >
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>

			<th class="ui-widget-header ">
					USUARIO
			</th>
            <th>MESA</th>
            <th>TOTAL</th>
            <th class="ui-widget-header ">
                    FECHA
            </th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
<?php $c=1; foreach ($ventas as $key => $ventas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
            <td><?php echo $c;  $c++; ?></td>
            <td>
                <?= Usuarios::getAll('where id='.$ventas->mesero_id)[0]->getNombres() ?>
            </td>
			<td>
                <?php $vm= VentasMesas::getAll('where venta_id='.$ventas->id);
                if (count($vm)>1){
                $mmm = '';
                foreach ($vm as $v){
                $mmm.=Mesas::getAll('where id='.$v->mesa_id)[0]->mesa;
                }
                echo $mmm;
                }else{
                echo Mesas::getAll('where id='.$vm[0]->mesa_id)[0]->mesa;
                } ?>
            </td>
            <td>
                S/. <?= PedidoProducto::getAll('where venta_id='.$ventas->id)[0]->getTotal() ?>
            </td>
			<td><?php echo h($ventas->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
                <a class="btn btn-info"
                   target="_blank"
                   href="<?= site_url('movimientos-de-dineros/transaccion/'.VentasMovimiento::getAll('where venta_id='.$ventas->id)[0]->getMovimiento_id()) ?>">
                    ver <i class="fa fa-search "></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>