<style>
    .pan-mesa{
        padding-left: 10px;
        padding-bottom: 50px;
        cursor: pointer;
    }
    .mesa{
        height: 130px;
        background: #024C51;
    }
    .mesa:hover{
        background: orange;
    }
    .circle{
        background: white;
        color: grey;
        border-radius: 50%;
        height: 100%;
        text-align:center ;
        line-height: 120px;
    }
    .red{
        background: red;
    }
    .none{
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-3">
        <a href="<?= site_url('ventas')?>">mesas</a> /
        <a href="<?= site_url('delivery')?>">delivery</a>/
        <a href="<?= site_url('ventas?li=0')?>">Historial ventas</a>
    </div>
    <div class="col-md-9" align="right">
        <button class="btn btn-info" onclick="show_check(this)">Unir Mesas</button>
        <a class="btn btn-info"  onclick="if (confirm('desea agregar mesa?'))
                { window.location.href =
                '<?php echo site_url('mesas/save/') ?>'; }
                return false">+1 Agregar Mesa </a >
    </div>
    <br><br>
    <?php foreach ($mesas as $m): ?>
    <div class="col-md-2 pan-mesa" >
            <div class=" mesa <?php $b=true;
                if ($m['estado_mesa'] == 'ocupado'){ echo 'red"'; $b=false; }
                else{echo '"';}
            ?> " onclick="hacer_pedido(this)" href="<?php if($b){echo site_url('ventas/save/?mesas='.$m['mesa_id']);}else{echo site_url('pedido-productos/?id='.$m['venta_id']);} ?>">
                <div class="circle">

                    <?= $m['mesa'] ?>
                    <?php if($b): ?>
                        <input class="mesas none" type="checkbox" name="mesas[]" value="<?=$m['mesa_id']?>" >
                    <?php endif; ?>
                </div>

            </div>
        </div>
    <?php endforeach; ?>
</div>

<script>


    function show_check(t) {
        $(t).text('Unir');
        $(t).attr('onclick','unir()');
        $('.mesa').removeAttr('onclick');
        $("input[type=checkbox").each(
            function () {
                $(this).removeClass('none');
            }

        );
    }
    function unir() {
        mesas = $("input[type=checkbox").serialize();
        $.post("<?php echo site_url('ventas/save/') ?>",mesas).done(function (data) {
            window.location.href = "<?php echo site_url('pedido-productos/?id=') ?>"+data;
        });


    }

    function hacer_pedido(t) {
        window.location.href =$(t).attr('href');
    }
</script>