<style>
    .comanda{
        border-radius: 5px;
        border: 1px solid skyblue;
        padding: 50px;
        background: white;
    }
</style>
<?php $vm= VentasMesas::getAll('where venta_id='.$id) ?>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 comanda">
            <a href="<?= site_url('pedido-productos/?id='.$id) ?>">Atrás</a>
        <h2 align="center">"rinconcito Piurano"</h2>
        <h4 align="center"> COMANDA N° <?= '0'.$id ?> - MESA N° <?php
            if (count($vm)>1){
                $mmm = '';
                foreach ($vm as $v){
                    $mmm.=Mesas::getAll('where id='.$v->mesa_id)[0]->mesa;
                }
                echo $mmm;
            }else{
                echo Mesas::getAll('where id='.$vm[0]->mesa_id)[0]->mesa;
            }
            ?>   </h4>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th>Cant.</th>
                <th>Descripción</th>
                <th>S/. Total</th>
            </tr>
            </thead>
            <tbody>
            <?php $tt=0; $pp = PedidoProducto::getAll('Where venta_id='.$id.' and created_at is null');  if (count($pp)>0) foreach ( $pp as $p): ?>
                <tr>
                    <td>
                        <?= $p->getCantidad() ?>
                    </td>
                    <td>
                        <?php $pro=Productos::getAll('where id='.$p->getProducto_id()); echo $pro[0]->getNombre()?>
                    </td>
                    <td>
                        <?php echo 'S/. '.$p->getTotal(); $tt+=$p->getTotal(); ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>

        </table>
        <div align="right">
            Total: S/. <?= number_format($tt,2,'.',',') ?>
        </div>
    </div>
        <div class="col-md-3"></div>
    </div>
</div>
<div align="center">
    <a class="btn btn-info" href="<?= site_url('ventas/save/'.$id.'/?up=com') ?>">Guardar Comanda</a>
</div>
