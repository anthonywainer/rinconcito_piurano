<div class="col-md-3">
    <a href="<?= site_url('ventas')?>">Mesas</a> /
    <a href="<?= site_url('delivery')?>">Delivery</a>/
    <a href="<?= site_url('ventas?li=0')?>">Historial ventas</a>
</div>
<div class="col-md-9" align="center">
    <h1>Historial</h1>
</div>

<div class="ui-widget-content ui-corner-all">
	<?php View::load('ventas/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>