<h1>View Ventas</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('ventas/edit/' . $ventas->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Ventas">
		Edit	</a>
	<a href="<?php echo site_url('ventas/delete/' . $ventas->getId()) ?>"
		class="button" data-icon="trash" title="Delete Ventas"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('cronogramas-de-pagos?venta_id=' . $ventas->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Cronogramas De Pagos Ventas">
		Cronogramas De Pagos	</a>
	<a href="<?php echo site_url('pedido-productos?venta_id=' . $ventas->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Pedido Productos Ventas">
		Pedido Productos	</a>
	<a href="<?php echo site_url('ventas-mesas?venta_id=' . $ventas->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Mesas Ventas">
		Ventas Mesas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Mesero</span>
		<?php echo h($ventas->getMeserosRelatedByMeseroId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cliente</span>
		<?php echo h($ventas->getClientesRelatedByClienteId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($ventas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($ventas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($ventas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado Venta</span>
		<?php if ($ventas->getEstadoVenta() === 1) echo 'Yes'; elseif ($ventas->getEstadoVenta() === 0) echo 'No' ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha</span>
		<?php echo h($ventas->getFecha(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>