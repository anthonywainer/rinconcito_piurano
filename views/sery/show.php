<h1>View Series</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('sery/edit/' . $series->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Series">
		Edit	</a>
	<a href="<?php echo site_url('sery/delete/' . $series->getId()) ?>"
		class="button" data-icon="trash" title="Delete Series"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Serie</span>
		<?php echo h($series->getSerie()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Correlativo</span>
		<?php echo h($series->getCorrelativo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Voucher</span>
		<?php echo h($series->getVouchersRelatedByVoucherId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($series->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($series->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($series->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>