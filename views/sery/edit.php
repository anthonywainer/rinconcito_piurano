<div id="usa">
<h6 align="center"><?php echo $series->isNew() ? "NUEVA" : "EDITAR" ?> SERIES</h6>
<form method="post" action="<?php echo site_url('sery/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($series->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_serie">Serie</label>
			<input required class="form-control"id="series_serie" type="text" name="serie" value="<?php echo h($series->getSerie()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_correlativo">Correlativo</label>
			<input required class="form-control"id="series_correlativo" type="text" name="correlativo" value="<?php echo h($series->getCorrelativo()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_voucher_id">Vouchers</label>
			<select required class="form-control" id="series_voucher_id" name="voucher_id">
			<?php foreach (Vouchers::doSelect() as $vouchers): ?>
				<option <?php if ($series->getVoucherId() === $vouchers->getId()) echo 'selected="selected"' ?> value="<?php echo $vouchers->getId() ?>"><?php echo $vouchers?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix " align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $series->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>