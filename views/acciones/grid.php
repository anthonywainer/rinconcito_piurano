<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid acciones-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					ACCIÓN
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($acciones as $key => $acciones): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($acciones->getId()) ?>&nbsp;</td>
			<td><?php echo h($acciones->getAccion()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('acciones/edit/' . $acciones->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('acciones/delete/' . $acciones->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
				<a class="btn btn-warning" title="ver modulos"href="<?php echo site_url('permisos?idaccion=' . $acciones->getId()) ?>">
                    <i class="fa fa-search "></i> Permisos
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>