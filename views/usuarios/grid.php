<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid usuarios-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					APELLIDO
			</th>

			<th class="ui-widget-header ">
					CORREO
			</th>
			<th class="ui-widget-header ">
					TELÉFONO
			</th>
			<th class="ui-widget-header ">
					DIRECCIÓN
			</th>
            <th>
                    GRUPO
            </th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES </th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($usuarios as $key => $usuarios): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php $id = $usuarios->getId(); echo h($usuarios->getId()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getCorreo()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getDireccion()) ?>&nbsp;</td>
            <td>
                <?php echo $usuarios->UsuariosGrupos($id)[0];  ?>
            </td>
            <td>
                <a class="btn btn-success" title="ver permisos" href="<?php echo site_url('usuarios/permisos/' . $usuarios->getId()) ?>">
                    <i class="fa fa-lock "></i>
                </a>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('usuarios/edit/' . $usuarios->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Está seguro de eliminar?')) { window.location.href = '<?php echo site_url('usuarios/delete/' . $usuarios->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>