<div id="usa">
<h6 align="center"><?php echo $usuarios->isNew() ? "NUEVO" : "EDITAR" ?> USUARIO</h6>
<form method="post"  action="<?php echo site_url('usuarios/save') ?>" >
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($usuarios->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_nombres">Nombres</label>
			<input required class="form-control" id="usuarios_nombres" type="text" name="nombres" value="<?php echo h($usuarios->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_apellidos">Apellidos</label>
			<input required class="form-control" id="usuarios_apellidos" type="text" name="apellidos" value="<?php echo h($usuarios->getApellidos()) ?>" />
		</div>
        <div class="form-field-wrapper">
            <label class="form-field-label" for="usuarios_correo">Correo</label>
            <input required class="form-control" id="usuarios_correo" type="email" name="correo" value="<?php echo h($usuarios->getCorreo()) ?>" />
        </div>

         <label class="form-field-label" for="usuarios_clave">Clave</label>
        <div class="form-inline">
            <div class="form-group">
                <input id="password"  required  class="form-control"
                       type="password" name="clave" value="<?php echo h($usuarios->getClave()) ?>" />
                <button onclick="ver_clave(); return false;" class="btn"><i class="fa fa-eye-slash"></i></button>
            </div>
        </div>
        <div class="form-field-wrapper">
            <label class="form-field-label" for="grupos">Grupo</label>
            <select name="grupos" id="grupo" class="form-control">
                <?php foreach ($grupos as $g):  ?>
                    <option <?php if (isset($idgrupo )){ if ($idgrupo[0]->grupo_id == $g->id){echo "selected"; } }  ?> value="<?php echo $g->id ?>"><?php echo $g->descripcion ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_telefono">Teléfono</label>
			<input required class="form-control" id="usuarios_telefono" type="number" name="telefono" value="<?php echo h($usuarios->getTelefono()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_direccion">Dirección</label>
			<input required class="form-control" id="usuarios_direccion" type="text" name="direccion" value="<?php echo h($usuarios->getDireccion()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span data-icon="disk" >
			<input  class="btn btn-primary"  type="submit" value="<?php echo $usuarios->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>

	</div>
</form>
</div>
