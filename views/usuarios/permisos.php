<h2 align="center">Lista de Permisos</h2>
<form action="<?php echo site_url('usuarios/permisos/'.$idp) ?>" method="POST">

    <h3>Permisos</h3>
    <?php foreach ($modulos as $m ):?>
        <!-- Recorremos cada módulo -->
        <ul>
            <?= $m->nombre ?>
            <?php $perM= Permisos::getAll("WHERE idmodulo= ".$m->id); if(isset($perM)){   ?>
                <!--
                    Recorremos todos los permisos por cada módulo para verificar si este permiso ya
                    existe dentro de permisos grupos, asi evitar su duplicado
                    $permisos_g = permisos por grupo
                    $permisos_u = permisos por usuario
                 -->
                <input type="checkbox"
                    <?php if($perM) if(in_array($perM[0]->id,$permisos_g)==1){echo "checked disabled";}
                elseif(in_array($perM[0]->id,$permisos_u)==1){echo "checked";}
                ?> name="permisos[]" value="<?= $perM[0]->id ?>">
            <?php } ?>
            <!-- NUMERO 1
                if:
                preguntamos si el permiso ya existe dentro de permisos por grupo,
                si se encuentra lo checkeamos y luego lo desabilitamos para que el
                usuario no lo vuelva a marcar

                 elseif: de ser lo contrario preguntamos dentro de permisos por usuario
                 si se encuentra lo checkeamos.
                -->

            <?php foreach (Submodulo::getAll('WHERE idmodulo= '.$m->id) as $sm){?>
                <li><?= $sm->nombre ?> ====>
                    <?php $persM= Permisos::getAll("WHERE idsubmodulo= ".$sm->id); if(isset($persM)){
                        foreach ($persM as $pm){?>  <!-- Recorremos los permisos de cada submódulo-->
                            <label for=""><?= $pm->nombre ?></label>
                            <input type="checkbox"
                            <?php if(in_array($pm->id,$permisos_g)==1){echo "checked disabled";}
                                elseif (in_array($pm->id,$permisos_u)==1){echo "checked";}
                            ?> name="permisos[]" value="<?= $pm->id ?>">
                        <?php }  } ?>
                        <!-- hacemos los mismo del NUMERO 1 -->
                </li>
            <?php }?>
            <!-- Recorremos cada submódulo por cada módulo -->

        </ul>
    <?php endforeach; ?>
<div class="form-action-buttons ui-helper-clearfix" align="right">
    <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
        <a class="btn btn-secondary" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
            Cancelar
        </a>
    <?php endif ?>
    <span data-icon="disk" >
			<input  class="btn btn-primary"  type="submit" value="Guardar" />
    </span>

</div>
</form>