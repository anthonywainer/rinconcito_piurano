<h1>View Usuarios</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('usuarios/edit/' . $usuarios->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Usuarios">
		Edit	</a>
	<a href="<?php echo site_url('usuarios/delete/' . $usuarios->getId()) ?>"
		class="button" data-icon="trash" title="Delete Usuarios"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('cajas?usuario_id=' . $usuarios->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Cajas Usuarios">
		Cajas	</a>
	<a href="<?php echo site_url('movimientos-de-dineros?usuario_id=' . $usuarios->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Movimientos De Dineros Usuarios">
		Movimientos De Dineros	</a>
	<a href="<?php echo site_url('permisos-usuarios?usuario_id=' . $usuarios->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Permisos Usuarios Usuarios">
		Permisos Usuarios	</a>
	<a href="<?php echo site_url('usuarios-grupos?usuario_id=' . $usuarios->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Usuarios Grupos Usuarios">
		Usuarios Grupos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombres</span>
		<?php echo h($usuarios->getNombres()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Apellidos</span>
		<?php echo h($usuarios->getApellidos()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Clave</span>
		<?php echo h($usuarios->getClave()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Ingreso</span>
		<?php echo h($usuarios->getFechaIngreso(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Salida</span>
		<?php echo h($usuarios->getFechaSalida(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Esta Activo</span>
		<?php echo h($usuarios->getEstaActivo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Correo</span>
		<?php echo h($usuarios->getCorreo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Telefono</span>
		<?php echo h($usuarios->getTelefono()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Direccion</span>
		<?php echo h($usuarios->getDireccion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($usuarios->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($usuarios->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($usuarios->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>