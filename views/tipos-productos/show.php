<h1>View Tipos Producto</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('tipos-productos/edit/' . $tipos_producto->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Tipos_producto">
		Edit	</a>
	<a href="<?php echo site_url('tipos-productos/delete/' . $tipos_producto->getId()) ?>"
		class="button" data-icon="trash" title="Delete Tipos_producto"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('productos?tipo_producto_id=' . $tipos_producto->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Productos Tipos_producto">
		Productos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($tipos_producto->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($tipos_producto->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($tipos_producto->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($tipos_producto->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($tipos_producto->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>