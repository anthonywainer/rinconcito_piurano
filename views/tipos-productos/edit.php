<div id="usa" >
<h6>
    <div align="center" >
    <?php echo $tipos_producto->isNew() ? "NUEVO" : "EDITAR" ?> TIPOS DE PRODUCTO</h6>
<form method="post" action="<?php echo site_url('tipos-productos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($tipos_producto->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_producto_nombre">Nombre</label>
			<input required class="form-control" id="tipos_producto_nombre" type="text" name="nombre" value="<?php echo h($tipos_producto->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_producto_descripcion">Descripción</label>
			<textarea required class="form-control" id="tipos_producto_descripcion" name="descripcion"><?php echo h($tipos_producto->getDescripcion()) ?></textarea>
		</div>
        <br>

	</div>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $tipos_producto->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>
</div>