<h1>View Permisos Grupo</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('permisos-grupos/edit/' . $permisos_grupo->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Permisos_grupo">
		Edit	</a>
	<a href="<?php echo site_url('permisos-grupos/delete/' . $permisos_grupo->getId()) ?>"
		class="button" data-icon="trash" title="Delete Permisos_grupo"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Idpermiso</span>
		<?php echo h($permisos_grupo->getPermisosRelatedByIdpermiso()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Idgrupo</span>
		<?php echo h($permisos_grupo->getUsuariosGrupoRelatedByIdgrupo()) ?>
	</div>
</div>