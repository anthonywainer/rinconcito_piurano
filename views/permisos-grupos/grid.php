<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid permisos_grupo-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					IDPERMISO
			</th>
			<th class="ui-widget-header ">
					IDGRUPO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($permisos_grupos as $key => $permisos_grupo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos_grupo->getId()) ?>&nbsp;</td>
			<td><?php echo $permisos_grupo->getPermisosT($permisos_grupo->getIdpermiso())[0]->nombre ?>&nbsp;</td>
			<td><?php echo $permisos_grupo->getGrupoT($permisos_grupo->getIdgrupo())[0]->descripcion ?>&nbsp;</td>
			<td>
                <a class="btn btn-warning" title="ver modulos" href="<?php echo site_url('permisos_grupos/' . $permisos_grupo->getId()) ?>">
                    <i class="fa fa-search "></i>
                </a>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('permisos_grupos/edit/' . $permisos_grupo->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('permisos_grupos/delete/' . $permisos_grupo->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>