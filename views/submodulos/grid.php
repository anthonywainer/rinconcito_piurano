<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid submodulo-grid table table-condensed" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					URL
			</th>
			<th class="ui-widget-header ">
					ICON
			</th>
			<th class="ui-widget-header ">
					IDMÓDULO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($submodulos as $key => $submodulo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($submodulo->getId()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getUrl()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getIcon()) ?>&nbsp;</td>
			<td><?php echo $submodulo->getModulosT($submodulo->getIdModulo())[0]->nombre ?>&nbsp;</td>
			<td>
                <i class="btn btn-info" onclick="openmodal(this)" href="<?php echo site_url('submodulos/edit/' . $submodulo->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('submodulos/delete/' . $submodulo->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>
                </a>
                <a href="<?= site_url('submodulos/permisos/' . $submodulo->getId()) ?>">ver permisos</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>