<h1>View Submodulo</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('submodulos/edit/' . $submodulo->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Submodulo">
		Edit	</a>
	<a href="<?php echo site_url('submodulos/delete/' . $submodulo->getId()) ?>"
		class="button" data-icon="trash" title="Delete Submodulo"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($submodulo->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Url</span>
		<?php echo h($submodulo->getUrl()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Icon</span>
		<?php echo h($submodulo->getIcon()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Idmodulo</span>
		<?php echo h($submodulo->getModulosRelatedByIdmodulo()) ?>
	</div>
</div>