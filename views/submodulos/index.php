
	<i href="<?php echo site_url('submodulos/edit') ?>"onclick="openmodal(this)"
	   class="button"
	   data-icon="plusthick"
	   title="New Submodulo">
        <button class="btn btn-default" style="background: #024c51; color: white">
            +1 Nuevo Submódulo
        </button>
	</i>
    <h4 align="center">
	LISTA SUBMÓDULOS
</h4>
<div class="row">
    <div class="form-group col-md-4">
        <div class="input-group">
            <input id="filtrar" href="<?= site_url('submodulos/index?search=') ?>" value="<?= $_GET['search'] ?>" type="search" onkeyup="buscar_tabla_ajax(this)" class="form-control" placeholder="Buscar Submódulos">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>
<div id="grilla">
    <div class="ui-widget-content ui-corner-all" >
        <?php View::load('submodulos/grid', $params) ?>
    </div>
    <?php View::load('pager', compact('pager')) ?>
</div>
    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #024C51">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
