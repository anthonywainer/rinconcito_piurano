<div id="usa">
<h6>
    <div align="center" >
    <?php echo $submodulo->isNew() ? "NUEVO" : "EDITAR" ?> SUBMÓDULOS</h6>
<form method="post" action="<?php echo site_url('submodulos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($submodulo->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="submodulo_nombre">Nombre</label>
			<input required class="form-control"id="submodulo_nombre" type="text" name="nombre" value="<?php echo h($submodulo->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="submodulo_url">Url</label>
			<input required class="form-control"id="submodulo_url" type="text" name="url" value="<?php echo h($submodulo->getUrl()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="submodulo_icon">Icon</label>
			<input required class="form-control"id="submodulo_icon" type="text" name="icon" value="<?php echo h($submodulo->getIcon()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="submodulo_idmodulo">Modulos</label>
			<select required class="form-control" id="submodulo_idmodulo" name="idmodulo">
			<?php foreach (Modulos::getAll() as $modulos): ?>
				<option <?php if ($submodulo->getIdmodulo() === $modulos->id) echo 'selected="selected"' ?> value="<?php echo $modulos->id ?>"><?php echo $modulos->nombre?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary"  type="submit" value="<?php echo $submodulo->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>