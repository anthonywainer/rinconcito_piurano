<h1><?php echo $medios_de_envio->isNew() ? "New" : "Edit" ?> Medios De Envio</h1>
<form method="post" action="<?php echo site_url('medios-de-envios/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($medios_de_envio->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="medios_de_envio_descripcion">Descripcion</label>
			<input id="medios_de_envio_descripcion" type="text" name="descripcion" value="<?php echo h($medios_de_envio->getDescripcion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="medios_de_envio_created_at">Created At</label>
			<input id="medios_de_envio_created_at" type="text" name="created_at" value="<?php echo h($medios_de_envio->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="medios_de_envio_updated_at">Updated At</label>
			<input id="medios_de_envio_updated_at" type="text" name="updated_at" value="<?php echo h($medios_de_envio->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="medios_de_envio_deleted_at">Deleted At</label>
			<input id="medios_de_envio_deleted_at" type="text" name="deleted_at" value="<?php echo h($medios_de_envio->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $medios_de_envio->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>