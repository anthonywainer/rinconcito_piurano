<h1>
	<a href="<?php echo site_url('medios-de-envios/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Medios de envio">
		New Medios de envio
	</a>
	Medios De Envios
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('medios-de-envios/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>