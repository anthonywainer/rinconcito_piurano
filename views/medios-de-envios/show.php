<h1>View Medios De Envio</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('medios-de-envios/edit/' . $medios_de_envio->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Medios_de_envio">
		Edit	</a>
	<a href="<?php echo site_url('medios-de-envios/delete/' . $medios_de_envio->getId()) ?>"
		class="button" data-icon="trash" title="Delete Medios_de_envio"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('ventas?medio_de_envio_id=' . $medios_de_envio->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Ventas Medios_de_envio">
		Ventas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($medios_de_envio->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($medios_de_envio->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($medios_de_envio->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($medios_de_envio->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>