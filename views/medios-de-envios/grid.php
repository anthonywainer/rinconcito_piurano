<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid medios_de_envio-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MediosDeEnvio::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MediosDeEnvio::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MediosDeEnvio::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MediosDeEnvio::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MediosDeEnvio::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MediosDeEnvio::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MediosDeEnvio::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MediosDeEnvio::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MediosDeEnvio::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MediosDeEnvio::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($medios_de_envios as $key => $medios_de_envio): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($medios_de_envio->getId()) ?>&nbsp;</td>
			<td><?php echo h($medios_de_envio->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($medios_de_envio->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($medios_de_envio->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($medios_de_envio->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Medios_de_envio"
					href="<?php echo site_url('medios-de-envios/show/' . $medios_de_envio->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Medios_de_envio"
					href="<?php echo site_url('medios-de-envios/edit/' . $medios_de_envio->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Medios_de_envio"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('medios-de-envios/delete/' . $medios_de_envio->getId()) ?>'; } return false">
					Delete
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('ventas?medio_de_envio_id=' . $medios_de_envio->getId()) ?>">
					Ventas
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>