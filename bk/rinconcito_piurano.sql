/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : rinconcito_piurano

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-05 13:47:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acciones
-- ----------------------------
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of acciones
-- ----------------------------
INSERT INTO `acciones` VALUES ('1', 'index');
INSERT INTO `acciones` VALUES ('2', 'edit');
INSERT INTO `acciones` VALUES ('3', 'delete');
INSERT INTO `acciones` VALUES ('4', 'save');
INSERT INTO `acciones` VALUES ('5', 'permisos');
INSERT INTO `acciones` VALUES ('6', 'aperturar');
INSERT INTO `acciones` VALUES ('7', 'cerrar');
INSERT INTO `acciones` VALUES ('8', 'show');
INSERT INTO `acciones` VALUES ('9', 'editarprecio');
INSERT INTO `acciones` VALUES ('10', 'transaccion');

-- ----------------------------
-- Table structure for amortizaciones
-- ----------------------------
DROP TABLE IF EXISTS `amortizaciones`;
CREATE TABLE `amortizaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` float NOT NULL,
  `cronograma_de_pago_id` int(11) NOT NULL,
  `movimiento_de_dinero_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cronograma_de_pago_id` (`cronograma_de_pago_id`),
  KEY `movimiento_de_dinero_id` (`movimiento_de_dinero_id`),
  CONSTRAINT `amortizaciones_ibfk_1` FOREIGN KEY (`cronograma_de_pago_id`) REFERENCES `cronogramas_de_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `amortizaciones_ibfk_2` FOREIGN KEY (`movimiento_de_dinero_id`) REFERENCES `movimientos_de_dinero` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of amortizaciones
-- ----------------------------

-- ----------------------------
-- Table structure for cajas
-- ----------------------------
DROP TABLE IF EXISTS `cajas`;
CREATE TABLE `cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto_abierto` float NOT NULL,
  `monto_cerrado` float NOT NULL,
  `fecha_cerrada` datetime NOT NULL,
  `fecha_abierta` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `estado` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  CONSTRAINT `cajas_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cajas
-- ----------------------------
INSERT INTO `cajas` VALUES ('9', '100', '1000', '2017-11-02 14:58:13', '2017-11-02 14:58:04', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('10', '1000', '1010', '2017-11-02 15:07:04', '2017-11-02 14:58:18', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('11', '1020', '2000', '0000-00-00 00:00:00', '2017-11-02 15:09:25', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('12', '2000', '2030', '2017-11-07 10:39:41', '2017-11-07 09:00:36', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('13', '2030', '2040', '2017-11-08 03:12:21', '2017-11-08 03:11:08', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('14', '30', '30', '2017-11-09 07:11:17', '2017-11-09 07:01:36', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('15', '0', '10', '2017-11-10 04:41:32', '2017-11-10 04:40:30', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('16', '100', '655', '2017-11-23 08:53:54', '2017-11-14 07:51:38', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('17', '200', '410', '2017-11-27 03:07:13', '2017-11-26 03:23:18', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('18', '410', '492', '2017-11-28 11:42:29', '2017-11-27 04:22:37', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('19', '492', '532', '2017-11-28 11:53:27', '2017-11-28 11:48:28', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('20', '532', '532', '2017-11-28 12:03:20', '2017-11-28 11:55:10', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('21', '532', '622', '2017-11-29 10:25:38', '2017-11-28 12:03:22', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('22', '622', '632', '2017-11-30 01:22:26', '2017-11-29 10:25:41', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('23', '10', '20', '2017-11-30 02:35:28', '2017-11-30 01:22:27', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('24', '50', '60', '2017-11-30 03:37:05', '2017-11-30 02:35:32', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('25', '-60', '-60', '2017-11-30 03:37:25', '2017-11-30 03:37:06', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('26', '60', '22', '2017-11-30 08:06:36', '2017-11-30 03:37:27', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('27', '50', '10', '2017-11-30 08:45:18', '2017-11-30 08:10:22', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('28', '40', '1', '2017-11-30 08:46:27', '2017-11-30 08:45:19', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('29', '40', '120', '2017-11-30 09:38:41', '2017-11-30 08:46:28', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('30', '120', '192', '2017-12-02 06:52:09', '2017-12-02 11:29:45', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');
INSERT INTO `cajas` VALUES ('31', '100', '192', '0000-00-00 00:00:00', '2017-12-02 06:52:10', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(40) NOT NULL,
  `apellidos` varchar(40) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `direccion` varchar(40) NOT NULL,
  `correo` varchar(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `ruc` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of clientes
-- ----------------------------
INSERT INTO `clientes` VALUES ('16', 'este', 'van dido', '912301230', 's/N', 'aaaa@fff.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '91231021', null);
INSERT INTO `clientes` VALUES ('17', 'roza', 'meltrozo', '91239912', 'jasjdjas', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '91230301', null);
INSERT INTO `clientes` VALUES ('19', 'acca', null, null, 'morales', null, null, null, null, null, '10715350306');

-- ----------------------------
-- Table structure for conceptos_de_pago
-- ----------------------------
DROP TABLE IF EXISTS `conceptos_de_pago`;
CREATE TABLE `conceptos_de_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `movimiento_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `tipo_de_pago_id` (`movimiento_id`),
  CONSTRAINT `conceptos_de_pago_ibfk_1` FOREIGN KEY (`movimiento_id`) REFERENCES `movimientos_de_dinero` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of conceptos_de_pago
-- ----------------------------

-- ----------------------------
-- Table structure for cronogramas_de_pago
-- ----------------------------
DROP TABLE IF EXISTS `cronogramas_de_pago`;
CREATE TABLE `cronogramas_de_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_pagar` date NOT NULL,
  `cuota` float NOT NULL,
  `monto_pagado` float NOT NULL,
  `monto_total` float NOT NULL,
  `monto_pagar` float NOT NULL,
  `venta_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venta_id` (`venta_id`),
  CONSTRAINT `cronogramas_de_pago_ibfk_1` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cronogramas_de_pago
-- ----------------------------

-- ----------------------------
-- Table structure for deliveries
-- ----------------------------
DROP TABLE IF EXISTS `deliveries`;
CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `direccion` varchar(40) DEFAULT NULL,
  `referencia` varchar(40) DEFAULT NULL,
  `deliverista_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `tiempo_demora` int(11) DEFAULT NULL,
  `estado_delivery` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `empleado_id` (`deliverista_id`),
  KEY `cliente_id` (`cliente_id`),
  CONSTRAINT `deliveries_ibfk_1` FOREIGN KEY (`deliverista_id`) REFERENCES `deliverista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `deliveries_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of deliveries
-- ----------------------------
INSERT INTO `deliveries` VALUES ('82', '2017-12-05', 'xczxcmz', 'skfsdfk', '1', '2017-12-05 00:00:00', '2017-12-05 00:00:00', null, null, '10', '2');
INSERT INTO `deliveries` VALUES ('86', '2017-12-05', 'jjxjc', 'kaksdk', '1', '2017-12-05 00:00:00', '2017-12-05 00:00:00', null, null, '10', '2');

-- ----------------------------
-- Table structure for deliverista
-- ----------------------------
DROP TABLE IF EXISTS `deliverista`;
CREATE TABLE `deliverista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(40) NOT NULL,
  `apellidos` varchar(40) NOT NULL,
  `telefono` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of deliverista
-- ----------------------------
INSERT INTO `deliverista` VALUES ('1', 'hali', 'huaccha', '12312', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `deliverista` VALUES ('2', 'asdkak', 'kajsdj', '1293192', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for formas_pago
-- ----------------------------
DROP TABLE IF EXISTS `formas_pago`;
CREATE TABLE `formas_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of formas_pago
-- ----------------------------
INSERT INTO `formas_pago` VALUES ('12', 'contado', '2017-10-19 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `formas_pago` VALUES ('13', 'tarjeta visa', '2017-10-19 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('2', 'Administrador', '2017-12-05 00:00:00', null, null);
INSERT INTO `grupos` VALUES ('3', 'Empleado', '2017-12-05 00:00:00', null, null);

-- ----------------------------
-- Table structure for mesas
-- ----------------------------
DROP TABLE IF EXISTS `mesas`;
CREATE TABLE `mesas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `numero` int(255) NOT NULL,
  `estado_mesa` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'disponible',
  `mesa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mesas
-- ----------------------------
INSERT INTO `mesas` VALUES ('21', '1', 'disponible', 'M1');
INSERT INTO `mesas` VALUES ('22', '2', 'disponible', 'M2');
INSERT INTO `mesas` VALUES ('23', '3', 'disponible', 'M3');
INSERT INTO `mesas` VALUES ('24', '4', 'disponible', 'M4');
INSERT INTO `mesas` VALUES ('25', '5', 'disponible', 'M5');
INSERT INTO `mesas` VALUES ('26', '6', 'disponible', 'M6');
INSERT INTO `mesas` VALUES ('27', '7', 'disponible', 'M7');
INSERT INTO `mesas` VALUES ('28', '8', 'disponible', 'M8');
INSERT INTO `mesas` VALUES ('29', '9', 'disponible', 'M9');
INSERT INTO `mesas` VALUES ('30', '10', 'disponible', 'M10');
INSERT INTO `mesas` VALUES ('31', '11', 'disponible', 'M11');
INSERT INTO `mesas` VALUES ('32', '12', 'disponible', 'M12');
INSERT INTO `mesas` VALUES ('33', '13', 'disponible', 'M13');
INSERT INTO `mesas` VALUES ('34', '14', 'disponible', 'M14');
INSERT INTO `mesas` VALUES ('35', '15', 'disponible', 'M15');
INSERT INTO `mesas` VALUES ('36', '16', 'disponible', 'M16');
INSERT INTO `mesas` VALUES ('37', '17', 'disponible', 'M17');
INSERT INTO `mesas` VALUES ('38', '18', 'disponible', 'M18');

-- ----------------------------
-- Table structure for meseros
-- ----------------------------
DROP TABLE IF EXISTS `meseros`;
CREATE TABLE `meseros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(40) NOT NULL,
  `apellidos` varchar(40) NOT NULL,
  `sexo` varchar(20) NOT NULL,
  `sueldo_basico` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of meseros
-- ----------------------------
INSERT INTO `meseros` VALUES ('1', 'pancracio', 'aguileÃ±o', 'masculino', '800', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `meseros` VALUES ('2', 'Juan', 'Luna', 'masculino', '600', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for modulos
-- ----------------------------
DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modulos
-- ----------------------------
INSERT INTO `modulos` VALUES ('1', 'CONFIGURACIÃ“N', 'configuraciÃ³n', 'icon-puzzle');
INSERT INTO `modulos` VALUES ('3', 'RESTAURANTE', '#', 'icon-puzzle');
INSERT INTO `modulos` VALUES ('4', 'PAGOS', 'pagos', 'icon-puzzle');
INSERT INTO `modulos` VALUES ('6', 'MANTENIMIENTOS', 'mantenimientos', 'icon-puzzle');
INSERT INTO `modulos` VALUES ('7', 'REPORTES', 'reportes', 'icon-puzzle');
INSERT INTO `modulos` VALUES ('8', 'SEGURIDAD', '#', 'icon-puzzle');

-- ----------------------------
-- Table structure for movimientos_de_dinero
-- ----------------------------
DROP TABLE IF EXISTS `movimientos_de_dinero`;
CREATE TABLE `movimientos_de_dinero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_voucher` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `concepto_de_pago_id` int(11) DEFAULT NULL,
  `caja_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `forma_pago_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voucher_id` (`voucher_id`),
  KEY `concepto_de_pago_id` (`concepto_de_pago_id`),
  KEY `caja_id` (`caja_id`),
  KEY `usuario_id` (`usuario_id`),
  KEY `forma_pago_id` (`forma_pago_id`),
  CONSTRAINT `movimientos_de_dinero_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movimientos_de_dinero_ibfk_3` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movimientos_de_dinero_ibfk_4` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movimientos_de_dinero_ibfk_5` FOREIGN KEY (`forma_pago_id`) REFERENCES `formas_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movimientos_de_dinero_ibfk_6` FOREIGN KEY (`concepto_de_pago_id`) REFERENCES `tipos_concepto_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of movimientos_de_dinero
-- ----------------------------
INSERT INTO `movimientos_de_dinero` VALUES ('216', null, '1', '1', '31', '4', '13', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('217', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('218', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('219', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('223', null, '2', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('224', null, '1', '1', '31', '13', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('225', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('226', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('227', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);
INSERT INTO `movimientos_de_dinero` VALUES ('228', null, '1', '1', '31', '4', '12', '2017-12-05 00:00:00', null, null);

-- ----------------------------
-- Table structure for pedido_producto
-- ----------------------------
DROP TABLE IF EXISTS `pedido_producto`;
CREATE TABLE `pedido_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `venta_id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT '1',
  `precio` float DEFAULT NULL,
  `total` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `tiempo_espera` varchar(255) DEFAULT '0',
  `estado_pedido` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `producto_id` (`producto_id`),
  KEY `pedidos_id` (`venta_id`) USING BTREE,
  CONSTRAINT `pedido_producto_ibfk_2` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_producto` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pedido_producto
-- ----------------------------
INSERT INTO `pedido_producto` VALUES ('299', '3', '177', '1', '10', '10', '2017-12-05 11:12:22', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('300', '18', '177', '1', '5', '5', '2017-12-05 11:12:22', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('303', '3', '179', '1', '10', '10', '2017-12-05 12:12:26', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('304', '15', '179', '1', '12', '12', '2017-12-05 12:12:26', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('305', '2', '180', '1', '10', '10', '2017-12-05 12:12:31', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('306', '3', '180', '1', '10', '10', '2017-12-05 12:12:31', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('314', '3', '186', '1', '10', '10', '2017-12-05 12:12:19', null, null, '0', '');
INSERT INTO `pedido_producto` VALUES ('315', '15', '186', '1', '12', '12', '2017-12-05 12:12:19', null, null, '0', '');

-- ----------------------------
-- Table structure for permisos
-- ----------------------------
DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idaccion` int(11) NOT NULL,
  `idsubmodulo` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idaccion` (`idaccion`),
  KEY `idmodulo` (`idsubmodulo`),
  KEY `permisos_ibfk_3` (`idmodulo`),
  CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`idaccion`) REFERENCES `acciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_ibfk_3` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_ibfk_4` FOREIGN KEY (`idsubmodulo`) REFERENCES `submodulo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos
-- ----------------------------
INSERT INTO `permisos` VALUES ('1', '1', null, 'ver configuraciÃ³n', '1');
INSERT INTO `permisos` VALUES ('2', '1', '22', 'ver acciones', null);
INSERT INTO `permisos` VALUES ('3', '2', '22', 'editar acciones', null);
INSERT INTO `permisos` VALUES ('4', '3', '22', 'eliminar acciones', null);
INSERT INTO `permisos` VALUES ('5', '4', '22', 'guardar acciones', null);
INSERT INTO `permisos` VALUES ('6', '1', '23', 'ver permisos', null);
INSERT INTO `permisos` VALUES ('7', '1', '1', 'ver modulos', null);
INSERT INTO `permisos` VALUES ('8', '1', '2', 'ver submodulo', null);
INSERT INTO `permisos` VALUES ('10', '1', null, 'ver restaurante', '3');
INSERT INTO `permisos` VALUES ('25', '1', '6', 'ver clientes', null);
INSERT INTO `permisos` VALUES ('30', '1', null, 'Ver pagos', '4');
INSERT INTO `permisos` VALUES ('34', '1', '11', 'ver conceptos de pago', null);
INSERT INTO `permisos` VALUES ('35', '1', '12', 'ver tipos concepto de pago', null);
INSERT INTO `permisos` VALUES ('41', '1', null, 'ver mantenimientos', '6');
INSERT INTO `permisos` VALUES ('42', '1', '16', 'ver usuarios', null);
INSERT INTO `permisos` VALUES ('43', '1', '17', 'ver grupos', null);
INSERT INTO `permisos` VALUES ('44', '1', '19', 'ver formas de pago', null);
INSERT INTO `permisos` VALUES ('45', '1', '20', 'ver vouchers', null);
INSERT INTO `permisos` VALUES ('46', '1', '21', 'ver series', null);
INSERT INTO `permisos` VALUES ('47', '1', null, 'ver restaurante', '3');
INSERT INTO `permisos` VALUES ('51', '1', null, 'ver restaurant', '3');
INSERT INTO `permisos` VALUES ('53', '1', '20', 'efectuar reembolso', null);
INSERT INTO `permisos` VALUES ('54', '1', null, 'ver restaurante', '3');
INSERT INTO `permisos` VALUES ('55', '1', '25', 'ver urls permitidas', null);
INSERT INTO `permisos` VALUES ('56', '2', '23', 'editar permisos', null);
INSERT INTO `permisos` VALUES ('57', '3', '23', 'eliminar permisos', null);
INSERT INTO `permisos` VALUES ('58', '4', '23', 'guardar permisos', null);
INSERT INTO `permisos` VALUES ('63', '4', '17', 'guardar grupos', null);
INSERT INTO `permisos` VALUES ('64', '2', '17', 'agregar/editar grupos', null);
INSERT INTO `permisos` VALUES ('65', '3', '17', 'eliminar grupos', null);
INSERT INTO `permisos` VALUES ('66', '2', '1', 'agregar/editar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('67', '3', '1', 'eliminar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('68', '4', '1', 'guardar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('69', '2', '2', 'agregar/editar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('70', '3', '2', 'eliminar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('71', '4', '2', 'guardar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('76', '5', '16', 'ver permisos de usuario', null);
INSERT INTO `permisos` VALUES ('83', '2', '21', 'agregar/editar series', null);
INSERT INTO `permisos` VALUES ('84', '3', '21', 'eliminar series', null);
INSERT INTO `permisos` VALUES ('85', '4', '21', 'guardar series', null);
INSERT INTO `permisos` VALUES ('86', '2', '20', 'agregar/editar vouchers', null);
INSERT INTO `permisos` VALUES ('87', '4', '20', 'guardar vouchers', null);
INSERT INTO `permisos` VALUES ('88', '3', '20', 'eliminar vouchers', null);
INSERT INTO `permisos` VALUES ('89', '2', '19', 'agregar/editar formas de pago', null);
INSERT INTO `permisos` VALUES ('90', '3', '19', 'eliminar formas de pago', null);
INSERT INTO `permisos` VALUES ('91', '4', '19', 'guardar formas de pago', null);
INSERT INTO `permisos` VALUES ('92', '2', '12', 'agregar/editar tipos de concepto de pago', null);
INSERT INTO `permisos` VALUES ('93', '3', '12', 'elliminar tipos de concepto de pago', null);
INSERT INTO `permisos` VALUES ('94', '4', '12', 'guardar tipos de concepto de pago', null);
INSERT INTO `permisos` VALUES ('95', '2', '11', 'agregar/editar concepto de pago', null);
INSERT INTO `permisos` VALUES ('96', '3', '11', 'eliminar concepto de pago', null);
INSERT INTO `permisos` VALUES ('97', '4', '11', 'guardar conceptos de apgo', null);
INSERT INTO `permisos` VALUES ('106', '2', '16', 'editar usuarios', null);
INSERT INTO `permisos` VALUES ('110', '4', '16', 'guardar usuarios', null);
INSERT INTO `permisos` VALUES ('111', '5', '2', 'crear permisos submodulo', null);
INSERT INTO `permisos` VALUES ('112', '5', '1', 'crear permisos modulos', null);
INSERT INTO `permisos` VALUES ('152', '1', '28', ' mostrar  Meseros', null);
INSERT INTO `permisos` VALUES ('153', '2', '28', ' agregar/editar  Meseros', null);
INSERT INTO `permisos` VALUES ('154', '3', '28', ' eliminar  Meseros', null);
INSERT INTO `permisos` VALUES ('155', '4', '28', ' guardar  Meseros', null);
INSERT INTO `permisos` VALUES ('156', '1', '30', ' mostrar  clientes', null);
INSERT INTO `permisos` VALUES ('157', '2', '30', ' agregar/editar  clientes', null);
INSERT INTO `permisos` VALUES ('158', '3', '30', ' eliminar  clientes', null);
INSERT INTO `permisos` VALUES ('159', '4', '30', ' guardar  clientes', null);
INSERT INTO `permisos` VALUES ('160', '1', '31', ' mostrar  mesas', null);
INSERT INTO `permisos` VALUES ('161', '2', '31', ' agregar/editar  mesas', null);
INSERT INTO `permisos` VALUES ('162', '3', '31', ' eliminar  mesas', null);
INSERT INTO `permisos` VALUES ('163', '4', '31', ' guardar  mesas', null);
INSERT INTO `permisos` VALUES ('164', '1', '32', ' mostrar  ventas_mesas', null);
INSERT INTO `permisos` VALUES ('165', '2', '32', ' agregar/editar  ventas_mesas', null);
INSERT INTO `permisos` VALUES ('166', '3', '32', ' eliminar  ventas_mesas', null);
INSERT INTO `permisos` VALUES ('167', '4', '32', ' guardar  ventas_mesas', null);
INSERT INTO `permisos` VALUES ('180', '1', '3', ' mostrar  Tipos Productos', null);
INSERT INTO `permisos` VALUES ('181', '2', '3', ' agregar/editar  Tipos Productos', null);
INSERT INTO `permisos` VALUES ('182', '3', '3', ' eliminar  Tipos Productos', null);
INSERT INTO `permisos` VALUES ('183', '4', '3', ' guardar  Tipos Productos', null);
INSERT INTO `permisos` VALUES ('188', '1', '4', ' mostrar  Productos', null);
INSERT INTO `permisos` VALUES ('189', '2', '4', ' agregar/editar  Productos', null);
INSERT INTO `permisos` VALUES ('190', '3', '4', ' eliminar  Productos', null);
INSERT INTO `permisos` VALUES ('191', '4', '4', ' guardar  Productos', null);
INSERT INTO `permisos` VALUES ('192', '8', '4', ' ver detalle de  Productos', null);
INSERT INTO `permisos` VALUES ('193', '1', '34', ' mostrar  deliverista', null);
INSERT INTO `permisos` VALUES ('194', '2', '34', ' agregar/editar  deliverista', null);
INSERT INTO `permisos` VALUES ('195', '3', '34', ' eliminar  deliverista', null);
INSERT INTO `permisos` VALUES ('196', '4', '34', ' guardar  deliverista', null);
INSERT INTO `permisos` VALUES ('197', '8', '34', ' ver detalle de  deliverista', null);
INSERT INTO `permisos` VALUES ('198', '1', '5', ' mostrar  Delivery', null);
INSERT INTO `permisos` VALUES ('199', '2', '5', ' agregar/editar  Delivery', null);
INSERT INTO `permisos` VALUES ('200', '3', '5', ' eliminar  Delivery', null);
INSERT INTO `permisos` VALUES ('201', '4', '5', ' guardar  Delivery', null);
INSERT INTO `permisos` VALUES ('202', '8', '5', ' ver detalle de  Delivery', null);
INSERT INTO `permisos` VALUES ('203', '1', '37', ' mostrar  ventas-deliveries', null);
INSERT INTO `permisos` VALUES ('204', '2', '37', ' agregar/editar  ventas-deliveries', null);
INSERT INTO `permisos` VALUES ('205', '3', '37', ' eliminar  ventas-deliveries', null);
INSERT INTO `permisos` VALUES ('206', '4', '37', ' guardar  ventas-deliveries', null);
INSERT INTO `permisos` VALUES ('221', '1', '36', ' mostrar  producto_delivery', null);
INSERT INTO `permisos` VALUES ('222', '2', '36', ' agregar/editar  producto_delivery', null);
INSERT INTO `permisos` VALUES ('223', '3', '36', ' eliminar  producto_delivery', null);
INSERT INTO `permisos` VALUES ('224', '4', '36', ' guardar  producto_delivery', null);
INSERT INTO `permisos` VALUES ('225', '1', null, 'VER REPORTE', '7');
INSERT INTO `permisos` VALUES ('226', '1', '7', ' mostrar  Ventas', null);
INSERT INTO `permisos` VALUES ('227', '2', '7', ' agregar/editar  Ventas', null);
INSERT INTO `permisos` VALUES ('228', '3', '7', ' eliminar  Ventas', null);
INSERT INTO `permisos` VALUES ('229', '4', '7', ' guardar  Ventas', null);
INSERT INTO `permisos` VALUES ('230', '8', '7', ' ver detalle de  Ventas', null);
INSERT INTO `permisos` VALUES ('238', '1', '10', ' mostrar  Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('239', '3', '10', ' eliminar  Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('240', '4', '10', ' guardar  Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('241', '6', '10', 'aperturar Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('242', '7', '10', 'cerrar Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('243', '8', '10', ' ver detalle de  Aperturas y cierres de caja', null);
INSERT INTO `permisos` VALUES ('244', '1', '40', ' mostrar  caja', null);
INSERT INTO `permisos` VALUES ('245', '8', '40', ' ver detalle de  caja', null);
INSERT INTO `permisos` VALUES ('246', '1', '38', ' mostrar  pedido-productos', null);
INSERT INTO `permisos` VALUES ('247', '2', '38', ' agregar/editar  pedido-productos', null);
INSERT INTO `permisos` VALUES ('248', '3', '38', ' eliminar  pedido-productos', null);
INSERT INTO `permisos` VALUES ('249', '4', '38', ' guardar  pedido-productos', null);
INSERT INTO `permisos` VALUES ('250', '8', '38', ' ver detalle de  pedido-productos', null);
INSERT INTO `permisos` VALUES ('251', '9', '38', 'editar_precio pedido-productos', null);
INSERT INTO `permisos` VALUES ('255', '1', '41', ' mostrar  reportes', null);
INSERT INTO `permisos` VALUES ('256', '1', '42', ' mostrar  reportes delivery', null);
INSERT INTO `permisos` VALUES ('258', '2', '39', ' agregar/editar  ventas movimientos', null);
INSERT INTO `permisos` VALUES ('259', '3', '39', ' eliminar  ventas movimientos', null);
INSERT INTO `permisos` VALUES ('260', '4', '39', ' guardar  ventas movimientos', null);
INSERT INTO `permisos` VALUES ('261', '10', '39', 'transaccion ventas movimientos', null);
INSERT INTO `permisos` VALUES ('262', '1', '8', ' mostrar  Transacciones', null);
INSERT INTO `permisos` VALUES ('263', '2', '8', ' agregar/editar  Transacciones', null);
INSERT INTO `permisos` VALUES ('264', '3', '8', ' eliminar  Transacciones', null);
INSERT INTO `permisos` VALUES ('265', '4', '8', ' guardar  Transacciones', null);
INSERT INTO `permisos` VALUES ('266', '8', '8', ' ver detalle de  Transacciones', null);
INSERT INTO `permisos` VALUES ('267', '10', '8', 'transaccion Transacciones', null);

-- ----------------------------
-- Table structure for permisos_grupo
-- ----------------------------
DROP TABLE IF EXISTS `permisos_grupo`;
CREATE TABLE `permisos_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpermiso` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permisos_grupo_ibfk_1` (`idpermiso`),
  KEY `permisos_grupo_ibfk_2` (`idgrupo`),
  CONSTRAINT `permisos_grupo_ibfk_1` FOREIGN KEY (`idpermiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_grupo_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4863 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos_grupo
-- ----------------------------
INSERT INTO `permisos_grupo` VALUES ('4622', '1', '2');
INSERT INTO `permisos_grupo` VALUES ('4623', '7', '2');
INSERT INTO `permisos_grupo` VALUES ('4624', '66', '2');
INSERT INTO `permisos_grupo` VALUES ('4625', '67', '2');
INSERT INTO `permisos_grupo` VALUES ('4626', '68', '2');
INSERT INTO `permisos_grupo` VALUES ('4627', '112', '2');
INSERT INTO `permisos_grupo` VALUES ('4628', '8', '2');
INSERT INTO `permisos_grupo` VALUES ('4629', '69', '2');
INSERT INTO `permisos_grupo` VALUES ('4630', '70', '2');
INSERT INTO `permisos_grupo` VALUES ('4631', '71', '2');
INSERT INTO `permisos_grupo` VALUES ('4632', '111', '2');
INSERT INTO `permisos_grupo` VALUES ('4633', '2', '2');
INSERT INTO `permisos_grupo` VALUES ('4634', '3', '2');
INSERT INTO `permisos_grupo` VALUES ('4635', '4', '2');
INSERT INTO `permisos_grupo` VALUES ('4636', '5', '2');
INSERT INTO `permisos_grupo` VALUES ('4637', '6', '2');
INSERT INTO `permisos_grupo` VALUES ('4638', '56', '2');
INSERT INTO `permisos_grupo` VALUES ('4639', '57', '2');
INSERT INTO `permisos_grupo` VALUES ('4640', '58', '2');
INSERT INTO `permisos_grupo` VALUES ('4641', '55', '2');
INSERT INTO `permisos_grupo` VALUES ('4642', '10', '2');
INSERT INTO `permisos_grupo` VALUES ('4643', '198', '2');
INSERT INTO `permisos_grupo` VALUES ('4644', '199', '2');
INSERT INTO `permisos_grupo` VALUES ('4645', '200', '2');
INSERT INTO `permisos_grupo` VALUES ('4646', '201', '2');
INSERT INTO `permisos_grupo` VALUES ('4647', '202', '2');
INSERT INTO `permisos_grupo` VALUES ('4648', '25', '2');
INSERT INTO `permisos_grupo` VALUES ('4649', '226', '2');
INSERT INTO `permisos_grupo` VALUES ('4650', '227', '2');
INSERT INTO `permisos_grupo` VALUES ('4651', '228', '2');
INSERT INTO `permisos_grupo` VALUES ('4652', '229', '2');
INSERT INTO `permisos_grupo` VALUES ('4653', '230', '2');
INSERT INTO `permisos_grupo` VALUES ('4654', '30', '2');
INSERT INTO `permisos_grupo` VALUES ('4655', '262', '2');
INSERT INTO `permisos_grupo` VALUES ('4656', '263', '2');
INSERT INTO `permisos_grupo` VALUES ('4657', '264', '2');
INSERT INTO `permisos_grupo` VALUES ('4658', '265', '2');
INSERT INTO `permisos_grupo` VALUES ('4659', '266', '2');
INSERT INTO `permisos_grupo` VALUES ('4660', '267', '2');
INSERT INTO `permisos_grupo` VALUES ('4661', '238', '2');
INSERT INTO `permisos_grupo` VALUES ('4662', '239', '2');
INSERT INTO `permisos_grupo` VALUES ('4663', '240', '2');
INSERT INTO `permisos_grupo` VALUES ('4664', '241', '2');
INSERT INTO `permisos_grupo` VALUES ('4665', '242', '2');
INSERT INTO `permisos_grupo` VALUES ('4666', '243', '2');
INSERT INTO `permisos_grupo` VALUES ('4667', '34', '2');
INSERT INTO `permisos_grupo` VALUES ('4668', '95', '2');
INSERT INTO `permisos_grupo` VALUES ('4669', '96', '2');
INSERT INTO `permisos_grupo` VALUES ('4670', '97', '2');
INSERT INTO `permisos_grupo` VALUES ('4671', '244', '2');
INSERT INTO `permisos_grupo` VALUES ('4672', '245', '2');
INSERT INTO `permisos_grupo` VALUES ('4673', '41', '2');
INSERT INTO `permisos_grupo` VALUES ('4674', '180', '2');
INSERT INTO `permisos_grupo` VALUES ('4675', '181', '2');
INSERT INTO `permisos_grupo` VALUES ('4676', '182', '2');
INSERT INTO `permisos_grupo` VALUES ('4677', '183', '2');
INSERT INTO `permisos_grupo` VALUES ('4678', '188', '2');
INSERT INTO `permisos_grupo` VALUES ('4679', '189', '2');
INSERT INTO `permisos_grupo` VALUES ('4680', '190', '2');
INSERT INTO `permisos_grupo` VALUES ('4681', '191', '2');
INSERT INTO `permisos_grupo` VALUES ('4682', '192', '2');
INSERT INTO `permisos_grupo` VALUES ('4683', '35', '2');
INSERT INTO `permisos_grupo` VALUES ('4684', '92', '2');
INSERT INTO `permisos_grupo` VALUES ('4685', '93', '2');
INSERT INTO `permisos_grupo` VALUES ('4686', '94', '2');
INSERT INTO `permisos_grupo` VALUES ('4687', '42', '2');
INSERT INTO `permisos_grupo` VALUES ('4688', '76', '2');
INSERT INTO `permisos_grupo` VALUES ('4689', '106', '2');
INSERT INTO `permisos_grupo` VALUES ('4690', '110', '2');
INSERT INTO `permisos_grupo` VALUES ('4691', '43', '2');
INSERT INTO `permisos_grupo` VALUES ('4692', '63', '2');
INSERT INTO `permisos_grupo` VALUES ('4693', '64', '2');
INSERT INTO `permisos_grupo` VALUES ('4694', '65', '2');
INSERT INTO `permisos_grupo` VALUES ('4695', '44', '2');
INSERT INTO `permisos_grupo` VALUES ('4696', '89', '2');
INSERT INTO `permisos_grupo` VALUES ('4697', '90', '2');
INSERT INTO `permisos_grupo` VALUES ('4698', '91', '2');
INSERT INTO `permisos_grupo` VALUES ('4699', '45', '2');
INSERT INTO `permisos_grupo` VALUES ('4700', '86', '2');
INSERT INTO `permisos_grupo` VALUES ('4701', '87', '2');
INSERT INTO `permisos_grupo` VALUES ('4702', '88', '2');
INSERT INTO `permisos_grupo` VALUES ('4703', '46', '2');
INSERT INTO `permisos_grupo` VALUES ('4704', '83', '2');
INSERT INTO `permisos_grupo` VALUES ('4705', '84', '2');
INSERT INTO `permisos_grupo` VALUES ('4706', '85', '2');
INSERT INTO `permisos_grupo` VALUES ('4707', '152', '2');
INSERT INTO `permisos_grupo` VALUES ('4708', '153', '2');
INSERT INTO `permisos_grupo` VALUES ('4709', '154', '2');
INSERT INTO `permisos_grupo` VALUES ('4710', '155', '2');
INSERT INTO `permisos_grupo` VALUES ('4711', '156', '2');
INSERT INTO `permisos_grupo` VALUES ('4712', '157', '2');
INSERT INTO `permisos_grupo` VALUES ('4713', '158', '2');
INSERT INTO `permisos_grupo` VALUES ('4714', '159', '2');
INSERT INTO `permisos_grupo` VALUES ('4715', '160', '2');
INSERT INTO `permisos_grupo` VALUES ('4716', '161', '2');
INSERT INTO `permisos_grupo` VALUES ('4717', '162', '2');
INSERT INTO `permisos_grupo` VALUES ('4718', '163', '2');
INSERT INTO `permisos_grupo` VALUES ('4719', '193', '2');
INSERT INTO `permisos_grupo` VALUES ('4720', '194', '2');
INSERT INTO `permisos_grupo` VALUES ('4721', '195', '2');
INSERT INTO `permisos_grupo` VALUES ('4722', '196', '2');
INSERT INTO `permisos_grupo` VALUES ('4723', '197', '2');
INSERT INTO `permisos_grupo` VALUES ('4724', '225', '2');
INSERT INTO `permisos_grupo` VALUES ('4725', '255', '2');
INSERT INTO `permisos_grupo` VALUES ('4726', '256', '2');
INSERT INTO `permisos_grupo` VALUES ('4727', '164', '2');
INSERT INTO `permisos_grupo` VALUES ('4728', '165', '2');
INSERT INTO `permisos_grupo` VALUES ('4729', '166', '2');
INSERT INTO `permisos_grupo` VALUES ('4730', '167', '2');
INSERT INTO `permisos_grupo` VALUES ('4731', '221', '2');
INSERT INTO `permisos_grupo` VALUES ('4732', '222', '2');
INSERT INTO `permisos_grupo` VALUES ('4733', '223', '2');
INSERT INTO `permisos_grupo` VALUES ('4734', '224', '2');
INSERT INTO `permisos_grupo` VALUES ('4735', '203', '2');
INSERT INTO `permisos_grupo` VALUES ('4736', '204', '2');
INSERT INTO `permisos_grupo` VALUES ('4737', '205', '2');
INSERT INTO `permisos_grupo` VALUES ('4738', '206', '2');
INSERT INTO `permisos_grupo` VALUES ('4739', '246', '2');
INSERT INTO `permisos_grupo` VALUES ('4740', '247', '2');
INSERT INTO `permisos_grupo` VALUES ('4741', '248', '2');
INSERT INTO `permisos_grupo` VALUES ('4742', '249', '2');
INSERT INTO `permisos_grupo` VALUES ('4743', '250', '2');
INSERT INTO `permisos_grupo` VALUES ('4744', '258', '2');
INSERT INTO `permisos_grupo` VALUES ('4745', '259', '2');
INSERT INTO `permisos_grupo` VALUES ('4746', '260', '2');
INSERT INTO `permisos_grupo` VALUES ('4747', '261', '2');
INSERT INTO `permisos_grupo` VALUES ('4785', '10', '3');
INSERT INTO `permisos_grupo` VALUES ('4786', '198', '3');
INSERT INTO `permisos_grupo` VALUES ('4787', '200', '3');
INSERT INTO `permisos_grupo` VALUES ('4788', '201', '3');
INSERT INTO `permisos_grupo` VALUES ('4789', '202', '3');
INSERT INTO `permisos_grupo` VALUES ('4790', '25', '3');
INSERT INTO `permisos_grupo` VALUES ('4791', '226', '3');
INSERT INTO `permisos_grupo` VALUES ('4792', '228', '3');
INSERT INTO `permisos_grupo` VALUES ('4793', '230', '3');
INSERT INTO `permisos_grupo` VALUES ('4794', '30', '3');
INSERT INTO `permisos_grupo` VALUES ('4795', '262', '3');
INSERT INTO `permisos_grupo` VALUES ('4796', '263', '3');
INSERT INTO `permisos_grupo` VALUES ('4797', '264', '3');
INSERT INTO `permisos_grupo` VALUES ('4798', '265', '3');
INSERT INTO `permisos_grupo` VALUES ('4799', '266', '3');
INSERT INTO `permisos_grupo` VALUES ('4800', '267', '3');
INSERT INTO `permisos_grupo` VALUES ('4801', '41', '3');
INSERT INTO `permisos_grupo` VALUES ('4802', '180', '3');
INSERT INTO `permisos_grupo` VALUES ('4803', '181', '3');
INSERT INTO `permisos_grupo` VALUES ('4804', '182', '3');
INSERT INTO `permisos_grupo` VALUES ('4805', '183', '3');
INSERT INTO `permisos_grupo` VALUES ('4806', '188', '3');
INSERT INTO `permisos_grupo` VALUES ('4807', '189', '3');
INSERT INTO `permisos_grupo` VALUES ('4808', '190', '3');
INSERT INTO `permisos_grupo` VALUES ('4809', '191', '3');
INSERT INTO `permisos_grupo` VALUES ('4810', '35', '3');
INSERT INTO `permisos_grupo` VALUES ('4811', '92', '3');
INSERT INTO `permisos_grupo` VALUES ('4812', '93', '3');
INSERT INTO `permisos_grupo` VALUES ('4813', '94', '3');
INSERT INTO `permisos_grupo` VALUES ('4814', '44', '3');
INSERT INTO `permisos_grupo` VALUES ('4815', '89', '3');
INSERT INTO `permisos_grupo` VALUES ('4816', '90', '3');
INSERT INTO `permisos_grupo` VALUES ('4817', '91', '3');
INSERT INTO `permisos_grupo` VALUES ('4818', '45', '3');
INSERT INTO `permisos_grupo` VALUES ('4819', '53', '3');
INSERT INTO `permisos_grupo` VALUES ('4820', '86', '3');
INSERT INTO `permisos_grupo` VALUES ('4821', '87', '3');
INSERT INTO `permisos_grupo` VALUES ('4822', '88', '3');
INSERT INTO `permisos_grupo` VALUES ('4823', '46', '3');
INSERT INTO `permisos_grupo` VALUES ('4824', '83', '3');
INSERT INTO `permisos_grupo` VALUES ('4825', '84', '3');
INSERT INTO `permisos_grupo` VALUES ('4826', '85', '3');
INSERT INTO `permisos_grupo` VALUES ('4827', '152', '3');
INSERT INTO `permisos_grupo` VALUES ('4828', '153', '3');
INSERT INTO `permisos_grupo` VALUES ('4829', '154', '3');
INSERT INTO `permisos_grupo` VALUES ('4830', '155', '3');
INSERT INTO `permisos_grupo` VALUES ('4831', '156', '3');
INSERT INTO `permisos_grupo` VALUES ('4832', '157', '3');
INSERT INTO `permisos_grupo` VALUES ('4833', '158', '3');
INSERT INTO `permisos_grupo` VALUES ('4834', '159', '3');
INSERT INTO `permisos_grupo` VALUES ('4835', '193', '3');
INSERT INTO `permisos_grupo` VALUES ('4836', '195', '3');
INSERT INTO `permisos_grupo` VALUES ('4837', '196', '3');
INSERT INTO `permisos_grupo` VALUES ('4838', '197', '3');
INSERT INTO `permisos_grupo` VALUES ('4839', '225', '3');
INSERT INTO `permisos_grupo` VALUES ('4840', '255', '3');
INSERT INTO `permisos_grupo` VALUES ('4841', '256', '3');
INSERT INTO `permisos_grupo` VALUES ('4842', '164', '3');
INSERT INTO `permisos_grupo` VALUES ('4843', '165', '3');
INSERT INTO `permisos_grupo` VALUES ('4844', '166', '3');
INSERT INTO `permisos_grupo` VALUES ('4845', '167', '3');
INSERT INTO `permisos_grupo` VALUES ('4846', '221', '3');
INSERT INTO `permisos_grupo` VALUES ('4847', '222', '3');
INSERT INTO `permisos_grupo` VALUES ('4848', '223', '3');
INSERT INTO `permisos_grupo` VALUES ('4849', '224', '3');
INSERT INTO `permisos_grupo` VALUES ('4850', '203', '3');
INSERT INTO `permisos_grupo` VALUES ('4851', '204', '3');
INSERT INTO `permisos_grupo` VALUES ('4852', '205', '3');
INSERT INTO `permisos_grupo` VALUES ('4853', '206', '3');
INSERT INTO `permisos_grupo` VALUES ('4854', '246', '3');
INSERT INTO `permisos_grupo` VALUES ('4855', '247', '3');
INSERT INTO `permisos_grupo` VALUES ('4856', '248', '3');
INSERT INTO `permisos_grupo` VALUES ('4857', '249', '3');
INSERT INTO `permisos_grupo` VALUES ('4858', '250', '3');
INSERT INTO `permisos_grupo` VALUES ('4859', '258', '3');
INSERT INTO `permisos_grupo` VALUES ('4860', '259', '3');
INSERT INTO `permisos_grupo` VALUES ('4861', '260', '3');
INSERT INTO `permisos_grupo` VALUES ('4862', '261', '3');

-- ----------------------------
-- Table structure for permisos_usuario
-- ----------------------------
DROP TABLE IF EXISTS `permisos_usuario`;
CREATE TABLE `permisos_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL,
  `idpermiso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permisos_usuario_ibfk_1` (`idpermiso`),
  KEY `permisos_usuario_ibfk_2` (`idusuario`),
  CONSTRAINT `permisos_usuario_ibfk_1` FOREIGN KEY (`idpermiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_usuario_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos_usuario
-- ----------------------------
INSERT INTO `permisos_usuario` VALUES ('22', '7', '30');
INSERT INTO `permisos_usuario` VALUES ('23', '12', '30');
INSERT INTO `permisos_usuario` VALUES ('36', '11', '10');
INSERT INTO `permisos_usuario` VALUES ('37', '11', '226');
INSERT INTO `permisos_usuario` VALUES ('38', '11', '30');

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `precio` float NOT NULL,
  `tipo_producto_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_producto_id` (`tipo_producto_id`),
  CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`tipo_producto_id`) REFERENCES `tipos_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of productos
-- ----------------------------
INSERT INTO `productos` VALUES ('1', 'inchicapi', '10', '6', '2017-10-23 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('2', 'tacacho con cecina', '10', '7', '2017-10-23 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('3', 'ceviche', '10', '7', '2017-10-23 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('4', 'arroz ala cubana', '12', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('14', 'producto 10', '12', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('15', 'producto 12', '12', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('16', 'papa a huacaina', '15', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('17', 'PorciÃ³n  de patacones', '5', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('18', 'PorciÃ³n  de arroz', '5', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('19', 'PorciÃ³n  de ajÃ­ ', '4', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('20', 'Pollo al horno', '10', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('21', 'Patacones con  paiche', '20', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `productos` VALUES ('22', 'Gaseosa Coca cola  1L.', '5', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for producto_delivery
-- ----------------------------
DROP TABLE IF EXISTS `producto_delivery`;
CREATE TABLE `producto_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '1',
  `total` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `producto_id` (`producto_id`),
  KEY `delivery_id` (`delivery_id`),
  CONSTRAINT `producto_delivery_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `producto_delivery_ibfk_2` FOREIGN KEY (`delivery_id`) REFERENCES `deliveries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of producto_delivery
-- ----------------------------
INSERT INTO `producto_delivery` VALUES ('181', '3', '82', '1', '10', null, null, null, '10.00');
INSERT INTO `producto_delivery` VALUES ('182', '15', '82', '1', '12', null, null, null, '12.00');
INSERT INTO `producto_delivery` VALUES ('189', '3', '86', '1', '10', null, null, null, '10.00');
INSERT INTO `producto_delivery` VALUES ('190', '15', '86', '1', '12', null, null, null, '12.00');

-- ----------------------------
-- Table structure for series
-- ----------------------------
DROP TABLE IF EXISTS `series`;
CREATE TABLE `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serie` int(11) NOT NULL,
  `correlativo` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `voucher_id` (`voucher_id`),
  CONSTRAINT `series_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of series
-- ----------------------------
INSERT INTO `series` VALUES ('1', '1', '2', '1', '2017-10-24 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for submodulo
-- ----------------------------
DROP TABLE IF EXISTS `submodulo`;
CREATE TABLE `submodulo` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idmodulo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_mod` (`idmodulo`),
  CONSTRAINT `submodulo_ibfk_1` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of submodulo
-- ----------------------------
INSERT INTO `submodulo` VALUES ('1', 'MÃ“DULOS', 'modulos', 'icon-puzzle', '1');
INSERT INTO `submodulo` VALUES ('2', 'Sub MÃ³dulos', 'submodulos', 'icon-puzzle', '1');
INSERT INTO `submodulo` VALUES ('3', 'Tipos Productos', 'tipos-productos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('4', 'Productos', 'productos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('5', 'Delivery', 'delivery', 'icon-puzzle', '3');
INSERT INTO `submodulo` VALUES ('6', 'Clientes', 'clientes', 'icon-puzzle', '3');
INSERT INTO `submodulo` VALUES ('7', 'Ventas', 'ventas', 'icon-puzzle', '3');
INSERT INTO `submodulo` VALUES ('8', 'Transacciones', 'movimientos-de-dineros', 'icon-puzzle', '4');
INSERT INTO `submodulo` VALUES ('10', 'Historial de Cajas', 'cajas', 'icon-puzzle', '4');
INSERT INTO `submodulo` VALUES ('11', 'Operaciones', 'conceptos-de-pagos', 'icon-puzzle', '4');
INSERT INTO `submodulo` VALUES ('12', 'Tipos  concepto de pago', 'tipos-concepto-pagos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('16', 'Usuarios', 'usuarios', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('17', 'Grupos', 'grupos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('18', 'Permisos', 'permisos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('19', 'Formas de pago', 'formas-pagos', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('20', 'Vouchers', 'vouchers', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('21', 'Series', 'sery', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('22', 'acciones', 'acciones', 'icon-puzzle', '1');
INSERT INTO `submodulo` VALUES ('23', 'permisos', 'permisos', 'icon-puzzle', '1');
INSERT INTO `submodulo` VALUES ('25', 'urls permitidas', 'urls-permitidas', 'icon-puzzle', '1');
INSERT INTO `submodulo` VALUES ('28', 'Meseros', 'meseros', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('30', 'clientes', 'clientes', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('31', 'mesas', 'mesas', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('32', 'ventas_mesas', 'ventas_mesas', 'icon-puzzle', '8');
INSERT INTO `submodulo` VALUES ('34', 'deliverista', 'deliveristas', 'icon-puzzle', '6');
INSERT INTO `submodulo` VALUES ('36', 'producto_delivery', 'producto-deliveries', 'Ã±sdjfbeuir', '8');
INSERT INTO `submodulo` VALUES ('37', 'ventas-deliveries', 'ventas-deliveries', 'icon-puzzle', '8');
INSERT INTO `submodulo` VALUES ('38', 'pedido-productos', 'pedido-productos', 'icon-puzzle', '8');
INSERT INTO `submodulo` VALUES ('39', 'ventas movimientos', 'ventas-movimientos', 'icon-puzzle', '8');
INSERT INTO `submodulo` VALUES ('40', 'Caja', 'cajas/show', 'icon-puzzle', '4');
INSERT INTO `submodulo` VALUES ('41', 'reportes ventas', 'reportes', 'icon-puzzle', '7');
INSERT INTO `submodulo` VALUES ('42', 'reportes delivery', 'reportedelivery', 'icon-puzzle', '7');

-- ----------------------------
-- Table structure for tipos_concepto_pago
-- ----------------------------
DROP TABLE IF EXISTS `tipos_concepto_pago`;
CREATE TABLE `tipos_concepto_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipos_concepto_pago
-- ----------------------------
INSERT INTO `tipos_concepto_pago` VALUES ('1', 'ingreso', '2017-10-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tipos_concepto_pago` VALUES ('2', 'egreso', '2017-10-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for tipos_producto
-- ----------------------------
DROP TABLE IF EXISTS `tipos_producto`;
CREATE TABLE `tipos_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipos_producto
-- ----------------------------
INSERT INTO `tipos_producto` VALUES ('6', 'plato tipico', 'de la selva', '2017-10-23 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tipos_producto` VALUES ('7', 'mariscos', 'plato costeÃ±o\r\n\r\n\r\n', '2017-10-23 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tipos_producto` VALUES ('10', 'Extras', 'comidas extras\r\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for urls_permitidas
-- ----------------------------
DROP TABLE IF EXISTS `urls_permitidas`;
CREATE TABLE `urls_permitidas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of urls_permitidas
-- ----------------------------
INSERT INTO `urls_permitidas` VALUES ('1', 'generator');
INSERT INTO `urls_permitidas` VALUES ('2', 'admin');
INSERT INTO `urls_permitidas` VALUES ('3', 'index');
INSERT INTO `urls_permitidas` VALUES ('4', 'logout');
INSERT INTO `urls_permitidas` VALUES ('5', 'rest');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_salida` date NOT NULL,
  `esta_activo` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('4', 'hali', 'huaccha', '123', '0000-00-00', '0000-00-00', '', 'halisarid@gmail.com', '123123', 'allame si puedes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('7', 'hali', 'chiclayo', '1234', '0000-00-00', '0000-00-00', '', 'halisarid@gmail.com', '12334556', 'weret', '2017-10-17 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('9', 'yordan', 'cassanova', '123456', '0000-00-00', '0000-00-00', '', 'yordan@gmail.com', '3456789', '345hy', '2017-10-19 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('10', 'kevin', 'ochavan', '12345', '0000-00-00', '0000-00-00', '', 'kevin@gmail.com', '984756473', 'lima', '2017-10-24 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('11', 'Ever', 'Vasquez', '123456', '0000-00-00', '0000-00-00', '', 'ever@gmail.com', '985435181', 'tupac', '2017-10-24 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('12', 'jordan', 'casanova', '123', '0000-00-00', '0000-00-00', '', 'jordan@admin.com', '12939', 'askd', '2017-11-07 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('13', 'pancracio', 'pancracio', '1', '0000-00-00', '0000-00-00', '', 'pancracio@gmail.com', '10230', '123', '2017-12-05 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for usuarios_grupo
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_grupo`;
CREATE TABLE `usuarios_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  KEY `grupo_id` (`grupo_id`),
  CONSTRAINT `usuarios_grupo_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarios_grupo_ibfk_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuarios_grupo
-- ----------------------------
INSERT INTO `usuarios_grupo` VALUES ('5', '4', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios_grupo` VALUES ('24', '7', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios_grupo` VALUES ('27', '10', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios_grupo` VALUES ('30', '12', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios_grupo` VALUES ('31', '13', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for ventas
-- ----------------------------
DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesero_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `estado_venta` bit(1) NOT NULL DEFAULT b'1',
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mesero_id` (`mesero_id`),
  KEY `cliente_id` (`cliente_id`),
  CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`mesero_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ventas
-- ----------------------------
INSERT INTO `ventas` VALUES ('177', '4', null, '2017-12-05 11:12:17', '2017-12-05 11:12:20', null, '\0', '2017-12-05 11:12:17');
INSERT INTO `ventas` VALUES ('179', '4', null, '2017-12-05 12:12:19', '2017-12-05 12:12:23', null, '\0', '2017-12-05 12:12:19');
INSERT INTO `ventas` VALUES ('180', '13', null, '2017-12-05 12:12:26', '2017-12-05 12:12:30', null, '\0', '2017-12-05 12:12:26');
INSERT INTO `ventas` VALUES ('186', '4', null, '2017-12-05 12:12:12', '2017-12-05 12:12:18', null, '\0', '2017-12-05 12:12:12');

-- ----------------------------
-- Table structure for ventas_delivery
-- ----------------------------
DROP TABLE IF EXISTS `ventas_delivery`;
CREATE TABLE `ventas_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movimiento_id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `fecha` datetime NOT NULL,
  `descuento` float DEFAULT NULL,
  `porcentaje` int(11) DEFAULT NULL,
  `total` float NOT NULL,
  `efectivo` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `movimiento_id` (`movimiento_id`),
  KEY `delivery` (`delivery_id`),
  KEY `ventas_delivery_ibfk_3` (`cliente_id`),
  CONSTRAINT `ventas_delivery_ibfk_1` FOREIGN KEY (`movimiento_id`) REFERENCES `movimientos_de_dinero` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_delivery_ibfk_2` FOREIGN KEY (`delivery_id`) REFERENCES `deliveries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_delivery_ibfk_3` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ventas_delivery
-- ----------------------------
INSERT INTO `ventas_delivery` VALUES ('31', '217', '82', '22', '2017-12-05 00:00:00', '0', null, '22', '22', '16');
INSERT INTO `ventas_delivery` VALUES ('35', '223', '86', '22', '2017-12-05 00:00:00', '2', null, '24', '50', '19');

-- ----------------------------
-- Table structure for ventas_mesas
-- ----------------------------
DROP TABLE IF EXISTS `ventas_mesas`;
CREATE TABLE `ventas_mesas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venta_id` int(11) NOT NULL,
  `mesa_id` int(11) NOT NULL,
  `estado_venta_mesa` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `pedido_id` (`venta_id`),
  KEY `mesas_id` (`mesa_id`),
  CONSTRAINT `ventas_mesas_ibfk_1` FOREIGN KEY (`mesa_id`) REFERENCES `mesas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_mesas_ibfk_2` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ventas_mesas
-- ----------------------------
INSERT INTO `ventas_mesas` VALUES ('192', '177', '29', '\0');
INSERT INTO `ventas_mesas` VALUES ('194', '179', '21', '\0');
INSERT INTO `ventas_mesas` VALUES ('195', '180', '21', '\0');
INSERT INTO `ventas_mesas` VALUES ('201', '186', '22', '\0');

-- ----------------------------
-- Table structure for ventas_movimiento
-- ----------------------------
DROP TABLE IF EXISTS `ventas_movimiento`;
CREATE TABLE `ventas_movimiento` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `venta_id` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `movimiento_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `descuento` float DEFAULT NULL,
  `porcentaje` int(11) DEFAULT NULL,
  `total` float NOT NULL,
  `efectivo` int(11) DEFAULT '0',
  `resta` decimal(8,2) DEFAULT NULL,
  `cuota` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venta_id` (`venta_id`),
  KEY `movimiento_de_dinero_id` (`movimiento_id`),
  KEY `cliente_id` (`cliente_id`),
  CONSTRAINT `ventas_movimiento_ibfk_2` FOREIGN KEY (`movimiento_id`) REFERENCES `movimientos_de_dinero` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_movimiento_ibfk_3` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_movimiento_ibfk_4` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ventas_movimiento
-- ----------------------------
INSERT INTO `ventas_movimiento` VALUES ('6', '177', '15', '216', '2017-12-05 00:00:00', '16', '0', null, '15', '0', null, '5.00');
INSERT INTO `ventas_movimiento` VALUES ('8', '179', '22', '219', '2017-12-05 00:00:00', '17', '2', null, '20', '22', '-2.00', '22.00');
INSERT INTO `ventas_movimiento` VALUES ('9', '180', '20', '224', '2017-12-05 00:00:00', '16', '0', null, '20', '0', null, '20.00');
INSERT INTO `ventas_movimiento` VALUES ('13', '186', '22', '228', '2017-12-05 00:00:00', '16', '0', null, '22', '50', '-30.00', '50.00');

-- ----------------------------
-- Table structure for vouchers
-- ----------------------------
DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_comprobante` varchar(40) NOT NULL,
  `fecha` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vouchers
-- ----------------------------
INSERT INTO `vouchers` VALUES ('1', 'Boleta', '0000-00-00', '2017-10-17 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `vouchers` VALUES ('2', 'factura', '0000-00-00', '2017-10-17 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
