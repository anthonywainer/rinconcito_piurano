<?php

use Dabl\Controller\Controller;
use Dabl\Controller\ControllerRoute;
use Dabl\StringFormat\StringFormat;

abstract class ApplicationController extends Controller {

	function __construct(ControllerRoute $route = null) {
		parent::__construct($route);

        if(isset($_SESSION['user'])) { #preguntamos si el usuario esta logueado
            $idu = $_SESSION['user']->id; #id del usuario logueado

            $idg = UsuariosGrupo::getAll('WHERE usuario_id = ' . $idu)[0]->grupo_id;#extraemos el id grupo del usuario
            $_SESSION['id_grupo'] = $idg; #guardamos el id_grupo en la sessión

            $select = "SELECT modulos.nombre, modulos.url, modulos.icon, modulos.id ";

            $sql = $select."
                    FROM permisos_grupo 
                    INNER JOIN permisos 
                    ON permisos_grupo.idpermiso = permisos.id 
                    INNER JOIN modulos ON permisos.idmodulo = modulos.id 
                    WHERE permisos_grupo.idgrupo = " . $idg; #consultamos todos los módulos y sus permisos por grupo

            $sqlu = $select."
                     FROM permisos INNER JOIN modulos ON permisos.idmodulo = modulos.id 
                     INNER JOIN permisos_usuario 
                     ON permisos_usuario.idpermiso = permisos.id 
                     WHERE permisos_usuario.idusuario = ".$idu; #consultamos todos los módulos y sus permisos por usuario

            $result = DBManager::getConnection()->query($sql." UNION ALL ".$sqlu);#unimos todos los permisos

            $select = "SELECT submodulo.id, submodulo.nombre, submodulo.url, submodulo.icon, submodulo.idmodulo ";
            $sql =  $select."
                    FROM permisos_grupo 
                    INNER JOIN permisos 
                    ON permisos_grupo.idpermiso = permisos.id 
                    INNER JOIN submodulo 
                    ON permisos.idsubmodulo = submodulo.id 
                    WHERE permisos_grupo.idgrupo = " . $idg." 
                    group by submodulo.nombre"; #consultamos todos los submódulos y sus permisos por grupo

            $sqlu = $select." 
                     FROM permisos 
                     INNER JOIN submodulo 
                     ON permisos.idsubmodulo = submodulo.id 
                     INNER JOIN permisos_usuario 
                     ON permisos_usuario.idpermiso = permisos.id 
                     WHERE permisos_usuario.idusuario = ".$idu." 
                     group by submodulo.nombre"; #consultamos todos los submódulos y sus permisos por usuario

            $result2 = DBManager::getConnection()->query($sql." UNION ALL ".$sqlu);#unimos todos los permisos

            $this['modulosPrincipal'] = $result->fetchAll();
            $this['SubmodulosPrincipal'] = $result2->fetchAll();
        }
		$this['title'] = 'Rinconcito Piurano';

		$this['actions'] = array(
			'Home' => site_url()
		);

		$current_controller = str_replace('Controller', '', get_class($this));

		if ('Index' == $current_controller) {
			$this['current_page'] = 'Home';
		} else {
			$this['current_page'] = StringFormat::titleCase($current_controller, ' ');
		}
		foreach (glob(CONTROLLERS_DIR . '*.php') as $controller_file) {
			$controller = str_replace('Controller.php', '', basename($controller_file));
			if ($controller == 'Application' || $controller == 'Index') {
				continue;
			}
			$this['actions'][StringFormat::titleCase($controller, ' ')] = site_url(StringFormat::url($controller));
		}
	}
    public function check_permissions($route){ #función para comprobar permisos
        $idg = $_SESSION['id_grupo']; #obtenemos el idgrupo de la sesión activa
        $idu = $_SESSION['user']->id; #obtenemos el idusuario de la sesión activa
        $select = "SELECT submodulo.url, acciones.accion";
        $from = " FROM  permisos_grupo 
                  INNER JOIN permisos 
                  ON permisos_grupo.idpermiso = permisos.id 
                  INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id 
                  INNER JOIN acciones ON permisos.idaccion = acciones.id 
                  WHERE permisos_grupo.idgrupo = " . $idg;#obtener las urls y acciones por módulos

        $fromu = " FROM permisos INNER JOIN submodulo 
                   ON permisos.idsubmodulo = submodulo.id 
                   INNER JOIN acciones 
                   ON permisos.idaccion = acciones.id 
                   INNER JOIN permisos_usuario 
                   ON permisos_usuario.idpermiso = permisos.id  
                   WHERE permisos_usuario.idusuario = ".$idu;#obtener las urls y acciones por submódulos

        $sql = $select.$from;
        $sqlu = $select.$fromu;
        $result2 = DBManager::getConnection()->query($sql.' UNION ALL '.$sqlu);#unimos las consultas
        $urls = $result2->fetchAll();


        # $route[0] => es la acción capturada a ejecutar ejemplo "/seguridad/mostrar" $route[0]= "/seguridad/mostrar"
        if (isset($route[0])) {#comprobamos que no este vacía
            $ro = $route[0];
        } else {
            $ro = 'index';
        }

        $up= UrlsPermitidas::getAll();#consultamos las urls permitidas
        $nup = [];#creamos un nuevo array que contendrá a las urls permitidas
        foreach ($up as $u){
            array_push($nup, $u->url);
        }

        if (isset($up)) { #preguntamos si la variable $up no se encuentra vacía

            if (in_array($ro, $nup) == 1) {#preguntamos si la url esta dentro de las urls permitidas
                return true;

            } else {
                if (in_array($ro, array_column($urls, 'url')) == 1) {
                    #preguntamos si la url se encuentra dentro de la consulta

                    $sql2 = $sql . ' and url = "' . $ro . '"';#lo validamos para preguntar por la acción
                    $result2 = DBManager::getConnection()->query($sql2);
                    $urls2 = $result2->fetchAll();

                # $route[1] => es la acción capturada a ejecutar ejemplo "/seguridad/mostrar" $route[1]= "mostrar"
                    if (isset($route[1])) {  #comprobamos si la accion existe
                        $rr = $route[1];
                    } else {
                        $rr = 'index';
                    }
                    if (in_array($rr, array_column($urls2, 'accion')) == 1) {
                        #comprobar que la accion este dentro
                        return true;
                    }
                }
            }
        }
        return false;
    }

	public function doAction($action_name = null, $params = array()) {

		if ($this->outputFormat != 'html') {
			unset($this['title'], $this['current_page'], $this['actions']);

		}

        if((isset($_SESSION['user']))) { #preguntamos si l session esta activa

            if ($this->check_permissions($this->route->getSegments())) { #enviamos los datos para validar
                # ::return al sistema
            } else {
                http_response_code(404);
                return $this->loadView("error404"); #retornamos error 404
            }
        }

        if (in_array($this->outputFormat, array('json', 'jsonp', 'xml'), true)) {
            try {
                return parent::doAction($action_name, $params);
            } catch (Exception $e) {
                error_log($e);
                $this['errors'][] = $e->getMessage();
                if (!$this->loadView) {
                    return;
                }
                $this->loadView('');
            }
        } else {
            return parent::doAction($action_name, $params);
        }

	}

}