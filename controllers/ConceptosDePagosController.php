<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ConceptosDePagosController extends ApplicationController {

	/**
	 * Returns all ConceptosDePago records matching the query. Examples:
	 * GET /conceptos-de-pagos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/conceptos-de-pagos.json&limit=5
	 *
	 * @return ConceptosDePago[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }else{
            $ca = Cajas::getAll('where estado= true');
            if (!count($ca)>0){
                $this->flash['errors'][] = "Falta Aperturar Caja";
                return $this->redirect('cajas/show');
            }
        }
    }

	function index() {
		$q = ConceptosDePago::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 5 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ConceptosDePago';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		 $this['conceptos_de_pagos'] = $this['pager']->fetchPage();
        $this['u'] = 'conceptos-de-pagos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a ConceptosDePago. Example:
	 * GET /conceptos-de-pagos/edit/1
	 *
	 * @return ConceptosDePago
	 */
	function edit($id = null) {
        $ca = Cajas::getAll('where estado= true');
        $this['max'] = $ca[0]->getMontoAbierto();
		return $this->getConceptosDePago($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a ConceptosDePago. Examples:
	 * POST /conceptos-de-pagos/save/1
	 * POST /rest/conceptos-de-pagos/.json
	 * PUT /rest/conceptos-de-pagos/1.json
	 */
	function save($id = null) {
		$conceptos_de_pago = $this->getConceptosDePago($id);
        $fecha_actual = date("Y-m-d");
		if ($id){
            $movimientos_de_dinero = MovimientosDeDinero::retrieveByPK($_REQUEST['movimiento_id']);
            $_REQUEST["updated_at"] = $fecha_actual;
        }else{
            $movimientos_de_dinero = new MovimientosDeDinero;
            $_REQUEST["created_at"] = $fecha_actual;
        }
        $ca = Cajas::getAll('where estado= true');
        $_REQUEST['forma_pago_id']    = 12;
        $_REQUEST['caja_id']    = $ca[0]->id;
        $_REQUEST['usuario_id'] = $_SESSION['user']->id;


        $movimientos_de_dinero->fromArray($_REQUEST);
        $movimientos_de_dinero->save();
        $_REQUEST['movimiento_id'] = $movimientos_de_dinero->getId();

		try {
			$conceptos_de_pago->fromArray($_REQUEST);
			if ($conceptos_de_pago->validate()) {
				$conceptos_de_pago->save();
				$this->flash['messages'][] = 'Conceptos de Pago guardado';
				$this->redirect('conceptos-de-pagos/');
			}
			$this->flash['errors'] = $conceptos_de_pago->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('conceptos-de-pagos/edit/' . $conceptos_de_pago->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ConceptosDePago with the id. Examples:
	 * GET /conceptos-de-pagos/show/1o
	 * GET /rest/conceptos-de-pagos/1.json
	 *
	 * @return ConceptosDePago
	 */
	function show($id = null) {
		return $this->getConceptosDePago($id);
	}

	/**
	 * Deletes the ConceptosDePago with the id. Examples:
	 * GET /conceptos-de-pagos/delete/1
	 * DELETE /rest/conceptos-de-pagos/1.json
	 */
	function delete($id = null) {
		$conceptos_de_pago = $this->getConceptosDePago($id);

		try {
			if (null !== $conceptos_de_pago && $conceptos_de_pago->delete()) {
				$this['messages'][] = 'Conceptos De Pago deleted';
			} else {
				$this['errors'][] = 'Conceptos De Pago could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('conceptos-de-pagos');
		}
	}

	/**
	 * @return ConceptosDePago
	 */
	private function getConceptosDePago($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ConceptosDePago::getPrimaryKey()])) {
			$id = $_REQUEST[ConceptosDePago::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ConceptosDePago
			$this['conceptos_de_pago'] = new ConceptosDePago;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['conceptos_de_pago'] = ConceptosDePago::retrieveByPK($id);
		}
		return $this['conceptos_de_pago'];
	}

}