<?php

use Dabl\Query\QueryPager;

class CajasController extends ApplicationController {

	/**
	 * Returns all Cajas records matching the query. Examples:
	 * GET /cajas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/cajas.json&limit=5
	 *
	 * @return Cajas[]
	 */
	function index() {
        $_GET['order_by']= 'cajas.id';
        $_GET['dir'] = 'DESC';
		$q = Cajas::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Cajas';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['cajas'] = $this['pager']->fetchPage();
        $this['u'] = 'cajas/index';
        $ca = Cajas::getAll('where estado= true');
        $this['estado'] = $ca;
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Cajas. Example:
	 * GET /cajas/edit/1
	 *
	 * @return Cajas
	 */
	function aperturar($id = null) {
	    $ca = Cajas::getAll('where estado= true');
	    if($ca) {
	        if(!isset($_REQUEST['id'])){
                $this['errors'] = "hay caja aperturada";
                $this->loadView('cajas/error_caja');
            }else{
                $cc = Cajas::getAll('where id='.$_REQUEST['id']);
                $this['monto_ap'] = $cc[0]->getMonto_abierto();
                $this->getCajas($id)->fromArray(@$_GET);
                $this['u'] = 'cajas/edit';
            }
        }else{
            $ca = Cajas::getAll('ORDER BY id DESC');
            $this['monto_ap']= $ca[0]->getMonto_cerrado();
            $this->getCajas($id)->fromArray(@$_GET);
            $this['u'] = 'cajas/edit';
        }
        $this['estado'] = true;
        $this->loadView("admin/index", $this);
    }

    function cerrar($id = null) {
	    if($id) {
            $this->getCajas($id)->fromArray(@$_GET);

            $_REQUEST['estado'] = 0;
            $_REQUEST['fecha_cerrada'] = date("Y-m-d h:i:s");

            $this->save($id);
            return redirect("cajas/show", $this);
        }else{
            $this['errors'] = "seleccione una caja";
            $this['u'] = 'cajas/error_caja';
            return $this->loadView("admin/index", $this);
        }
    }
	/**
	 * Saves a Cajas. Examples:
	 * POST /cajas/save/1
	 * POST /rest/cajas/.json
	 * PUT /rest/cajas/1.json
	 */
	function save($id = null) {
		$cajas = $this->getCajas($id);

		try {
			$cajas->fromArray($_REQUEST);
			if ($cajas->validate()) {
				$cajas->save();
				$this->flash['messages'][] = 'Cajas guardada';
				$this->redirect('cajas/show/');
			}
			$this->flash['errors'] = $cajas->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('cajas/edit/' . $cajas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Cajas with the id. Examples:
	 * GET /cajas/show/1
	 * GET /rest/cajas/1.json
	 *
	 * @return Cajas
	 */
	function show($id = null) {
		$this->getCajas($id);
        $this['u'] = 'cajas/show';
        $ca = Cajas::getAll('where estado= true');
        $this['estado'] = $ca;
        return $this->loadView("admin/index", $this);
	}

	/**
	 * Deletes the Cajas with the id. Examples:
	 * GET /cajas/delete/1
	 * DELETE /rest/cajas/1.json
	 */
	function delete($id = null) {
		$cajas = $this->getCajas($id);

		try {
			if (null !== $cajas && $cajas->delete()) {
				$this['messages'][] = 'Cajas deleted';
			} else {
				$this['errors'][] = 'Cajas could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('cajas');
		}
	}

	/**
	 * @return Cajas
	 */
	private function getCajas($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Cajas::getPrimaryKey()])) {
			$id = $_REQUEST[Cajas::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Cajas
			$this['cajas'] = new Cajas;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['cajas'] = Cajas::retrieveByPK($id);
		}
		return $this['cajas'];
	}

}