<?php

use Dabl\Query\QueryPager;
use Dabl\Controller\ControllerRoute;

class TiposProductosController extends ApplicationController {

	/**
	 * Returns all TiposProducto records matching the query. Examples:
	 * GET /tipos-productos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipos-productos.json&limit=5
	 *
	 * @return TiposProducto[]
	 */
	function __construct( ControllerRoute $route = null)
    {
        if (!$_SESSION){
            $this->redirect('/login');
        }
        parent::__construct($route);
    }

    function index() {
		$q = TiposProducto::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'TiposProducto';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['tipos_productos'] = $this['pager']->fetchPage();
        $this['u'] = 'tipos-productos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a TiposProducto. Example:
	 * GET /tipos-productos/edit/1
	 *
	 * @return TiposProducto
	 */
	function edit($id = null) {
		$this->getTiposProducto($id)->fromArray(@$_GET);
        $this['u'] = 'tipos-productos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a TiposProducto. Examples:
	 * POST /tipos-productos/save/1
	 * POST /rest/tipos-productos/.json
	 * PUT /rest/tipos-productos/1.json
	 */
	function save($id = null) {
		$tipos_producto = $this->getTiposProducto($id);

		try {
			$tipos_producto->fromArray($_REQUEST);
			if ($tipos_producto->validate()) {
				$tipos_producto->save();
				$this->flash['messages'][] = 'Tipos Producto saved';
				$this->redirect('tipos-productos/index/');
			}
			$this->flash['errors'] = $tipos_producto->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipos-productos/edit/' . $tipos_producto->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the TiposProducto with the id. Examples:
	 * GET /tipos-productos/show/1
	 * GET /rest/tipos-productos/1.json
	 *
	 * @return TiposProducto
	 */
	function show($id = null) {
		return $this->getTiposProducto($id);
	}

	/**
	 * Deletes the TiposProducto with the id. Examples:
	 * GET /tipos-productos/delete/1
	 * DELETE /rest/tipos-productos/1.json
	 */
	function delete($id = null) {
		$tipos_producto = $this->getTiposProducto($id);

		try {
			if (null !== $tipos_producto && $tipos_producto->delete()) {
				$this['messages'][] = 'Tipos Producto deleted';
			} else {
				$this['errors'][] = 'Tipos Producto could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipos-productos');
		}
	}

	/**
	 * @return TiposProducto
	 */
	private function getTiposProducto($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[TiposProducto::getPrimaryKey()])) {
			$id = $_REQUEST[TiposProducto::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new TiposProducto
			$this['tipos_producto'] = new TiposProducto;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipos_producto'] = TiposProducto::retrieveByPK($id);
		}
		return $this['tipos_producto'];
	}

}