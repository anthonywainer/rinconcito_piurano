<?php

use Dabl\Query\QueryPager;
use Dabl\Controller\ControllerRoute;

class MeserosController extends ApplicationController {

	/**
	 * Returns all Meseros records matching the query. Examples:
	 * GET /meseros?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/meseros.json&limit=5
	 *
	 * @return Meseros[]
	 */
	function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            $this->redirect('/login');
        }
    }

    function index() {
		$q = Meseros::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Meseros';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['meseros'] = $this['pager']->fetchPage();
        $this['u'] = 'meseros/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Meseros. Example:
	 * GET /meseros/edit/1
	 *
	 * @return Meseros
	 */
	function edit($id = null) {
		$this->getMeseros($id)->fromArray(@$_GET);
        $this['u'] = 'meseros/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Meseros. Examples:
	 * POST /meseros/save/1
	 * POST /rest/meseros/.json
	 * PUT /rest/meseros/1.json
	 */
	function save($id = null) {
		$meseros = $this->getMeseros($id);

		try {
			$meseros->fromArray($_REQUEST);
			if ($meseros->validate()) {
				$meseros->save();
				$this->flash['messages'][] = 'Meseros saved';
				$this->redirect('meseros/index/');
			}
			$this->flash['errors'] = $meseros->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('meseros/edit/' . $meseros->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Meseros with the id. Examples:
	 * GET /meseros/show/1
	 * GET /rest/meseros/1.json
	 *
	 * @return Meseros
	 */
	function show($id = null) {
		return $this->getMeseros($id);
	}

	/**
	 * Deletes the Meseros with the id. Examples:
	 * GET /meseros/delete/1
	 * DELETE /rest/meseros/1.json
	 */
	function delete($id = null) {
		$meseros = $this->getMeseros($id);

		try {
			if (null !== $meseros && $meseros->delete()) {
				$this['messages'][] = 'Meseros deleted';
			} else {
				$this['errors'][] = 'Meseros could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('meseros');
		}
	}

	/**
	 * @return Meseros
	 */
	private function getMeseros($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Meseros::getPrimaryKey()])) {
			$id = $_REQUEST[Meseros::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Meseros
			$this['meseros'] = new Meseros;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['meseros'] = Meseros::retrieveByPK($id);
		}
		return $this['meseros'];
	}

}