<?php

use Dabl\Query\QueryPager;

class SubmodulosController extends ApplicationController {

	/**
	 * Returns all Submodulo records matching the query. Examples:
	 * GET /submodulos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/submodulos.json&limit=5
	 *
	 * @return Submodulo[]
	 */
	function index() {
        $_GET['ORDER_BY'] = 'submodulo.id';
        $_GET['dir'] = 'DESC';
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
		$q = Submodulo::getQuery(@$_GET)->andLike('submodulo.nombre','%'.$_GET['search'].'%');;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 5 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Submodulo';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['submodulos'] = $this['pager']->fetchPage();
        $this['u'] = 'submodulos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Submodulo. Example:
	 * GET /submodulos/edit/1
	 *
	 * @return Submodulo
	 */
	function edit($id = null) {
		$this->getSubmodulo($id)->fromArray(@$_GET);
        $this['u'] = 'submodulos/edit';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Saves a Submodulo. Examples:
	 * POST /submodulos/save/1
	 * POST /rest/submodulos/.json
	 * PUT /rest/submodulos/1.json
	 */
	function save($id = null) {
		$submodulo = $this->getSubmodulo($id);

		try {
			$submodulo->fromArray($_REQUEST);
			if ($submodulo->validate()) {
				$submodulo->save();
				$this->flash['messages'][] = 'Submodulo saved';
				$this->redirect('submodulos/index/');
			}
			$this->flash['errors'] = $submodulo->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('submodulos/edit/' . $submodulo->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Submodulo with the id. Examples:
	 * GET /submodulos/show/1
	 * GET /rest/submodulos/1.json
	 *
	 * @return Submodulo
	 */
	function show($id = null) {
		return $this->getSubmodulo($id);
	}


	function permisos($id=null){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $sql ="DELETE FROM permisos WHERE permisos.idsubmodulo = $id";
            $result = Permisos::getConnection()->query($sql);
            $result->execute();
            $r = [1 => ' mostrar ',2 => ' agregar/editar ', 3 => ' eliminar ', 4 => ' guardar ',8=>' ver detalle de '];

            foreach ($_REQUEST['acciones'] as $per) {
                $arr = [];
                $ac = Acciones::getAll('where id='.$per)[0];
                if (array_key_exists($ac->id,$r)){
                    $na = $r[$ac->id];
                }else{
                    $na = $ac->accion;
                }
                $arr['idaccion'] = $per;
                $arr['idsubmodulo'] = $id;
                $arr['nombre']      = $na.' '.Submodulo::getAll('where id= '.$id)[0]->getNombre();
                $permisos = new Permisos();
                $permisos->fromArray($arr);
                $permisos->save();
            }
            $this['messages'][] = "se ha guardado los permisos correctamente";
            $this->flash['messages'] = @$this['messages'];
            $this->redirect('submodulos');

        }
	    $this['query'] = ' idsubmodulo = '.$id;
        $this['id']    = $id;
        $this['u'] = 'submodulos/permisos/';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Deletes the Submodulo with the id. Examples:
	 * GET /submodulos/delete/1
	 * DELETE /rest/submodulos/1.json
	 */
	function delete($id = null) {
		$submodulo = $this->getSubmodulo($id);

		try {
			if (null !== $submodulo && $submodulo->delete()) {
				$this['messages'][] = 'Submódulo eliminado';
			} else {
				$this['errors'][] = 'Submodulo could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('submodulos');
		}
	}

	/**
	 * @return Submodulo
	 */
	private function getSubmodulo($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Submodulo::getPrimaryKey()])) {
			$id = $_REQUEST[Submodulo::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Submodulo
			$this['submodulo'] = new Submodulo;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['submodulo'] = Submodulo::retrieveByPK($id);
		}
		return $this['submodulo'];
	}

}