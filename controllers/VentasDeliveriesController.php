<?php

use Dabl\Query\QueryPager;

class VentasDeliveriesController extends ApplicationController {

	/**
	 * Returns all VentasDelivery records matching the query. Examples:
	 * GET /ventas-deliveries?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ventas-deliveries.json&limit=5
	 *
	 * @return VentasDelivery[]
	 */
	function index() {
		$q = VentasDelivery::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'VentasDelivery';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['ventas_deliveries'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a VentasDelivery. Example:
	 * GET /ventas-deliveries/edit/1
	 *
	 * @return VentasDelivery
	 */
	function edit($id = null) {
		return $this->getVentasDelivery($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a VentasDelivery. Examples:
	 * POST /ventas-deliveries/save/1
	 * POST /rest/ventas-deliveries/.json
	 * PUT /rest/ventas-deliveries/1.json
	 */
	function save($id = null) {
		$ventas_delivery = $this->getVentasDelivery($id);

		try {
			$ventas_delivery->fromArray($_REQUEST);
			if ($ventas_delivery->validate()) {
				$ventas_delivery->save();
				$this->flash['messages'][] = 'Ventas Delivery saved';
				$this->redirect('ventas-deliveries/show/' . $ventas_delivery->getId());
			}
			$this->flash['errors'] = $ventas_delivery->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('ventas-deliveries/edit/' . $ventas_delivery->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the VentasDelivery with the id. Examples:
	 * GET /ventas-deliveries/show/1
	 * GET /rest/ventas-deliveries/1.json
	 *
	 * @return VentasDelivery
	 */
	function show($id = null) {
		return $this->getVentasDelivery($id);
	}

	/**
	 * Deletes the VentasDelivery with the id. Examples:
	 * GET /ventas-deliveries/delete/1
	 * DELETE /rest/ventas-deliveries/1.json
	 */
	function delete($id = null) {
		$ventas_delivery = $this->getVentasDelivery($id);

		try {
			if (null !== $ventas_delivery && $ventas_delivery->delete()) {
				$this['messages'][] = 'Ventas Delivery deleted';
			} else {
				$this['errors'][] = 'Ventas Delivery could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ventas-deliveries');
		}
	}

	/**
	 * @return VentasDelivery
	 */
	private function getVentasDelivery($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[VentasDelivery::getPrimaryKey()])) {
			$id = $_REQUEST[VentasDelivery::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new VentasDelivery
			$this['ventas_delivery'] = new VentasDelivery;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ventas_delivery'] = VentasDelivery::retrieveByPK($id);
		}
		return $this['ventas_delivery'];
	}

}