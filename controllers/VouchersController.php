<?php

use Dabl\Query\QueryPager;

class VouchersController extends ApplicationController {

	/**
	 * Returns all Vouchers records matching the query. Examples:
	 * GET /vouchers?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/vouchers.json&limit=5
	 *
	 * @return Vouchers[]
	 */
	function index() {
		$q = Vouchers::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Vouchers';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['vouchers'] = $this['pager']->fetchPage();
        $this['u'] = 'vouchers/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Vouchers. Example:
	 * GET /vouchers/edit/1
	 *
	 * @return Vouchers
	 */
	function edit($id = null) {
		$this->getVouchers($id)->fromArray(@$_GET);
        $this['u'] = 'vouchers/edit';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Saves a Vouchers. Examples:
	 * POST /vouchers/save/1
	 * POST /rest/vouchers/.json
	 * PUT /rest/vouchers/1.json
	 */
	function save($id = null) {
		$vouchers = $this->getVouchers($id);

		try {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
			$vouchers->fromArray($_REQUEST);
			if ($vouchers->validate()) {
				$vouchers->save();
				$this->flash['messages'][] = 'Vouchers ';
				$this->redirect('vouchers/index');
			}
			$this->flash['errors'] = $vouchers->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('vouchers/edit/' . $vouchers->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Vouchers with the id. Examples:
	 * GET /vouchers/show/1
	 * GET /rest/vouchers/1.json
	 *
	 * @return Vouchers
	 */
	function show($id = null) {
		return $this->getVouchers($id);
	}

	/**
	 * Deletes the Vouchers with the id. Examples:
	 * GET /vouchers/delete/1
	 * DELETE /rest/vouchers/1.json
	 */
	function delete($id = null) {
		$vouchers = $this->getVouchers($id);

		try {
			if (null !== $vouchers && $vouchers->delete()) {
				$this['errors'][] = 'Voucher ';
			} else {
				$this['errors'][] = 'Vouchers could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('vouchers');
		}
	}

	/**
	 * @return Vouchers
	 */
	private function getVouchers($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Vouchers::getPrimaryKey()])) {
			$id = $_REQUEST[Vouchers::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Vouchers
			$this['vouchers'] = new Vouchers;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['vouchers'] = Vouchers::retrieveByPK($id);
		}
		return $this['vouchers'];
	}

}