<?php

use Dabl\Query\QueryPager;

class AccionesController extends ApplicationController {

	/**
	 * Returns all Acciones records matching the query. Examples:
	 * GET /acciones?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/acciones.json&limit=5
	 *
	 * @return Acciones[]
	 */
	function index() {
		$q = Acciones::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Acciones';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['acciones'] = $this['pager']->fetchPage();
        $this['u'] = 'acciones/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Acciones. Example:
	 * GET /acciones/edit/1
	 *
	 * @return Acciones
	 */
	function edit($id = null) {
		$this->getAcciones($id)->fromArray(@$_GET);
        $this['u'] = 'acciones/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Acciones. Examples:
	 * POST /acciones/save/1
	 * POST /rest/acciones/.json
	 * PUT /rest/acciones/1.json
	 */
	function save($id = null) {
		$acciones = $this->getAcciones($id);

		try {
			$acciones->fromArray($_REQUEST);
			if ($acciones->validate()) {
				$acciones->save();
				$this->flash['messages'][] = 'Acciones saved';
				$this->redirect('acciones/index/');
			}
			$this->flash['errors'] = $acciones->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('acciones/edit/' . $acciones->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Acciones with the id. Examples:
	 * GET /acciones/show/1
	 * GET /rest/acciones/1.json
	 *
	 * @return Acciones
	 */
	function show($id = null) {
		return $this->getAcciones($id);
	}

	/**
	 * Deletes the Acciones with the id. Examples:
	 * GET /acciones/delete/1
	 * DELETE /rest/acciones/1.json
	 */
	function delete($id = null) {
		$acciones = $this->getAcciones($id);

		try {
			if (null !== $acciones && $acciones->delete()) {
				$this['messages'][] = 'Acciones deleted';
			} else {
				$this['errors'][] = 'Acciones could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('acciones');
		}
	}

	/**
	 * @return Acciones
	 */
	private function getAcciones($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Acciones::getPrimaryKey()])) {
			$id = $_REQUEST[Acciones::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Acciones
			$this['acciones'] = new Acciones;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['acciones'] = Acciones::retrieveByPK($id);
		}
		return $this['acciones'];
	}

}