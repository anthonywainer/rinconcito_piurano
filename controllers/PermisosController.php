<?php

use Dabl\Query\QueryPager;

class PermisosController extends ApplicationController {

	/**
	 * Returns all Permisos records matching the query. Examples:
	 * GET /permisos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/permisos.json&limit=5
	 *
	 * @return Permisos[]
	 */
	function index() {
        $_GET['order_by']= 'permisos.id';
        $_GET['dir'] = 'DESC';
		$q = Permisos::getQuery(@$_GET);
		// paginate
		$limit = empty($_REQUEST['limit']) ? 5 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Permisos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['permisos'] = $this['pager']->fetchPage();
        $this['u'] = 'permisos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Permisos. Example:
	 * GET /permisos/edit/1
	 *
	 * @return Permisos
	 */
	function edit($id = null) {
		$this->getPermisos($id)->fromArray(@$_GET);
        $this['u'] = 'permisos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Permisos. Examples:
	 * POST /permisos/save/1
	 * POST /rest/permisos/.json
	 * PUT /rest/permisos/1.json
	 */
	function save($id = null) {
		$permisos = $this->getPermisos($id);

		try {
			$permisos->fromArray($_REQUEST);
			if ($permisos->validate()) {
				$permisos->save();
				$this->flash['messages'][] = 'Permisos ';
				$this->redirect('permisos/index/');
			}
			$this->flash['errors'] = $permisos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('permisos/edit/' . $permisos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Permisos with the id. Examples:
	 * GET /permisos/show/1
	 * GET /rest/permisos/1.json
	 *
	 * @return Permisos
	 */
	function show($id = null) {
		return $this->getPermisos($id);
	}

	/**
	 * Deletes the Permisos with the id. Examples:
	 * GET /permisos/delete/1
	 * DELETE /rest/permisos/1.json
	 */
	function delete($id = null) {
		$permisos = $this->getPermisos($id);

		try {
			if (null !== $permisos && $permisos->delete()) {
				$this['messages'][] = 'Permisos ';
			} else {
				$this['errors'][] = 'Permisos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['errors'] = @$this['messages'];
			$this->redirect('permisos');
		}
	}

	/**
	 * @return Permisos
	 */
	private function getPermisos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Permisos::getPrimaryKey()])) {
			$id = $_REQUEST[Permisos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Permisos
			$this['permisos'] = new Permisos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['permisos'] = Permisos::retrieveByPK($id);
		}
		return $this['permisos'];
	}

}