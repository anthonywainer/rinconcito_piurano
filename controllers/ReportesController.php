<?php


class ReportesController extends ApplicationController {

    function index() {
        $this['u'] = 'reportes/index';
        if (isset($_REQUEST['date_ini'])) {

            $begin = $_REQUEST['date_ini'];
            $end   = $_REQUEST['date_end'];

            $this['fechai'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_ini'])->format('d/m/Y');
            $this['fechae'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_end'])->format('d/m/Y');

        } else {
            $begin = date('Y-m-d');
            $end   = date('Y-m-d');
            $this['fechai'] = date("d/m/Y");
            $this['fechae'] = date("d/m/Y");
        }

        $sql = "
SELECT
ventas.id,
clientes.nombres,
ventas.fecha,
formas_pago.descripcion,
vouchers.tipo_comprobante,
productos.nombre,
pedido_producto.cantidad,
pedido_producto.precio,
pedido_producto.total
FROM
ventas
INNER JOIN ventas_movimiento ON ventas_movimiento.venta_id = ventas.id
INNER JOIN movimientos_de_dinero ON ventas_movimiento.movimiento_id = movimientos_de_dinero.id
INNER JOIN vouchers ON movimientos_de_dinero.voucher_id = vouchers.id
INNER JOIN formas_pago ON movimientos_de_dinero.forma_pago_id = formas_pago.id
INNER JOIN pedido_producto ON pedido_producto.venta_id = ventas.id
INNER JOIN productos ON pedido_producto.producto_id = productos.id
INNER JOIN clientes ON ventas.cliente_id = clientes.id

WHERE (ventas.fecha BETWEEN '".$begin."' AND '".$end."') GROUP BY id       
        ";
        $this['ventas'] = Ventas::getConnection()->query($sql)->fetchAll();

        return $this->loadView("admin/index",$this);
    }

}