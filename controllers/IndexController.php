<?php

class IndexController extends ApplicationController {

	function index() {
        if ($_SESSION) {
            header('Location: /rinconcito_piurano/admin');
        }
	}
	function admin(){
        if ($_SESSION) {
            return $this['u'] = 'dashboard/index';
        }else{
            header('Location: /rinconcito_piurano/');
        }
    }
    function logout(){
        session_destroy();
        header('location: /rinconcito_piurano/');
    }
}