<?php

use Dabl\Query\QueryPager;

class MediosDeEnviosController extends ApplicationController {

	/**
	 * Returns all MediosDeEnvio records matching the query. Examples:
	 * GET /medios-de-envios?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/medios-de-envios.json&limit=5
	 *
	 * @return MediosDeEnvio[]
	 */
	function index() {
		$q = MediosDeEnvio::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'MediosDeEnvio';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['medios_de_envios'] = $this['pager']->fetchPage();
        $this['u'] = 'medios-de-envios/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a MediosDeEnvio. Example:
	 * GET /medios-de-envios/edit/1
	 *
	 * @return MediosDeEnvio
	 */
	function edit($id = null) {
		 $this->getMediosDeEnvio($id)->fromArray(@$_GET);
        $this['u'] = 'medios-de-envios/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a MediosDeEnvio. Examples:
	 * POST /medios-de-envios/save/1
	 * POST /rest/medios-de-envios/.json
	 * PUT /rest/medios-de-envios/1.json
	 */
	function save($id = null) {
		$medios_de_envio = $this->getMediosDeEnvio($id);

		try {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
			$medios_de_envio->fromArray($_REQUEST);
			if ($medios_de_envio->validate()) {
				$medios_de_envio->save();
				$this->flash['messages'][] = 'Medios De Envio ';
				$this->redirect('medios-de-envios/index' );
			}
			$this->flash['errors'] = $medios_de_envio->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('medios-de-envios/edit/' . $medios_de_envio->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the MediosDeEnvio with the id. Examples:
	 * GET /medios-de-envios/show/1
	 * GET /rest/medios-de-envios/1.json
	 *
	 * @return MediosDeEnvio
	 */
	function show($id = null) {
		return $this->getMediosDeEnvio($id);
	}

	/**
	 * Deletes the MediosDeEnvio with the id. Examples:
	 * GET /medios-de-envios/delete/1
	 * DELETE /rest/medios-de-envios/1.json
	 */
	function delete($id = null) {
		$medios_de_envio = $this->getMediosDeEnvio($id);

		try {
			if (null !== $medios_de_envio && $medios_de_envio->delete()) {
				$this['messages'][] = 'Medios De Envio ';
			} else {
				$this['errors'][] = 'Medios De Envio could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('medios-de-envios');
		}
	}

	/**
	 * @return MediosDeEnvio
	 */
	private function getMediosDeEnvio($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[MediosDeEnvio::getPrimaryKey()])) {
			$id = $_REQUEST[MediosDeEnvio::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new MediosDeEnvio
			$this['medios_de_envio'] = new MediosDeEnvio;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['medios_de_envio'] = MediosDeEnvio::retrieveByPK($id);
		}
		return $this['medios_de_envio'];
	}

}