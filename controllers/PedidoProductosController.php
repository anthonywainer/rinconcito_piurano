<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class PedidoProductosController extends ApplicationController {

	/**
	 * Returns all PedidoProducto records matching the query. Examples:
	 * GET /pedido-productos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/pedido-productos.json&limit=5
	 *
	 * @return PedidoProducto[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }

	function index() {
        if (isset($_REQUEST['id'])){
            $pp = PedidoProducto::getAll('Where venta_id='.$_GET['id'].'  and created_at IS NULL');
            $na= [];
            foreach ($pp as $i){
                array_push($na,$i->getProducto_id());
            }
            $this['pp'] = $na;
            $this['u'] = 'pedido-productos/pedidos/';
            $this->loadView("admin/index",$this);
        }else{
            $this->flash['messages'][] = 'seleccione una mesa';
            $this->redirect('ventas');
        }

	}

	function show($id = null){
        $this['id'] = $id;
        $this['u'] = 'pedido-productos/show';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Form to create or edit a PedidoProducto. Example:
	 * GET /pedido-productos/edit/1
	 *
	 * @return PedidoProducto
	 */
	function edit() {

		return $this->getPedidoProducto()->fromArray(@$_GET);
	}

	/**
	 * Saves a PedidoProducto. Examples:
	 * POST /pedido-productos/save/1
	 * POST /rest/pedido-productos/.json
	 * PUT /rest/pedido-productos/1.json
	 */
	function save($id=null) {
	    if ($id){
	        PedidoProducto::getConnection()->query('UPDATE pedido_producto set
               cantidad='.$_REQUEST["cantidad"].',precio= '.$_REQUEST["precio"].', total='.$_REQUEST["total"].' Where id='.$id)->execute();
            $this['id'] = $_REQUEST['venta_id'];
            return $this->loadView('pedido-productos/edit_prod',$this);
        }
		$pedido_producto = $this->getPedidoProducto();
		try {
		    $precio = Productos::getAll('where id='.$_REQUEST['producto_id'])[0]->getPrecio();
		    $_REQUEST['total'] = $precio;
		    $_REQUEST['precio'] = $precio;
			$pedido_producto->fromArray($_REQUEST);

			if ($pedido_producto->validate()) {
				$pedido_producto->save();
				$this->flash['messages'][] = 'Pedido Producto ';
				$this['id'] = $_REQUEST['venta_id'];
				return $this->loadView('pedido-productos/edit_prod',$this);
			}
			$this->flash['errors'] = $pedido_producto->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}
		#$this->redirect('pedido-productos/edit/' . '?' . http_build_query($_REQUEST));
	}
    function editarprecio($id = null ){
        return $this->save($id);
    }


    function delete($id = null) {
	    $p = PedidoProducto::getAll('where id='.$id);
        PedidoProducto::getConnection()->query('Delete from pedido_producto where id='.$id)->execute();
        $this->flash['errors'] = "pedido eliminado correctamente";
        $this->redirect('pedido-productos/?id='.$p[0]->getVenta_id());
    }
	/**
	 * @return PedidoProducto
	 */
	private function getPedidoProducto() {
		return $this['pedido_producto'] = new PedidoProducto;
	}

}