<?php

use Dabl\Query\QueryPager;

class DeliveristasController extends ApplicationController {

	/**
	 * Returns all Deliverista records matching the query. Examples:
	 * GET /deliveristas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/deliveristas.json&limit=5
	 *
	 * @return Deliverista[]
	 */
	function index() {
		$q = Deliverista::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Deliverista';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['deliveristas'] = $this['pager']->fetchPage();
        $this['u'] = 'deliveristas/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Deliverista. Example:
	 * GET /deliveristas/edit/1
	 *
	 * @return Deliverista
	 */
	function edit($id = null) {
		$this->getDeliverista($id)->fromArray(@$_GET);
        $this['u'] = 'deliveristas/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Deliverista. Examples:
	 * POST /deliveristas/save/1
	 * POST /rest/deliveristas/.json
	 * PUT /rest/deliveristas/1.json
	 */
	function save($id = null) {
		$deliverista = $this->getDeliverista($id);

		try {
			$deliverista->fromArray($_REQUEST);
			if ($deliverista->validate()) {
				$deliverista->save();
				$this->flash['messages'][] = 'Deliverista saved';
				$this->redirect('deliveristas/index/');
			}
			$this->flash['errors'] = $deliverista->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('deliveristas/edit/' . $deliverista->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Deliverista with the id. Examples:
	 * GET /deliveristas/show/1
	 * GET /rest/deliveristas/1.json
	 *
	 * @return Deliverista
	 */
	function show($id = null) {
		return $this->getDeliverista($id);
	}

	/**
	 * Deletes the Deliverista with the id. Examples:
	 * GET /deliveristas/delete/1
	 * DELETE /rest/deliveristas/1.json
	 */
	function delete($id = null) {
		$deliverista = $this->getDeliverista($id);

		try {
			if (null !== $deliverista && $deliverista->delete()) {
				$this['messages'][] = 'Deliverista deleted';
			} else {
				$this['errors'][] = 'Deliverista could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('deliveristas');
		}
	}

	/**
	 * @return Deliverista
	 */
	private function getDeliverista($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Deliverista::getPrimaryKey()])) {
			$id = $_REQUEST[Deliverista::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Deliverista
			$this['deliverista'] = new Deliverista;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['deliverista'] = Deliverista::retrieveByPK($id);
		}
		return $this['deliverista'];
	}

}