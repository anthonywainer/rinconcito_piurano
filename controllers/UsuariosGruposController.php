<?php

use Dabl\Query\QueryPager;

class UsuariosGruposController extends ApplicationController {

	/**
	 * Returns all UsuariosGrupo records matching the query. Examples:
	 * GET /usuarios-grupos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/usuarios-grupos.json&limit=5
	 *
	 * @return UsuariosGrupo[]
	 */
	function index() {
		$q = UsuariosGrupo::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'UsuariosGrupo';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['usuarios_grupos'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a UsuariosGrupo. Example:
	 * GET /usuarios-grupos/edit/1
	 *
	 * @return UsuariosGrupo
	 */
	function edit($id = null) {
		return $this->getUsuariosGrupo($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a UsuariosGrupo. Examples:
	 * POST /usuarios-grupos/save/1
	 * POST /rest/usuarios-grupos/.json
	 * PUT /rest/usuarios-grupos/1.json
	 */
	function save($id = null) {
		$usuarios_grupo = $this->getUsuariosGrupo($id);

		try {

            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;

			$usuarios_grupo->fromArray($_REQUEST);



			if ($usuarios_grupo->validate()) {
				$usuarios_grupo->save();
				$this->flash['messages'][] = 'Usuarios Grupo saved';
				$this->redirect('usuarios-grupos/show/' . $usuarios_grupo->getId());
			}
			$this->flash['errors'] = $usuarios_grupo->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('usuarios-grupos/edit/' . $usuarios_grupo->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the UsuariosGrupo with the id. Examples:
	 * GET /usuarios-grupos/show/1
	 * GET /rest/usuarios-grupos/1.json
	 *
	 * @return UsuariosGrupo
	 */
	function show($id = null) {
		return $this->getUsuariosGrupo($id);
	}

	/**
	 * Deletes the UsuariosGrupo with the id. Examples:
	 * GET /usuarios-grupos/delete/1
	 * DELETE /rest/usuarios-grupos/1.json
	 */
	function delete($id = null) {
		$usuarios_grupo = $this->getUsuariosGrupo($id);

		try {
			if (null !== $usuarios_grupo && $usuarios_grupo->delete()) {
				$this['messages'][] = 'Usuarios Grupo deleted';
			} else {
				$this['errors'][] = 'Usuarios Grupo could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('usuarios-grupos');
		}
	}

	/**
	 * @return UsuariosGrupo
	 */
	private function getUsuariosGrupo($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[UsuariosGrupo::getPrimaryKey()])) {
			$id = $_REQUEST[UsuariosGrupo::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new UsuariosGrupo
			$this['usuarios_grupo'] = new UsuariosGrupo;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['usuarios_grupo'] = UsuariosGrupo::retrieveByPK($id);
		}
		return $this['usuarios_grupo'];
	}

}