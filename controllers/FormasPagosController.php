<?php

use Dabl\Query\QueryPager;

class FormasPagosController extends ApplicationController {

	/**
	 * Returns all FormasPago records matching the query. Examples:
	 * GET /formas-pagos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/formas-pagos.json&limit=5
	 *
	 * @return FormasPago[]
	 */
	function index() {
		$q = FormasPago::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'FormasPago';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['formas_pagos'] = $this['pager']->fetchPage();
        $this['u'] = 'formas-pagos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a FormasPago. Example:
	 * GET /formas-pagos/edit/1
	 *
	 * @return FormasPago
	 */
	function edit($id = null) {
		$this->getFormasPago($id)->fromArray(@$_GET);
        $this['u'] = 'formas-pagos/edit';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Saves a FormasPago. Examples:
	 * POST /formas-pagos/save/1
	 * POST /rest/formas-pagos/.json
	 * PUT /rest/formas-pagos/1.json
	 */
	function save($id = null) {
		$formas_pago = $this->getFormasPago($id);

		try {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
            $formas_pago->fromArray($_REQUEST);
			if ($formas_pago->validate()) {
				$formas_pago->save();
				$this->flash['messages'][] = 'Formas de Pago ';
				$this->redirect('formas-pagos/index');
			}
			$this->flash['errors'] = $formas_pago->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('formas-pagos/edit/' . $formas_pago->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the FormasPago with the id. Examples:
	 * GET /formas-pagos/show/1
	 * GET /rest/formas-pagos/1.json
	 *
	 * @return FormasPago
	 */
	function show($id = null) {
		return $this->getFormasPago($id);
	}

	/**
	 * Deletes the FormasPago with the id. Examples:
	 * GET /formas-pagos/delete/1
	 * DELETE /rest/formas-pagos/1.json
	 */
	function delete($id = null) {
		$formas_pago = $this->getFormasPago($id);

		try {
			if (null !== $formas_pago && $formas_pago->delete()) {
				$this['errors'][] = 'Formas de Pago ';
			} else {
				$this['errors'][] = 'Formas Pago ';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('formas-pagos');
		}
	}

	/**
	 * @return FormasPago
	 */
	private function getFormasPago($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[FormasPago::getPrimaryKey()])) {
			$id = $_REQUEST[FormasPago::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new FormasPago
			$this['formas_pago'] = new FormasPago;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['formas_pago'] = FormasPago::retrieveByPK($id);
		}
		return $this['formas_pago'];
	}

}