<?php

use Dabl\Query\QueryPager;

class ClientesController extends ApplicationController {

	/**
	 * Returns all Clientes records matching the query. Examples:
	 * GET /clientes?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/clientes.json&limit=5
	 *
	 * @return Clientes[]
	 */


	function index() {

		$q = Clientes::getQuery(@$_GET);
		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Clientes';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['clientes'] = $this['pager']->fetchPage();
        $this['u'] = 'clientes/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Clientes. Example:
	 * GET /clientes/edit/1
	 *
	 * @return Clientes
	 */
	function edit($id = null) {
	    if (isset($_REQUEST['tipo'])){
	        if($_REQUEST['tipo']==12){
                $this->getClientes($id)->fromArray(@$_GET);
                $this['u'] = 'clientes/dni';
                return $this->loadView("admin/index",$this);
            }else{
                $this->getClientes($id)->fromArray(@$_GET);
                $this['u'] = 'clientes/ruc';
                return $this->loadView("admin/index",$this);
            }
        }
		$this->getClientes($id)->fromArray(@$_GET);
        $this['u'] = 'clientes/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Clientes. Examples:
	 * POST /clientes/save/1
	 * POST /rest/clientes/.json
	 * PUT /rest/clientes/1.json
	 */
	function save($id = null) {
		$clientes = $this->getClientes($id);

		try {
			$clientes->fromArray($_REQUEST);
			if ($clientes->validate()) {
				$clientes->save();
                $uid = $clientes->getId();
                $this['id'] =$uid;
                if (isset($_REQUEST['json'])){
                    return $this->loadView('ventas/datos/',$this);
                }
				$this->flash['messages'][] = 'Clientes Guardado';
				$this->redirect('clientes/index/');
			}
			$this->flash['errors'] = $clientes->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('clientes/edit/' . $clientes->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Clientes with the id. Examples:
	 * GET /clientes/show/1
	 * GET /rest/clientes/1.json
	 *
	 * @return Clientes
	 */
	function show($id = null) {
		return $this->getClientes($id);
	}

	/**
	 * Deletes the Clientes with the id. Examples:
	 * GET /clientes/delete/1
	 * DELETE /rest/clientes/1.json
	 */
	function delete($id = null) {
		$clientes = $this->getClientes($id);

		try {
			if (null !== $clientes && $clientes->delete()) {
				$this['messages'][] = 'Clientes deleted';
			} else {
				$this['errors'][] = 'Clientes could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('clientes');
		}
	}

	/**
	 * @return Clientes
	 */
	private function getClientes($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Clientes::getPrimaryKey()])) {
			$id = $_REQUEST[Clientes::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Clientes
			$this['clientes'] = new Clientes;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['clientes'] = Clientes::retrieveByPK($id);
		}
		return $this['clientes'];
	}

}