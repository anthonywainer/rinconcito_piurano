<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class MovimientosDeDinerosController extends ApplicationController {

	/**
	 * Returns all MovimientosDeDinero records matching the query. Examples:
	 * GET /movimientos-de-dineros?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/movimientos-de-dineros.json&limit=5
	 *
	 * @return MovimientosDeDinero[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }

	function index($id=null) {
	    if (isset($_REQUEST['del'])){
            $this['idv'] = $id;
            #$this['u'] = 'movimientos-de-dineros/pagos-deli';
            return $this->loadView('movimientos-de-dineros/pagos-deli', $this);
        }else {
            if ($id) {
                $this['idv'] = $id;
                $this['u'] = 'movimientos-de-dineros/pagos';
                return $this->loadView("admin/index", $this);
            }
        }
		$q = MovimientosDeDinero::getQuery(@$_GET)->andNotNull('voucher_id');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'MovimientosDeDinero';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['movimientos_de_dineros'] = $this['pager']->fetchPage();
        $this['u'] = 'movimientos-de-dineros/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a MovimientosDeDinero. Example:
	 * GET /movimientos-de-dineros/edit/1
	 *
	 * @return MovimientosDeDinero
	 */
	function edit($id = null) {
		$this->getMovimientosDeDinero($id)->fromArray(@$_GET);
        $this['u'] = 'movimientos-de-dineros/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a MovimientosDeDinero. Examples:
	 * POST /movimientos-de-dineros/save/1
	 * POST /rest/movimientos-de-dineros/.json
	 * PUT /rest/movimientos-de-dineros/1.json
	 */
	function save($id = null) {
        if (isset($_REQUEST['delicia'])) {
            $this['id'] = $id;
            return $this->loadView('movimientos-de-dineros/boleta-d', $this);
        }else {
            $movimientos_de_dinero = $this->getMovimientosDeDinero($id);
            if ($id) {
                $movimientos_de_dinero->fromArray($_REQUEST);
                $movimientos_de_dinero->save();
                $this['idv'] = $_REQUEST['venta_id'];
                return $this->loadView('pedido-productos/formas_pagos/', $this);
            }
            try {
                $ca = Cajas::getAll('where estado= true');
                if (count($ca) > 0) {

                    $fecha_actual = date("Y-m-d");
                    $_REQUEST["created_at"] = $fecha_actual;
                    $_REQUEST['caja_id'] = $ca[0]->id;
                    $_REQUEST['usuario_id'] = $_SESSION['user']->id;

                    $movimientos_de_dinero->fromArray($_REQUEST);
                    if ($movimientos_de_dinero->validate()) {
                        $movimientos_de_dinero->save();
                        $_REQUEST['movimiento_id'] = $movimientos_de_dinero->getId();
                        $_REQUEST['fecha'] = $fecha_actual;

                        if (isset($_REQUEST['del'])) {
                            $vd = new VentasDelivery;
                            $vd->fromArray($_REQUEST);
                            $vd->save();

                            $uvm = Deliveries::retrieveByPK($_REQUEST['delivery_id']);
                            $uvm->fromArray(['estado_delivery' => 1]);
                            $uvm->save();
                            $this['id'] = $_REQUEST['delivery_id'];
                            return $this->loadView('movimientos-de-dineros/boleta-d', $this);

                        } else {
                            $vm = new VentasMovimiento;
                            $vm->fromArray($_REQUEST);
                            $vm->save();

                            $vm = VentasMesas::getAll('where venta_id=' . $_REQUEST['venta_id']);
                            if (count($vm) > 1) {
                                foreach ($vm as $m) {
                                    $uvmm = VentasMesas::retrieveByPK($m->id);
                                    $uvmm->fromArray(['estado_venta_mesa' => 0]);
                                    $uvmm->save();

                                    $me = Mesas::retrieveByPK($m->getMesa_id());
                                    $me->fromArray(['estado_mesa' => 'disponible']);
                                    $me->save();
                                }
                            } else {
                                $uvmm = VentasMesas::retrieveByPK($vm[0]->id);
                                $uvmm->fromArray(['estado_venta_mesa' => 0]);
                                $uvmm->save();

                                $me = Mesas::retrieveByPK($vm[0]->getMesa_id());
                                $me->fromArray(['estado_mesa' => 'disponible']);
                                $me->save();
                            }

                            $uvm = Ventas::retrieveByPK($_REQUEST['venta_id']);
                            $uvm->fromArray(['estado_venta' => 0]);
                            $uvm->save();
                            $this->flash['messages'][] = 'Movimientos De Dinero guardado';
                            return $this->redirect('ventas');
                        }
                        $this->flash['messages'][] = 'Movimientos De Dinero guardado';
                        return $this->redirect('delivery');
                    }
                } else {
                    $this->flash['errors'][] = "Falta Aperturar Caja";
                    if (isset($_REQUEST['del'])) {
                        return $this->redirect('movimientos-de-dineros/index/' . $_REQUEST['delivery_id'] . '/?del=');
                    }
                    return $this->redirect('movimientos-de-dineros/index/' . $_REQUEST['venta_id']);
                }
                $this->flash['errors'] = $movimientos_de_dinero->getValidationErrors();

            } catch (Exception $e) {
                $this->flash['errors'][] = $e->getMessage();
            }

            $this->redirect('movimientos-de-dineros/edit/' . $movimientos_de_dinero->getId() . '?' . http_build_query($_REQUEST));
        }
	}

	function transaccion($id= null){
	    $vm = VentasMovimiento::getAll('where movimiento_id ='.$id);
	    $vd = VentasDelivery::getAll('where movimiento_id ='.$id);
	    if ($vm){
	        $this['id']=$vm[0]->getVenta_id();
            return $this->loadView('movimientos-de-dineros/boleta', $this);
        }else{
            $this['id']=$vd[0]->getDelivery_id();
            return $this->loadView('movimientos-de-dineros/boleta-d', $this);
        }

    }
	/**
	 * Returns the MovimientosDeDinero with the id. Examples:
	 * GET /movimientos-de-dineros/show/1
	 * GET /rest/movimientos-de-dineros/1.json
	 *
	 * @return MovimientosDeDinero
	 */
	function show($id = null) {

            $this->getMovimientosDeDinero($id);
            $this['id'] = $id;

            $vm = VentasMesas::getAll('where venta_id=' . $id);
            if (count($vm) > 1) {
                foreach ($vm as $m) {
                    $uvmm = VentasMesas::retrieveByPK($m->id);
                    $uvmm->fromArray(['estado_venta_mesa' => 0]);
                    $uvmm->save();

                    $me = Mesas::retrieveByPK($m->getMesa_id());
                    $me->fromArray(['estado_mesa' => 'disponible']);
                    $me->save();
                }
            } else {
                $uvmm = VentasMesas::retrieveByPK($vm[0]->id);
                $uvmm->fromArray(['estado_venta_mesa' => 0]);
                $uvmm->save();

                $me = Mesas::retrieveByPK($vm[0]->getMesa_id());
                $me->fromArray(['estado_mesa' => 'disponible']);
                $me->save();
            }

            $uvm = Ventas::retrieveByPK($id);
            $uvm->fromArray(['estado_venta' => 0]);
            $uvm->save();

            if (isset($_REQUEST['tip'])) {
                return $this->loadView('movimientos-de-dineros/boleta', $this);
            } else {

                $ventas = VentasMesas::getConnection()->query('select * from ventas_mesas where estado_venta_mesa = 1')
                    ->fetchAll();
                $mesas = Mesas::getAll('order by numero');
                $am = [];
                foreach ($mesas as $m) {
                    $b = array_search($m->id, array_column($ventas, 'mesa_id'));
                    if (is_numeric($b)) {
                        array_push($am, ['venta_id' => $ventas[$b]['venta_id'], 'mesa' => $m->mesa, 'estado_mesa' => $m->getEstadoMesa()]);
                    } else {
                        array_push($am, ['venta_id' => null, 'mesa' => $m->mesa, 'mesa_id' => $m->id, 'estado_mesa' => $m->getEstadoMesa()]);
                    }
                }
                $dna = [];
                foreach ($am as $i) {
                    if ($i['estado_mesa'] == 'disponible') {
                        array_push($dna, $i);
                    } else {
                        if (array_key_exists('v' . $i['venta_id'], $dna)) {
                            $dna['v' . $i['venta_id']]['mesa'] = $dna['v' . $i['venta_id']]['mesa'] . $i['mesa'];
                        } else {
                            $dna['v' . $i['venta_id']]['mesa'] = $i['mesa'];
                            $dna['v' . $i['venta_id']]['estado_mesa'] = 'ocupado';
                            $dna['v' . $i['venta_id']]['venta_id'] = $i['venta_id'];
                        }
                    }
                }
                $this['mesas'] = $dna;
                $this['u'] = 'ventas/mesas';
                $this->flash['messages'] = 'guardado correctamente';
                return $this->loadView("ventas/mesas", $this);
            }

	}

	/**
	 * Deletes the MovimientosDeDinero with the id. Examples:
	 * GET /movimientos-de-dineros/delete/1
	 * DELETE /rest/movimientos-de-dineros/1.json
	 */
	function delete($id = null) {
		$movimientos_de_dinero = $this->getMovimientosDeDinero($id);

		try {
			if (null !== $movimientos_de_dinero && $movimientos_de_dinero->delete()) {
				$this['messages'][] = 'Movimientos De Dinero deleted';
			} else {
				$this['errors'][] = 'Movimientos De Dinero could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('movimientos-de-dineros');
		}
	}

	/**
	 * @return MovimientosDeDinero
	 */
	private function getMovimientosDeDinero($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[MovimientosDeDinero::getPrimaryKey()])) {
			$id = $_REQUEST[MovimientosDeDinero::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new MovimientosDeDinero
			$this['movimientos_de_dinero'] = new MovimientosDeDinero;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['movimientos_de_dinero'] = MovimientosDeDinero::retrieveByPK($id);
		}
		return $this['movimientos_de_dinero'];
	}

}