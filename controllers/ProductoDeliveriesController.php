<?php

use Dabl\Query\QueryPager;

class ProductoDeliveriesController extends ApplicationController {

	/**
	 * Returns all ProductoDelivery records matching the query. Examples:
	 * GET /producto-deliveries?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/producto-deliveries.json&limit=5
	 *
	 * @return ProductoDelivery[]
	 */
	function index() {
		$q = ProductoDelivery::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ProductoDelivery';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['producto_deliveries'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a ProductoDelivery. Example:
	 * GET /producto-deliveries/edit/1
	 *
	 * @return ProductoDelivery
	 */
	function edit($id = null) {
		return $this->getProductoDelivery($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a ProductoDelivery. Examples:
	 * POST /producto-deliveries/save/1
	 * POST /rest/producto-deliveries/.json
	 * PUT /rest/producto-deliveries/1.json
	 */
	function save($id = null) {
        if ($id){
            ProductoDelivery::getConnection()->query('UPDATE producto_delivery set
               cantidad='.$_REQUEST["cantidad"].',precio='.$_REQUEST["precio"].', total='.$_REQUEST["total"].' Where id='.$id)->execute();
            $this['id'] = $_REQUEST['delivery_id'];
            return $this->loadView('producto-deliveries/edit_prod',$this);
        }
		$producto_delivery = $this->getProductoDelivery($id);
		try {
		    $pre = Productos::getAll('where id='.$_REQUEST['producto_id'])[0]->getPrecio();
		    $_REQUEST['precio'] = $pre;
            $_REQUEST['total'] = $pre;
            $_REQUEST['cantidad'] = 1;
			$producto_delivery->fromArray($_REQUEST);
			if ($producto_delivery->validate()) {
				$producto_delivery->save();
				$this->flash['messages'][] = 'Producto Delivery agregado';
                $this['id'] = $_REQUEST['delivery_id'];
                return $this->loadView('producto-deliveries/edit_prod',$this);
			}
			$this->flash['errors'] = $producto_delivery->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('producto-deliveries/edit/' . $producto_delivery->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ProductoDelivery with the id. Examples:
	 * GET /producto-deliveries/show/1
	 * GET /rest/producto-deliveries/1.json
	 *
	 * @return ProductoDelivery
	 */
	function show($id = null) {
		return $this->getProductoDelivery($id);
	}

	/**
	 * Deletes the ProductoDelivery with the id. Examples:
	 * GET /producto-deliveries/delete/1
	 * DELETE /rest/producto-deliveries/1.json
	 */
	function delete($id = null) {
        $p = ProductoDelivery::getAll('where id='.$id);
		$producto_delivery = $this->getProductoDelivery($id);

		try {
			if (null !== $producto_delivery && $producto_delivery->delete()) {
				$this['messages'][] = 'Producto Delivery deleted';
			} else {
				$this['errors'][] = 'Producto Delivery could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('delivery/edit/'.$p[0]->getDelivery_id());
		}
	}

	/**
	 * @return ProductoDelivery
	 */
	private function getProductoDelivery($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ProductoDelivery::getPrimaryKey()])) {
			$id = $_REQUEST[ProductoDelivery::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ProductoDelivery
			$this['producto_delivery'] = new ProductoDelivery;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['producto_delivery'] = ProductoDelivery::retrieveByPK($id);
		}
		return $this['producto_delivery'];
	}

}