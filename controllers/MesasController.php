<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class MesasController extends ApplicationController {

	/**
	 * Returns all Mesas records matching the query. Examples:
	 * GET /mesas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/mesas.json&limit=5
	 *
	 * @return Mesas[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }


    function index() {
		$q = Mesas::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Mesas';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['mesas'] = $this['pager']->fetchPage();
        $this['u'] = 'mesas/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Mesas. Example:
	 * GET /mesas/edit/1
	 *
	 * @return Mesas
	 */
	function edit($id = null) {
		$this->getMesas($id)->fromArray(@$_GET);
        $this['u'] = 'mesas/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Mesas. Examples:
	 * POST /mesas/save/1
	 * POST /rest/mesas/.json
	 * PUT /rest/mesas/1.json
	 */
	function save($id = null) {
		$mesas = $this->getMesas($id);

        if(!isset($_REQUEST['id'])) {
            $m = Mesas::getAll('ORDER BY numero DESC');
            if ($m) {
                $nu = ($m[0]->getNumero() + 1);
            } else {
                $nu = 1;
            }

            $_REQUEST['estado_mesa'] = 'disponible';
            $_REQUEST['mesa'] = 'M' . $nu;
            $_REQUEST['numero'] = $nu;
        }
		try {
			$mesas->fromArray($_REQUEST);
			if ($mesas->validate()) {
				$mesas->save();
				$this->flash['messages'][] = 'Mesa agregada';
				$this->redirect('ventas/index/');
			}
			$this->flash['errors'] = $mesas->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('mesas/edit/' . $mesas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Mesas with the id. Examples:
	 * GET /mesas/show/1
	 * GET /rest/mesas/1.json
	 *
	 * @return Mesas
	 */
	function show($id = null) {
		return $this->getMesas($id);
	}

	/**
	 * Deletes the Mesas with the id. Examples:
	 * GET /mesas/delete/1
	 * DELETE /rest/mesas/1.json
	 */
	function delete($id = null) {
		$mesas = $this->getMesas($id);

		try {
			if (null !== $mesas && $mesas->delete()) {
				$this['messages'][] = 'Mesas deleted';
			} else {
				$this['errors'][] = 'Mesas could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('mesas');
		}
	}

	/**
	 * @return Mesas
	 */
	private function getMesas($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Mesas::getPrimaryKey()])) {
			$id = $_REQUEST[Mesas::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Mesas
			$this['mesas'] = new Mesas;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['mesas'] = Mesas::retrieveByPK($id);
		}
		return $this['mesas'];
	}

}