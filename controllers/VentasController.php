<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class VentasController extends ApplicationController {

	/**
	 * Returns all Ventas records matching the query. Examples:
	 * GET /ventas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ventas.json&limit=5
	 *
	 * @return Ventas[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
    function listar(){
        $q = Ventas::getQuery(@$_GET);
        // paginate
        $limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
        $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
        $class = 'Ventas';
        $method = 'doSelectIterator';
        $this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

        if (isset($_GET['count_only'])) {
            return $this['pager'];
        }
        $this['ventas'] = $this['pager']->fetchPage();
        $this['u'] = 'ventas/index';
        return $this->loadView("admin/index",$this);
    }
	function index() {

		if (isset($_REQUEST['li'])){
		   return $this->listar();
        }

        $this['u'] = 'ventas/mesas';
        $ventas = VentasMesas::getConnection()->query('select * from ventas_mesas where estado_venta_mesa = 1')
            ->fetchAll();
        $mesas  = Mesas::getAll('order by numero');
        $am = [];
        foreach ($mesas as $m){
           $b = array_search($m->id, array_column($ventas, 'mesa_id'));
           if  (is_numeric($b)){
                array_push($am,['venta_id' => $ventas[$b]['venta_id'], 'mesa' => $m->mesa,'estado_mesa'=>$m->getEstadoMesa()]);
           }else{
               array_push($am,['venta_id'=>null, 'mesa' => $m->mesa, 'mesa_id'=>$m->id,'estado_mesa'=>$m->getEstadoMesa()]);
           }
        }
        $dna = [];
        foreach ($am as $i) {
            if ($i['estado_mesa'] == 'disponible'){
                array_push($dna, $i);
            }else{
               if (array_key_exists ('v'.$i['venta_id'],$dna)){
                   $dna['v'.$i['venta_id']]['mesa'] = $dna['v'.$i['venta_id']]['mesa'].$i['mesa'];
               }else{
                   $dna['v'.$i['venta_id']]['mesa'] = $i['mesa'];
                   $dna['v'.$i['venta_id']]['estado_mesa'] = 'ocupado';
                   $dna['v'.$i['venta_id']]['venta_id'] = $i['venta_id'];
               }
            }
        }
        $this['mesas'] = $dna;
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Ventas. Example:
	 * GET /ventas/edit/1
	 *
	 * @return Ventas
	 */
	function edit($id = null) {
		$this->getVentas($id)->fromArray(@$_GET);
        $this['u'] = 'ventas/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Ventas. Examples:
	 * POST /ventas/save/1
	 * POST /rest/ventas/.json
	 * PUT /rest/ventas/1.json
	 */
	function save($id = null) {

        $fecha_actual = date("Y-m-d H:m:s");
		$ventas = $this->getVentas($id);
        if (isset($_REQUEST['up'])){
            if ($_REQUEST['up'] == 'com'){
                PedidoProducto::getConnection()->query('UPDATE pedido_producto set
               created_at="'.$fecha_actual.'" Where venta_id='.$id)->execute();
                return $this->redirect('ventas');
            }
            $_REQUEST["updated_at"] = $fecha_actual;
            $ventas->fromArray($_REQUEST);
            $ventas->save();

            $this['id'] = $_REQUEST['venta_id'];

            return $this->redirect('ventas/show/'.$id);
        }

		try {

            $_REQUEST["created_at"] = $fecha_actual;
            $_REQUEST['fecha'] = $fecha_actual;

			if(!$id){
			    $_REQUEST['mesero_id'] = $_SESSION['user']->id;
            }
            $ventas->fromArray($_REQUEST);
			if ($ventas->validate()) {
				$ventas->save();
                $uid = $ventas->getId();
                $cm = count($_REQUEST['mesas']);
                if ($cm>1) {
                    foreach ($_REQUEST['mesas'] as $me) {
                        $arr['venta_id'] = $uid;
                        $arr['mesa_id'] = $me;
                        $vm = new VentasMesas();
                        $vm->fromArray($arr);
                        $vm->save();
                        $me = $this->getMesas($me);
                        $me->fromArray(['estado_mesa' => 'ocupado']);
                        $me->save();
                    }
                }else{
                    $arr['venta_id'] = $uid;
                    $arr['mesa_id'] = $_REQUEST['mesas'];
                    $vm = new VentasMesas();
                    $vm->fromArray($arr);
                    $vm->save();
                    $me = $this->getMesas($_REQUEST['mesas']);
                    $me->fromArray(['estado_mesa' => 'ocupado']);
                    $me->save();
                    $this->flash['messages'][] = 'Pedido Guardado';
                    $this->redirect('pedido-productos/?id='.$uid);
                }
                $this['id'] =$uid;
				return $this->loadView('ventas/datos',$this);
			}
			$this->flash['errors'] = $ventas->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('ventas/edit/' . $ventas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Ventas with the id. Examples:
	 * GET /ventas/show/1
	 * GET /rest/ventas/1.json
	 *
	 * @return Ventas
	 */
	function show($id = null) {
	    $this['id'] = $id;
		return $this->loadView('ventas/comanda',$this);
	}

	/**
	 * Deletes the Ventas with the id. Examples:
	 * GET /ventas/delete/1
	 * DELETE /rest/ventas/1.json
	 */
	function delete($id = null) {
		$ventas = $this->getVentas($id);
		$vm = VentasMesas::getAll('where venta_id='.$id);
		$vvvm= VentasMovimiento::getAll('where venta_id=' . $ventas->getId());
		if ($vvvm){
        $m = MovimientosDeDinero::retrieveByPK( $vvvm[0]->getMovimiento_id());
        $m->delete();
            }

		foreach ($vm as $em) {
            $me = $this->getMesas($em->mesa_id);
            $me->fromArray(['estado_mesa' => 'disponible']);
            $me->save();
        }
		try {
			if (null !== $ventas && $ventas->delete()) {
				$this['messages'][] = 'Pedido Eliminado';
			} else {
				$this['errors'][] = 'Pedido no se puede eliminar';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ventas');
		}
	}

	/**
	 * @return Ventas
	 */
	private function getVentas($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Ventas::getPrimaryKey()])) {
			$id = $_REQUEST[Ventas::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Ventas
			$this['ventas'] = new Ventas;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ventas'] = Ventas::retrieveByPK($id);
		}
		return $this['ventas'];
	}
    private function getMesas($id = null) {
        // look for id in param or in $_REQUEST array
        if (null === $id && isset($_REQUEST[Mesas::getPrimaryKey()])) {
            $id = $_REQUEST[Mesas::getPrimaryKey()];
        }

        if ('' === $id || null === $id) {
            // if no primary key provided, create new Mesas
            $this['mesas'] = new Mesas;
        } else {
            // if primary key provided, retrieve the record from the db
            $this['mesas'] = Mesas::retrieveByPK($id);
        }
        return $this['mesas'];
    }
}