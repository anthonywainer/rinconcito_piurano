<?php

use Dabl\Query\QueryPager;

class VentasMovimientosController extends ApplicationController {

	/**
	 * Returns all VentasMovimiento records matching the query. Examples:
	 * GET /ventas-movimientos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ventas-movimientos.json&limit=5
	 *
	 * @return VentasMovimiento[]
	 */
	function index() {
		$q = VentasMovimiento::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'VentasMovimiento';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['ventas_movimientos'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a VentasMovimiento. Example:
	 * GET /ventas-movimientos/edit/1
	 *
	 * @return VentasMovimiento
	 */
	function edit($id = null) {
		return $this->getVentasMovimiento($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a VentasMovimiento. Examples:
	 * POST /ventas-movimientos/save/1
	 * POST /rest/ventas-movimientos/.json
	 * PUT /rest/ventas-movimientos/1.json
	 */
	function save($id = null) {
		$ventas_movimiento = $this->getVentasMovimiento($id);

        $ca = Cajas::getAll('where estado= true');
        if (count($ca)>0){


		try {
		    if ($id){
                $ventas_movimiento->fromArray($_REQUEST);
                $ventas_movimiento->save();
                $this['idv'] = $ventas_movimiento->getVenta_id();
                return $this->loadView('pedido-productos/formas_pagos/',$this);
            }
            $rvm =VentasMovimiento::getAll('where venta_id='.$_REQUEST['venta_id'].' order by id desc ');
            if($rvm){
                if ($rvm[0]->getMovimiento()->forma_pago_id == 12){
                    $_REQUEST['forma_pago_id']=13;
                }else{
                    $_REQUEST['forma_pago_id']=12;
                }
                $_REQUEST['cuota']=$rvm[0]->getResta();
            }else{
                $_REQUEST['forma_pago_id']=12;
            }
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
            $_REQUEST['caja_id']    = $ca[0]->id;
            $_REQUEST['usuario_id'] = $_SESSION['user']->id;


            $m = new MovimientosDeDinero;
            $m->fromArray($_REQUEST);
            $m->save();
            $_REQUEST['movimiento_id'] = $m->getId();
            $_REQUEST['fecha'] = $fecha_actual;

            $ventas_movimiento->fromArray($_REQUEST);

			if ($ventas_movimiento->validate()) {
				$ventas_movimiento->save();
				$this->flash['messages'][] = 'Ventas Movimiento saved';
				$this['idv'] = $_REQUEST['venta_id'];
				return $this->loadView('pedido-productos/formas_pagos/',$this);
			}
			$this->flash['errors'] = $ventas_movimiento->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}
            $this->redirect('ventas-movimientos/edit/' . $ventas_movimiento->getId() . '?' . http_build_query($_REQUEST));
        }else {
            $this->flash['errors'][] = "Falta Aperturar Caja";
            $this->redirect('movimientos-de-dineros/index/'.$_REQUEST['venta_id']);
        }


	}

	/**
	 * Returns the VentasMovimiento with the id. Examples:
	 * GET /ventas-movimientos/show/1
	 * GET /rest/ventas-movimientos/1.json
	 *
	 * @return VentasMovimiento
	 */
	function show($id = null) {
		return $this->getVentasMovimiento($id);
	}

	/**
	 * Deletes the VentasMovimiento with the id. Examples:
	 * GET /ventas-movimientos/delete/1
	 * DELETE /rest/ventas-movimientos/1.json
	 */
	function delete($id = null) {
		$ventas_movimiento = $this->getVentasMovimiento($id);

		try {
			if (null !== $ventas_movimiento && $ventas_movimiento->delete()) {
				$this['messages'][] = 'Ventas Movimiento deleted';
				return $this->redirect('movimientos-de-dineros/index/'.$_REQUEST['venta_id']);
			} else {
				$this['errors'][] = 'Ventas Movimiento could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ventas-movimientos');
		}
	}

	/**
	 * @return VentasMovimiento
	 */
	private function getVentasMovimiento($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[VentasMovimiento::getPrimaryKey()])) {
			$id = $_REQUEST[VentasMovimiento::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new VentasMovimiento
			$this['ventas_movimiento'] = new VentasMovimiento;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ventas_movimiento'] = VentasMovimiento::retrieveByPK($id);
		}
		return $this['ventas_movimiento'];
	}

}