<?php

use Dabl\Query\QueryPager;

class DeliveryController extends ApplicationController {

	/**
	 * Returns all Deliveries records matching the query. Examples:
	 * GET /delivery?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/delivery.json&limit=5
	 *
	 * @return Deliveries[]
	 */
	function index() {
        $_GET['order_by']= 'deliveries.id';
        $_GET['dir'] = 'DESC';
		$q = Deliveries::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Deliveries';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['delivery'] = $this['pager']->fetchPage();
        $this['u'] = 'delivery/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Deliveries. Example:
	 * GET /delivery/edit/1
	 *
	 * @return Deliveries
	 */
	function edit($id = null) {
	    if($id) {
            $pp = ProductoDelivery::getAll('Where delivery_id='.$id);
            $na= [];
            foreach ($pp as $i){
                array_push($na,$i->getProducto_id());
            }
            $this->getDeliveries($id)->fromArray(@$_GET);
            $this['u'] = 'delivery/edit';
            $this['pp'] = $na;
            $this->loadView("admin/index", $this);
        }else{
	        $this->save($id=null);
        }
	}

	/**
	 * Saves a Deliveries. Examples:
	 * POST /delivery/save/1
	 * POST /rest/delivery/.json
	 * PUT /rest/delivery/1.json
	 */
	function save($id = null) {
		$deliveries = $this->getDeliveries($id);

		try {
            $fecha_actual = date("Y-m-d");
		    if ($id){
                $_REQUEST["updated_at"] = $fecha_actual;
            }

            $_REQUEST["created_at"] = $fecha_actual;
            $_REQUEST["Fecha"] = $fecha_actual;
			$deliveries->fromArray($_REQUEST);
			if ($deliveries->validate()) {
				$deliveries->save();
				$this->flash['messages'][] = 'Deliveries Guardado';
				if (isset($_REQUEST['estado_delivery'])){
                    $this->redirect('delivery/index/');
                }
				if ($id) {
                    $this->redirect('delivery/show/' . $deliveries->getId());
                }else{
                    $this->redirect('delivery/edit/' . $deliveries->getId());
                }
			}
			$this->flash['errors'] = $deliveries->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('delivery/edit/' . $deliveries->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Deliveries with the id. Examples:
	 * GET /delivery/show/1
	 * GET /rest/delivery/1.json
	 *
	 * @return Deliveries
	 */
	function show($id = null) {
        $this['id'] = $id;

        if (isset($_REQUEST['com'])){
            return $this->loadView("delivery/comanda",$this);
        }
        $this['u'] = 'delivery/conf_com';
        return $this->loadView("admin/index",$this);
	}

	/**
	 * Deletes the Deliveries with the id. Examples:
	 * GET /delivery/delete/1
	 * DELETE /rest/delivery/1.json
	 */
	function delete($id = null) {
		$deliveries = $this->getDeliveries($id);
        $vvd = VentasDelivery::getAll('where delivery_id='.$deliveries->getId());
        if ($vvd){
            $m = MovimientosDeDinero::retrieveByPK($vvd[0]->getMovimiento_id());
            $m->delete();
        }
		try {
			if (null !== $deliveries && $deliveries->delete()) {
				$this['messages'][] = 'Deliveries deleted';
			} else {
				$this['errors'][] = 'Deliveries could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('delivery');
		}
	}

	/**
	 * @return Deliveries
	 */
	private function getDeliveries($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Deliveries::getPrimaryKey()])) {
			$id = $_REQUEST[Deliveries::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Deliveries
			$this['deliveries'] = new Deliveries;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['deliveries'] = Deliveries::retrieveByPK($id);
		}
		return $this['deliveries'];
	}

}