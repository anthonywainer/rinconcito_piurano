<?php


class ReporteDeliveryController extends ApplicationController {

    function index() {
        $this['u'] = 'reportes/delivery';
        if (isset($_REQUEST['date_ini'])) {

            $begin = $_REQUEST['date_ini'];
            $end   = $_REQUEST['date_end'];

            $this['fechai'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_ini'])->format('d/m/Y');
            $this['fechae'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_end'])->format('d/m/Y');

        } else {
            $begin = date('Y-m-d');
            $end   = date('Y-m-d');
            $this['fechai'] = date("d/m/Y");
            $this['fechae'] = date("d/m/Y");
        }

        $sql = "
SELECT
deliveries.id,
ventas_delivery.fecha,
deliverista.nombres AS deliverista,
productos.nombre,
producto_delivery.cantidad,
producto_delivery.precio,
producto_delivery.total
FROM
deliveries
INNER JOIN deliverista ON deliveries.deliverista_id = deliverista.id
INNER JOIN ventas_delivery ON ventas_delivery.delivery_id = deliveries.id
INNER JOIN producto_delivery ON producto_delivery.delivery_id = deliveries.id
INNER JOIN productos ON producto_delivery.producto_id = productos.id
WHERE (ventas_delivery.fecha BETWEEN '".$begin."' AND '".$end."') GROUP BY id       
        ";
        $this['delivery'] = Deliveries::getConnection()->query($sql)->fetchAll();

        return $this->loadView("admin/index",$this);
    }

}