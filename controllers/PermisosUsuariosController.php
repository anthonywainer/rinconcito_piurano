<?php

use Dabl\Query\QueryPager;

class PermisosUsuariosController extends ApplicationController {

	/**
	 * Returns all PermisosUsuario records matching the query. Examples:
	 * GET /permisos-usuarios?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/permisos-usuarios.json&limit=5
	 *
	 * @return PermisosUsuario[]
	 */
	function index() {
		$q = PermisosUsuario::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'PermisosUsuario';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['permisos_usuarios'] = $this['pager']->fetchPage();
        $this['u'] = 'permisos-usuarios/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a PermisosUsuario. Example:
	 * GET /permisos-usuarios/edit/1
	 *
	 * @return PermisosUsuario
	 */
	function edit($id = null) {
		$this->getPermisosUsuario($id)->fromArray(@$_GET);
        $this['u'] = 'permisos-usuarios/edit';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Saves a PermisosUsuario. Examples:
	 * POST /permisos-usuarios/save/1
	 * POST /rest/permisos-usuarios/.json
	 * PUT /rest/permisos-usuarios/1.json
	 */
	function save($id = null) {
		$permisos_usuario = $this->getPermisosUsuario($id);

		try {
			$permisos_usuario->fromArray($_REQUEST);
			if ($permisos_usuario->validate()) {
				$permisos_usuario->save();
				$this->flash['messages'][] = 'Permisos Usuario saved';
				$this->redirect('permisos-usuarios/index/');
			}
			$this->flash['errors'] = $permisos_usuario->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('permisos-usuarios/edit/' . $permisos_usuario->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the PermisosUsuario with the id. Examples:
	 * GET /permisos-usuarios/show/1
	 * GET /rest/permisos-usuarios/1.json
	 *
	 * @return PermisosUsuario
	 */
	function show($id = null) {
		return $this->getPermisosUsuario($id);
	}

	/**
	 * Deletes the PermisosUsuario with the id. Examples:
	 * GET /permisos-usuarios/delete/1
	 * DELETE /rest/permisos-usuarios/1.json
	 */
	function delete($id = null) {
		$permisos_usuario = $this->getPermisosUsuario($id);

		try {
			if (null !== $permisos_usuario && $permisos_usuario->delete()) {
				$this['messages'][] = 'Permisos Usuario deleted';
			} else {
				$this['errors'][] = 'Permisos Usuario could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('permisos-usuarios');
		}
	}

	/**
	 * @return PermisosUsuario
	 */
	private function getPermisosUsuario($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[PermisosUsuario::getPrimaryKey()])) {
			$id = $_REQUEST[PermisosUsuario::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new PermisosUsuario
			$this['permisos_usuario'] = new PermisosUsuario;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['permisos_usuario'] = PermisosUsuario::retrieveByPK($id);
		}
		return $this['permisos_usuario'];
	}

}