<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseSeries extends ApplicationModel {

	const ID = 'series.id';
	const SERIE = 'series.serie';
	const CORRELATIVO = 'series.correlativo';
	const VOUCHER_ID = 'series.voucher_id';
	const CREATED_AT = 'series.created_at';
	const UPDATED_AT = 'series.updated_at';
	const DELETED_AT = 'series.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'series';

	/**
	 * Cache of objects retrieved from the database
	 * @var Series[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'serie' => Model::COLUMN_TYPE_INTEGER,
		'correlativo' => Model::COLUMN_TYPE_INTEGER,
		'voucher_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `serie` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $serie;

	/**
	 * `correlativo` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $correlativo;

	/**
	 * `voucher_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $voucher_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Series
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the serie field
	 */
	function getSerie() {
		return $this->serie;
	}

	/**
	 * Sets the value of the serie field
	 * @return Series
	 */
	function setSerie($value) {
		return $this->setColumnValue('serie', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the correlativo field
	 */
	function getCorrelativo() {
		return $this->correlativo;
	}

	/**
	 * Sets the value of the correlativo field
	 * @return Series
	 */
	function setCorrelativo($value) {
		return $this->setColumnValue('correlativo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the voucher_id field
	 */
	function getVoucherId() {
		return $this->voucher_id;
	}

	/**
	 * Sets the value of the voucher_id field
	 * @return Series
	 */
	function setVoucherId($value) {
		return $this->setColumnValue('voucher_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Series::getVoucherId
	 * final because getVoucherId should be extended instead
	 * to ensure consistent behavior
	 * @see Series::getVoucherId
	 */
	final function getVoucher_id() {
		return $this->getVoucherId();
	}

	/**
	 * Convenience function for Series::setVoucherId
	 * final because setVoucherId should be extended instead
	 * to ensure consistent behavior
	 * @see Series::setVoucherId
	 * @return Series
	 */
	final function setVoucher_id($value) {
		return $this->setVoucherId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Series
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Series::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Series::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::setCreatedAt
	 * @return Series
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Series
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Series::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Series::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::setUpdatedAt
	 * @return Series
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Series
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Series::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Series::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Series::setDeletedAt
	 * @return Series
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Series
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Series
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveById($value) {
		return Series::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a serie
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveBySerie($value) {
		return static::retrieveByColumn('serie', $value);
	}

	/**
	 * Searches the database for a row with a correlativo
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveByCorrelativo($value) {
		return static::retrieveByColumn('correlativo', $value);
	}

	/**
	 * Searches the database for a row with a voucher_id
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveByVoucherId($value) {
		return static::retrieveByColumn('voucher_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Series
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Series
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->serie = (null === $this->serie) ? null : (int) $this->serie;
		$this->correlativo = (null === $this->correlativo) ? null : (int) $this->correlativo;
		$this->voucher_id = (null === $this->voucher_id) ? null : (int) $this->voucher_id;
		return $this;
	}

	/**
	 * @return Series
	 */
	function setVoucher(Vouchers $vouchers = null) {
		return $this->setVouchersRelatedByVoucherId($vouchers);
	}

	/**
	 * @return Series
	 */
	function setVouchersRelatedByVoucherId(Vouchers $vouchers = null) {
		if (null === $vouchers) {
			$this->setvoucher_id(null);
		} else {
			if (!$vouchers->getid()) {
				throw new Exception('Cannot connect a Vouchers without a id');
			}
			$this->setvoucher_id($vouchers->getid());
		}
		return $this;
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVoucher() {
		return $this->getVouchersRelatedByVoucherId();
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVouchersRelatedByVoucherId() {
		$fk_value = $this->getvoucher_id();
		if (null === $fk_value) {
			return null;
		}
		return Vouchers::retrieveByPK($fk_value);
	}

	static function doSelectJoinVoucher(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVouchersRelatedByVoucherId($q, $join_type);
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVouchers() {
		return $this->getVouchersRelatedByVoucherId();
	}

	/**
	 * @return Series
	 */
	function setVouchers(Vouchers $vouchers = null) {
		return $this->setVouchersRelatedByVoucherId($vouchers);
	}

	/**
	 * @return Series[]
	 */
	static function doSelectJoinVouchersRelatedByVoucherId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Vouchers::getTableName();
		$q->join($to_table, $this_table . '.voucher_id = ' . $to_table . '.id', $join_type);
		foreach (Vouchers::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Vouchers'));
	}

	/**
	 * @return Series[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Vouchers::getTableName();
		$q->join($to_table, $this_table . '.voucher_id = ' . $to_table . '.id', $join_type);
		foreach (Vouchers::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Vouchers';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getserie()) {
			$this->_validationErrors[] = 'serie must not be null';
		}
		if (null === $this->getcorrelativo()) {
			$this->_validationErrors[] = 'correlativo must not be null';
		}
		if (null === $this->getvoucher_id()) {
			$this->_validationErrors[] = 'voucher_id must not be null';

		}
		return 0 === count($this->_validationErrors);
	}

}