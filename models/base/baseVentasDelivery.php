<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentasDelivery extends ApplicationModel {

	const ID = 'ventas_delivery.id';
	const MOVIMIENTO_ID = 'ventas_delivery.movimiento_id';
	const DELIVERY_ID = 'ventas_delivery.delivery_id';
	const SUBTOTAL = 'ventas_delivery.subtotal';
	const FECHA = 'ventas_delivery.fecha';
	const DESCUENTO = 'ventas_delivery.descuento';
	const PORCENTAJE = 'ventas_delivery.porcentaje';
	const TOTAL = 'ventas_delivery.total';
	const EFECTIVO = 'ventas_delivery.efectivo';
	const CLIENTE_ID = 'ventas_delivery.cliente_id';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ventas_delivery';

	/**
	 * Cache of objects retrieved from the database
	 * @var VentasDelivery[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
		'delivery_id' => Model::COLUMN_TYPE_INTEGER,
		'subtotal' => Model::COLUMN_TYPE_FLOAT,
		'fecha' => Model::COLUMN_TYPE_TIMESTAMP,
		'descuento' => Model::COLUMN_TYPE_FLOAT,
		'porcentaje' => Model::COLUMN_TYPE_INTEGER,
		'total' => Model::COLUMN_TYPE_FLOAT,
		'efectivo' => Model::COLUMN_TYPE_INTEGER,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `movimiento_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $movimiento_id;

	/**
	 * `delivery_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $delivery_id;

	/**
	 * `subtotal` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $subtotal;

	/**
	 * `fecha` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha;

	/**
	 * `descuento` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $descuento;

	/**
	 * `porcentaje` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $porcentaje;

	/**
	 * `total` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $total;

	/**
	 * `efectivo` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $efectivo;

	/**
	 * `cliente_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return VentasDelivery
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the movimiento_id field
	 */
	function getMovimientoId() {
		return $this->movimiento_id;
	}

	/**
	 * Sets the value of the movimiento_id field
	 * @return VentasDelivery
	 */
	function setMovimientoId($value) {
		return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasDelivery::getMovimientoId
	 * final because getMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::getMovimientoId
	 */
	final function getMovimiento_id() {
		return $this->getMovimientoId();
	}

	/**
	 * Convenience function for VentasDelivery::setMovimientoId
	 * final because setMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::setMovimientoId
	 * @return VentasDelivery
	 */
	final function setMovimiento_id($value) {
		return $this->setMovimientoId($value);
	}

	/**
	 * Gets the value of the delivery_id field
	 */
	function getDeliveryId() {
		return $this->delivery_id;
	}

	/**
	 * Sets the value of the delivery_id field
	 * @return VentasDelivery
	 */
	function setDeliveryId($value) {
		return $this->setColumnValue('delivery_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasDelivery::getDeliveryId
	 * final because getDeliveryId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::getDeliveryId
	 */
	final function getDelivery_id() {
		return $this->getDeliveryId();
	}

	/**
	 * Convenience function for VentasDelivery::setDeliveryId
	 * final because setDeliveryId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::setDeliveryId
	 * @return VentasDelivery
	 */
	final function setDelivery_id($value) {
		return $this->setDeliveryId($value);
	}

	/**
	 * Gets the value of the subtotal field
	 */
	function getSubtotal() {
		return $this->subtotal;
	}

	/**
	 * Sets the value of the subtotal field
	 * @return VentasDelivery
	 */
	function setSubtotal($value) {
		return $this->setColumnValue('subtotal', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return VentasDelivery
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Gets the value of the descuento field
	 */
	function getDescuento() {
		return $this->descuento;
	}

	/**
	 * Sets the value of the descuento field
	 * @return VentasDelivery
	 */
	function setDescuento($value) {
		return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the porcentaje field
	 */
	function getPorcentaje() {
		return $this->porcentaje;
	}

	/**
	 * Sets the value of the porcentaje field
	 * @return VentasDelivery
	 */
	function setPorcentaje($value) {
		return $this->setColumnValue('porcentaje', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the total field
	 */
	function getTotal() {
		return $this->total;
	}

	/**
	 * Sets the value of the total field
	 * @return VentasDelivery
	 */
	function setTotal($value) {
		return $this->setColumnValue('total', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the efectivo field
	 */
	function getEfectivo() {
		return $this->efectivo;
	}

	/**
	 * Sets the value of the efectivo field
	 * @return VentasDelivery
	 */
	function setEfectivo($value) {
		return $this->setColumnValue('efectivo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return VentasDelivery
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasDelivery::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for VentasDelivery::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasDelivery::setClienteId
	 * @return VentasDelivery
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return VentasDelivery
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return VentasDelivery
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveById($value) {
		return VentasDelivery::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a movimiento_id
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByMovimientoId($value) {
		return static::retrieveByColumn('movimiento_id', $value);
	}

	/**
	 * Searches the database for a row with a delivery_id
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByDeliveryId($value) {
		return static::retrieveByColumn('delivery_id', $value);
	}

	/**
	 * Searches the database for a row with a subtotal
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveBySubtotal($value) {
		return static::retrieveByColumn('subtotal', $value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}

	/**
	 * Searches the database for a row with a descuento
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByDescuento($value) {
		return static::retrieveByColumn('descuento', $value);
	}

	/**
	 * Searches the database for a row with a porcentaje
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByPorcentaje($value) {
		return static::retrieveByColumn('porcentaje', $value);
	}

	/**
	 * Searches the database for a row with a total
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByTotal($value) {
		return static::retrieveByColumn('total', $value);
	}

	/**
	 * Searches the database for a row with a efectivo
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByEfectivo($value) {
		return static::retrieveByColumn('efectivo', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return VentasDelivery
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return VentasDelivery
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
		$this->delivery_id = (null === $this->delivery_id) ? null : (int) $this->delivery_id;
		$this->porcentaje = (null === $this->porcentaje) ? null : (int) $this->porcentaje;
		$this->efectivo = (null === $this->efectivo) ? null : (int) $this->efectivo;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		return $this;
	}

	/**
	 * @return VentasDelivery
	 */
	function setMovimiento(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasDelivery
	 */
	function setMovimientosDeDineroRelatedByMovimientoId(MovimientosDeDinero $movimientosdedinero = null) {
		if (null === $movimientosdedinero) {
			$this->setmovimiento_id(null);
		} else {
			if (!$movimientosdedinero->getid()) {
				throw new Exception('Cannot connect a MovimientosDeDinero without a id');
			}
			$this->setmovimiento_id($movimientosdedinero->getid());
		}
		return $this;
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimiento() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDineroRelatedByMovimientoId() {
		$fk_value = $this->getmovimiento_id();
		if (null === $fk_value) {
			return null;
		}
		return MovimientosDeDinero::retrieveByPK($fk_value);
	}

	static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMovimientosDeDineroRelatedByMovimientoId($q, $join_type);
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDinero() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * @return VentasDelivery
	 */
	function setMovimientosDeDinero(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasDelivery[]
	 */
	static function doSelectJoinMovimientosDeDineroRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('MovimientosDeDinero'));
	}

	/**
	 * @return VentasDelivery
	 */
	function setDelivery(Deliveries $deliveries = null) {
		return $this->setDeliveriesRelatedByDeliveryId($deliveries);
	}

	/**
	 * @return VentasDelivery
	 */
	function setDeliveriesRelatedByDeliveryId(Deliveries $deliveries = null) {
		if (null === $deliveries) {
			$this->setdelivery_id(null);
		} else {
			if (!$deliveries->getid()) {
				throw new Exception('Cannot connect a Deliveries without a id');
			}
			$this->setdelivery_id($deliveries->getid());
		}
		return $this;
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDelivery() {
		return $this->getDeliveriesRelatedByDeliveryId();
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDeliveriesRelatedByDeliveryId() {
		$fk_value = $this->getdelivery_id();
		if (null === $fk_value) {
			return null;
		}
		return Deliveries::retrieveByPK($fk_value);
	}

	static function doSelectJoinDelivery(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinDeliveriesRelatedByDeliveryId($q, $join_type);
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDeliveries() {
		return $this->getDeliveriesRelatedByDeliveryId();
	}

	/**
	 * @return VentasDelivery
	 */
	function setDeliveries(Deliveries $deliveries = null) {
		return $this->setDeliveriesRelatedByDeliveryId($deliveries);
	}

	/**
	 * @return VentasDelivery[]
	 */
	static function doSelectJoinDeliveriesRelatedByDeliveryId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Deliveries::getTableName();
		$q->join($to_table, $this_table . '.delivery_id = ' . $to_table . '.id', $join_type);
		foreach (Deliveries::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Deliveries'));
	}

	/**
	 * @return VentasDelivery
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return VentasDelivery
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return VentasDelivery
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return VentasDelivery[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return VentasDelivery[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'MovimientosDeDinero';
	
		$to_table = Deliveries::getTableName();
		$q->join($to_table, $this_table . '.delivery_id = ' . $to_table . '.id', $join_type);
		foreach (Deliveries::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Deliveries';
	
		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getmovimiento_id()) {
			$this->_validationErrors[] = 'movimiento_id must not be null';
		}
		if (null === $this->getdelivery_id()) {
			$this->_validationErrors[] = 'delivery_id must not be null';
		}
		if (null === $this->getsubtotal()) {
			$this->_validationErrors[] = 'subtotal must not be null';
		}
		if (null === $this->getfecha()) {
			$this->_validationErrors[] = 'fecha must not be null';
		}
		if (null === $this->gettotal()) {
			$this->_validationErrors[] = 'total must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}