<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMesas extends ApplicationModel {

	const ID = 'mesas.id';
	const NUMERO = 'mesas.numero';
	const ESTADO_MESA = 'mesas.estado_mesa';
	const MESA = 'mesas.mesa';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'mesas';

	/**
	 * Cache of objects retrieved from the database
	 * @var Mesas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero' => Model::COLUMN_TYPE_INTEGER,
		'estado_mesa' => Model::COLUMN_TYPE_VARCHAR,
		'mesa' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $numero;

	/**
	 * `estado_mesa` VARCHAR NOT NULL DEFAULT 'disponible'
	 * @var string
	 */
	protected $estado_mesa = 'disponible';

	/**
	 * `mesa` VARCHAR NOT NULL
	 * @var string
	 */
	protected $mesa;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Mesas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero field
	 */
	function getNumero() {
		return $this->numero;
	}

	/**
	 * Sets the value of the numero field
	 * @return Mesas
	 */
	function setNumero($value) {
		return $this->setColumnValue('numero', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the estado_mesa field
	 */
	function getEstadoMesa() {
		return $this->estado_mesa;
	}

	/**
	 * Sets the value of the estado_mesa field
	 * @return Mesas
	 */
	function setEstadoMesa($value) {
		return $this->setColumnValue('estado_mesa', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Mesas::getEstadoMesa
	 * final because getEstadoMesa should be extended instead
	 * to ensure consistent behavior
	 * @see Mesas::getEstadoMesa
	 */
	final function getEstado_mesa() {
		return $this->getEstadoMesa();
	}

	/**
	 * Convenience function for Mesas::setEstadoMesa
	 * final because setEstadoMesa should be extended instead
	 * to ensure consistent behavior
	 * @see Mesas::setEstadoMesa
	 * @return Mesas
	 */
	final function setEstado_mesa($value) {
		return $this->setEstadoMesa($value);
	}

	/**
	 * Gets the value of the mesa field
	 */
	function getMesa() {
		return $this->mesa;
	}

	/**
	 * Sets the value of the mesa field
	 * @return Mesas
	 */
	function setMesa($value) {
		return $this->setColumnValue('mesa', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Mesas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Mesas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Mesas
	 */
	static function retrieveById($value) {
		return Mesas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero
	 * value that matches the one provided
	 * @return Mesas
	 */
	static function retrieveByNumero($value) {
		return static::retrieveByColumn('numero', $value);
	}

	/**
	 * Searches the database for a row with a estado_mesa
	 * value that matches the one provided
	 * @return Mesas
	 */
	static function retrieveByEstadoMesa($value) {
		return static::retrieveByColumn('estado_mesa', $value);
	}

	/**
	 * Searches the database for a row with a mesa
	 * value that matches the one provided
	 * @return Mesas
	 */
	static function retrieveByMesa($value) {
		return static::retrieveByColumn('mesa', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Mesas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->numero = (null === $this->numero) ? null : (int) $this->numero;
		return $this;
	}

	/**
	 * @return Mesas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting ventas_mesas Objects(rows) from the ventas_mesas table
	 * with a mesa_id that matches $this->id.
	 * @return Query
	 */
	function getVentasMesassRelatedByMesaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_mesas', 'mesa_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasMesas Objects(rows) from the ventas_mesas table
	 * with a mesa_id that matches $this->id.
	 * @return int
	 */
	function countVentasMesassRelatedByMesaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasMesas::doCount($this->getVentasMesassRelatedByMesaIdQuery($q));
	}

	/**
	 * Deletes the ventas_mesas Objects(rows) from the ventas_mesas table
	 * with a mesa_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasMesassRelatedByMesaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasMesassRelatedByMesaId_c = array();
		return VentasMesas::doDelete($this->getVentasMesassRelatedByMesaIdQuery($q));
	}

	protected $VentasMesassRelatedByMesaId_c = array();

	/**
	 * Returns an array of VentasMesas objects with a mesa_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasMesas[]
	 */
	function getVentasMesassRelatedByMesaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasMesassRelatedByMesaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasMesassRelatedByMesaId_c;
		}

		$result = VentasMesas::doSelect($this->getVentasMesassRelatedByMesaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasMesassRelatedByMesaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Mesas::getVentasMesassRelatedBymesa_id
	 * @return VentasMesas[]
	 * @see Mesas::getVentasMesassRelatedByMesaId
	 */
	function getVentasMesass($extra = null) {
		return $this->getVentasMesassRelatedByMesaId($extra);
	}

	/**
	  * Convenience function for Mesas::getVentasMesassRelatedBymesa_idQuery
	  * @return Query
	  * @see Mesas::getVentasMesassRelatedBymesa_idQuery
	  */
	function getVentasMesassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_mesas', 'mesa_id','id', $q);
	}

	/**
	  * Convenience function for Mesas::deleteVentasMesassRelatedBymesa_id
	  * @return int
	  * @see Mesas::deleteVentasMesassRelatedBymesa_id
	  */
	function deleteVentasMesass(Query $q = null) {
		return $this->deleteVentasMesassRelatedByMesaId($q);
	}

	/**
	  * Convenience function for Mesas::countVentasMesassRelatedBymesa_id
	  * @return int
	  * @see Mesas::countVentasMesassRelatedByMesaId
	  */
	function countVentasMesass(Query $q = null) {
		return $this->countVentasMesassRelatedByMesaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnumero()) {
			$this->_validationErrors[] = 'numero must not be null';
		}
		if (null === $this->getmesa()) {
			$this->_validationErrors[] = 'mesa must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}