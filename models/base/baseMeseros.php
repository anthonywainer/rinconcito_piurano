<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMeseros extends ApplicationModel {

	const ID = 'meseros.id';
	const NOMBRES = 'meseros.nombres';
	const APELLIDOS = 'meseros.apellidos';
	const SEXO = 'meseros.sexo';
	const SUELDO_BASICO = 'meseros.sueldo_basico';
	const CREATED_AT = 'meseros.created_at';
	const UPDATED_AT = 'meseros.updated_at';
	const DELETED_AT = 'meseros.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'meseros';

	/**
	 * Cache of objects retrieved from the database
	 * @var Meseros[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'sexo' => Model::COLUMN_TYPE_VARCHAR,
		'sueldo_basico' => Model::COLUMN_TYPE_FLOAT,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR NOT NULL
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `sexo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $sexo;

	/**
	 * `sueldo_basico` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $sueldo_basico;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Meseros
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Meseros
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Meseros
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the sexo field
	 */
	function getSexo() {
		return $this->sexo;
	}

	/**
	 * Sets the value of the sexo field
	 * @return Meseros
	 */
	function setSexo($value) {
		return $this->setColumnValue('sexo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the sueldo_basico field
	 */
	function getSueldoBasico() {
		return $this->sueldo_basico;
	}

	/**
	 * Sets the value of the sueldo_basico field
	 * @return Meseros
	 */
	function setSueldoBasico($value) {
		return $this->setColumnValue('sueldo_basico', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Convenience function for Meseros::getSueldoBasico
	 * final because getSueldoBasico should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::getSueldoBasico
	 */
	final function getSueldo_basico() {
		return $this->getSueldoBasico();
	}

	/**
	 * Convenience function for Meseros::setSueldoBasico
	 * final because setSueldoBasico should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::setSueldoBasico
	 * @return Meseros
	 */
	final function setSueldo_basico($value) {
		return $this->setSueldoBasico($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Meseros
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Meseros::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Meseros::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::setCreatedAt
	 * @return Meseros
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Meseros
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Meseros::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Meseros::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::setUpdatedAt
	 * @return Meseros
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Meseros
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Meseros::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Meseros::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Meseros::setDeletedAt
	 * @return Meseros
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Meseros
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Meseros
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveById($value) {
		return Meseros::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a sexo
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveBySexo($value) {
		return static::retrieveByColumn('sexo', $value);
	}

	/**
	 * Searches the database for a row with a sueldo_basico
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveBySueldoBasico($value) {
		return static::retrieveByColumn('sueldo_basico', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Meseros
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Meseros
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Meseros[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting ventas Objects(rows) from the ventas table
	 * with a mesero_id that matches $this->id.
	 * @return Query
	 */
	function getVentassRelatedByMeseroIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'mesero_id', 'id', $q);
	}

	/**
	 * Returns the count of Ventas Objects(rows) from the ventas table
	 * with a mesero_id that matches $this->id.
	 * @return int
	 */
	function countVentassRelatedByMeseroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Ventas::doCount($this->getVentassRelatedByMeseroIdQuery($q));
	}

	/**
	 * Deletes the ventas Objects(rows) from the ventas table
	 * with a mesero_id that matches $this->id.
	 * @return int
	 */
	function deleteVentassRelatedByMeseroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentassRelatedByMeseroId_c = array();
		return Ventas::doDelete($this->getVentassRelatedByMeseroIdQuery($q));
	}

	protected $VentassRelatedByMeseroId_c = array();

	/**
	 * Returns an array of Ventas objects with a mesero_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Ventas[]
	 */
	function getVentassRelatedByMeseroId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentassRelatedByMeseroId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentassRelatedByMeseroId_c;
		}

		$result = Ventas::doSelect($this->getVentassRelatedByMeseroIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentassRelatedByMeseroId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Meseros::getVentassRelatedBymesero_id
	 * @return Ventas[]
	 * @see Meseros::getVentassRelatedByMeseroId
	 */
	function getVentass($extra = null) {
		return $this->getVentassRelatedByMeseroId($extra);
	}

	/**
	  * Convenience function for Meseros::getVentassRelatedBymesero_idQuery
	  * @return Query
	  * @see Meseros::getVentassRelatedBymesero_idQuery
	  */
	function getVentassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'mesero_id','id', $q);
	}

	/**
	  * Convenience function for Meseros::deleteVentassRelatedBymesero_id
	  * @return int
	  * @see Meseros::deleteVentassRelatedBymesero_id
	  */
	function deleteVentass(Query $q = null) {
		return $this->deleteVentassRelatedByMeseroId($q);
	}

	/**
	  * Convenience function for Meseros::countVentassRelatedBymesero_id
	  * @return int
	  * @see Meseros::countVentassRelatedByMeseroId
	  */
	function countVentass(Query $q = null) {
		return $this->countVentassRelatedByMeseroId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}
		if (null === $this->getapellidos()) {
			$this->_validationErrors[] = 'apellidos must not be null';
		}
		if (null === $this->getsexo()) {
			$this->_validationErrors[] = 'sexo must not be null';
		}
		if (null === $this->getsueldo_basico()) {
			$this->_validationErrors[] = 'sueldo_basico must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}