<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductos extends ApplicationModel {

	const ID = 'productos.id';
	const NOMBRE = 'productos.nombre';
	const PRECIO = 'productos.precio';
	const TIPO_PRODUCTO_ID = 'productos.tipo_producto_id';
	const CREATED_AT = 'productos.created_at';
	const UPDATED_AT = 'productos.updated_at';
	const DELETED_AT = 'productos.deleted_at';
	const DESCRIPCION = 'productos.descripcion';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'productos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Productos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'precio' => Model::COLUMN_TYPE_FLOAT,
		'tipo_producto_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `precio` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $precio;

	/**
	 * `tipo_producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $tipo_producto_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `descripcion` VARCHAR
	 * @var string
	 */
	protected $descripcion;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Productos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Productos
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return Productos
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the tipo_producto_id field
	 */
	function getTipoProductoId() {
		return $this->tipo_producto_id;
	}

	/**
	 * Sets the value of the tipo_producto_id field
	 * @return Productos
	 */
	function setTipoProductoId($value) {
		return $this->setColumnValue('tipo_producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Productos::getTipoProductoId
	 * final because getTipoProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getTipoProductoId
	 */
	final function getTipo_producto_id() {
		return $this->getTipoProductoId();
	}

	/**
	 * Convenience function for Productos::setTipoProductoId
	 * final because setTipoProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setTipoProductoId
	 * @return Productos
	 */
	final function setTipo_producto_id($value) {
		return $this->setTipoProductoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Productos
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Productos::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setCreatedAt
	 * @return Productos
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Productos
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Productos::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setUpdatedAt
	 * @return Productos
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Productos
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Productos::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setDeletedAt
	 * @return Productos
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Productos
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Productos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Productos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveById($value) {
		return Productos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}

	/**
	 * Searches the database for a row with a tipo_producto_id
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByTipoProductoId($value) {
		return static::retrieveByColumn('tipo_producto_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Productos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->tipo_producto_id = (null === $this->tipo_producto_id) ? null : (int) $this->tipo_producto_id;
		return $this;
	}

	/**
	 * @return Productos
	 */
	function setTipoProducto(TiposProducto $tiposproducto = null) {
		return $this->setTiposProductoRelatedByTipoProductoId($tiposproducto);
	}

	/**
	 * @return Productos
	 */
	function setTiposProductoRelatedByTipoProductoId(TiposProducto $tiposproducto = null) {
		if (null === $tiposproducto) {
			$this->settipo_producto_id(null);
		} else {
			if (!$tiposproducto->getid()) {
				throw new Exception('Cannot connect a TiposProducto without a id');
			}
			$this->settipo_producto_id($tiposproducto->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos_producto object with a id
	 * that matches $this->tipo_producto_id.
	 * @return TiposProducto
	 */
	function getTipoProducto() {
		return $this->getTiposProductoRelatedByTipoProductoId();
	}

	/**
	 * Returns a tipos_producto object with a id
	 * that matches $this->tipo_producto_id.
	 * @return TiposProducto
	 */
	function getTiposProductoRelatedByTipoProductoId() {
		$fk_value = $this->gettipo_producto_id();
		if (null === $fk_value) {
			return null;
		}
		return TiposProducto::retrieveByPK($fk_value);
	}

	static function doSelectJoinTipoProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTiposProductoRelatedByTipoProductoId($q, $join_type);
	}

	/**
	 * Returns a tipos_producto object with a id
	 * that matches $this->tipo_producto_id.
	 * @return TiposProducto
	 */
	function getTiposProducto() {
		return $this->getTiposProductoRelatedByTipoProductoId();
	}

	/**
	 * @return Productos
	 */
	function setTiposProducto(TiposProducto $tiposproducto = null) {
		return $this->setTiposProductoRelatedByTipoProductoId($tiposproducto);
	}

	/**
	 * @return Productos[]
	 */
	static function doSelectJoinTiposProductoRelatedByTipoProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposProducto::getTableName();
		$q->join($to_table, $this_table . '.tipo_producto_id = ' . $to_table . '.id', $join_type);
		foreach (TiposProducto::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TiposProducto'));
	}

	/**
	 * @return Productos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposProducto::getTableName();
		$q->join($to_table, $this_table . '.tipo_producto_id = ' . $to_table . '.id', $join_type);
		foreach (TiposProducto::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TiposProducto';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting pedido_producto Objects(rows) from the pedido_producto table
	 * with a producto_id that matches $this->id.
	 * @return Query
	 */
	function getPedidoProductosRelatedByProductoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('pedido_producto', 'producto_id', 'id', $q);
	}

	/**
	 * Returns the count of PedidoProducto Objects(rows) from the pedido_producto table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function countPedidoProductosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PedidoProducto::doCount($this->getPedidoProductosRelatedByProductoIdQuery($q));
	}

	/**
	 * Deletes the pedido_producto Objects(rows) from the pedido_producto table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function deletePedidoProductosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PedidoProductosRelatedByProductoId_c = array();
		return PedidoProducto::doDelete($this->getPedidoProductosRelatedByProductoIdQuery($q));
	}

	protected $PedidoProductosRelatedByProductoId_c = array();

	/**
	 * Returns an array of PedidoProducto objects with a producto_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PedidoProducto[]
	 */
	function getPedidoProductosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PedidoProductosRelatedByProductoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PedidoProductosRelatedByProductoId_c;
		}

		$result = PedidoProducto::doSelect($this->getPedidoProductosRelatedByProductoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PedidoProductosRelatedByProductoId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting producto_delivery Objects(rows) from the producto_delivery table
	 * with a producto_id that matches $this->id.
	 * @return Query
	 */
	function getProductoDeliverysRelatedByProductoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_delivery', 'producto_id', 'id', $q);
	}

	/**
	 * Returns the count of ProductoDelivery Objects(rows) from the producto_delivery table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function countProductoDeliverysRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ProductoDelivery::doCount($this->getProductoDeliverysRelatedByProductoIdQuery($q));
	}

	/**
	 * Deletes the producto_delivery Objects(rows) from the producto_delivery table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function deleteProductoDeliverysRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductoDeliverysRelatedByProductoId_c = array();
		return ProductoDelivery::doDelete($this->getProductoDeliverysRelatedByProductoIdQuery($q));
	}

	protected $ProductoDeliverysRelatedByProductoId_c = array();

	/**
	 * Returns an array of ProductoDelivery objects with a producto_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ProductoDelivery[]
	 */
	function getProductoDeliverysRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductoDeliverysRelatedByProductoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductoDeliverysRelatedByProductoId_c;
		}

		$result = ProductoDelivery::doSelect($this->getProductoDeliverysRelatedByProductoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductoDeliverysRelatedByProductoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Productos::getPedidoProductosRelatedByproducto_id
	 * @return PedidoProducto[]
	 * @see Productos::getPedidoProductosRelatedByProductoId
	 */
	function getPedidoProductos($extra = null) {
		return $this->getPedidoProductosRelatedByProductoId($extra);
	}

	/**
	  * Convenience function for Productos::getPedidoProductosRelatedByproducto_idQuery
	  * @return Query
	  * @see Productos::getPedidoProductosRelatedByproducto_idQuery
	  */
	function getPedidoProductosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('pedido_producto', 'producto_id','id', $q);
	}

	/**
	  * Convenience function for Productos::deletePedidoProductosRelatedByproducto_id
	  * @return int
	  * @see Productos::deletePedidoProductosRelatedByproducto_id
	  */
	function deletePedidoProductos(Query $q = null) {
		return $this->deletePedidoProductosRelatedByProductoId($q);
	}

	/**
	  * Convenience function for Productos::countPedidoProductosRelatedByproducto_id
	  * @return int
	  * @see Productos::countPedidoProductosRelatedByProductoId
	  */
	function countPedidoProductos(Query $q = null) {
		return $this->countPedidoProductosRelatedByProductoId($q);
	}

	/**
	 * Convenience function for Productos::getProductoDeliverysRelatedByproducto_id
	 * @return ProductoDelivery[]
	 * @see Productos::getProductoDeliverysRelatedByProductoId
	 */
	function getProductoDeliverys($extra = null) {
		return $this->getProductoDeliverysRelatedByProductoId($extra);
	}

	/**
	  * Convenience function for Productos::getProductoDeliverysRelatedByproducto_idQuery
	  * @return Query
	  * @see Productos::getProductoDeliverysRelatedByproducto_idQuery
	  */
	function getProductoDeliverysQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_delivery', 'producto_id','id', $q);
	}

	/**
	  * Convenience function for Productos::deleteProductoDeliverysRelatedByproducto_id
	  * @return int
	  * @see Productos::deleteProductoDeliverysRelatedByproducto_id
	  */
	function deleteProductoDeliverys(Query $q = null) {
		return $this->deleteProductoDeliverysRelatedByProductoId($q);
	}

	/**
	  * Convenience function for Productos::countProductoDeliverysRelatedByproducto_id
	  * @return int
	  * @see Productos::countProductoDeliverysRelatedByProductoId
	  */
	function countProductoDeliverys(Query $q = null) {
		return $this->countProductoDeliverysRelatedByProductoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->getprecio()) {
			$this->_validationErrors[] = 'precio must not be null';
		}
		if (null === $this->gettipo_producto_id()) {
			$this->_validationErrors[] = 'tipo_producto_id must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}