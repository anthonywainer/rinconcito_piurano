<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMovimientosDeDinero extends ApplicationModel {

	const ID = 'movimientos_de_dinero.id';
	const NUMERO_VOUCHER = 'movimientos_de_dinero.numero_voucher';
	const VOUCHER_ID = 'movimientos_de_dinero.voucher_id';
	const CONCEPTO_DE_PAGO_ID = 'movimientos_de_dinero.concepto_de_pago_id';
	const CAJA_ID = 'movimientos_de_dinero.caja_id';
	const USUARIO_ID = 'movimientos_de_dinero.usuario_id';
	const FORMA_PAGO_ID = 'movimientos_de_dinero.forma_pago_id';
	const CREATED_AT = 'movimientos_de_dinero.created_at';
	const UPDATED_AT = 'movimientos_de_dinero.updated_at';
	const DELETED_AT = 'movimientos_de_dinero.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'movimientos_de_dinero';

	/**
	 * Cache of objects retrieved from the database
	 * @var MovimientosDeDinero[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero_voucher' => Model::COLUMN_TYPE_INTEGER,
		'voucher_id' => Model::COLUMN_TYPE_INTEGER,
		'concepto_de_pago_id' => Model::COLUMN_TYPE_INTEGER,
		'caja_id' => Model::COLUMN_TYPE_INTEGER,
		'usuario_id' => Model::COLUMN_TYPE_INTEGER,
		'forma_pago_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero_voucher` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $numero_voucher;

	/**
	 * `voucher_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $voucher_id;

	/**
	 * `concepto_de_pago_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $concepto_de_pago_id;

	/**
	 * `caja_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $caja_id;

	/**
	 * `usuario_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $usuario_id;

	/**
	 * `forma_pago_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $forma_pago_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return MovimientosDeDinero
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero_voucher field
	 */
	function getNumeroVoucher() {
		return $this->numero_voucher;
	}

	/**
	 * Sets the value of the numero_voucher field
	 * @return MovimientosDeDinero
	 */
	function setNumeroVoucher($value) {
		return $this->setColumnValue('numero_voucher', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getNumeroVoucher
	 * final because getNumeroVoucher should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getNumeroVoucher
	 */
	final function getNumero_voucher() {
		return $this->getNumeroVoucher();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setNumeroVoucher
	 * final because setNumeroVoucher should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setNumeroVoucher
	 * @return MovimientosDeDinero
	 */
	final function setNumero_voucher($value) {
		return $this->setNumeroVoucher($value);
	}

	/**
	 * Gets the value of the voucher_id field
	 */
	function getVoucherId() {
		return $this->voucher_id;
	}

	/**
	 * Sets the value of the voucher_id field
	 * @return MovimientosDeDinero
	 */
	function setVoucherId($value) {
		return $this->setColumnValue('voucher_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getVoucherId
	 * final because getVoucherId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getVoucherId
	 */
	final function getVoucher_id() {
		return $this->getVoucherId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setVoucherId
	 * final because setVoucherId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setVoucherId
	 * @return MovimientosDeDinero
	 */
	final function setVoucher_id($value) {
		return $this->setVoucherId($value);
	}

	/**
	 * Gets the value of the concepto_de_pago_id field
	 */
	function getConceptoDePagoId() {
		return $this->concepto_de_pago_id;
	}

	/**
	 * Sets the value of the concepto_de_pago_id field
	 * @return MovimientosDeDinero
	 */
	function setConceptoDePagoId($value) {
		return $this->setColumnValue('concepto_de_pago_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getConceptoDePagoId
	 * final because getConceptoDePagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getConceptoDePagoId
	 */
	final function getConcepto_de_pago_id() {
		return $this->getConceptoDePagoId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setConceptoDePagoId
	 * final because setConceptoDePagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setConceptoDePagoId
	 * @return MovimientosDeDinero
	 */
	final function setConcepto_de_pago_id($value) {
		return $this->setConceptoDePagoId($value);
	}

	/**
	 * Gets the value of the caja_id field
	 */
	function getCajaId() {
		return $this->caja_id;
	}

	/**
	 * Sets the value of the caja_id field
	 * @return MovimientosDeDinero
	 */
	function setCajaId($value) {
		return $this->setColumnValue('caja_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getCajaId
	 * final because getCajaId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getCajaId
	 */
	final function getCaja_id() {
		return $this->getCajaId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setCajaId
	 * final because setCajaId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setCajaId
	 * @return MovimientosDeDinero
	 */
	final function setCaja_id($value) {
		return $this->setCajaId($value);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return MovimientosDeDinero
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setUsuarioId
	 * @return MovimientosDeDinero
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the forma_pago_id field
	 */
	function getFormaPagoId() {
		return $this->forma_pago_id;
	}

	/**
	 * Sets the value of the forma_pago_id field
	 * @return MovimientosDeDinero
	 */
	function setFormaPagoId($value) {
		return $this->setColumnValue('forma_pago_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getFormaPagoId
	 * final because getFormaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getFormaPagoId
	 */
	final function getForma_pago_id() {
		return $this->getFormaPagoId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setFormaPagoId
	 * final because setFormaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setFormaPagoId
	 * @return MovimientosDeDinero
	 */
	final function setForma_pago_id($value) {
		return $this->setFormaPagoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return MovimientosDeDinero
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setCreatedAt
	 * @return MovimientosDeDinero
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return MovimientosDeDinero
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setUpdatedAt
	 * @return MovimientosDeDinero
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return MovimientosDeDinero
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setDeletedAt
	 * @return MovimientosDeDinero
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return MovimientosDeDinero
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return MovimientosDeDinero
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveById($value) {
		return MovimientosDeDinero::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero_voucher
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByNumeroVoucher($value) {
		return static::retrieveByColumn('numero_voucher', $value);
	}

	/**
	 * Searches the database for a row with a voucher_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByVoucherId($value) {
		return static::retrieveByColumn('voucher_id', $value);
	}

	/**
	 * Searches the database for a row with a concepto_de_pago_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByConceptoDePagoId($value) {
		return static::retrieveByColumn('concepto_de_pago_id', $value);
	}

	/**
	 * Searches the database for a row with a caja_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByCajaId($value) {
		return static::retrieveByColumn('caja_id', $value);
	}

	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a forma_pago_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByFormaPagoId($value) {
		return static::retrieveByColumn('forma_pago_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return MovimientosDeDinero
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->numero_voucher = (null === $this->numero_voucher) ? null : (int) $this->numero_voucher;
		$this->voucher_id = (null === $this->voucher_id) ? null : (int) $this->voucher_id;
		$this->concepto_de_pago_id = (null === $this->concepto_de_pago_id) ? null : (int) $this->concepto_de_pago_id;
		$this->caja_id = (null === $this->caja_id) ? null : (int) $this->caja_id;
		$this->usuario_id = (null === $this->usuario_id) ? null : (int) $this->usuario_id;
		$this->forma_pago_id = (null === $this->forma_pago_id) ? null : (int) $this->forma_pago_id;
		return $this;
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setVoucher(Vouchers $vouchers = null) {
		return $this->setVouchersRelatedByVoucherId($vouchers);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setVouchersRelatedByVoucherId(Vouchers $vouchers = null) {
		if (null === $vouchers) {
			$this->setvoucher_id(null);
		} else {
			if (!$vouchers->getid()) {
				throw new Exception('Cannot connect a Vouchers without a id');
			}
			$this->setvoucher_id($vouchers->getid());
		}
		return $this;
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVoucher() {
		return $this->getVouchersRelatedByVoucherId();
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVouchersRelatedByVoucherId() {
		$fk_value = $this->getvoucher_id();
		if (null === $fk_value) {
			return null;
		}
		return Vouchers::retrieveByPK($fk_value);
	}

	static function doSelectJoinVoucher(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVouchersRelatedByVoucherId($q, $join_type);
	}

	/**
	 * Returns a vouchers object with a id
	 * that matches $this->voucher_id.
	 * @return Vouchers
	 */
	function getVouchers() {
		return $this->getVouchersRelatedByVoucherId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setVouchers(Vouchers $vouchers = null) {
		return $this->setVouchersRelatedByVoucherId($vouchers);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinVouchersRelatedByVoucherId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Vouchers::getTableName();
		$q->join($to_table, $this_table . '.voucher_id = ' . $to_table . '.id', $join_type);
		foreach (Vouchers::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Vouchers'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCaja(Cajas $cajas = null) {
		return $this->setCajasRelatedByCajaId($cajas);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCajasRelatedByCajaId(Cajas $cajas = null) {
		if (null === $cajas) {
			$this->setcaja_id(null);
		} else {
			if (!$cajas->getid()) {
				throw new Exception('Cannot connect a Cajas without a id');
			}
			$this->setcaja_id($cajas->getid());
		}
		return $this;
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCaja() {
		return $this->getCajasRelatedByCajaId();
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCajasRelatedByCajaId() {
		$fk_value = $this->getcaja_id();
		if (null === $fk_value) {
			return null;
		}
		return Cajas::retrieveByPK($fk_value);
	}

	static function doSelectJoinCaja(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinCajasRelatedByCajaId($q, $join_type);
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCajas() {
		return $this->getCajasRelatedByCajaId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCajas(Cajas $cajas = null) {
		return $this->setCajasRelatedByCajaId($cajas);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinCajasRelatedByCajaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Cajas::getTableName();
		$q->join($to_table, $this_table . '.caja_id = ' . $to_table . '.id', $join_type);
		foreach (Cajas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Cajas'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setUsuario(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setUsuariosRelatedByUsuarioId(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setusuario_id(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setusuario_id($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuario() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByUsuarioId() {
		$fk_value = $this->getusuario_id();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	static function doSelectJoinUsuario(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinUsuariosRelatedByUsuarioId($q, $join_type);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinUsuariosRelatedByUsuarioId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormaPago(FormasPago $formaspago = null) {
		return $this->setFormasPagoRelatedByFormaPagoId($formaspago);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormasPagoRelatedByFormaPagoId(FormasPago $formaspago = null) {
		if (null === $formaspago) {
			$this->setforma_pago_id(null);
		} else {
			if (!$formaspago->getid()) {
				throw new Exception('Cannot connect a FormasPago without a id');
			}
			$this->setforma_pago_id($formaspago->getid());
		}
		return $this;
	}

	/**
	 * Returns a formas_pago object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPago
	 */
	function getFormaPago() {
		return $this->getFormasPagoRelatedByFormaPagoId();
	}

	/**
	 * Returns a formas_pago object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPago
	 */
	function getFormasPagoRelatedByFormaPagoId() {
		$fk_value = $this->getforma_pago_id();
		if (null === $fk_value) {
			return null;
		}
		return FormasPago::retrieveByPK($fk_value);
	}

	static function doSelectJoinFormaPago(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinFormasPagoRelatedByFormaPagoId($q, $join_type);
	}

	/**
	 * Returns a formas_pago object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPago
	 */
	function getFormasPago() {
		return $this->getFormasPagoRelatedByFormaPagoId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormasPago(FormasPago $formaspago = null) {
		return $this->setFormasPagoRelatedByFormaPagoId($formaspago);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinFormasPagoRelatedByFormaPagoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = FormasPago::getTableName();
		$q->join($to_table, $this_table . '.forma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (FormasPago::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('FormasPago'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setConceptoDePago(TiposConceptoPago $tiposconceptopago = null) {
		return $this->setTiposConceptoPagoRelatedByConceptoDePagoId($tiposconceptopago);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setTiposConceptoPagoRelatedByConceptoDePagoId(TiposConceptoPago $tiposconceptopago = null) {
		if (null === $tiposconceptopago) {
			$this->setconcepto_de_pago_id(null);
		} else {
			if (!$tiposconceptopago->getid()) {
				throw new Exception('Cannot connect a TiposConceptoPago without a id');
			}
			$this->setconcepto_de_pago_id($tiposconceptopago->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos_concepto_pago object with a id
	 * that matches $this->concepto_de_pago_id.
	 * @return TiposConceptoPago
	 */
	function getConceptoDePago() {
		return $this->getTiposConceptoPagoRelatedByConceptoDePagoId();
	}

	/**
	 * Returns a tipos_concepto_pago object with a id
	 * that matches $this->concepto_de_pago_id.
	 * @return TiposConceptoPago
	 */
	function getTiposConceptoPagoRelatedByConceptoDePagoId() {
		$fk_value = $this->getconcepto_de_pago_id();
		if (null === $fk_value) {
			return null;
		}
		return TiposConceptoPago::retrieveByPK($fk_value);
	}

	static function doSelectJoinConceptoDePago(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTiposConceptoPagoRelatedByConceptoDePagoId($q, $join_type);
	}

	/**
	 * Returns a tipos_concepto_pago object with a id
	 * that matches $this->concepto_de_pago_id.
	 * @return TiposConceptoPago
	 */
	function getTiposConceptoPago() {
		return $this->getTiposConceptoPagoRelatedByConceptoDePagoId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setTiposConceptoPago(TiposConceptoPago $tiposconceptopago = null) {
		return $this->setTiposConceptoPagoRelatedByConceptoDePagoId($tiposconceptopago);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinTiposConceptoPagoRelatedByConceptoDePagoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposConceptoPago::getTableName();
		$q->join($to_table, $this_table . '.concepto_de_pago_id = ' . $to_table . '.id', $join_type);
		foreach (TiposConceptoPago::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TiposConceptoPago'));
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Vouchers::getTableName();
		$q->join($to_table, $this_table . '.voucher_id = ' . $to_table . '.id', $join_type);
		foreach (Vouchers::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Vouchers';
	
		$to_table = Cajas::getTableName();
		$q->join($to_table, $this_table . '.caja_id = ' . $to_table . '.id', $join_type);
		foreach (Cajas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Cajas';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$to_table = FormasPago::getTableName();
		$q->join($to_table, $this_table . '.forma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (FormasPago::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'FormasPago';
	
		$to_table = TiposConceptoPago::getTableName();
		$q->join($to_table, $this_table . '.concepto_de_pago_id = ' . $to_table . '.id', $join_type);
		foreach (TiposConceptoPago::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TiposConceptoPago';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_de_dinero_id that matches $this->id.
	 * @return Query
	 */
	function getAmortizacionessRelatedByMovimientoDeDineroIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'movimiento_de_dinero_id', 'id', $q);
	}

	/**
	 * Returns the count of Amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_de_dinero_id that matches $this->id.
	 * @return int
	 */
	function countAmortizacionessRelatedByMovimientoDeDineroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Amortizaciones::doCount($this->getAmortizacionessRelatedByMovimientoDeDineroIdQuery($q));
	}

	/**
	 * Deletes the amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_de_dinero_id that matches $this->id.
	 * @return int
	 */
	function deleteAmortizacionessRelatedByMovimientoDeDineroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AmortizacionessRelatedByMovimientoDeDineroId_c = array();
		return Amortizaciones::doDelete($this->getAmortizacionessRelatedByMovimientoDeDineroIdQuery($q));
	}

	protected $AmortizacionessRelatedByMovimientoDeDineroId_c = array();

	/**
	 * Returns an array of Amortizaciones objects with a movimiento_de_dinero_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Amortizaciones[]
	 */
	function getAmortizacionessRelatedByMovimientoDeDineroId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AmortizacionessRelatedByMovimientoDeDineroId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AmortizacionessRelatedByMovimientoDeDineroId_c;
		}

		$result = Amortizaciones::doSelect($this->getAmortizacionessRelatedByMovimientoDeDineroIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AmortizacionessRelatedByMovimientoDeDineroId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting conceptos_de_pago Objects(rows) from the conceptos_de_pago table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getConceptosDePagosRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('conceptos_de_pago', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of ConceptosDePago Objects(rows) from the conceptos_de_pago table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countConceptosDePagosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ConceptosDePago::doCount($this->getConceptosDePagosRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the conceptos_de_pago Objects(rows) from the conceptos_de_pago table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteConceptosDePagosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ConceptosDePagosRelatedByMovimientoId_c = array();
		return ConceptosDePago::doDelete($this->getConceptosDePagosRelatedByMovimientoIdQuery($q));
	}

	protected $ConceptosDePagosRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of ConceptosDePago objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ConceptosDePago[]
	 */
	function getConceptosDePagosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ConceptosDePagosRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ConceptosDePagosRelatedByMovimientoId_c;
		}

		$result = ConceptosDePago::doSelect($this->getConceptosDePagosRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ConceptosDePagosRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_delivery Objects(rows) from the ventas_delivery table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getVentasDeliverysRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_delivery', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasDelivery Objects(rows) from the ventas_delivery table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countVentasDeliverysRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasDelivery::doCount($this->getVentasDeliverysRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the ventas_delivery Objects(rows) from the ventas_delivery table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasDeliverysRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasDeliverysRelatedByMovimientoId_c = array();
		return VentasDelivery::doDelete($this->getVentasDeliverysRelatedByMovimientoIdQuery($q));
	}

	protected $VentasDeliverysRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of VentasDelivery objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasDelivery[]
	 */
	function getVentasDeliverysRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasDeliverysRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasDeliverysRelatedByMovimientoId_c;
		}

		$result = VentasDelivery::doSelect($this->getVentasDeliverysRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasDeliverysRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_movimiento Objects(rows) from the ventas_movimiento table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getVentasMovimientosRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_movimiento', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasMovimiento Objects(rows) from the ventas_movimiento table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countVentasMovimientosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasMovimiento::doCount($this->getVentasMovimientosRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the ventas_movimiento Objects(rows) from the ventas_movimiento table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasMovimientosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasMovimientosRelatedByMovimientoId_c = array();
		return VentasMovimiento::doDelete($this->getVentasMovimientosRelatedByMovimientoIdQuery($q));
	}

	protected $VentasMovimientosRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of VentasMovimiento objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasMovimiento[]
	 */
	function getVentasMovimientosRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasMovimientosRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasMovimientosRelatedByMovimientoId_c;
		}

		$result = VentasMovimiento::doSelect($this->getVentasMovimientosRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasMovimientosRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_de_dinero_id
	 * @return Amortizaciones[]
	 * @see MovimientosDeDinero::getAmortizacionessRelatedByMovimientoDeDineroId
	 */
	function getAmortizacioness($extra = null) {
		return $this->getAmortizacionessRelatedByMovimientoDeDineroId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_de_dinero_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_de_dinero_idQuery
	  */
	function getAmortizacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'movimiento_de_dinero_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteAmortizacionessRelatedBymovimiento_de_dinero_id
	  * @return int
	  * @see MovimientosDeDinero::deleteAmortizacionessRelatedBymovimiento_de_dinero_id
	  */
	function deleteAmortizacioness(Query $q = null) {
		return $this->deleteAmortizacionessRelatedByMovimientoDeDineroId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countAmortizacionessRelatedBymovimiento_de_dinero_id
	  * @return int
	  * @see MovimientosDeDinero::countAmortizacionessRelatedByMovimientoDeDineroId
	  */
	function countAmortizacioness(Query $q = null) {
		return $this->countAmortizacionessRelatedByMovimientoDeDineroId($q);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getConceptosDePagosRelatedBymovimiento_id
	 * @return ConceptosDePago[]
	 * @see MovimientosDeDinero::getConceptosDePagosRelatedByMovimientoId
	 */
	function getConceptosDePagos($extra = null) {
		return $this->getConceptosDePagosRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getConceptosDePagosRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getConceptosDePagosRelatedBymovimiento_idQuery
	  */
	function getConceptosDePagosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('conceptos_de_pago', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteConceptosDePagosRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteConceptosDePagosRelatedBymovimiento_id
	  */
	function deleteConceptosDePagos(Query $q = null) {
		return $this->deleteConceptosDePagosRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countConceptosDePagosRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countConceptosDePagosRelatedByMovimientoId
	  */
	function countConceptosDePagos(Query $q = null) {
		return $this->countConceptosDePagosRelatedByMovimientoId($q);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getVentasDeliverysRelatedBymovimiento_id
	 * @return VentasDelivery[]
	 * @see MovimientosDeDinero::getVentasDeliverysRelatedByMovimientoId
	 */
	function getVentasDeliverys($extra = null) {
		return $this->getVentasDeliverysRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getVentasDeliverysRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getVentasDeliverysRelatedBymovimiento_idQuery
	  */
	function getVentasDeliverysQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_delivery', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteVentasDeliverysRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteVentasDeliverysRelatedBymovimiento_id
	  */
	function deleteVentasDeliverys(Query $q = null) {
		return $this->deleteVentasDeliverysRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countVentasDeliverysRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countVentasDeliverysRelatedByMovimientoId
	  */
	function countVentasDeliverys(Query $q = null) {
		return $this->countVentasDeliverysRelatedByMovimientoId($q);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getVentasMovimientosRelatedBymovimiento_id
	 * @return VentasMovimiento[]
	 * @see MovimientosDeDinero::getVentasMovimientosRelatedByMovimientoId
	 */
	function getVentasMovimientos($extra = null) {
		return $this->getVentasMovimientosRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getVentasMovimientosRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getVentasMovimientosRelatedBymovimiento_idQuery
	  */
	function getVentasMovimientosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_movimiento', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteVentasMovimientosRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteVentasMovimientosRelatedBymovimiento_id
	  */
	function deleteVentasMovimientos(Query $q = null) {
		return $this->deleteVentasMovimientosRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countVentasMovimientosRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countVentasMovimientosRelatedByMovimientoId
	  */
	function countVentasMovimientos(Query $q = null) {
		return $this->countVentasMovimientosRelatedByMovimientoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getcaja_id()) {
			$this->_validationErrors[] = 'caja_id must not be null';
		}
		if (null === $this->getusuario_id()) {
			$this->_validationErrors[] = 'usuario_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}