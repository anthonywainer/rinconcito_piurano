<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseDeliverista extends ApplicationModel {

	const ID = 'deliverista.id';
	const NOMBRES = 'deliverista.nombres';
	const APELLIDOS = 'deliverista.apellidos';
	const TELEFONO = 'deliverista.telefono';
	const CREATED_AT = 'deliverista.created_at';
	const UPDATED_AT = 'deliverista.updated_at';
	const DELETED_AT = 'deliverista.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'deliverista';

	/**
	 * Cache of objects retrieved from the database
	 * @var Deliverista[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR NOT NULL
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `telefono` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $telefono;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Deliverista
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Deliverista
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Deliverista
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Deliverista
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Deliverista
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliverista::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Deliverista::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::setCreatedAt
	 * @return Deliverista
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Deliverista
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliverista::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Deliverista::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::setUpdatedAt
	 * @return Deliverista
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Deliverista
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliverista::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Deliverista::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliverista::setDeletedAt
	 * @return Deliverista
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Deliverista
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Deliverista
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveById($value) {
		return Deliverista::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Deliverista
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Deliverista
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->telefono = (null === $this->telefono) ? null : (int) $this->telefono;
		return $this;
	}

	/**
	 * @return Deliverista[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting deliveries Objects(rows) from the deliveries table
	 * with a deliverista_id that matches $this->id.
	 * @return Query
	 */
	function getDeliveriessRelatedByDeliveristaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('deliveries', 'deliverista_id', 'id', $q);
	}

	/**
	 * Returns the count of Deliveries Objects(rows) from the deliveries table
	 * with a deliverista_id that matches $this->id.
	 * @return int
	 */
	function countDeliveriessRelatedByDeliveristaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Deliveries::doCount($this->getDeliveriessRelatedByDeliveristaIdQuery($q));
	}

	/**
	 * Deletes the deliveries Objects(rows) from the deliveries table
	 * with a deliverista_id that matches $this->id.
	 * @return int
	 */
	function deleteDeliveriessRelatedByDeliveristaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->DeliveriessRelatedByDeliveristaId_c = array();
		return Deliveries::doDelete($this->getDeliveriessRelatedByDeliveristaIdQuery($q));
	}

	protected $DeliveriessRelatedByDeliveristaId_c = array();

	/**
	 * Returns an array of Deliveries objects with a deliverista_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Deliveries[]
	 */
	function getDeliveriessRelatedByDeliveristaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->DeliveriessRelatedByDeliveristaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->DeliveriessRelatedByDeliveristaId_c;
		}

		$result = Deliveries::doSelect($this->getDeliveriessRelatedByDeliveristaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->DeliveriessRelatedByDeliveristaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Deliverista::getDeliveriessRelatedBydeliverista_id
	 * @return Deliveries[]
	 * @see Deliverista::getDeliveriessRelatedByDeliveristaId
	 */
	function getDeliveriess($extra = null) {
		return $this->getDeliveriessRelatedByDeliveristaId($extra);
	}

	/**
	  * Convenience function for Deliverista::getDeliveriessRelatedBydeliverista_idQuery
	  * @return Query
	  * @see Deliverista::getDeliveriessRelatedBydeliverista_idQuery
	  */
	function getDeliveriessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('deliveries', 'deliverista_id','id', $q);
	}

	/**
	  * Convenience function for Deliverista::deleteDeliveriessRelatedBydeliverista_id
	  * @return int
	  * @see Deliverista::deleteDeliveriessRelatedBydeliverista_id
	  */
	function deleteDeliveriess(Query $q = null) {
		return $this->deleteDeliveriessRelatedByDeliveristaId($q);
	}

	/**
	  * Convenience function for Deliverista::countDeliveriessRelatedBydeliverista_id
	  * @return int
	  * @see Deliverista::countDeliveriessRelatedByDeliveristaId
	  */
	function countDeliveriess(Query $q = null) {
		return $this->countDeliveriessRelatedByDeliveristaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}
		if (null === $this->getapellidos()) {
			$this->_validationErrors[] = 'apellidos must not be null';
		}
		if (null === $this->gettelefono()) {
			$this->_validationErrors[] = 'telefono must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}