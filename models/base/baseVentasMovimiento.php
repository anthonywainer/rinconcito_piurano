<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentasMovimiento extends ApplicationModel {

	const ID = 'ventas_movimiento.id';
	const VENTA_ID = 'ventas_movimiento.venta_id';
	const SUBTOTAL = 'ventas_movimiento.subtotal';
	const MOVIMIENTO_ID = 'ventas_movimiento.movimiento_id';
	const FECHA = 'ventas_movimiento.fecha';
	const CLIENTE_ID = 'ventas_movimiento.cliente_id';
	const DESCUENTO = 'ventas_movimiento.descuento';
	const PORCENTAJE = 'ventas_movimiento.porcentaje';
	const TOTAL = 'ventas_movimiento.total';
	const EFECTIVO = 'ventas_movimiento.efectivo';
	const RESTA = 'ventas_movimiento.resta';
	const CUOTA = 'ventas_movimiento.cuota';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ventas_movimiento';

	/**
	 * Cache of objects retrieved from the database
	 * @var VentasMovimiento[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'venta_id' => Model::COLUMN_TYPE_INTEGER,
		'subtotal' => Model::COLUMN_TYPE_FLOAT,
		'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
		'fecha' => Model::COLUMN_TYPE_TIMESTAMP,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'descuento' => Model::COLUMN_TYPE_FLOAT,
		'porcentaje' => Model::COLUMN_TYPE_INTEGER,
		'total' => Model::COLUMN_TYPE_FLOAT,
		'efectivo' => Model::COLUMN_TYPE_INTEGER,
		'resta' => Model::COLUMN_TYPE_DECIMAL,
		'cuota' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `venta_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $venta_id;

	/**
	 * `subtotal` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $subtotal;

	/**
	 * `movimiento_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $movimiento_id;

	/**
	 * `fecha` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha;

	/**
	 * `cliente_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `descuento` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $descuento;

	/**
	 * `porcentaje` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $porcentaje;

	/**
	 * `total` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $total;

	/**
	 * `efectivo` INTEGER DEFAULT 0
	 * @var int
	 */
	protected $efectivo = 0;

	/**
	 * `resta` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $resta;

	/**
	 * `cuota` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $cuota;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return VentasMovimiento
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the venta_id field
	 */
	function getVentaId() {
		return $this->venta_id;
	}

	/**
	 * Sets the value of the venta_id field
	 * @return VentasMovimiento
	 */
	function setVentaId($value) {
		return $this->setColumnValue('venta_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasMovimiento::getVentaId
	 * final because getVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::getVentaId
	 */
	final function getVenta_id() {
		return $this->getVentaId();
	}

	/**
	 * Convenience function for VentasMovimiento::setVentaId
	 * final because setVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::setVentaId
	 * @return VentasMovimiento
	 */
	final function setVenta_id($value) {
		return $this->setVentaId($value);
	}

	/**
	 * Gets the value of the subtotal field
	 */
	function getSubtotal() {
		return $this->subtotal;
	}

	/**
	 * Sets the value of the subtotal field
	 * @return VentasMovimiento
	 */
	function setSubtotal($value) {
		return $this->setColumnValue('subtotal', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the movimiento_id field
	 */
	function getMovimientoId() {
		return $this->movimiento_id;
	}

	/**
	 * Sets the value of the movimiento_id field
	 * @return VentasMovimiento
	 */
	function setMovimientoId($value) {
		return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasMovimiento::getMovimientoId
	 * final because getMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::getMovimientoId
	 */
	final function getMovimiento_id() {
		return $this->getMovimientoId();
	}

	/**
	 * Convenience function for VentasMovimiento::setMovimientoId
	 * final because setMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::setMovimientoId
	 * @return VentasMovimiento
	 */
	final function setMovimiento_id($value) {
		return $this->setMovimientoId($value);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return VentasMovimiento
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return VentasMovimiento
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasMovimiento::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for VentasMovimiento::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMovimiento::setClienteId
	 * @return VentasMovimiento
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the descuento field
	 */
	function getDescuento() {
		return $this->descuento;
	}

	/**
	 * Sets the value of the descuento field
	 * @return VentasMovimiento
	 */
	function setDescuento($value) {
		return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the porcentaje field
	 */
	function getPorcentaje() {
		return $this->porcentaje;
	}

	/**
	 * Sets the value of the porcentaje field
	 * @return VentasMovimiento
	 */
	function setPorcentaje($value) {
		return $this->setColumnValue('porcentaje', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the total field
	 */
	function getTotal() {
		return $this->total;
	}

	/**
	 * Sets the value of the total field
	 * @return VentasMovimiento
	 */
	function setTotal($value) {
		return $this->setColumnValue('total', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the efectivo field
	 */
	function getEfectivo() {
		return $this->efectivo;
	}

	/**
	 * Sets the value of the efectivo field
	 * @return VentasMovimiento
	 */
	function setEfectivo($value) {
		return $this->setColumnValue('efectivo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the resta field
	 */
	function getResta() {
		return $this->resta;
	}

	/**
	 * Sets the value of the resta field
	 * @return VentasMovimiento
	 */
	function setResta($value) {
		return $this->setColumnValue('resta', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the cuota field
	 */
	function getCuota() {
		return $this->cuota;
	}

	/**
	 * Sets the value of the cuota field
	 * @return VentasMovimiento
	 */
	function setCuota($value) {
		return $this->setColumnValue('cuota', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return VentasMovimiento
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return VentasMovimiento
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveById($value) {
		return VentasMovimiento::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a venta_id
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByVentaId($value) {
		return static::retrieveByColumn('venta_id', $value);
	}

	/**
	 * Searches the database for a row with a subtotal
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveBySubtotal($value) {
		return static::retrieveByColumn('subtotal', $value);
	}

	/**
	 * Searches the database for a row with a movimiento_id
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByMovimientoId($value) {
		return static::retrieveByColumn('movimiento_id', $value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a descuento
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByDescuento($value) {
		return static::retrieveByColumn('descuento', $value);
	}

	/**
	 * Searches the database for a row with a porcentaje
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByPorcentaje($value) {
		return static::retrieveByColumn('porcentaje', $value);
	}

	/**
	 * Searches the database for a row with a total
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByTotal($value) {
		return static::retrieveByColumn('total', $value);
	}

	/**
	 * Searches the database for a row with a efectivo
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByEfectivo($value) {
		return static::retrieveByColumn('efectivo', $value);
	}

	/**
	 * Searches the database for a row with a resta
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByResta($value) {
		return static::retrieveByColumn('resta', $value);
	}

	/**
	 * Searches the database for a row with a cuota
	 * value that matches the one provided
	 * @return VentasMovimiento
	 */
	static function retrieveByCuota($value) {
		return static::retrieveByColumn('cuota', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return VentasMovimiento
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->venta_id = (null === $this->venta_id) ? null : (int) $this->venta_id;
		$this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		$this->porcentaje = (null === $this->porcentaje) ? null : (int) $this->porcentaje;
		$this->efectivo = (null === $this->efectivo) ? null : (int) $this->efectivo;
		return $this;
	}

	/**
	 * @return VentasMovimiento
	 */
	function setMovimiento(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasMovimiento
	 */
	function setMovimientosDeDineroRelatedByMovimientoId(MovimientosDeDinero $movimientosdedinero = null) {
		if (null === $movimientosdedinero) {
			$this->setmovimiento_id(null);
		} else {
			if (!$movimientosdedinero->getid()) {
				throw new Exception('Cannot connect a MovimientosDeDinero without a id');
			}
			$this->setmovimiento_id($movimientosdedinero->getid());
		}
		return $this;
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimiento() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDineroRelatedByMovimientoId() {
		$fk_value = $this->getmovimiento_id();
		if (null === $fk_value) {
			return null;
		}
		return MovimientosDeDinero::retrieveByPK($fk_value);
	}

	static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMovimientosDeDineroRelatedByMovimientoId($q, $join_type);
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDinero() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * @return VentasMovimiento
	 */
	function setMovimientosDeDinero(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasMovimiento[]
	 */
	static function doSelectJoinMovimientosDeDineroRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('MovimientosDeDinero'));
	}

	/**
	 * @return VentasMovimiento
	 */
	function setVenta(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return VentasMovimiento
	 */
	function setVentasRelatedByVentaId(Ventas $ventas = null) {
		if (null === $ventas) {
			$this->setventa_id(null);
		} else {
			if (!$ventas->getid()) {
				throw new Exception('Cannot connect a Ventas without a id');
			}
			$this->setventa_id($ventas->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVenta() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentasRelatedByVentaId() {
		$fk_value = $this->getventa_id();
		if (null === $fk_value) {
			return null;
		}
		return Ventas::retrieveByPK($fk_value);
	}

	static function doSelectJoinVenta(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVentasRelatedByVentaId($q, $join_type);
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentas() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * @return VentasMovimiento
	 */
	function setVentas(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return VentasMovimiento[]
	 */
	static function doSelectJoinVentasRelatedByVentaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ventas'));
	}

	/**
	 * @return VentasMovimiento
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return VentasMovimiento
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return VentasMovimiento
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return VentasMovimiento[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return VentasMovimiento[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'MovimientosDeDinero';
	
		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ventas';
	
		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getventa_id()) {
			$this->_validationErrors[] = 'venta_id must not be null';
		}
		if (null === $this->getsubtotal()) {
			$this->_validationErrors[] = 'subtotal must not be null';
		}
		if (null === $this->getmovimiento_id()) {
			$this->_validationErrors[] = 'movimiento_id must not be null';
		}
		if (null === $this->getfecha()) {
			$this->_validationErrors[] = 'fecha must not be null';
		}
		if (null === $this->getcliente_id()) {
			$this->_validationErrors[] = 'cliente_id must not be null';
		}
		if (null === $this->gettotal()) {
			$this->_validationErrors[] = 'total must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}