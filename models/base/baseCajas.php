<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCajas extends ApplicationModel {

	const ID = 'cajas.id';
	const MONTO_ABIERTO = 'cajas.monto_abierto';
	const MONTO_CERRADO = 'cajas.monto_cerrado';
	const FECHA_CERRADA = 'cajas.fecha_cerrada';
	const FECHA_ABIERTA = 'cajas.fecha_abierta';
	const USUARIO_ID = 'cajas.usuario_id';
	const CREATED_AT = 'cajas.created_at';
	const UPDATED_AT = 'cajas.updated_at';
	const DELETED_AT = 'cajas.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'cajas';

	/**
	 * Cache of objects retrieved from the database
	 * @var Cajas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'monto_abierto' => Model::COLUMN_TYPE_FLOAT,
		'monto_cerrado' => Model::COLUMN_TYPE_FLOAT,
		'fecha_cerrada' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_abierta' => Model::COLUMN_TYPE_TIMESTAMP,
		'usuario_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
        'estado' => Model::COLUMN_TYPE_FLOAT,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `monto_abierto` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $monto_abierto;

	/**
	 * `monto_cerrado` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $monto_cerrado;

	/**
	 * `fecha_cerrada` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $fecha_cerrada;

	/**
	 * `fecha_abierta` FLOAT NOT NULL DEFAULT ''
	 * @var double
	 */
	protected $fecha_abierta;

	/**
	 * `usuario_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $usuario_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

    protected $estado;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Cajas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the monto_abierto field
	 */
	function getMontoAbierto() {
		return $this->monto_abierto;
	}

	/**
	 * Sets the value of the monto_abierto field
	 * @return Cajas
	 */
	function setMontoAbierto($value) {
		return $this->setColumnValue('monto_abierto', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Convenience function for Cajas::getMontoAbierto
	 * final because getMontoAbierto should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getMontoAbierto
	 */
	final function getMonto_abierto() {
		return $this->getMontoAbierto();
	}

	/**
	 * Convenience function for Cajas::setMontoAbierto
	 * final because setMontoAbierto should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setMontoAbierto
	 * @return Cajas
	 */
	final function setMonto_abierto($value) {
		return $this->setMontoAbierto($value);
	}

	/**
	 * Gets the value of the monto_cerrado field
	 */
	function getMontoCerrado() {
		return $this->monto_cerrado;
	}

	/**
	 * Sets the value of the monto_cerrado field
	 * @return Cajas
	 */
	function setMontoCerrado($value) {
		return $this->setColumnValue('monto_cerrado', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Convenience function for Cajas::getMontoCerrado
	 * final because getMontoCerrado should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getMontoCerrado
	 */
	final function getMonto_cerrado() {
		return $this->getMontoCerrado();
	}

	/**
	 * Convenience function for Cajas::setMontoCerrado
	 * final because setMontoCerrado should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setMontoCerrado
	 * @return Cajas
	 */
	final function setMonto_cerrado($value) {
		return $this->setMontoCerrado($value);
	}

	/**
	 * Gets the value of the fecha_cerrada field
	 */
	function getFechaCerrada() {
		return $this->fecha_cerrada;
	}
    function getEstado() {
        return $this->estado;
    }
	/**
	 * Sets the value of the fecha_cerrada field
	 * @return Cajas
	 */
	function setFechaCerrada($value) {
		return $this->setColumnValue('fecha_cerrada', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

    function setEstado($value) {
        return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_FLOAT);
    }

	/**
	 * Convenience function for Cajas::getFechaCerrada
	 * final because getFechaCerrada should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getFechaCerrada
	 */
	final function getFecha_cerrada() {
		return $this->getFechaCerrada();
	}

	/**
	 * Convenience function for Cajas::setFechaCerrada
	 * final because setFechaCerrada should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setFechaCerrada
	 * @return Cajas
	 */
	final function setFecha_cerrada($value) {
		return $this->setFechaCerrada($value);
	}

	/**
	 * Gets the value of the fecha_abierta field
	 */
	function getFechaAbierta() {
		return $this->fecha_abierta;
	}

	/**
	 * Sets the value of the fecha_abierta field
	 * @return Cajas
	 */
	function setFechaAbierta($value) {
		return $this->setColumnValue('fecha_abierta', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getFechaAbierta
	 * final because getFechaAbierta should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getFechaAbierta
	 */
	final function getFecha_abierta() {
		return $this->getFechaAbierta();
	}

	/**
	 * Convenience function for Cajas::setFechaAbierta
	 * final because setFechaAbierta should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setFechaAbierta
	 * @return Cajas
	 */
	final function setFecha_abierta($value) {
		return $this->setFechaAbierta($value);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return Cajas
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Cajas::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for Cajas::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setUsuarioId
	 * @return Cajas
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Cajas
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Cajas::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setCreatedAt
	 * @return Cajas
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Cajas
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Cajas::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setUpdatedAt
	 * @return Cajas
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Cajas
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Cajas::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setDeletedAt
	 * @return Cajas
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Cajas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Cajas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveById($value) {
		return Cajas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a monto_abierto
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByMontoAbierto($value) {
		return static::retrieveByColumn('monto_abierto', $value);
	}

	/**
	 * Searches the database for a row with a monto_cerrado
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByMontoCerrado($value) {
		return static::retrieveByColumn('monto_cerrado', $value);
	}

	/**
	 * Searches the database for a row with a fecha_cerrada
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByFechaCerrada($value) {
		return static::retrieveByColumn('fecha_cerrada', $value);
	}

	/**
	 * Searches the database for a row with a fecha_abierta
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByFechaAbierta($value) {
		return static::retrieveByColumn('fecha_abierta', $value);
	}
    static function retrieveByEstado($value) {
        return static::retrieveByColumn('estado', $value);
    }
	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Cajas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->usuario_id = (null === $this->usuario_id) ? null : (int) $this->usuario_id;
		return $this;
	}

	/**
	 * @return Cajas
	 */
	function setUsuario(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return Cajas
	 */
	function setUsuariosRelatedByUsuarioId(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setusuario_id(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setusuario_id($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuario() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByUsuarioId() {
		$fk_value = $this->getusuario_id();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	static function doSelectJoinUsuario(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinUsuariosRelatedByUsuarioId($q, $join_type);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * @return Cajas
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return Cajas[]
	 */
	static function doSelectJoinUsuariosRelatedByUsuarioId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return Cajas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a caja_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientosDeDinerosRelatedByCajaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'caja_id', 'id', $q);
	}

	/**
	 * Returns the count of MovimientosDeDinero Objects(rows) from the movimientos_de_dinero table
	 * with a caja_id that matches $this->id.
	 * @return int
	 */
	function countMovimientosDeDinerosRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MovimientosDeDinero::doCount($this->getMovimientosDeDinerosRelatedByCajaIdQuery($q));
	}

	/**
	 * Deletes the movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a caja_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientosDeDinerosRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientosDeDinerosRelatedByCajaId_c = array();
		return MovimientosDeDinero::doDelete($this->getMovimientosDeDinerosRelatedByCajaIdQuery($q));
	}

	protected $MovimientosDeDinerosRelatedByCajaId_c = array();

	/**
	 * Returns an array of MovimientosDeDinero objects with a caja_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MovimientosDeDinero[]
	 */
	function getMovimientosDeDinerosRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientosDeDinerosRelatedByCajaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientosDeDinerosRelatedByCajaId_c;
		}

		$result = MovimientosDeDinero::doSelect($this->getMovimientosDeDinerosRelatedByCajaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientosDeDinerosRelatedByCajaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Cajas::getMovimientosDeDinerosRelatedBycaja_id
	 * @return MovimientosDeDinero[]
	 * @see Cajas::getMovimientosDeDinerosRelatedByCajaId
	 */
	function getMovimientosDeDineros($extra = null) {
		return $this->getMovimientosDeDinerosRelatedByCajaId($extra);
	}

	/**
	  * Convenience function for Cajas::getMovimientosDeDinerosRelatedBycaja_idQuery
	  * @return Query
	  * @see Cajas::getMovimientosDeDinerosRelatedBycaja_idQuery
	  */
	function getMovimientosDeDinerosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'caja_id','id', $q);
	}

	/**
	  * Convenience function for Cajas::deleteMovimientosDeDinerosRelatedBycaja_id
	  * @return int
	  * @see Cajas::deleteMovimientosDeDinerosRelatedBycaja_id
	  */
	function deleteMovimientosDeDineros(Query $q = null) {
		return $this->deleteMovimientosDeDinerosRelatedByCajaId($q);
	}

	/**
	  * Convenience function for Cajas::countMovimientosDeDinerosRelatedBycaja_id
	  * @return int
	  * @see Cajas::countMovimientosDeDinerosRelatedByCajaId
	  */
	function countMovimientosDeDineros(Query $q = null) {
		return $this->countMovimientosDeDinerosRelatedByCajaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getusuario_id()) {
			$this->_validationErrors[] = 'usuario_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}