<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseDeliveries extends ApplicationModel {

	const ID = 'deliveries.id';
	const FECHA = 'deliveries.Fecha';
	const DIRECCION = 'deliveries.direccion';
	const REFERENCIA = 'deliveries.referencia';
	const DELIVERISTA_ID = 'deliveries.deliverista_id';
	const CREATED_AT = 'deliveries.created_at';
	const UPDATED_AT = 'deliveries.updated_at';
	const DELETED_AT = 'deliveries.deleted_at';
	const CLIENTE_ID = 'deliveries.cliente_id';
	const TIEMPO_DEMORA = 'deliveries.tiempo_demora';
	const ESTADO_DELIVERY = 'deliveries.estado_delivery';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'deliveries';

	/**
	 * Cache of objects retrieved from the database
	 * @var Deliveries[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'Fecha' => Model::COLUMN_TYPE_DATE,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'referencia' => Model::COLUMN_TYPE_VARCHAR,
		'deliverista_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'tiempo_demora' => Model::COLUMN_TYPE_INTEGER,
		'estado_delivery' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `Fecha` DATE NOT NULL
	 * @var string
	 */
	protected $Fecha;

	/**
	 * `direccion` VARCHAR
	 * @var string
	 */
	protected $direccion;

	/**
	 * `referencia` VARCHAR
	 * @var string
	 */
	protected $referencia;

	/**
	 * `deliverista_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $deliverista_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `cliente_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `tiempo_demora` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $tiempo_demora;

	/**
	 * `estado_delivery` INTEGER DEFAULT 1
	 * @var int
	 */
	protected $estado_delivery = 1;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Deliveries
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the Fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->Fecha || null === $format) {
			return $this->Fecha;
		}
		if (0 === strpos($this->Fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->Fecha));
	}

	/**
	 * Sets the value of the Fecha field
	 * @return Deliveries
	 */
	function setFecha($value) {
		return $this->setColumnValue('Fecha', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Deliveries
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the referencia field
	 */
	function getReferencia() {
		return $this->referencia;
	}

	/**
	 * Sets the value of the referencia field
	 * @return Deliveries
	 */
	function setReferencia($value) {
		return $this->setColumnValue('referencia', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the deliverista_id field
	 */
	function getDeliveristaId() {
		return $this->deliverista_id;
	}

	/**
	 * Sets the value of the deliverista_id field
	 * @return Deliveries
	 */
	function setDeliveristaId($value) {
		return $this->setColumnValue('deliverista_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Deliveries::getDeliveristaId
	 * final because getDeliveristaId should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getDeliveristaId
	 */
	final function getDeliverista_id() {
		return $this->getDeliveristaId();
	}

	/**
	 * Convenience function for Deliveries::setDeliveristaId
	 * final because setDeliveristaId should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setDeliveristaId
	 * @return Deliveries
	 */
	final function setDeliverista_id($value) {
		return $this->setDeliveristaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Deliveries
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliveries::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Deliveries::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setCreatedAt
	 * @return Deliveries
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Deliveries
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliveries::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Deliveries::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setUpdatedAt
	 * @return Deliveries
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Deliveries
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Deliveries::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Deliveries::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setDeletedAt
	 * @return Deliveries
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return Deliveries
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Deliveries::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for Deliveries::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setClienteId
	 * @return Deliveries
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the tiempo_demora field
	 */
	function getTiempoDemora() {
		return $this->tiempo_demora;
	}

	/**
	 * Sets the value of the tiempo_demora field
	 * @return Deliveries
	 */
	function setTiempoDemora($value) {
		return $this->setColumnValue('tiempo_demora', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Deliveries::getTiempoDemora
	 * final because getTiempoDemora should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getTiempoDemora
	 */
	final function getTiempo_demora() {
		return $this->getTiempoDemora();
	}

	/**
	 * Convenience function for Deliveries::setTiempoDemora
	 * final because setTiempoDemora should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setTiempoDemora
	 * @return Deliveries
	 */
	final function setTiempo_demora($value) {
		return $this->setTiempoDemora($value);
	}

	/**
	 * Gets the value of the estado_delivery field
	 */
	function getEstadoDelivery() {
		return $this->estado_delivery;
	}

	/**
	 * Sets the value of the estado_delivery field
	 * @return Deliveries
	 */
	function setEstadoDelivery($value) {
		return $this->setColumnValue('estado_delivery', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Deliveries::getEstadoDelivery
	 * final because getEstadoDelivery should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::getEstadoDelivery
	 */
	final function getEstado_delivery() {
		return $this->getEstadoDelivery();
	}

	/**
	 * Convenience function for Deliveries::setEstadoDelivery
	 * final because setEstadoDelivery should be extended instead
	 * to ensure consistent behavior
	 * @see Deliveries::setEstadoDelivery
	 * @return Deliveries
	 */
	final function setEstado_delivery($value) {
		return $this->setEstadoDelivery($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Deliveries
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Deliveries
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveById($value) {
		return Deliveries::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a Fecha
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('Fecha', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a referencia
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByReferencia($value) {
		return static::retrieveByColumn('referencia', $value);
	}

	/**
	 * Searches the database for a row with a deliverista_id
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByDeliveristaId($value) {
		return static::retrieveByColumn('deliverista_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a tiempo_demora
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByTiempoDemora($value) {
		return static::retrieveByColumn('tiempo_demora', $value);
	}

	/**
	 * Searches the database for a row with a estado_delivery
	 * value that matches the one provided
	 * @return Deliveries
	 */
	static function retrieveByEstadoDelivery($value) {
		return static::retrieveByColumn('estado_delivery', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Deliveries
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->deliverista_id = (null === $this->deliverista_id) ? null : (int) $this->deliverista_id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		$this->tiempo_demora = (null === $this->tiempo_demora) ? null : (int) $this->tiempo_demora;
		$this->estado_delivery = (null === $this->estado_delivery) ? null : (int) $this->estado_delivery;
		return $this;
	}

	/**
	 * @return Deliveries
	 */
	function setDeliverista(Deliverista $deliverista = null) {
		return $this->setDeliveristaRelatedByDeliveristaId($deliverista);
	}

	/**
	 * @return Deliveries
	 */
	function setDeliveristaRelatedByDeliveristaId(Deliverista $deliverista = null) {
		if (null === $deliverista) {
			$this->setdeliverista_id(null);
		} else {
			if (!$deliverista->getid()) {
				throw new Exception('Cannot connect a Deliverista without a id');
			}
			$this->setdeliverista_id($deliverista->getid());
		}
		return $this;
	}

	/**
	 * Returns a deliverista object with a id
	 * that matches $this->deliverista_id.
	 * @return Deliverista
	 */
	function getDeliverista() {
		return $this->getDeliveristaRelatedByDeliveristaId();
	}

	/**
	 * Returns a deliverista object with a id
	 * that matches $this->deliverista_id.
	 * @return Deliverista
	 */
	function getDeliveristaRelatedByDeliveristaId() {
		$fk_value = $this->getdeliverista_id();
		if (null === $fk_value) {
			return null;
		}
		return Deliverista::retrieveByPK($fk_value);
	}

	static function doSelectJoinDeliverista(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinDeliveristaRelatedByDeliveristaId($q, $join_type);
	}

	/**
	 * @return Deliveries[]
	 */
	static function doSelectJoinDeliveristaRelatedByDeliveristaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Deliverista::getTableName();
		$q->join($to_table, $this_table . '.deliverista_id = ' . $to_table . '.id', $join_type);
		foreach (Deliverista::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Deliverista'));
	}

	/**
	 * @return Deliveries
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Deliveries
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return Deliveries
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Deliveries[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return Deliveries[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Deliverista::getTableName();
		$q->join($to_table, $this_table . '.deliverista_id = ' . $to_table . '.id', $join_type);
		foreach (Deliverista::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Deliverista';
	
		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting producto_delivery Objects(rows) from the producto_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return Query
	 */
	function getProductoDeliverysRelatedByDeliveryIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_delivery', 'delivery_id', 'id', $q);
	}

	/**
	 * Returns the count of ProductoDelivery Objects(rows) from the producto_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return int
	 */
	function countProductoDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ProductoDelivery::doCount($this->getProductoDeliverysRelatedByDeliveryIdQuery($q));
	}

	/**
	 * Deletes the producto_delivery Objects(rows) from the producto_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return int
	 */
	function deleteProductoDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductoDeliverysRelatedByDeliveryId_c = array();
		return ProductoDelivery::doDelete($this->getProductoDeliverysRelatedByDeliveryIdQuery($q));
	}

	protected $ProductoDeliverysRelatedByDeliveryId_c = array();

	/**
	 * Returns an array of ProductoDelivery objects with a delivery_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ProductoDelivery[]
	 */
	function getProductoDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductoDeliverysRelatedByDeliveryId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductoDeliverysRelatedByDeliveryId_c;
		}

		$result = ProductoDelivery::doSelect($this->getProductoDeliverysRelatedByDeliveryIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductoDeliverysRelatedByDeliveryId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_delivery Objects(rows) from the ventas_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return Query
	 */
	function getVentasDeliverysRelatedByDeliveryIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_delivery', 'delivery_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasDelivery Objects(rows) from the ventas_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return int
	 */
	function countVentasDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasDelivery::doCount($this->getVentasDeliverysRelatedByDeliveryIdQuery($q));
	}

	/**
	 * Deletes the ventas_delivery Objects(rows) from the ventas_delivery table
	 * with a delivery_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasDeliverysRelatedByDeliveryId_c = array();
		return VentasDelivery::doDelete($this->getVentasDeliverysRelatedByDeliveryIdQuery($q));
	}

	protected $VentasDeliverysRelatedByDeliveryId_c = array();

	/**
	 * Returns an array of VentasDelivery objects with a delivery_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasDelivery[]
	 */
	function getVentasDeliverysRelatedByDeliveryId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasDeliverysRelatedByDeliveryId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasDeliverysRelatedByDeliveryId_c;
		}

		$result = VentasDelivery::doSelect($this->getVentasDeliverysRelatedByDeliveryIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasDeliverysRelatedByDeliveryId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Deliveries::getProductoDeliverysRelatedBydelivery_id
	 * @return ProductoDelivery[]
	 * @see Deliveries::getProductoDeliverysRelatedByDeliveryId
	 */
	function getProductoDeliverys($extra = null) {
		return $this->getProductoDeliverysRelatedByDeliveryId($extra);
	}

	/**
	  * Convenience function for Deliveries::getProductoDeliverysRelatedBydelivery_idQuery
	  * @return Query
	  * @see Deliveries::getProductoDeliverysRelatedBydelivery_idQuery
	  */
	function getProductoDeliverysQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_delivery', 'delivery_id','id', $q);
	}

	/**
	  * Convenience function for Deliveries::deleteProductoDeliverysRelatedBydelivery_id
	  * @return int
	  * @see Deliveries::deleteProductoDeliverysRelatedBydelivery_id
	  */
	function deleteProductoDeliverys(Query $q = null) {
		return $this->deleteProductoDeliverysRelatedByDeliveryId($q);
	}

	/**
	  * Convenience function for Deliveries::countProductoDeliverysRelatedBydelivery_id
	  * @return int
	  * @see Deliveries::countProductoDeliverysRelatedByDeliveryId
	  */
	function countProductoDeliverys(Query $q = null) {
		return $this->countProductoDeliverysRelatedByDeliveryId($q);
	}

	/**
	 * Convenience function for Deliveries::getVentasDeliverysRelatedBydelivery_id
	 * @return VentasDelivery[]
	 * @see Deliveries::getVentasDeliverysRelatedByDeliveryId
	 */
	function getVentasDeliverys($extra = null) {
		return $this->getVentasDeliverysRelatedByDeliveryId($extra);
	}

	/**
	  * Convenience function for Deliveries::getVentasDeliverysRelatedBydelivery_idQuery
	  * @return Query
	  * @see Deliveries::getVentasDeliverysRelatedBydelivery_idQuery
	  */
	function getVentasDeliverysQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_delivery', 'delivery_id','id', $q);
	}

	/**
	  * Convenience function for Deliveries::deleteVentasDeliverysRelatedBydelivery_id
	  * @return int
	  * @see Deliveries::deleteVentasDeliverysRelatedBydelivery_id
	  */
	function deleteVentasDeliverys(Query $q = null) {
		return $this->deleteVentasDeliverysRelatedByDeliveryId($q);
	}

	/**
	  * Convenience function for Deliveries::countVentasDeliverysRelatedBydelivery_id
	  * @return int
	  * @see Deliveries::countVentasDeliverysRelatedByDeliveryId
	  */
	function countVentasDeliverys(Query $q = null) {
		return $this->countVentasDeliverysRelatedByDeliveryId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getFecha()) {
			$this->_validationErrors[] = 'Fecha must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}