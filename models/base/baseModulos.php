<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseModulos extends ApplicationModel {

	const ID = 'modulos.id';
	const NOMBRE = 'modulos.nombre';
	const URL = 'modulos.url';
	const ICON = 'modulos.icon';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'modulos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Modulos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'url' => Model::COLUMN_TYPE_VARCHAR,
		'icon' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `url` VARCHAR NOT NULL
	 * @var string
	 */
	protected $url;

	/**
	 * `icon` VARCHAR NOT NULL
	 * @var string
	 */
	protected $icon;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Modulos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Modulos
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the url field
	 */
	function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the value of the url field
	 * @return Modulos
	 */
	function setUrl($value) {
		return $this->setColumnValue('url', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the icon field
	 */
	function getIcon() {
		return $this->icon;
	}

	/**
	 * Sets the value of the icon field
	 * @return Modulos
	 */
	function setIcon($value) {
		return $this->setColumnValue('icon', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Modulos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Modulos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Modulos
	 */
	static function retrieveById($value) {
		return Modulos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Modulos
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a url
	 * value that matches the one provided
	 * @return Modulos
	 */
	static function retrieveByUrl($value) {
		return static::retrieveByColumn('url', $value);
	}

	/**
	 * Searches the database for a row with a icon
	 * value that matches the one provided
	 * @return Modulos
	 */
	static function retrieveByIcon($value) {
		return static::retrieveByColumn('icon', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Modulos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Modulos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting submodulo Objects(rows) from the submodulo table
	 * with a idmodulo that matches $this->id.
	 * @return Query
	 */
	function getSubmodulosRelatedByIdmoduloQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('submodulo', 'idmodulo', 'id', $q);
	}

	/**
	 * Returns the count of Submodulo Objects(rows) from the submodulo table
	 * with a idmodulo that matches $this->id.
	 * @return int
	 */
	function countSubmodulosRelatedByIdmodulo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Submodulo::doCount($this->getSubmodulosRelatedByIdmoduloQuery($q));
	}

	/**
	 * Deletes the submodulo Objects(rows) from the submodulo table
	 * with a idmodulo that matches $this->id.
	 * @return int
	 */
	function deleteSubmodulosRelatedByIdmodulo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->SubmodulosRelatedByIdmodulo_c = array();
		return Submodulo::doDelete($this->getSubmodulosRelatedByIdmoduloQuery($q));
	}

	protected $SubmodulosRelatedByIdmodulo_c = array();

	/**
	 * Returns an array of Submodulo objects with a idmodulo
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Submodulo[]
	 */
	function getSubmodulosRelatedByIdmodulo(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->SubmodulosRelatedByIdmodulo_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->SubmodulosRelatedByIdmodulo_c;
		}

		$result = Submodulo::doSelect($this->getSubmodulosRelatedByIdmoduloQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->SubmodulosRelatedByIdmodulo_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Modulos::getSubmodulosRelatedByidmodulo
	 * @return Submodulo[]
	 * @see Modulos::getSubmodulosRelatedByIdmodulo
	 */
	function getSubmodulos($extra = null) {
		return $this->getSubmodulosRelatedByIdmodulo($extra);
	}
    function getSubmodulosT($id = null) {
        $idu = $_SESSION['user']->id;
        $idg = UsuariosGrupo::getAll('WHERE usuario_id = '.$idu)[0]->grupo_id;
        $sql= "SELECT submodulo.id, submodulo.nombre, submodulo.url, submodulo.icon FROM permisos_grupo INNER JOIN permisos ON permisos_grupo.idpermiso = permisos.id INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id WHERE permisos_grupo.idgrupo = ".$idg;
        $result = PermisosGrupo::getConnection()->query($sql);

        return $result->fetchAll();
    }
	/**
	  * Convenience function for Modulos::getSubmodulosRelatedByidmoduloQuery
	  * @return Query
	  * @see Modulos::getSubmodulosRelatedByidmoduloQuery
	  */
	function getSubmodulosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('submodulo', 'idmodulo','id', $q);
	}

	/**
	  * Convenience function for Modulos::deleteSubmodulosRelatedByidmodulo
	  * @return int
	  * @see Modulos::deleteSubmodulosRelatedByidmodulo
	  */
	function deleteSubmodulos(Query $q = null) {
		return $this->deleteSubmodulosRelatedByIdmodulo($q);
	}

	/**
	  * Convenience function for Modulos::countSubmodulosRelatedByidmodulo
	  * @return int
	  * @see Modulos::countSubmodulosRelatedByIdmodulo
	  */
	function countSubmodulos(Query $q = null) {
		return $this->countSubmodulosRelatedByIdmodulo($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->geturl()) {
			$this->_validationErrors[] = 'url must not be null';
		}
		if (null === $this->geticon()) {
			$this->_validationErrors[] = 'icon must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}