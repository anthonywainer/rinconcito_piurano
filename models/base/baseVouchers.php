<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVouchers extends ApplicationModel {

	const ID = 'vouchers.id';
	const TIPO_COMPROBANTE = 'vouchers.tipo_comprobante';
	const FECHA = 'vouchers.fecha';
	const CREATED_AT = 'vouchers.created_at';
	const UPDATED_AT = 'vouchers.updated_at';
	const DELETED_AT = 'vouchers.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'vouchers';

	/**
	 * Cache of objects retrieved from the database
	 * @var Vouchers[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'tipo_comprobante' => Model::COLUMN_TYPE_VARCHAR,
		'fecha' => Model::COLUMN_TYPE_DATE,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `tipo_comprobante` VARCHAR NOT NULL
	 * @var string
	 */
	protected $tipo_comprobante;

	/**
	 * `fecha` DATE NOT NULL
	 * @var string
	 */
	protected $fecha;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Vouchers
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the tipo_comprobante field
	 */
	function getTipoComprobante() {
		return $this->tipo_comprobante;
	}

	/**
	 * Sets the value of the tipo_comprobante field
	 * @return Vouchers
	 */
	function setTipoComprobante($value) {
		return $this->setColumnValue('tipo_comprobante', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Vouchers::getTipoComprobante
	 * final because getTipoComprobante should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::getTipoComprobante
	 */
	final function getTipo_comprobante() {
		return $this->getTipoComprobante();
	}

	/**
	 * Convenience function for Vouchers::setTipoComprobante
	 * final because setTipoComprobante should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::setTipoComprobante
	 * @return Vouchers
	 */
	final function setTipo_comprobante($value) {
		return $this->setTipoComprobante($value);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return Vouchers
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Vouchers
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Vouchers::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Vouchers::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::setCreatedAt
	 * @return Vouchers
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Vouchers
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Vouchers::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Vouchers::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::setUpdatedAt
	 * @return Vouchers
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Vouchers
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Vouchers::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Vouchers::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Vouchers::setDeletedAt
	 * @return Vouchers
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Vouchers
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Vouchers
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveById($value) {
		return Vouchers::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a tipo_comprobante
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveByTipoComprobante($value) {
		return static::retrieveByColumn('tipo_comprobante', $value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Vouchers
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Vouchers
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Vouchers[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a voucher_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientosDeDinerosRelatedByVoucherIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'voucher_id', 'id', $q);
	}

	/**
	 * Returns the count of MovimientosDeDinero Objects(rows) from the movimientos_de_dinero table
	 * with a voucher_id that matches $this->id.
	 * @return int
	 */
	function countMovimientosDeDinerosRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MovimientosDeDinero::doCount($this->getMovimientosDeDinerosRelatedByVoucherIdQuery($q));
	}

	/**
	 * Deletes the movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a voucher_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientosDeDinerosRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientosDeDinerosRelatedByVoucherId_c = array();
		return MovimientosDeDinero::doDelete($this->getMovimientosDeDinerosRelatedByVoucherIdQuery($q));
	}

	protected $MovimientosDeDinerosRelatedByVoucherId_c = array();

	/**
	 * Returns an array of MovimientosDeDinero objects with a voucher_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MovimientosDeDinero[]
	 */
	function getMovimientosDeDinerosRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientosDeDinerosRelatedByVoucherId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientosDeDinerosRelatedByVoucherId_c;
		}

		$result = MovimientosDeDinero::doSelect($this->getMovimientosDeDinerosRelatedByVoucherIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientosDeDinerosRelatedByVoucherId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting series Objects(rows) from the series table
	 * with a voucher_id that matches $this->id.
	 * @return Query
	 */
	function getSeriessRelatedByVoucherIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('series', 'voucher_id', 'id', $q);
	}

	/**
	 * Returns the count of Series Objects(rows) from the series table
	 * with a voucher_id that matches $this->id.
	 * @return int
	 */
	function countSeriessRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Series::doCount($this->getSeriessRelatedByVoucherIdQuery($q));
	}

	/**
	 * Deletes the series Objects(rows) from the series table
	 * with a voucher_id that matches $this->id.
	 * @return int
	 */
	function deleteSeriessRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->SeriessRelatedByVoucherId_c = array();
		return Series::doDelete($this->getSeriessRelatedByVoucherIdQuery($q));
	}

	protected $SeriessRelatedByVoucherId_c = array();

	/**
	 * Returns an array of Series objects with a voucher_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Series[]
	 */
	function getSeriessRelatedByVoucherId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->SeriessRelatedByVoucherId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->SeriessRelatedByVoucherId_c;
		}

		$result = Series::doSelect($this->getSeriessRelatedByVoucherIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->SeriessRelatedByVoucherId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Vouchers::getMovimientosDeDinerosRelatedByvoucher_id
	 * @return MovimientosDeDinero[]
	 * @see Vouchers::getMovimientosDeDinerosRelatedByVoucherId
	 */
	function getMovimientosDeDineros($extra = null) {
		return $this->getMovimientosDeDinerosRelatedByVoucherId($extra);
	}

	/**
	  * Convenience function for Vouchers::getMovimientosDeDinerosRelatedByvoucher_idQuery
	  * @return Query
	  * @see Vouchers::getMovimientosDeDinerosRelatedByvoucher_idQuery
	  */
	function getMovimientosDeDinerosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'voucher_id','id', $q);
	}

	/**
	  * Convenience function for Vouchers::deleteMovimientosDeDinerosRelatedByvoucher_id
	  * @return int
	  * @see Vouchers::deleteMovimientosDeDinerosRelatedByvoucher_id
	  */
	function deleteMovimientosDeDineros(Query $q = null) {
		return $this->deleteMovimientosDeDinerosRelatedByVoucherId($q);
	}

	/**
	  * Convenience function for Vouchers::countMovimientosDeDinerosRelatedByvoucher_id
	  * @return int
	  * @see Vouchers::countMovimientosDeDinerosRelatedByVoucherId
	  */
	function countMovimientosDeDineros(Query $q = null) {
		return $this->countMovimientosDeDinerosRelatedByVoucherId($q);
	}

	/**
	 * Convenience function for Vouchers::getSeriessRelatedByvoucher_id
	 * @return Series[]
	 * @see Vouchers::getSeriessRelatedByVoucherId
	 */
	function getSeriess($extra = null) {
		return $this->getSeriessRelatedByVoucherId($extra);
	}

	/**
	  * Convenience function for Vouchers::getSeriessRelatedByvoucher_idQuery
	  * @return Query
	  * @see Vouchers::getSeriessRelatedByvoucher_idQuery
	  */
	function getSeriessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('series', 'voucher_id','id', $q);
	}

	/**
	  * Convenience function for Vouchers::deleteSeriessRelatedByvoucher_id
	  * @return int
	  * @see Vouchers::deleteSeriessRelatedByvoucher_id
	  */
	function deleteSeriess(Query $q = null) {
		return $this->deleteSeriessRelatedByVoucherId($q);
	}

	/**
	  * Convenience function for Vouchers::countSeriessRelatedByvoucher_id
	  * @return int
	  * @see Vouchers::countSeriessRelatedByVoucherId
	  */
	function countSeriess(Query $q = null) {
		return $this->countSeriessRelatedByVoucherId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->gettipo_comprobante()) {
			$this->_validationErrors[] = 'tipo_comprobante must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}