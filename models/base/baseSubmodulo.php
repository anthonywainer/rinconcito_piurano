<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseSubmodulo extends ApplicationModel {

	const ID = 'submodulo.id';
	const NOMBRE = 'submodulo.nombre';
	const URL = 'submodulo.url';
	const ICON = 'submodulo.icon';
	const IDMODULO = 'submodulo.idmodulo';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'submodulo';

	/**
	 * Cache of objects retrieved from the database
	 * @var Submodulo[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'url' => Model::COLUMN_TYPE_VARCHAR,
		'icon' => Model::COLUMN_TYPE_VARCHAR,
		'idmodulo' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `url` VARCHAR NOT NULL
	 * @var string
	 */
	protected $url;

	/**
	 * `icon` VARCHAR NOT NULL
	 * @var string
	 */
	protected $icon;

	/**
	 * `idmodulo` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $idmodulo;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Submodulo
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Submodulo
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the url field
	 */
	function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the value of the url field
	 * @return Submodulo
	 */
	function setUrl($value) {
		return $this->setColumnValue('url', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the icon field
	 */
	function getIcon() {
		return $this->icon;
	}

	/**
	 * Sets the value of the icon field
	 * @return Submodulo
	 */
	function setIcon($value) {
		return $this->setColumnValue('icon', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the idmodulo field
	 */
	function getIdmodulo() {
		return $this->idmodulo;
	}
    function getModulosT($id=null) {
        return Modulos::getAll('WHERE id= '.$id);
    }

	/**
	 * Sets the value of the idmodulo field
	 * @return Submodulo
	 */
	function setIdmodulo($value) {
		return $this->setColumnValue('idmodulo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Submodulo
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Submodulo
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Submodulo
	 */
	static function retrieveById($value) {
		return Submodulo::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Submodulo
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a url
	 * value that matches the one provided
	 * @return Submodulo
	 */
	static function retrieveByUrl($value) {
		return static::retrieveByColumn('url', $value);
	}

	/**
	 * Searches the database for a row with a icon
	 * value that matches the one provided
	 * @return Submodulo
	 */
	static function retrieveByIcon($value) {
		return static::retrieveByColumn('icon', $value);
	}

	/**
	 * Searches the database for a row with a idmodulo
	 * value that matches the one provided
	 * @return Submodulo
	 */
	static function retrieveByIdmodulo($value) {
		return static::retrieveByColumn('idmodulo', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Submodulo
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->idmodulo = (null === $this->idmodulo) ? null : (int) $this->idmodulo;
		return $this;
	}

	/**
	 * @return Submodulo
	 */
	function setModulosRelatedByIdmodulo(Modulos $modulos = null) {
		if (null === $modulos) {
			$this->setidmodulo(null);
		} else {
			if (!$modulos->getid()) {
				throw new Exception('Cannot connect a Modulos without a id');
			}
			$this->setidmodulo($modulos->getid());
		}
		return $this;
	}

	/**
	 * Returns a modulos object with a id
	 * that matches $this->idmodulo.
	 * @return Modulos
	 */
	function getModulosRelatedByIdmodulo() {
		$fk_value = $this->getidmodulo();
		if (null === $fk_value) {
			return null;
		}
		return Modulos::retrieveByPK($fk_value);
	}

	/**
	 * Returns a modulos object with a id
	 * that matches $this->idmodulo.
	 * @return Modulos
	 */
	function getModulos() {
		return $this->getModulosRelatedByIdmodulo();
	}

	/**
	 * @return Submodulo
	 */
	function setModulos(Modulos $modulos = null) {
		return $this->setModulosRelatedByIdmodulo($modulos);
	}

	/**
	 * @return Submodulo[]
	 */
	static function doSelectJoinModulosRelatedByIdmodulo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Modulos::getTableName();
		$q->join($to_table, $this_table . '.idmodulo = ' . $to_table . '.id', $join_type);
		foreach (Modulos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Modulos'));
	}

	/**
	 * @return Submodulo[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Modulos::getTableName();
		$q->join($to_table, $this_table . '.idmodulo = ' . $to_table . '.id', $join_type);
		foreach (Modulos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Modulos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->geturl()) {
			$this->_validationErrors[] = 'url must not be null';
		}
		if (null === $this->geticon()) {
			$this->_validationErrors[] = 'icon must not be null';
		}
		if (null === $this->getidmodulo()) {
			$this->_validationErrors[] = 'idmodulo must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}