<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductoDelivery extends ApplicationModel {

	const ID = 'producto_delivery.id';
	const PRODUCTO_ID = 'producto_delivery.producto_id';
	const DELIVERY_ID = 'producto_delivery.delivery_id';
	const CANTIDAD = 'producto_delivery.cantidad';
	const TOTAL = 'producto_delivery.total';
	const CREATED_AT = 'producto_delivery.created_at';
	const UPDATED_AT = 'producto_delivery.updated_at';
	const DELETED_AT = 'producto_delivery.deleted_at';
	const PRECIO = 'producto_delivery.precio';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'producto_delivery';

	/**
	 * Cache of objects retrieved from the database
	 * @var ProductoDelivery[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'producto_id' => Model::COLUMN_TYPE_INTEGER,
		'delivery_id' => Model::COLUMN_TYPE_INTEGER,
		'cantidad' => Model::COLUMN_TYPE_INTEGER,
		'total' => Model::COLUMN_TYPE_FLOAT,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'precio' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $producto_id;

	/**
	 * `delivery_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $delivery_id;

	/**
	 * `cantidad` INTEGER NOT NULL DEFAULT 1
	 * @var int
	 */
	protected $cantidad = 1;

	/**
	 * `total` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $total;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `precio` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $precio;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ProductoDelivery
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the producto_id field
	 */
	function getProductoId() {
		return $this->producto_id;
	}

	/**
	 * Sets the value of the producto_id field
	 * @return ProductoDelivery
	 */
	function setProductoId($value) {
		return $this->setColumnValue('producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductoDelivery::getProductoId
	 * final because getProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::getProductoId
	 */
	final function getProducto_id() {
		return $this->getProductoId();
	}

	/**
	 * Convenience function for ProductoDelivery::setProductoId
	 * final because setProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::setProductoId
	 * @return ProductoDelivery
	 */
	final function setProducto_id($value) {
		return $this->setProductoId($value);
	}

	/**
	 * Gets the value of the delivery_id field
	 */
	function getDeliveryId() {
		return $this->delivery_id;
	}

	/**
	 * Sets the value of the delivery_id field
	 * @return ProductoDelivery
	 */
	function setDeliveryId($value) {
		return $this->setColumnValue('delivery_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductoDelivery::getDeliveryId
	 * final because getDeliveryId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::getDeliveryId
	 */
	final function getDelivery_id() {
		return $this->getDeliveryId();
	}

	/**
	 * Convenience function for ProductoDelivery::setDeliveryId
	 * final because setDeliveryId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::setDeliveryId
	 * @return ProductoDelivery
	 */
	final function setDelivery_id($value) {
		return $this->setDeliveryId($value);
	}

	/**
	 * Gets the value of the cantidad field
	 */
	function getCantidad() {
		return $this->cantidad;
	}

	/**
	 * Sets the value of the cantidad field
	 * @return ProductoDelivery
	 */
	function setCantidad($value) {
		return $this->setColumnValue('cantidad', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the total field
	 */
	function getTotal() {
		return $this->total;
	}

	/**
	 * Sets the value of the total field
	 * @return ProductoDelivery
	 */
	function setTotal($value) {
		return $this->setColumnValue('total', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ProductoDelivery
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductoDelivery::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ProductoDelivery::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::setCreatedAt
	 * @return ProductoDelivery
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ProductoDelivery
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductoDelivery::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ProductoDelivery::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::setUpdatedAt
	 * @return ProductoDelivery
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ProductoDelivery
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductoDelivery::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ProductoDelivery::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoDelivery::setDeletedAt
	 * @return ProductoDelivery
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return ProductoDelivery
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ProductoDelivery
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ProductoDelivery
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveById($value) {
		return ProductoDelivery::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a producto_id
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByProductoId($value) {
		return static::retrieveByColumn('producto_id', $value);
	}

	/**
	 * Searches the database for a row with a delivery_id
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByDeliveryId($value) {
		return static::retrieveByColumn('delivery_id', $value);
	}

	/**
	 * Searches the database for a row with a cantidad
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByCantidad($value) {
		return static::retrieveByColumn('cantidad', $value);
	}

	/**
	 * Searches the database for a row with a total
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByTotal($value) {
		return static::retrieveByColumn('total', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return ProductoDelivery
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ProductoDelivery
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->producto_id = (null === $this->producto_id) ? null : (int) $this->producto_id;
		$this->delivery_id = (null === $this->delivery_id) ? null : (int) $this->delivery_id;
		$this->cantidad = (null === $this->cantidad) ? null : (int) $this->cantidad;
		return $this;
	}

	/**
	 * @return ProductoDelivery
	 */
	function setProducto(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoDelivery
	 */
	function setProductosRelatedByProductoId(Productos $productos = null) {
		if (null === $productos) {
			$this->setproducto_id(null);
		} else {
			if (!$productos->getid()) {
				throw new Exception('Cannot connect a Productos without a id');
			}
			$this->setproducto_id($productos->getid());
		}
		return $this;
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProducto() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductosRelatedByProductoId() {
		$fk_value = $this->getproducto_id();
		if (null === $fk_value) {
			return null;
		}
		return Productos::retrieveByPK($fk_value);
	}

	static function doSelectJoinProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinProductosRelatedByProductoId($q, $join_type);
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductos() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * @return ProductoDelivery
	 */
	function setProductos(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoDelivery[]
	 */
	static function doSelectJoinProductosRelatedByProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Productos'));
	}

	/**
	 * @return ProductoDelivery
	 */
	function setDelivery(Deliveries $deliveries = null) {
		return $this->setDeliveriesRelatedByDeliveryId($deliveries);
	}

	/**
	 * @return ProductoDelivery
	 */
	function setDeliveriesRelatedByDeliveryId(Deliveries $deliveries = null) {
		if (null === $deliveries) {
			$this->setdelivery_id(null);
		} else {
			if (!$deliveries->getid()) {
				throw new Exception('Cannot connect a Deliveries without a id');
			}
			$this->setdelivery_id($deliveries->getid());
		}
		return $this;
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDelivery() {
		return $this->getDeliveriesRelatedByDeliveryId();
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDeliveriesRelatedByDeliveryId() {
		$fk_value = $this->getdelivery_id();
		if (null === $fk_value) {
			return null;
		}
		return Deliveries::retrieveByPK($fk_value);
	}

	static function doSelectJoinDelivery(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinDeliveriesRelatedByDeliveryId($q, $join_type);
	}

	/**
	 * Returns a deliveries object with a id
	 * that matches $this->delivery_id.
	 * @return Deliveries
	 */
	function getDeliveries() {
		return $this->getDeliveriesRelatedByDeliveryId();
	}

	/**
	 * @return ProductoDelivery
	 */
	function setDeliveries(Deliveries $deliveries = null) {
		return $this->setDeliveriesRelatedByDeliveryId($deliveries);
	}

	/**
	 * @return ProductoDelivery[]
	 */
	static function doSelectJoinDeliveriesRelatedByDeliveryId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Deliveries::getTableName();
		$q->join($to_table, $this_table . '.delivery_id = ' . $to_table . '.id', $join_type);
		foreach (Deliveries::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Deliveries'));
	}

	/**
	 * @return ProductoDelivery[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Productos';
	
		$to_table = Deliveries::getTableName();
		$q->join($to_table, $this_table . '.delivery_id = ' . $to_table . '.id', $join_type);
		foreach (Deliveries::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Deliveries';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getproducto_id()) {
			$this->_validationErrors[] = 'producto_id must not be null';
		}
		if (null === $this->getdelivery_id()) {
			$this->_validationErrors[] = 'delivery_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}