<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUsuariosGrupo extends ApplicationModel {

	const ID = 'usuarios_grupo.id';
	const USUARIO_ID = 'usuarios_grupo.usuario_id';
	const GRUPO_ID = 'usuarios_grupo.grupo_id';
	const CREATED_AT = 'usuarios_grupo.created_at';
	const UPDATED_AT = 'usuarios_grupo.updated_at';
	const DELETED_AT = 'usuarios_grupo.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'usuarios_grupo';

	/**
	 * Cache of objects retrieved from the database
	 * @var UsuariosGrupo[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'usuario_id' => Model::COLUMN_TYPE_INTEGER,
		'grupo_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `usuario_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $usuario_id;

	/**
	 * `grupo_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $grupo_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return UsuariosGrupo
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return UsuariosGrupo
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for UsuariosGrupo::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for UsuariosGrupo::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::setUsuarioId
	 * @return UsuariosGrupo
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the grupo_id field
	 */
	function getGrupoId() {
		return $this->grupo_id;
	}

	/**
	 * Sets the value of the grupo_id field
	 * @return UsuariosGrupo
	 */
	function setGrupoId($value) {
		return $this->setColumnValue('grupo_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for UsuariosGrupo::getGrupoId
	 * final because getGrupoId should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::getGrupoId
	 */
	final function getGrupo_id() {
		return $this->getGrupoId();
	}

	/**
	 * Convenience function for UsuariosGrupo::setGrupoId
	 * final because setGrupoId should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::setGrupoId
	 * @return UsuariosGrupo
	 */
	final function setGrupo_id($value) {
		return $this->setGrupoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return UsuariosGrupo
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UsuariosGrupo::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for UsuariosGrupo::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::setCreatedAt
	 * @return UsuariosGrupo
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return UsuariosGrupo
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UsuariosGrupo::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for UsuariosGrupo::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::setUpdatedAt
	 * @return UsuariosGrupo
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return UsuariosGrupo
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UsuariosGrupo::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for UsuariosGrupo::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UsuariosGrupo::setDeletedAt
	 * @return UsuariosGrupo
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return UsuariosGrupo
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return UsuariosGrupo
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveById($value) {
		return UsuariosGrupo::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a grupo_id
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveByGrupoId($value) {
		return static::retrieveByColumn('grupo_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return UsuariosGrupo
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return UsuariosGrupo
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->usuario_id = (null === $this->usuario_id) ? null : (int) $this->usuario_id;
		$this->grupo_id = (null === $this->grupo_id) ? null : (int) $this->grupo_id;
		return $this;
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setUsuario(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setUsuariosRelatedByUsuarioId(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setusuario_id(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setusuario_id($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuario() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByUsuarioId() {
		$fk_value = $this->getusuario_id();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	static function doSelectJoinUsuario(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinUsuariosRelatedByUsuarioId($q, $join_type);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->usuario_id.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByUsuarioId();
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByUsuarioId($usuarios);
	}

	/**
	 * @return UsuariosGrupo[]
	 */
	static function doSelectJoinUsuariosRelatedByUsuarioId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setGrupo(Grupos $grupos = null) {
		return $this->setGruposRelatedByGrupoId($grupos);
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setGruposRelatedByGrupoId(Grupos $grupos = null) {
		if (null === $grupos) {
			$this->setgrupo_id(null);
		} else {
			if (!$grupos->getid()) {
				throw new Exception('Cannot connect a Grupos without a id');
			}
			$this->setgrupo_id($grupos->getid());
		}
		return $this;
	}

	/**
	 * Returns a grupos object with a id
	 * that matches $this->grupo_id.
	 * @return Grupos
	 */
	function getGrupo() {
		return $this->getGruposRelatedByGrupoId();
	}

	/**
	 * Returns a grupos object with a id
	 * that matches $this->grupo_id.
	 * @return Grupos
	 */
	function getGruposRelatedByGrupoId() {
		$fk_value = $this->getgrupo_id();
		if (null === $fk_value) {
			return null;
		}
		return Grupos::retrieveByPK($fk_value);
	}

	static function doSelectJoinGrupo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinGruposRelatedByGrupoId($q, $join_type);
	}

	/**
	 * Returns a grupos object with a id
	 * that matches $this->grupo_id.
	 * @return Grupos
	 */
	function getGrupos() {
		return $this->getGruposRelatedByGrupoId();
	}

	/**
	 * @return UsuariosGrupo
	 */
	function setGrupos(Grupos $grupos = null) {
		return $this->setGruposRelatedByGrupoId($grupos);
	}

	/**
	 * @return UsuariosGrupo[]
	 */
	static function doSelectJoinGruposRelatedByGrupoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Grupos::getTableName();
		$q->join($to_table, $this_table . '.grupo_id = ' . $to_table . '.id', $join_type);
		foreach (Grupos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Grupos'));
	}

	/**
	 * @return UsuariosGrupo[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.usuario_id = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$to_table = Grupos::getTableName();
		$q->join($to_table, $this_table . '.grupo_id = ' . $to_table . '.id', $join_type);
		foreach (Grupos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Grupos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getusuario_id()) {
			$this->_validationErrors[] = 'usuario_id must not be null';
		}
		if (null === $this->getgrupo_id()) {
			$this->_validationErrors[] = 'grupo_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}