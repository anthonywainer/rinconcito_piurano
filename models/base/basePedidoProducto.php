<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePedidoProducto extends ApplicationModel {

	const ID = 'pedido_producto.id';
	const PRODUCTO_ID = 'pedido_producto.producto_id';
	const VENTA_ID = 'pedido_producto.venta_id';
	const CANTIDAD = 'pedido_producto.cantidad';
	const PRECIO = 'pedido_producto.precio';
	const TOTAL = 'pedido_producto.total';
	const CREATED_AT = 'pedido_producto.created_at';
	const UPDATED_AT = 'pedido_producto.updated_at';
	const DELETED_AT = 'pedido_producto.deleted_at';
	const TIEMPO_ESPERA = 'pedido_producto.tiempo_espera';
	const ESTADO_PEDIDO = 'pedido_producto.estado_pedido';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'pedido_producto';

	/**
	 * Cache of objects retrieved from the database
	 * @var PedidoProducto[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'producto_id' => Model::COLUMN_TYPE_INTEGER,
		'venta_id' => Model::COLUMN_TYPE_INTEGER,
		'cantidad' => Model::COLUMN_TYPE_INTEGER,
		'precio' => Model::COLUMN_TYPE_FLOAT,
		'total' => Model::COLUMN_TYPE_FLOAT,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'tiempo_espera' => Model::COLUMN_TYPE_VARCHAR,
		'estado_pedido' => Model::COLUMN_TYPE_BOOLEAN,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $producto_id;

	/**
	 * `venta_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $venta_id;

	/**
	 * `cantidad` INTEGER DEFAULT 1
	 * @var int
	 */
	protected $cantidad = 1;

	/**
	 * `precio` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $precio;

	/**
	 * `total` FLOAT DEFAULT 0
	 * @var double
	 */
	protected $total = 0;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `tiempo_espera` VARCHAR DEFAULT 0
	 * @var string
	 */
	protected $tiempo_espera = '0';

	/**
	 * `estado_pedido` BOOLEAN DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $estado_pedido = 'b\'1\'';

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return PedidoProducto
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the producto_id field
	 */
	function getProductoId() {
		return $this->producto_id;
	}

	/**
	 * Sets the value of the producto_id field
	 * @return PedidoProducto
	 */
	function setProductoId($value) {
		return $this->setColumnValue('producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for PedidoProducto::getProductoId
	 * final because getProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getProductoId
	 */
	final function getProducto_id() {
		return $this->getProductoId();
	}

	/**
	 * Convenience function for PedidoProducto::setProductoId
	 * final because setProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setProductoId
	 * @return PedidoProducto
	 */
	final function setProducto_id($value) {
		return $this->setProductoId($value);
	}

	/**
	 * Gets the value of the venta_id field
	 */
	function getVentaId() {
		return $this->venta_id;
	}

	/**
	 * Sets the value of the venta_id field
	 * @return PedidoProducto
	 */
	function setVentaId($value) {
		return $this->setColumnValue('venta_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for PedidoProducto::getVentaId
	 * final because getVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getVentaId
	 */
	final function getVenta_id() {
		return $this->getVentaId();
	}

	/**
	 * Convenience function for PedidoProducto::setVentaId
	 * final because setVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setVentaId
	 * @return PedidoProducto
	 */
	final function setVenta_id($value) {
		return $this->setVentaId($value);
	}

	/**
	 * Gets the value of the cantidad field
	 */
	function getCantidad() {
		return $this->cantidad;
	}

	/**
	 * Sets the value of the cantidad field
	 * @return PedidoProducto
	 */
	function setCantidad($value) {
		return $this->setColumnValue('cantidad', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return PedidoProducto
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the total field
	 */
	function getTotal() {
		return $this->total;
	}

	/**
	 * Sets the value of the total field
	 * @return PedidoProducto
	 */
	function setTotal($value) {
		return $this->setColumnValue('total', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return PedidoProducto
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for PedidoProducto::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for PedidoProducto::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setCreatedAt
	 * @return PedidoProducto
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return PedidoProducto
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for PedidoProducto::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for PedidoProducto::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setUpdatedAt
	 * @return PedidoProducto
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return PedidoProducto
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for PedidoProducto::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for PedidoProducto::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setDeletedAt
	 * @return PedidoProducto
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the tiempo_espera field
	 */
	function getTiempoEspera() {
		return $this->tiempo_espera;
	}

	/**
	 * Sets the value of the tiempo_espera field
	 * @return PedidoProducto
	 */
	function setTiempoEspera($value) {
		return $this->setColumnValue('tiempo_espera', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for PedidoProducto::getTiempoEspera
	 * final because getTiempoEspera should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getTiempoEspera
	 */
	final function getTiempo_espera() {
		return $this->getTiempoEspera();
	}

	/**
	 * Convenience function for PedidoProducto::setTiempoEspera
	 * final because setTiempoEspera should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setTiempoEspera
	 * @return PedidoProducto
	 */
	final function setTiempo_espera($value) {
		return $this->setTiempoEspera($value);
	}

	/**
	 * Gets the value of the estado_pedido field
	 */
	function getEstadoPedido() {
		return $this->estado_pedido;
	}

	/**
	 * Sets the value of the estado_pedido field
	 * @return PedidoProducto
	 */
	function setEstadoPedido($value) {
		return $this->setColumnValue('estado_pedido', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for PedidoProducto::getEstadoPedido
	 * final because getEstadoPedido should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::getEstadoPedido
	 */
	final function getEstado_pedido() {
		return $this->getEstadoPedido();
	}

	/**
	 * Convenience function for PedidoProducto::setEstadoPedido
	 * final because setEstadoPedido should be extended instead
	 * to ensure consistent behavior
	 * @see PedidoProducto::setEstadoPedido
	 * @return PedidoProducto
	 */
	final function setEstado_pedido($value) {
		return $this->setEstadoPedido($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return PedidoProducto
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return PedidoProducto
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveById($value) {
		return PedidoProducto::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a producto_id
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByProductoId($value) {
		return static::retrieveByColumn('producto_id', $value);
	}

	/**
	 * Searches the database for a row with a venta_id
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByVentaId($value) {
		return static::retrieveByColumn('venta_id', $value);
	}

	/**
	 * Searches the database for a row with a cantidad
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByCantidad($value) {
		return static::retrieveByColumn('cantidad', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}

	/**
	 * Searches the database for a row with a total
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByTotal($value) {
		return static::retrieveByColumn('total', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a tiempo_espera
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByTiempoEspera($value) {
		return static::retrieveByColumn('tiempo_espera', $value);
	}

	/**
	 * Searches the database for a row with a estado_pedido
	 * value that matches the one provided
	 * @return PedidoProducto
	 */
	static function retrieveByEstadoPedido($value) {
		return static::retrieveByColumn('estado_pedido', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return PedidoProducto
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->producto_id = (null === $this->producto_id) ? null : (int) $this->producto_id;
		$this->venta_id = (null === $this->venta_id) ? null : (int) $this->venta_id;
		$this->cantidad = (null === $this->cantidad) ? null : (int) $this->cantidad;
		$this->estado_pedido = (null === $this->estado_pedido) ? null : (int) $this->estado_pedido;
		return $this;
	}

	/**
	 * @return PedidoProducto
	 */
	function setVenta(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return PedidoProducto
	 */
	function setVentasRelatedByVentaId(Ventas $ventas = null) {
		if (null === $ventas) {
			$this->setventa_id(null);
		} else {
			if (!$ventas->getid()) {
				throw new Exception('Cannot connect a Ventas without a id');
			}
			$this->setventa_id($ventas->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVenta() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentasRelatedByVentaId() {
		$fk_value = $this->getventa_id();
		if (null === $fk_value) {
			return null;
		}
		return Ventas::retrieveByPK($fk_value);
	}

	static function doSelectJoinVenta(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVentasRelatedByVentaId($q, $join_type);
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentas() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * @return PedidoProducto
	 */
	function setVentas(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return PedidoProducto[]
	 */
	static function doSelectJoinVentasRelatedByVentaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ventas'));
	}

	/**
	 * @return PedidoProducto
	 */
	function setProducto(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return PedidoProducto
	 */
	function setProductosRelatedByProductoId(Productos $productos = null) {
		if (null === $productos) {
			$this->setproducto_id(null);
		} else {
			if (!$productos->getid()) {
				throw new Exception('Cannot connect a Productos without a id');
			}
			$this->setproducto_id($productos->getid());
		}
		return $this;
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProducto() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductosRelatedByProductoId() {
		$fk_value = $this->getproducto_id();
		if (null === $fk_value) {
			return null;
		}
		return Productos::retrieveByPK($fk_value);
	}

	static function doSelectJoinProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinProductosRelatedByProductoId($q, $join_type);
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductos() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * @return PedidoProducto
	 */
	function setProductos(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return PedidoProducto[]
	 */
	static function doSelectJoinProductosRelatedByProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Productos'));
	}

	/**
	 * @return PedidoProducto[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ventas';
	
		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Productos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getproducto_id()) {
			$this->_validationErrors[] = 'producto_id must not be null';
		}
		if (null === $this->getventa_id()) {
			$this->_validationErrors[] = 'venta_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}