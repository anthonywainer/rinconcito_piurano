<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentas extends ApplicationModel {

	const ID = 'ventas.id';
	const MESERO_ID = 'ventas.mesero_id';
	const CLIENTE_ID = 'ventas.cliente_id';
	const CREATED_AT = 'ventas.created_at';
	const UPDATED_AT = 'ventas.updated_at';
	const DELETED_AT = 'ventas.deleted_at';
	const ESTADO_VENTA = 'ventas.estado_venta';
	const FECHA = 'ventas.fecha';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ventas';

	/**
	 * Cache of objects retrieved from the database
	 * @var Ventas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'mesero_id' => Model::COLUMN_TYPE_INTEGER,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'estado_venta' => Model::COLUMN_TYPE_BOOLEAN,
		'fecha' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `mesero_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $mesero_id;

	/**
	 * `cliente_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `estado_venta` BOOLEAN NOT NULL DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $estado_venta = 'b\'1\'';

	/**
	 * `fecha` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Ventas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the mesero_id field
	 */
	function getMeseroId() {
		return $this->mesero_id;
	}

	/**
	 * Sets the value of the mesero_id field
	 * @return Ventas
	 */
	function setMeseroId($value) {
		return $this->setColumnValue('mesero_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Ventas::getMeseroId
	 * final because getMeseroId should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getMeseroId
	 */
	final function getMesero_id() {
		return $this->getMeseroId();
	}

	/**
	 * Convenience function for Ventas::setMeseroId
	 * final because setMeseroId should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setMeseroId
	 * @return Ventas
	 */
	final function setMesero_id($value) {
		return $this->setMeseroId($value);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return Ventas
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Ventas::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for Ventas::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setClienteId
	 * @return Ventas
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Ventas
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Ventas::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Ventas::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setCreatedAt
	 * @return Ventas
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Ventas
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Ventas::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Ventas::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setUpdatedAt
	 * @return Ventas
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Ventas
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Ventas::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Ventas::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setDeletedAt
	 * @return Ventas
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the estado_venta field
	 */
	function getEstadoVenta() {
		return $this->estado_venta;
	}

	/**
	 * Sets the value of the estado_venta field
	 * @return Ventas
	 */
	function setEstadoVenta($value) {
		return $this->setColumnValue('estado_venta', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for Ventas::getEstadoVenta
	 * final because getEstadoVenta should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::getEstadoVenta
	 */
	final function getEstado_venta() {
		return $this->getEstadoVenta();
	}

	/**
	 * Convenience function for Ventas::setEstadoVenta
	 * final because setEstadoVenta should be extended instead
	 * to ensure consistent behavior
	 * @see Ventas::setEstadoVenta
	 * @return Ventas
	 */
	final function setEstado_venta($value) {
		return $this->setEstadoVenta($value);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return Ventas
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Ventas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Ventas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveById($value) {
		return Ventas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a mesero_id
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByMeseroId($value) {
		return static::retrieveByColumn('mesero_id', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a estado_venta
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByEstadoVenta($value) {
		return static::retrieveByColumn('estado_venta', $value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return Ventas
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Ventas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->mesero_id = (null === $this->mesero_id) ? null : (int) $this->mesero_id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		$this->estado_venta = (null === $this->estado_venta) ? null : (int) $this->estado_venta;
		return $this;
	}

	/**
	 * @return Ventas
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Ventas
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return Ventas
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Ventas[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return Ventas
	 */
	function setMesero(Meseros $meseros = null) {
		return $this->setMeserosRelatedByMeseroId($meseros);
	}

	/**
	 * @return Ventas
	 */
	function setMeserosRelatedByMeseroId(Meseros $meseros = null) {
		if (null === $meseros) {
			$this->setmesero_id(null);
		} else {
			if (!$meseros->getid()) {
				throw new Exception('Cannot connect a Meseros without a id');
			}
			$this->setmesero_id($meseros->getid());
		}
		return $this;
	}

	/**
	 * Returns a meseros object with a id
	 * that matches $this->mesero_id.
	 * @return Meseros
	 */
	function getMesero() {
		return $this->getMeserosRelatedByMeseroId();
	}

	/**
	 * Returns a meseros object with a id
	 * that matches $this->mesero_id.
	 * @return Meseros
	 */
	function getMeserosRelatedByMeseroId() {
		$fk_value = $this->getmesero_id();
		if (null === $fk_value) {
			return null;
		}
		return Meseros::retrieveByPK($fk_value);
	}

	static function doSelectJoinMesero(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMeserosRelatedByMeseroId($q, $join_type);
	}

	/**
	 * Returns a meseros object with a id
	 * that matches $this->mesero_id.
	 * @return Meseros
	 */
	function getMeseros() {
		return $this->getMeserosRelatedByMeseroId();
	}

	/**
	 * @return Ventas
	 */
	function setMeseros(Meseros $meseros = null) {
		return $this->setMeserosRelatedByMeseroId($meseros);
	}

	/**
	 * @return Ventas[]
	 */
	static function doSelectJoinMeserosRelatedByMeseroId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Meseros::getTableName();
		$q->join($to_table, $this_table . '.mesero_id = ' . $to_table . '.id', $join_type);
		foreach (Meseros::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Meseros'));
	}

	/**
	 * @return Ventas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$to_table = Meseros::getTableName();
		$q->join($to_table, $this_table . '.mesero_id = ' . $to_table . '.id', $join_type);
		foreach (Meseros::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Meseros';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting cronogramas_de_pago Objects(rows) from the cronogramas_de_pago table
	 * with a venta_id that matches $this->id.
	 * @return Query
	 */
	function getCronogramasDePagosRelatedByVentaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cronogramas_de_pago', 'venta_id', 'id', $q);
	}

	/**
	 * Returns the count of CronogramasDePago Objects(rows) from the cronogramas_de_pago table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function countCronogramasDePagosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return CronogramasDePago::doCount($this->getCronogramasDePagosRelatedByVentaIdQuery($q));
	}

	/**
	 * Deletes the cronogramas_de_pago Objects(rows) from the cronogramas_de_pago table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function deleteCronogramasDePagosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->CronogramasDePagosRelatedByVentaId_c = array();
		return CronogramasDePago::doDelete($this->getCronogramasDePagosRelatedByVentaIdQuery($q));
	}

	protected $CronogramasDePagosRelatedByVentaId_c = array();

	/**
	 * Returns an array of CronogramasDePago objects with a venta_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return CronogramasDePago[]
	 */
	function getCronogramasDePagosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->CronogramasDePagosRelatedByVentaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->CronogramasDePagosRelatedByVentaId_c;
		}

		$result = CronogramasDePago::doSelect($this->getCronogramasDePagosRelatedByVentaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->CronogramasDePagosRelatedByVentaId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting pedido_producto Objects(rows) from the pedido_producto table
	 * with a venta_id that matches $this->id.
	 * @return Query
	 */
	function getPedidoProductosRelatedByVentaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('pedido_producto', 'venta_id', 'id', $q);
	}

	/**
	 * Returns the count of PedidoProducto Objects(rows) from the pedido_producto table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function countPedidoProductosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PedidoProducto::doCount($this->getPedidoProductosRelatedByVentaIdQuery($q));
	}

	/**
	 * Deletes the pedido_producto Objects(rows) from the pedido_producto table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function deletePedidoProductosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PedidoProductosRelatedByVentaId_c = array();
		return PedidoProducto::doDelete($this->getPedidoProductosRelatedByVentaIdQuery($q));
	}

	protected $PedidoProductosRelatedByVentaId_c = array();

	/**
	 * Returns an array of PedidoProducto objects with a venta_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PedidoProducto[]
	 */
	function getPedidoProductosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PedidoProductosRelatedByVentaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PedidoProductosRelatedByVentaId_c;
		}

		$result = PedidoProducto::doSelect($this->getPedidoProductosRelatedByVentaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PedidoProductosRelatedByVentaId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_mesas Objects(rows) from the ventas_mesas table
	 * with a venta_id that matches $this->id.
	 * @return Query
	 */
	function getVentasMesassRelatedByVentaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_mesas', 'venta_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasMesas Objects(rows) from the ventas_mesas table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function countVentasMesassRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasMesas::doCount($this->getVentasMesassRelatedByVentaIdQuery($q));
	}

	/**
	 * Deletes the ventas_mesas Objects(rows) from the ventas_mesas table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasMesassRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasMesassRelatedByVentaId_c = array();
		return VentasMesas::doDelete($this->getVentasMesassRelatedByVentaIdQuery($q));
	}

	protected $VentasMesassRelatedByVentaId_c = array();

	/**
	 * Returns an array of VentasMesas objects with a venta_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasMesas[]
	 */
	function getVentasMesassRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasMesassRelatedByVentaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasMesassRelatedByVentaId_c;
		}

		$result = VentasMesas::doSelect($this->getVentasMesassRelatedByVentaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasMesassRelatedByVentaId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_movimiento Objects(rows) from the ventas_movimiento table
	 * with a venta_id that matches $this->id.
	 * @return Query
	 */
	function getVentasMovimientosRelatedByVentaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_movimiento', 'venta_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasMovimiento Objects(rows) from the ventas_movimiento table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function countVentasMovimientosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasMovimiento::doCount($this->getVentasMovimientosRelatedByVentaIdQuery($q));
	}

	/**
	 * Deletes the ventas_movimiento Objects(rows) from the ventas_movimiento table
	 * with a venta_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasMovimientosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasMovimientosRelatedByVentaId_c = array();
		return VentasMovimiento::doDelete($this->getVentasMovimientosRelatedByVentaIdQuery($q));
	}

	protected $VentasMovimientosRelatedByVentaId_c = array();

	/**
	 * Returns an array of VentasMovimiento objects with a venta_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasMovimiento[]
	 */
	function getVentasMovimientosRelatedByVentaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasMovimientosRelatedByVentaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasMovimientosRelatedByVentaId_c;
		}

		$result = VentasMovimiento::doSelect($this->getVentasMovimientosRelatedByVentaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasMovimientosRelatedByVentaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Ventas::getCronogramasDePagosRelatedByventa_id
	 * @return CronogramasDePago[]
	 * @see Ventas::getCronogramasDePagosRelatedByVentaId
	 */
	function getCronogramasDePagos($extra = null) {
		return $this->getCronogramasDePagosRelatedByVentaId($extra);
	}

	/**
	  * Convenience function for Ventas::getCronogramasDePagosRelatedByventa_idQuery
	  * @return Query
	  * @see Ventas::getCronogramasDePagosRelatedByventa_idQuery
	  */
	function getCronogramasDePagosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cronogramas_de_pago', 'venta_id','id', $q);
	}

	/**
	  * Convenience function for Ventas::deleteCronogramasDePagosRelatedByventa_id
	  * @return int
	  * @see Ventas::deleteCronogramasDePagosRelatedByventa_id
	  */
	function deleteCronogramasDePagos(Query $q = null) {
		return $this->deleteCronogramasDePagosRelatedByVentaId($q);
	}

	/**
	  * Convenience function for Ventas::countCronogramasDePagosRelatedByventa_id
	  * @return int
	  * @see Ventas::countCronogramasDePagosRelatedByVentaId
	  */
	function countCronogramasDePagos(Query $q = null) {
		return $this->countCronogramasDePagosRelatedByVentaId($q);
	}

	/**
	 * Convenience function for Ventas::getPedidoProductosRelatedByventa_id
	 * @return PedidoProducto[]
	 * @see Ventas::getPedidoProductosRelatedByVentaId
	 */
	function getPedidoProductos($extra = null) {
		return $this->getPedidoProductosRelatedByVentaId($extra);
	}

	/**
	  * Convenience function for Ventas::getPedidoProductosRelatedByventa_idQuery
	  * @return Query
	  * @see Ventas::getPedidoProductosRelatedByventa_idQuery
	  */
	function getPedidoProductosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('pedido_producto', 'venta_id','id', $q);
	}

	/**
	  * Convenience function for Ventas::deletePedidoProductosRelatedByventa_id
	  * @return int
	  * @see Ventas::deletePedidoProductosRelatedByventa_id
	  */
	function deletePedidoProductos(Query $q = null) {
		return $this->deletePedidoProductosRelatedByVentaId($q);
	}

	/**
	  * Convenience function for Ventas::countPedidoProductosRelatedByventa_id
	  * @return int
	  * @see Ventas::countPedidoProductosRelatedByVentaId
	  */
	function countPedidoProductos(Query $q = null) {
		return $this->countPedidoProductosRelatedByVentaId($q);
	}

	/**
	 * Convenience function for Ventas::getVentasMesassRelatedByventa_id
	 * @return VentasMesas[]
	 * @see Ventas::getVentasMesassRelatedByVentaId
	 */
	function getVentasMesass($extra = null) {
		return $this->getVentasMesassRelatedByVentaId($extra);
	}

	/**
	  * Convenience function for Ventas::getVentasMesassRelatedByventa_idQuery
	  * @return Query
	  * @see Ventas::getVentasMesassRelatedByventa_idQuery
	  */
	function getVentasMesassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_mesas', 'venta_id','id', $q);
	}

	/**
	  * Convenience function for Ventas::deleteVentasMesassRelatedByventa_id
	  * @return int
	  * @see Ventas::deleteVentasMesassRelatedByventa_id
	  */
	function deleteVentasMesass(Query $q = null) {
		return $this->deleteVentasMesassRelatedByVentaId($q);
	}

	/**
	  * Convenience function for Ventas::countVentasMesassRelatedByventa_id
	  * @return int
	  * @see Ventas::countVentasMesassRelatedByVentaId
	  */
	function countVentasMesass(Query $q = null) {
		return $this->countVentasMesassRelatedByVentaId($q);
	}

	/**
	 * Convenience function for Ventas::getVentasMovimientosRelatedByventa_id
	 * @return VentasMovimiento[]
	 * @see Ventas::getVentasMovimientosRelatedByVentaId
	 */
	function getVentasMovimientos($extra = null) {
		return $this->getVentasMovimientosRelatedByVentaId($extra);
	}

	/**
	  * Convenience function for Ventas::getVentasMovimientosRelatedByventa_idQuery
	  * @return Query
	  * @see Ventas::getVentasMovimientosRelatedByventa_idQuery
	  */
	function getVentasMovimientosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_movimiento', 'venta_id','id', $q);
	}

	/**
	  * Convenience function for Ventas::deleteVentasMovimientosRelatedByventa_id
	  * @return int
	  * @see Ventas::deleteVentasMovimientosRelatedByventa_id
	  */
	function deleteVentasMovimientos(Query $q = null) {
		return $this->deleteVentasMovimientosRelatedByVentaId($q);
	}

	/**
	  * Convenience function for Ventas::countVentasMovimientosRelatedByventa_id
	  * @return int
	  * @see Ventas::countVentasMovimientosRelatedByVentaId
	  */
	function countVentasMovimientos(Query $q = null) {
		return $this->countVentasMovimientosRelatedByVentaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}