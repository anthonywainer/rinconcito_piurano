<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseClientes extends ApplicationModel {

	const ID = 'clientes.id';
	const NOMBRES = 'clientes.nombres';
	const APELLIDOS = 'clientes.apellidos';
	const TELEFONO = 'clientes.telefono';
	const DIRECCION = 'clientes.direccion';
	const CORREO = 'clientes.correo';
	const CREATED_AT = 'clientes.created_at';
	const UPDATED_AT = 'clientes.updated_at';
	const DELETED_AT = 'clientes.deleted_at';
	const DNI = 'clientes.dni';
	const RUC = 'clientes.ruc';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'clientes';

	/**
	 * Cache of objects retrieved from the database
	 * @var Clientes[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_VARCHAR,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'correo' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'dni' => Model::COLUMN_TYPE_VARCHAR,
		'ruc' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `telefono` VARCHAR NOT NULL
	 * @var string
	 */
	protected $telefono;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `correo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $correo;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `dni` VARCHAR
	 * @var string
	 */
	protected $dni;

	/**
	 * `ruc` VARCHAR
	 * @var string
	 */
	protected $ruc;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Clientes
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Clientes
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Clientes
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Clientes
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Clientes
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the correo field
	 */
	function getCorreo() {
		return $this->correo;
	}

	/**
	 * Sets the value of the correo field
	 * @return Clientes
	 */
	function setCorreo($value) {
		return $this->setColumnValue('correo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Clientes
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Clientes::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setCreatedAt
	 * @return Clientes
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Clientes
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Clientes::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setUpdatedAt
	 * @return Clientes
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Clientes
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Clientes::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setDeletedAt
	 * @return Clientes
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the dni field
	 */
	function getDni() {
		return $this->dni;
	}

	/**
	 * Sets the value of the dni field
	 * @return Clientes
	 */
	function setDni($value) {
		return $this->setColumnValue('dni', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the ruc field
	 */
	function getRuc() {
		return $this->ruc;
	}

	/**
	 * Sets the value of the ruc field
	 * @return Clientes
	 */
	function setRuc($value) {
		return $this->setColumnValue('ruc', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Clientes
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Clientes
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveById($value) {
		return Clientes::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a correo
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByCorreo($value) {
		return static::retrieveByColumn('correo', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a dni
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDni($value) {
		return static::retrieveByColumn('dni', $value);
	}

	/**
	 * Searches the database for a row with a ruc
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByRuc($value) {
		return static::retrieveByColumn('ruc', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Clientes
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Clientes[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting deliveries Objects(rows) from the deliveries table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getDeliveriessRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('deliveries', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of Deliveries Objects(rows) from the deliveries table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countDeliveriessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Deliveries::doCount($this->getDeliveriessRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the deliveries Objects(rows) from the deliveries table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteDeliveriessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->DeliveriessRelatedByClienteId_c = array();
		return Deliveries::doDelete($this->getDeliveriessRelatedByClienteIdQuery($q));
	}

	protected $DeliveriessRelatedByClienteId_c = array();

	/**
	 * Returns an array of Deliveries objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Deliveries[]
	 */
	function getDeliveriessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->DeliveriessRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->DeliveriessRelatedByClienteId_c;
		}

		$result = Deliveries::doSelect($this->getDeliveriessRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->DeliveriessRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas Objects(rows) from the ventas table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getVentassRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of Ventas Objects(rows) from the ventas table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countVentassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Ventas::doCount($this->getVentassRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the ventas Objects(rows) from the ventas table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteVentassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentassRelatedByClienteId_c = array();
		return Ventas::doDelete($this->getVentassRelatedByClienteIdQuery($q));
	}

	protected $VentassRelatedByClienteId_c = array();

	/**
	 * Returns an array of Ventas objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Ventas[]
	 */
	function getVentassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentassRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentassRelatedByClienteId_c;
		}

		$result = Ventas::doSelect($this->getVentassRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentassRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Clientes::getDeliveriessRelatedBycliente_id
	 * @return Deliveries[]
	 * @see Clientes::getDeliveriessRelatedByClienteId
	 */
	function getDeliveriess($extra = null) {
		return $this->getDeliveriessRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getDeliveriessRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getDeliveriessRelatedBycliente_idQuery
	  */
	function getDeliveriessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('deliveries', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteDeliveriessRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteDeliveriessRelatedBycliente_id
	  */
	function deleteDeliveriess(Query $q = null) {
		return $this->deleteDeliveriessRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countDeliveriessRelatedBycliente_id
	  * @return int
	  * @see Clientes::countDeliveriessRelatedByClienteId
	  */
	function countDeliveriess(Query $q = null) {
		return $this->countDeliveriessRelatedByClienteId($q);
	}

	/**
	 * Convenience function for Clientes::getVentassRelatedBycliente_id
	 * @return Ventas[]
	 * @see Clientes::getVentassRelatedByClienteId
	 */
	function getVentass($extra = null) {
		return $this->getVentassRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getVentassRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getVentassRelatedBycliente_idQuery
	  */
	function getVentassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteVentassRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteVentassRelatedBycliente_id
	  */
	function deleteVentass(Query $q = null) {
		return $this->deleteVentassRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countVentassRelatedBycliente_id
	  * @return int
	  * @see Clientes::countVentassRelatedByClienteId
	  */
	function countVentass(Query $q = null) {
		return $this->countVentassRelatedByClienteId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}

		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}