<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePermisos extends ApplicationModel {

	const ID = 'permisos.id';
	const IDACCION = 'permisos.idaccion';
	const IDSUBMODULO = 'permisos.idsubmodulo';
    const IDMODULO = 'permisos.idmodulo';
	const NOMBRE = 'permisos.nombre';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'permisos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Permisos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'idaccion' => Model::COLUMN_TYPE_INTEGER,
		'idsubmodulo' => Model::COLUMN_TYPE_INTEGER,
        'idmodulo' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `idaccion` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $idaccion;

	/**
	 * `idsubmodulo` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $idsubmodulo;
    protected $idmodulo;
	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Permisos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idaccion field
	 */
	function getIdaccion() {
		return $this->idaccion;
	}

    function getAccion($id = null) {
        return Acciones::getAll('Where id='.$id);
    }

    function getModulos($id = null) {
        return Modulos::getAll('Where id='.$id);
    }

    function getSubModulos($id = null) {
        return Submodulo::getAll('Where id='.$id);
    }
	/**
	 * Sets the value of the idaccion field
	 * @return Permisos
	 */
	function setIdaccion($value) {
		return $this->setColumnValue('idaccion', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idsubmodulo field
	 */
	function getIdsubmodulo() {
		return $this->idsubmodulo;
	}
    function getIdmodulo() {
        return $this->idmodulo;
    }
	/**
	 * Sets the value of the idsubmodulo field
	 * @return Permisos
	 */
	function setIdsubmodulo($value) {
		return $this->setColumnValue('idsubmodulo', $value, Model::COLUMN_TYPE_INTEGER);
	}
    function setIdmodulo($value) {
        return $this->setColumnValue('idmodulo', $value, Model::COLUMN_TYPE_INTEGER);
    }
	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Permisos
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Permisos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Permisos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveById($value) {
		return Permisos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a idaccion
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByIdaccion($value) {
		return static::retrieveByColumn('idaccion', $value);
	}

	/**
	 * Searches the database for a row with a idsubmodulo
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByIdsubmodulo($value) {
		return static::retrieveByColumn('idsubmodulo', $value);
	}
    static function retrieveByIdmodulo($value) {
        return static::retrieveByColumn('idmodulo', $value);
    }
	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Permisos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->idaccion = (null === $this->idaccion) ? null : (int) $this->idaccion;
		$this->idsubmodulo = (null === $this->idsubmodulo) ? null : (int) $this->idsubmodulo;
		return $this;
	}

	/**
	 * @return Permisos
	 */
	function setSubmoduloRelatedByIdsubmodulo(Submodulo $submodulo = null) {
		if (null === $submodulo) {
			$this->setidsubmodulo(null);
		} else {
			if (!$submodulo->getid()) {
				throw new Exception('Cannot connect a Submodulo without a id');
			}
			$this->setidsubmodulo($submodulo->getid());
		}
		return $this;
	}

	/**
	 * Returns a submodulo object with a id
	 * that matches $this->idsubmodulo.
	 * @return Submodulo
	 */
	function getSubmoduloRelatedByIdsubmodulo() {
		$fk_value = $this->getidsubmodulo();
		if (null === $fk_value) {
			return null;
		}
		return Submodulo::retrieveByPK($fk_value);
	}
    function getmoduloRelatedByIdmodulo() {
        $fk_value = $this->getidmodulo();
        if (null === $fk_value) {
            return null;
        }
        return Modulo::retrieveByPK($fk_value);
    }
	/**
	 * Returns a submodulo object with a id
	 * that matches $this->idsubmodulo.
	 * @return Submodulo
	 */
	function getSubmodulo() {
		return $this->getSubmoduloRelatedByIdsubmodulo();
	}
    function getmodulo() {
        return $this->getmoduloRelatedByIdmodulo();
    }
	/**
	 * @return Permisos
	 */
	function setSubmodulo(Submodulo $submodulo = null) {
		return $this->setSubmoduloRelatedByIdsubmodulo($submodulo);
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinSubmoduloRelatedByIdsubmodulo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Submodulo::getTableName();
		$q->join($to_table, $this_table . '.idsubmodulo = ' . $to_table . '.id', $join_type);
		foreach (Submodulo::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Submodulo'));
	}

	/**
	 * @return Permisos
	 */
	function setAccionesRelatedByIdaccion(Acciones $acciones = null) {
		if (null === $acciones) {
			$this->setidaccion(null);
		} else {
			if (!$acciones->getid()) {
				throw new Exception('Cannot connect a Acciones without a id');
			}
			$this->setidaccion($acciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a acciones object with a id
	 * that matches $this->idaccion.
	 * @return Acciones
	 */
	function getAccionesRelatedByIdaccion() {
		$fk_value = $this->getidaccion();
		if (null === $fk_value) {
			return null;
		}
		return Acciones::retrieveByPK($fk_value);
	}

	/**
	 * Returns a acciones object with a id
	 * that matches $this->idaccion.
	 * @return Acciones
	 */
	function getAcciones() {
		return $this->getAccionesRelatedByIdaccion();
	}

	/**
	 * @return Permisos
	 */
	function setAcciones(Acciones $acciones = null) {
		return $this->setAccionesRelatedByIdaccion($acciones);
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinAccionesRelatedByIdaccion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Acciones::getTableName();
		$q->join($to_table, $this_table . '.idaccion = ' . $to_table . '.id', $join_type);
		foreach (Acciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Acciones'));
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Submodulo::getTableName();
		$q->join($to_table, $this_table . '.idsubmodulo = ' . $to_table . '.id', $join_type);
		foreach (Submodulo::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Submodulo';
	
		$to_table = Acciones::getTableName();
		$q->join($to_table, $this_table . '.idaccion = ' . $to_table . '.id', $join_type);
		foreach (Acciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Acciones';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getidaccion()) {
			$this->_validationErrors[] = 'idaccion must not be null';
		}
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}