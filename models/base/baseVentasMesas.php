<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentasMesas extends ApplicationModel {

	const ID = 'ventas_mesas.id';
	const VENTA_ID = 'ventas_mesas.venta_id';
	const MESA_ID = 'ventas_mesas.mesa_id';
	const ESTADO_VENTA_MESA = 'ventas_mesas.estado_venta_mesa';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ventas_mesas';

	/**
	 * Cache of objects retrieved from the database
	 * @var VentasMesas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'venta_id' => Model::COLUMN_TYPE_INTEGER,
		'mesa_id' => Model::COLUMN_TYPE_INTEGER,
		'estado_venta_mesa' => Model::COLUMN_TYPE_BOOLEAN,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `venta_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $venta_id;

	/**
	 * `mesa_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $mesa_id;

	/**
	 * `estado_venta_mesa` BOOLEAN NOT NULL DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $estado_venta_mesa = 'b\'1\'';

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return VentasMesas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the venta_id field
	 */
	function getVentaId() {
		return $this->venta_id;
	}

	/**
	 * Sets the value of the venta_id field
	 * @return VentasMesas
	 */
	function setVentaId($value) {
		return $this->setColumnValue('venta_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasMesas::getVentaId
	 * final because getVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::getVentaId
	 */
	final function getVenta_id() {
		return $this->getVentaId();
	}

	/**
	 * Convenience function for VentasMesas::setVentaId
	 * final because setVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::setVentaId
	 * @return VentasMesas
	 */
	final function setVenta_id($value) {
		return $this->setVentaId($value);
	}

	/**
	 * Gets the value of the mesa_id field
	 */
	function getMesaId() {
		return $this->mesa_id;
	}

	/**
	 * Sets the value of the mesa_id field
	 * @return VentasMesas
	 */
	function setMesaId($value) {
		return $this->setColumnValue('mesa_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasMesas::getMesaId
	 * final because getMesaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::getMesaId
	 */
	final function getMesa_id() {
		return $this->getMesaId();
	}

	/**
	 * Convenience function for VentasMesas::setMesaId
	 * final because setMesaId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::setMesaId
	 * @return VentasMesas
	 */
	final function setMesa_id($value) {
		return $this->setMesaId($value);
	}

	/**
	 * Gets the value of the estado_venta_mesa field
	 */
	function getEstadoVentaMesa() {
		return $this->estado_venta_mesa;
	}

	/**
	 * Sets the value of the estado_venta_mesa field
	 * @return VentasMesas
	 */
	function setEstadoVentaMesa($value) {
		return $this->setColumnValue('estado_venta_mesa', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for VentasMesas::getEstadoVentaMesa
	 * final because getEstadoVentaMesa should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::getEstadoVentaMesa
	 */
	final function getEstado_venta_mesa() {
		return $this->getEstadoVentaMesa();
	}

	/**
	 * Convenience function for VentasMesas::setEstadoVentaMesa
	 * final because setEstadoVentaMesa should be extended instead
	 * to ensure consistent behavior
	 * @see VentasMesas::setEstadoVentaMesa
	 * @return VentasMesas
	 */
	final function setEstado_venta_mesa($value) {
		return $this->setEstadoVentaMesa($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return VentasMesas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return VentasMesas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return VentasMesas
	 */
	static function retrieveById($value) {
		return VentasMesas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a venta_id
	 * value that matches the one provided
	 * @return VentasMesas
	 */
	static function retrieveByVentaId($value) {
		return static::retrieveByColumn('venta_id', $value);
	}

	/**
	 * Searches the database for a row with a mesa_id
	 * value that matches the one provided
	 * @return VentasMesas
	 */
	static function retrieveByMesaId($value) {
		return static::retrieveByColumn('mesa_id', $value);
	}

	/**
	 * Searches the database for a row with a estado_venta_mesa
	 * value that matches the one provided
	 * @return VentasMesas
	 */
	static function retrieveByEstadoVentaMesa($value) {
		return static::retrieveByColumn('estado_venta_mesa', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return VentasMesas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->venta_id = (null === $this->venta_id) ? null : (int) $this->venta_id;
		$this->mesa_id = (null === $this->mesa_id) ? null : (int) $this->mesa_id;
		$this->estado_venta_mesa = (null === $this->estado_venta_mesa) ? null : (int) $this->estado_venta_mesa;
		return $this;
	}

	/**
	 * @return VentasMesas
	 */
	function setMesa(Mesas $mesas = null) {
		return $this->setMesasRelatedByMesaId($mesas);
	}

	/**
	 * @return VentasMesas
	 */
	function setMesasRelatedByMesaId(Mesas $mesas = null) {
		if (null === $mesas) {
			$this->setmesa_id(null);
		} else {
			if (!$mesas->getid()) {
				throw new Exception('Cannot connect a Mesas without a id');
			}
			$this->setmesa_id($mesas->getid());
		}
		return $this;
	}

	/**
	 * Returns a mesas object with a id
	 * that matches $this->mesa_id.
	 * @return Mesas
	 */
	function getMesa() {
		return $this->getMesasRelatedByMesaId();
	}

	/**
	 * Returns a mesas object with a id
	 * that matches $this->mesa_id.
	 * @return Mesas
	 */
	function getMesasRelatedByMesaId() {
		$fk_value = $this->getmesa_id();
		if (null === $fk_value) {
			return null;
		}
		return Mesas::retrieveByPK($fk_value);
	}

	static function doSelectJoinMesa(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMesasRelatedByMesaId($q, $join_type);
	}

	/**
	 * Returns a mesas object with a id
	 * that matches $this->mesa_id.
	 * @return Mesas
	 */
	function getMesas() {
		return $this->getMesasRelatedByMesaId();
	}

	/**
	 * @return VentasMesas
	 */
	function setMesas(Mesas $mesas = null) {
		return $this->setMesasRelatedByMesaId($mesas);
	}

	/**
	 * @return VentasMesas[]
	 */
	static function doSelectJoinMesasRelatedByMesaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Mesas::getTableName();
		$q->join($to_table, $this_table . '.mesa_id = ' . $to_table . '.id', $join_type);
		foreach (Mesas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Mesas'));
	}

	/**
	 * @return VentasMesas
	 */
	function setVenta(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return VentasMesas
	 */
	function setVentasRelatedByVentaId(Ventas $ventas = null) {
		if (null === $ventas) {
			$this->setventa_id(null);
		} else {
			if (!$ventas->getid()) {
				throw new Exception('Cannot connect a Ventas without a id');
			}
			$this->setventa_id($ventas->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVenta() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentasRelatedByVentaId() {
		$fk_value = $this->getventa_id();
		if (null === $fk_value) {
			return null;
		}
		return Ventas::retrieveByPK($fk_value);
	}

	static function doSelectJoinVenta(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVentasRelatedByVentaId($q, $join_type);
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentas() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * @return VentasMesas
	 */
	function setVentas(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return VentasMesas[]
	 */
	static function doSelectJoinVentasRelatedByVentaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ventas'));
	}

	/**
	 * @return VentasMesas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Mesas::getTableName();
		$q->join($to_table, $this_table . '.mesa_id = ' . $to_table . '.id', $join_type);
		foreach (Mesas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Mesas';
	
		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ventas';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getventa_id()) {
			$this->_validationErrors[] = 'venta_id must not be null';
		}
		if (null === $this->getmesa_id()) {
			$this->_validationErrors[] = 'mesa_id must not be null';
		}
		if (null === $this->getmesa_id()) {
			$this->_validationErrors[] = 'mesa_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}