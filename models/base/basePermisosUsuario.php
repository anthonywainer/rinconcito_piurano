<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePermisosUsuario extends ApplicationModel {

	const ID = 'permisos_usuario.id';
	const IDUSUARIO = 'permisos_usuario.idusuario';
	const IDPERMISO = 'permisos_usuario.idpermiso';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'permisos_usuario';

	/**
	 * Cache of objects retrieved from the database
	 * @var PermisosUsuario[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = false;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'idusuario' => Model::COLUMN_TYPE_INTEGER,
		'idpermiso' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `idusuario` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $idusuario;

	/**
	 * `idpermiso` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $idpermiso;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return PermisosUsuario
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idusuario field
	 */
	function getIdusuario() {
		return $this->idusuario;
	}

	/**
	 * Sets the value of the idusuario field
	 * @return PermisosUsuario
	 */
	function setIdusuario($value) {
		return $this->setColumnValue('idusuario', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idpermiso field
	 */
	function getIdpermiso() {
		return $this->idpermiso;
	}

	/**
	 * Sets the value of the idpermiso field
	 * @return PermisosUsuario
	 */
	function setIdpermiso($value) {
		return $this->setColumnValue('idpermiso', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return PermisosUsuario
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return PermisosUsuario
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return PermisosUsuario
	 */
	static function retrieveById($value) {
		return PermisosUsuario::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a idusuario
	 * value that matches the one provided
	 * @return PermisosUsuario
	 */
	static function retrieveByIdusuario($value) {
		return static::retrieveByColumn('idusuario', $value);
	}

	/**
	 * Searches the database for a row with a idpermiso
	 * value that matches the one provided
	 * @return PermisosUsuario
	 */
	static function retrieveByIdpermiso($value) {
		return static::retrieveByColumn('idpermiso', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return PermisosUsuario
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->idusuario = (null === $this->idusuario) ? null : (int) $this->idusuario;
		$this->idpermiso = (null === $this->idpermiso) ? null : (int) $this->idpermiso;
		return $this;
	}

	/**
	 * @return PermisosUsuario
	 */
	function setPermisosRelatedByIdpermiso(Permisos $permisos = null) {
		if (null === $permisos) {
			$this->setidpermiso(null);
		} else {
			if (!$permisos->getid()) {
				throw new Exception('Cannot connect a Permisos without a id');
			}
			$this->setidpermiso($permisos->getid());
		}
		return $this;
	}

	/**
	 * Returns a permisos object with a id
	 * that matches $this->idpermiso.
	 * @return Permisos
	 */
	function getPermisosRelatedByIdpermiso() {
		$fk_value = $this->getidpermiso();
		if (null === $fk_value) {
			return null;
		}
		return Permisos::retrieveByPK($fk_value);
	}

	/**
	 * Returns a permisos object with a id
	 * that matches $this->idpermiso.
	 * @return Permisos
	 */
	function getPermisos() {
		return $this->getPermisosRelatedByIdpermiso();
	}

	/**
	 * @return PermisosUsuario
	 */
	function setPermisos(Permisos $permisos = null) {
		return $this->setPermisosRelatedByIdpermiso($permisos);
	}

	/**
	 * @return PermisosUsuario[]
	 */
	static function doSelectJoinPermisosRelatedByIdpermiso(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Permisos::getTableName();
		$q->join($to_table, $this_table . '.idpermiso = ' . $to_table . '.id', $join_type);
		foreach (Permisos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Permisos'));
	}

	/**
	 * @return PermisosUsuario
	 */
	function setUsuariosRelatedByIdusuario(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setidusuario(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setidusuario($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->idusuario.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByIdusuario() {
		$fk_value = $this->getidusuario();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->idusuario.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByIdusuario();
	}

	/**
	 * @return PermisosUsuario
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByIdusuario($usuarios);
	}

	/**
	 * @return PermisosUsuario[]
	 */
	static function doSelectJoinUsuariosRelatedByIdusuario(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.idusuario = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return PermisosUsuario[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Permisos::getTableName();
		$q->join($to_table, $this_table . '.idpermiso = ' . $to_table . '.id', $join_type);
		foreach (Permisos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Permisos';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.idusuario = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}