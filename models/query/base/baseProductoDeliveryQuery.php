<?php

use Dabl\Query\Query;

abstract class baseProductoDeliveryQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ProductoDelivery::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductoDeliveryQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductoDeliveryQuery($table_name, $alias);
	}

	/**
	 * @return ProductoDelivery[]
	 */
	function select() {
		return ProductoDelivery::doSelect($this);
	}

	/**
	 * @return ProductoDelivery
	 */
	function selectOne() {
		return ProductoDelivery::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ProductoDelivery::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ProductoDelivery::doCount($this);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoDelivery::isTemporalType($type)) {
			$value = ProductoDelivery::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoDelivery::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoDelivery::isTemporalType($type)) {
			$value = ProductoDelivery::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoDelivery::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andId($integer) {
		return $this->addAnd(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdNull() {
		return $this->andNull(ProductoDelivery::ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ProductoDelivery::ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoDelivery::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orId($integer) {
		return $this->or(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdNull() {
		return $this->orNull(ProductoDelivery::ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ProductoDelivery::ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoDelivery::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ProductoDelivery::ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ProductoDelivery::ID, $integer);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ProductoDelivery::ID, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ProductoDelivery::ID, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupById() {
		return $this->groupBy(ProductoDelivery::ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoId($integer) {
		return $this->addAnd(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdNot($integer) {
		return $this->andNot(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdLike($integer) {
		return $this->andLike(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdNotLike($integer) {
		return $this->andNotLike(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdGreater($integer) {
		return $this->andGreater(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdLess($integer) {
		return $this->andLess(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdLessEqual($integer) {
		return $this->andLessEqual(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdNull() {
		return $this->andNull(ProductoDelivery::PRODUCTO_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdNotNull() {
		return $this->andNotNull(ProductoDelivery::PRODUCTO_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoDelivery::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdEndsWith($integer) {
		return $this->andEndsWith(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andProductoIdContains($integer) {
		return $this->andContains(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoId($integer) {
		return $this->or(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdNot($integer) {
		return $this->orNot(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdLike($integer) {
		return $this->orLike(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdNotLike($integer) {
		return $this->orNotLike(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdGreater($integer) {
		return $this->orGreater(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdLess($integer) {
		return $this->orLess(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdLessEqual($integer) {
		return $this->orLessEqual(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdNull() {
		return $this->orNull(ProductoDelivery::PRODUCTO_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdNotNull() {
		return $this->orNotNull(ProductoDelivery::PRODUCTO_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoDelivery::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdEndsWith($integer) {
		return $this->orEndsWith(ProductoDelivery::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orProductoIdContains($integer) {
		return $this->orContains(ProductoDelivery::PRODUCTO_ID, $integer);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByProductoIdAsc() {
		return $this->orderBy(ProductoDelivery::PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByProductoIdDesc() {
		return $this->orderBy(ProductoDelivery::PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByProductoId() {
		return $this->groupBy(ProductoDelivery::PRODUCTO_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryId($integer) {
		return $this->addAnd(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdNot($integer) {
		return $this->andNot(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdLike($integer) {
		return $this->andLike(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdNotLike($integer) {
		return $this->andNotLike(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdGreater($integer) {
		return $this->andGreater(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdLess($integer) {
		return $this->andLess(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdLessEqual($integer) {
		return $this->andLessEqual(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdNull() {
		return $this->andNull(ProductoDelivery::DELIVERY_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdNotNull() {
		return $this->andNotNull(ProductoDelivery::DELIVERY_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoDelivery::DELIVERY_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdEndsWith($integer) {
		return $this->andEndsWith(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeliveryIdContains($integer) {
		return $this->andContains(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryId($integer) {
		return $this->or(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdNot($integer) {
		return $this->orNot(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdLike($integer) {
		return $this->orLike(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdNotLike($integer) {
		return $this->orNotLike(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdGreater($integer) {
		return $this->orGreater(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdLess($integer) {
		return $this->orLess(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdLessEqual($integer) {
		return $this->orLessEqual(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdNull() {
		return $this->orNull(ProductoDelivery::DELIVERY_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdNotNull() {
		return $this->orNotNull(ProductoDelivery::DELIVERY_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoDelivery::DELIVERY_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdEndsWith($integer) {
		return $this->orEndsWith(ProductoDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeliveryIdContains($integer) {
		return $this->orContains(ProductoDelivery::DELIVERY_ID, $integer);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByDeliveryIdAsc() {
		return $this->orderBy(ProductoDelivery::DELIVERY_ID, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByDeliveryIdDesc() {
		return $this->orderBy(ProductoDelivery::DELIVERY_ID, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByDeliveryId() {
		return $this->groupBy(ProductoDelivery::DELIVERY_ID);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidad($integer) {
		return $this->addAnd(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadNot($integer) {
		return $this->andNot(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadLike($integer) {
		return $this->andLike(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadNotLike($integer) {
		return $this->andNotLike(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadGreater($integer) {
		return $this->andGreater(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadLess($integer) {
		return $this->andLess(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadLessEqual($integer) {
		return $this->andLessEqual(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadNull() {
		return $this->andNull(ProductoDelivery::CANTIDAD);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadNotNull() {
		return $this->andNotNull(ProductoDelivery::CANTIDAD);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadBetween($integer, $from, $to) {
		return $this->andBetween(ProductoDelivery::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadBeginsWith($integer) {
		return $this->andBeginsWith(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadEndsWith($integer) {
		return $this->andEndsWith(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCantidadContains($integer) {
		return $this->andContains(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidad($integer) {
		return $this->or(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadNot($integer) {
		return $this->orNot(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadLike($integer) {
		return $this->orLike(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadNotLike($integer) {
		return $this->orNotLike(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadGreater($integer) {
		return $this->orGreater(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadLess($integer) {
		return $this->orLess(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadLessEqual($integer) {
		return $this->orLessEqual(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadNull() {
		return $this->orNull(ProductoDelivery::CANTIDAD);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadNotNull() {
		return $this->orNotNull(ProductoDelivery::CANTIDAD);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadBetween($integer, $from, $to) {
		return $this->orBetween(ProductoDelivery::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadBeginsWith($integer) {
		return $this->orBeginsWith(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadEndsWith($integer) {
		return $this->orEndsWith(ProductoDelivery::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCantidadContains($integer) {
		return $this->orContains(ProductoDelivery::CANTIDAD, $integer);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByCantidadAsc() {
		return $this->orderBy(ProductoDelivery::CANTIDAD, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByCantidadDesc() {
		return $this->orderBy(ProductoDelivery::CANTIDAD, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByCantidad() {
		return $this->groupBy(ProductoDelivery::CANTIDAD);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotal($float) {
		return $this->addAnd(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalNot($float) {
		return $this->andNot(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalLike($float) {
		return $this->andLike(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalNotLike($float) {
		return $this->andNotLike(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalGreater($float) {
		return $this->andGreater(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalGreaterEqual($float) {
		return $this->andGreaterEqual(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalLess($float) {
		return $this->andLess(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalLessEqual($float) {
		return $this->andLessEqual(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalNull() {
		return $this->andNull(ProductoDelivery::TOTAL);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalNotNull() {
		return $this->andNotNull(ProductoDelivery::TOTAL);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalBetween($float, $from, $to) {
		return $this->andBetween(ProductoDelivery::TOTAL, $float, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalBeginsWith($float) {
		return $this->andBeginsWith(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalEndsWith($float) {
		return $this->andEndsWith(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andTotalContains($float) {
		return $this->andContains(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotal($float) {
		return $this->or(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalNot($float) {
		return $this->orNot(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalLike($float) {
		return $this->orLike(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalNotLike($float) {
		return $this->orNotLike(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalGreater($float) {
		return $this->orGreater(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalGreaterEqual($float) {
		return $this->orGreaterEqual(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalLess($float) {
		return $this->orLess(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalLessEqual($float) {
		return $this->orLessEqual(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalNull() {
		return $this->orNull(ProductoDelivery::TOTAL);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalNotNull() {
		return $this->orNotNull(ProductoDelivery::TOTAL);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalBetween($float, $from, $to) {
		return $this->orBetween(ProductoDelivery::TOTAL, $float, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalBeginsWith($float) {
		return $this->orBeginsWith(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalEndsWith($float) {
		return $this->orEndsWith(ProductoDelivery::TOTAL, $float);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orTotalContains($float) {
		return $this->orContains(ProductoDelivery::TOTAL, $float);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByTotalAsc() {
		return $this->orderBy(ProductoDelivery::TOTAL, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByTotalDesc() {
		return $this->orderBy(ProductoDelivery::TOTAL, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByTotal() {
		return $this->groupBy(ProductoDelivery::TOTAL);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(ProductoDelivery::CREATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(ProductoDelivery::CREATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ProductoDelivery::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(ProductoDelivery::CREATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(ProductoDelivery::CREATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ProductoDelivery::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ProductoDelivery::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(ProductoDelivery::CREATED_AT, $timestamp);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(ProductoDelivery::CREATED_AT, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(ProductoDelivery::CREATED_AT, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(ProductoDelivery::CREATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(ProductoDelivery::UPDATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(ProductoDelivery::UPDATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ProductoDelivery::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(ProductoDelivery::UPDATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(ProductoDelivery::UPDATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ProductoDelivery::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ProductoDelivery::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(ProductoDelivery::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(ProductoDelivery::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(ProductoDelivery::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(ProductoDelivery::UPDATED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(ProductoDelivery::DELETED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(ProductoDelivery::DELETED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ProductoDelivery::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(ProductoDelivery::DELETED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(ProductoDelivery::DELETED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ProductoDelivery::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(ProductoDelivery::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(ProductoDelivery::DELETED_AT, $timestamp);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(ProductoDelivery::DELETED_AT, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(ProductoDelivery::DELETED_AT, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(ProductoDelivery::DELETED_AT);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecio($decimal) {
		return $this->addAnd(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioNot($decimal) {
		return $this->andNot(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioLike($decimal) {
		return $this->andLike(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioNotLike($decimal) {
		return $this->andNotLike(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioGreater($decimal) {
		return $this->andGreater(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioGreaterEqual($decimal) {
		return $this->andGreaterEqual(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioLess($decimal) {
		return $this->andLess(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioLessEqual($decimal) {
		return $this->andLessEqual(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioNull() {
		return $this->andNull(ProductoDelivery::PRECIO);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(ProductoDelivery::PRECIO);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioBetween($decimal, $from, $to) {
		return $this->andBetween(ProductoDelivery::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioBeginsWith($decimal) {
		return $this->andBeginsWith(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioEndsWith($decimal) {
		return $this->andEndsWith(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function andPrecioContains($decimal) {
		return $this->andContains(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecio($decimal) {
		return $this->or(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioNot($decimal) {
		return $this->orNot(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioLike($decimal) {
		return $this->orLike(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioNotLike($decimal) {
		return $this->orNotLike(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioGreater($decimal) {
		return $this->orGreater(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioGreaterEqual($decimal) {
		return $this->orGreaterEqual(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioLess($decimal) {
		return $this->orLess(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioLessEqual($decimal) {
		return $this->orLessEqual(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioNull() {
		return $this->orNull(ProductoDelivery::PRECIO);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(ProductoDelivery::PRECIO);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioBetween($decimal, $from, $to) {
		return $this->orBetween(ProductoDelivery::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioBeginsWith($decimal) {
		return $this->orBeginsWith(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioEndsWith($decimal) {
		return $this->orEndsWith(ProductoDelivery::PRECIO, $decimal);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orPrecioContains($decimal) {
		return $this->orContains(ProductoDelivery::PRECIO, $decimal);
	}


	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(ProductoDelivery::PRECIO, self::ASC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(ProductoDelivery::PRECIO, self::DESC);
	}

	/**
	 * @return ProductoDeliveryQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(ProductoDelivery::PRECIO);
	}


}