<?php

use Dabl\Query\Query;

abstract class baseTiposProductoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = TiposProducto::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TiposProductoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TiposProductoQuery($table_name, $alias);
	}

	/**
	 * @return TiposProducto[]
	 */
	function select() {
		return TiposProducto::doSelect($this);
	}

	/**
	 * @return TiposProducto
	 */
	function selectOne() {
		return TiposProducto::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return TiposProducto::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return TiposProducto::doCount($this);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposProducto::isTemporalType($type)) {
			$value = TiposProducto::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposProducto::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposProducto::isTemporalType($type)) {
			$value = TiposProducto::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposProducto::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andId($integer) {
		return $this->addAnd(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdNull() {
		return $this->andNull(TiposProducto::ID);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(TiposProducto::ID);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(TiposProducto::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orId($integer) {
		return $this->or(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdNull() {
		return $this->orNull(TiposProducto::ID);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(TiposProducto::ID);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(TiposProducto::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(TiposProducto::ID, $integer);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(TiposProducto::ID, $integer);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(TiposProducto::ID, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(TiposProducto::ID, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupById() {
		return $this->groupBy(TiposProducto::ID);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreNull() {
		return $this->andNull(TiposProducto::NOMBRE);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(TiposProducto::NOMBRE);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(TiposProducto::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombre($varchar) {
		return $this->or(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreNull() {
		return $this->orNull(TiposProducto::NOMBRE);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(TiposProducto::NOMBRE);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(TiposProducto::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(TiposProducto::NOMBRE, $varchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(TiposProducto::NOMBRE, $varchar);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(TiposProducto::NOMBRE, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(TiposProducto::NOMBRE, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupByNombre() {
		return $this->groupBy(TiposProducto::NOMBRE);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcion($longvarchar) {
		return $this->addAnd(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionNot($longvarchar) {
		return $this->andNot(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionLike($longvarchar) {
		return $this->andLike(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionNotLike($longvarchar) {
		return $this->andNotLike(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionGreater($longvarchar) {
		return $this->andGreater(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionGreaterEqual($longvarchar) {
		return $this->andGreaterEqual(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionLess($longvarchar) {
		return $this->andLess(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionLessEqual($longvarchar) {
		return $this->andLessEqual(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(TiposProducto::DESCRIPCION);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(TiposProducto::DESCRIPCION);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionBetween($longvarchar, $from, $to) {
		return $this->andBetween(TiposProducto::DESCRIPCION, $longvarchar, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionBeginsWith($longvarchar) {
		return $this->andBeginsWith(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionEndsWith($longvarchar) {
		return $this->andEndsWith(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDescripcionContains($longvarchar) {
		return $this->andContains(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcion($longvarchar) {
		return $this->or(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionNot($longvarchar) {
		return $this->orNot(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionLike($longvarchar) {
		return $this->orLike(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionNotLike($longvarchar) {
		return $this->orNotLike(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionGreater($longvarchar) {
		return $this->orGreater(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionGreaterEqual($longvarchar) {
		return $this->orGreaterEqual(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionLess($longvarchar) {
		return $this->orLess(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionLessEqual($longvarchar) {
		return $this->orLessEqual(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(TiposProducto::DESCRIPCION);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(TiposProducto::DESCRIPCION);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionBetween($longvarchar, $from, $to) {
		return $this->orBetween(TiposProducto::DESCRIPCION, $longvarchar, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionBeginsWith($longvarchar) {
		return $this->orBeginsWith(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionEndsWith($longvarchar) {
		return $this->orEndsWith(TiposProducto::DESCRIPCION, $longvarchar);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDescripcionContains($longvarchar) {
		return $this->orContains(TiposProducto::DESCRIPCION, $longvarchar);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(TiposProducto::DESCRIPCION, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(TiposProducto::DESCRIPCION, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(TiposProducto::DESCRIPCION);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(TiposProducto::CREATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(TiposProducto::CREATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposProducto::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(TiposProducto::CREATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(TiposProducto::CREATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposProducto::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(TiposProducto::CREATED_AT, $timestamp);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(TiposProducto::CREATED_AT, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(TiposProducto::CREATED_AT, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(TiposProducto::CREATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(TiposProducto::UPDATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(TiposProducto::UPDATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposProducto::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(TiposProducto::UPDATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(TiposProducto::UPDATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposProducto::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(TiposProducto::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(TiposProducto::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(TiposProducto::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(TiposProducto::UPDATED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(TiposProducto::DELETED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(TiposProducto::DELETED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposProducto::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(TiposProducto::DELETED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(TiposProducto::DELETED_AT);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposProducto::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(TiposProducto::DELETED_AT, $timestamp);
	}


	/**
	 * @return TiposProductoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(TiposProducto::DELETED_AT, self::ASC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(TiposProducto::DELETED_AT, self::DESC);
	}

	/**
	 * @return TiposProductoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(TiposProducto::DELETED_AT);
	}


}