<?php

use Dabl\Query\Query;

abstract class baseProductosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Productos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductosQuery($table_name, $alias);
	}

	/**
	 * @return Productos[]
	 */
	function select() {
		return Productos::doSelect($this);
	}

	/**
	 * @return Productos
	 */
	function selectOne() {
		return Productos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Productos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Productos::doCount($this);
	}

	/**
	 * @return ProductosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Productos::isTemporalType($type)) {
			$value = Productos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Productos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Productos::isTemporalType($type)) {
			$value = Productos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Productos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNull() {
		return $this->andNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Productos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orId($integer) {
		return $this->or(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNull() {
		return $this->orNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Productos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Productos::ID, $integer);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Productos::ID, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Productos::ID, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupById() {
		return $this->groupBy(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNull() {
		return $this->andNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Productos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombre($varchar) {
		return $this->or(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNull() {
		return $this->orNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Productos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Productos::NOMBRE, $varchar);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Productos::NOMBRE, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Productos::NOMBRE, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecio($float) {
		return $this->addAnd(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNot($float) {
		return $this->andNot(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLike($float) {
		return $this->andLike(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNotLike($float) {
		return $this->andNotLike(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioGreater($float) {
		return $this->andGreater(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioGreaterEqual($float) {
		return $this->andGreaterEqual(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLess($float) {
		return $this->andLess(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLessEqual($float) {
		return $this->andLessEqual(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNull() {
		return $this->andNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioBetween($float, $from, $to) {
		return $this->andBetween(Productos::PRECIO, $float, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioBeginsWith($float) {
		return $this->andBeginsWith(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioEndsWith($float) {
		return $this->andEndsWith(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioContains($float) {
		return $this->andContains(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecio($float) {
		return $this->or(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNot($float) {
		return $this->orNot(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLike($float) {
		return $this->orLike(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNotLike($float) {
		return $this->orNotLike(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioGreater($float) {
		return $this->orGreater(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioGreaterEqual($float) {
		return $this->orGreaterEqual(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLess($float) {
		return $this->orLess(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLessEqual($float) {
		return $this->orLessEqual(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNull() {
		return $this->orNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioBetween($float, $from, $to) {
		return $this->orBetween(Productos::PRECIO, $float, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioBeginsWith($float) {
		return $this->orBeginsWith(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioEndsWith($float) {
		return $this->orEndsWith(Productos::PRECIO, $float);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioContains($float) {
		return $this->orContains(Productos::PRECIO, $float);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(Productos::PRECIO, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(Productos::PRECIO, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoId($integer) {
		return $this->addAnd(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdNot($integer) {
		return $this->andNot(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdLike($integer) {
		return $this->andLike(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdNotLike($integer) {
		return $this->andNotLike(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdGreater($integer) {
		return $this->andGreater(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdLess($integer) {
		return $this->andLess(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdLessEqual($integer) {
		return $this->andLessEqual(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdNull() {
		return $this->andNull(Productos::TIPO_PRODUCTO_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdNotNull() {
		return $this->andNotNull(Productos::TIPO_PRODUCTO_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(Productos::TIPO_PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdEndsWith($integer) {
		return $this->andEndsWith(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andTipoProductoIdContains($integer) {
		return $this->andContains(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoId($integer) {
		return $this->or(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdNot($integer) {
		return $this->orNot(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdLike($integer) {
		return $this->orLike(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdNotLike($integer) {
		return $this->orNotLike(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdGreater($integer) {
		return $this->orGreater(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdLess($integer) {
		return $this->orLess(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdLessEqual($integer) {
		return $this->orLessEqual(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdNull() {
		return $this->orNull(Productos::TIPO_PRODUCTO_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdNotNull() {
		return $this->orNotNull(Productos::TIPO_PRODUCTO_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(Productos::TIPO_PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdEndsWith($integer) {
		return $this->orEndsWith(Productos::TIPO_PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orTipoProductoIdContains($integer) {
		return $this->orContains(Productos::TIPO_PRODUCTO_ID, $integer);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByTipoProductoIdAsc() {
		return $this->orderBy(Productos::TIPO_PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByTipoProductoIdDesc() {
		return $this->orderBy(Productos::TIPO_PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByTipoProductoId() {
		return $this->groupBy(Productos::TIPO_PRODUCTO_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Productos::CREATED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Productos::CREATED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Productos::CREATED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Productos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Productos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Productos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Productos::DELETED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Productos::DELETED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Productos::DELETED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Productos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Productos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Productos::DESCRIPCION, $varchar);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Productos::DESCRIPCION, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Productos::DESCRIPCION, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Productos::DESCRIPCION);
	}


}