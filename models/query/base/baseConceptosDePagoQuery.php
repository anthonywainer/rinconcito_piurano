<?php

use Dabl\Query\Query;

abstract class baseConceptosDePagoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ConceptosDePago::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ConceptosDePagoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ConceptosDePagoQuery($table_name, $alias);
	}

	/**
	 * @return ConceptosDePago[]
	 */
	function select() {
		return ConceptosDePago::doSelect($this);
	}

	/**
	 * @return ConceptosDePago
	 */
	function selectOne() {
		return ConceptosDePago::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ConceptosDePago::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ConceptosDePago::doCount($this);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptosDePago::isTemporalType($type)) {
			$value = ConceptosDePago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptosDePago::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptosDePago::isTemporalType($type)) {
			$value = ConceptosDePago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptosDePago::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andId($integer) {
		return $this->addAnd(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdNull() {
		return $this->andNull(ConceptosDePago::ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ConceptosDePago::ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptosDePago::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orId($integer) {
		return $this->or(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdNull() {
		return $this->orNull(ConceptosDePago::ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ConceptosDePago::ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptosDePago::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ConceptosDePago::ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ConceptosDePago::ID, $integer);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ConceptosDePago::ID, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ConceptosDePago::ID, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupById() {
		return $this->groupBy(ConceptosDePago::ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(ConceptosDePago::DESCRIPCION);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(ConceptosDePago::DESCRIPCION);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(ConceptosDePago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(ConceptosDePago::DESCRIPCION);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(ConceptosDePago::DESCRIPCION);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(ConceptosDePago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(ConceptosDePago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(ConceptosDePago::DESCRIPCION, $varchar);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(ConceptosDePago::DESCRIPCION, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(ConceptosDePago::DESCRIPCION, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(ConceptosDePago::DESCRIPCION);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoId($integer) {
		return $this->addAnd(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdNot($integer) {
		return $this->andNot(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdLike($integer) {
		return $this->andLike(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdNotLike($integer) {
		return $this->andNotLike(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdGreater($integer) {
		return $this->andGreater(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdLess($integer) {
		return $this->andLess(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdLessEqual($integer) {
		return $this->andLessEqual(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdNull() {
		return $this->andNull(ConceptosDePago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdNotNull() {
		return $this->andNotNull(ConceptosDePago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptosDePago::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdEndsWith($integer) {
		return $this->andEndsWith(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMovimientoIdContains($integer) {
		return $this->andContains(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoId($integer) {
		return $this->or(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdNot($integer) {
		return $this->orNot(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdLike($integer) {
		return $this->orLike(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdNotLike($integer) {
		return $this->orNotLike(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdGreater($integer) {
		return $this->orGreater(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdLess($integer) {
		return $this->orLess(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdLessEqual($integer) {
		return $this->orLessEqual(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdNull() {
		return $this->orNull(ConceptosDePago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdNotNull() {
		return $this->orNotNull(ConceptosDePago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptosDePago::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdEndsWith($integer) {
		return $this->orEndsWith(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMovimientoIdContains($integer) {
		return $this->orContains(ConceptosDePago::MOVIMIENTO_ID, $integer);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByMovimientoIdAsc() {
		return $this->orderBy(ConceptosDePago::MOVIMIENTO_ID, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByMovimientoIdDesc() {
		return $this->orderBy(ConceptosDePago::MOVIMIENTO_ID, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByMovimientoId() {
		return $this->groupBy(ConceptosDePago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(ConceptosDePago::CREATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(ConceptosDePago::CREATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosDePago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(ConceptosDePago::CREATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(ConceptosDePago::CREATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosDePago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosDePago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(ConceptosDePago::CREATED_AT, $timestamp);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(ConceptosDePago::CREATED_AT, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(ConceptosDePago::CREATED_AT, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(ConceptosDePago::CREATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(ConceptosDePago::UPDATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(ConceptosDePago::UPDATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosDePago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(ConceptosDePago::UPDATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(ConceptosDePago::UPDATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosDePago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosDePago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(ConceptosDePago::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(ConceptosDePago::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(ConceptosDePago::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(ConceptosDePago::UPDATED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(ConceptosDePago::DELETED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(ConceptosDePago::DELETED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosDePago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(ConceptosDePago::DELETED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(ConceptosDePago::DELETED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosDePago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosDePago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(ConceptosDePago::DELETED_AT, $timestamp);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(ConceptosDePago::DELETED_AT, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(ConceptosDePago::DELETED_AT, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(ConceptosDePago::DELETED_AT);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMonto($decimal) {
		return $this->addAnd(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoNot($decimal) {
		return $this->andNot(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoLike($decimal) {
		return $this->andLike(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoNotLike($decimal) {
		return $this->andNotLike(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoGreater($decimal) {
		return $this->andGreater(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoGreaterEqual($decimal) {
		return $this->andGreaterEqual(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoLess($decimal) {
		return $this->andLess(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoLessEqual($decimal) {
		return $this->andLessEqual(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoNull() {
		return $this->andNull(ConceptosDePago::MONTO);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(ConceptosDePago::MONTO);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoBetween($decimal, $from, $to) {
		return $this->andBetween(ConceptosDePago::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoBeginsWith($decimal) {
		return $this->andBeginsWith(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoEndsWith($decimal) {
		return $this->andEndsWith(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function andMontoContains($decimal) {
		return $this->andContains(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMonto($decimal) {
		return $this->or(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoNot($decimal) {
		return $this->orNot(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoLike($decimal) {
		return $this->orLike(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoNotLike($decimal) {
		return $this->orNotLike(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoGreater($decimal) {
		return $this->orGreater(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoGreaterEqual($decimal) {
		return $this->orGreaterEqual(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoLess($decimal) {
		return $this->orLess(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoLessEqual($decimal) {
		return $this->orLessEqual(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoNull() {
		return $this->orNull(ConceptosDePago::MONTO);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(ConceptosDePago::MONTO);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoBetween($decimal, $from, $to) {
		return $this->orBetween(ConceptosDePago::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoBeginsWith($decimal) {
		return $this->orBeginsWith(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoEndsWith($decimal) {
		return $this->orEndsWith(ConceptosDePago::MONTO, $decimal);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orMontoContains($decimal) {
		return $this->orContains(ConceptosDePago::MONTO, $decimal);
	}


	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(ConceptosDePago::MONTO, self::ASC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(ConceptosDePago::MONTO, self::DESC);
	}

	/**
	 * @return ConceptosDePagoQuery
	 */
	function groupByMonto() {
		return $this->groupBy(ConceptosDePago::MONTO);
	}


}