<?php

use Dabl\Query\Query;

abstract class baseSubmoduloQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Submodulo::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return SubmoduloQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new SubmoduloQuery($table_name, $alias);
	}

	/**
	 * @return Submodulo[]
	 */
	function select() {
		return Submodulo::doSelect($this);
	}

	/**
	 * @return Submodulo
	 */
	function selectOne() {
		return Submodulo::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Submodulo::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Submodulo::doCount($this);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Submodulo::isTemporalType($type)) {
			$value = Submodulo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Submodulo::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Submodulo::isTemporalType($type)) {
			$value = Submodulo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Submodulo::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andId($integer) {
		return $this->addAnd(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdNull() {
		return $this->andNull(Submodulo::ID);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Submodulo::ID);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Submodulo::ID, $integer, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orId($integer) {
		return $this->or(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdNull() {
		return $this->orNull(Submodulo::ID);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Submodulo::ID);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Submodulo::ID, $integer, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Submodulo::ID, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Submodulo::ID, $integer);
	}


	/**
	 * @return SubmoduloQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Submodulo::ID, self::ASC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Submodulo::ID, self::DESC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function groupById() {
		return $this->groupBy(Submodulo::ID);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreNull() {
		return $this->andNull(Submodulo::NOMBRE);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Submodulo::NOMBRE);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Submodulo::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombre($varchar) {
		return $this->or(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreNull() {
		return $this->orNull(Submodulo::NOMBRE);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Submodulo::NOMBRE);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Submodulo::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Submodulo::NOMBRE, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Submodulo::NOMBRE, $varchar);
	}


	/**
	 * @return SubmoduloQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Submodulo::NOMBRE, self::ASC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Submodulo::NOMBRE, self::DESC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Submodulo::NOMBRE);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrl($varchar) {
		return $this->addAnd(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlNot($varchar) {
		return $this->andNot(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlLike($varchar) {
		return $this->andLike(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlNotLike($varchar) {
		return $this->andNotLike(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlGreater($varchar) {
		return $this->andGreater(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlGreaterEqual($varchar) {
		return $this->andGreaterEqual(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlLess($varchar) {
		return $this->andLess(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlLessEqual($varchar) {
		return $this->andLessEqual(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlNull() {
		return $this->andNull(Submodulo::URL);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlNotNull() {
		return $this->andNotNull(Submodulo::URL);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlBetween($varchar, $from, $to) {
		return $this->andBetween(Submodulo::URL, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlBeginsWith($varchar) {
		return $this->andBeginsWith(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlEndsWith($varchar) {
		return $this->andEndsWith(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andUrlContains($varchar) {
		return $this->andContains(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrl($varchar) {
		return $this->or(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlNot($varchar) {
		return $this->orNot(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlLike($varchar) {
		return $this->orLike(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlNotLike($varchar) {
		return $this->orNotLike(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlGreater($varchar) {
		return $this->orGreater(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlGreaterEqual($varchar) {
		return $this->orGreaterEqual(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlLess($varchar) {
		return $this->orLess(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlLessEqual($varchar) {
		return $this->orLessEqual(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlNull() {
		return $this->orNull(Submodulo::URL);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlNotNull() {
		return $this->orNotNull(Submodulo::URL);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlBetween($varchar, $from, $to) {
		return $this->orBetween(Submodulo::URL, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlBeginsWith($varchar) {
		return $this->orBeginsWith(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlEndsWith($varchar) {
		return $this->orEndsWith(Submodulo::URL, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orUrlContains($varchar) {
		return $this->orContains(Submodulo::URL, $varchar);
	}


	/**
	 * @return SubmoduloQuery
	 */
	function orderByUrlAsc() {
		return $this->orderBy(Submodulo::URL, self::ASC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orderByUrlDesc() {
		return $this->orderBy(Submodulo::URL, self::DESC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function groupByUrl() {
		return $this->groupBy(Submodulo::URL);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIcon($varchar) {
		return $this->addAnd(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconNot($varchar) {
		return $this->andNot(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconLike($varchar) {
		return $this->andLike(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconNotLike($varchar) {
		return $this->andNotLike(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconGreater($varchar) {
		return $this->andGreater(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconGreaterEqual($varchar) {
		return $this->andGreaterEqual(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconLess($varchar) {
		return $this->andLess(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconLessEqual($varchar) {
		return $this->andLessEqual(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconNull() {
		return $this->andNull(Submodulo::ICON);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconNotNull() {
		return $this->andNotNull(Submodulo::ICON);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconBetween($varchar, $from, $to) {
		return $this->andBetween(Submodulo::ICON, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconBeginsWith($varchar) {
		return $this->andBeginsWith(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconEndsWith($varchar) {
		return $this->andEndsWith(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIconContains($varchar) {
		return $this->andContains(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIcon($varchar) {
		return $this->or(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconNot($varchar) {
		return $this->orNot(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconLike($varchar) {
		return $this->orLike(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconNotLike($varchar) {
		return $this->orNotLike(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconGreater($varchar) {
		return $this->orGreater(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconGreaterEqual($varchar) {
		return $this->orGreaterEqual(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconLess($varchar) {
		return $this->orLess(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconLessEqual($varchar) {
		return $this->orLessEqual(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconNull() {
		return $this->orNull(Submodulo::ICON);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconNotNull() {
		return $this->orNotNull(Submodulo::ICON);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconBetween($varchar, $from, $to) {
		return $this->orBetween(Submodulo::ICON, $varchar, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconBeginsWith($varchar) {
		return $this->orBeginsWith(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconEndsWith($varchar) {
		return $this->orEndsWith(Submodulo::ICON, $varchar);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIconContains($varchar) {
		return $this->orContains(Submodulo::ICON, $varchar);
	}


	/**
	 * @return SubmoduloQuery
	 */
	function orderByIconAsc() {
		return $this->orderBy(Submodulo::ICON, self::ASC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orderByIconDesc() {
		return $this->orderBy(Submodulo::ICON, self::DESC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function groupByIcon() {
		return $this->groupBy(Submodulo::ICON);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmodulo($integer) {
		return $this->addAnd(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloNot($integer) {
		return $this->andNot(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloLike($integer) {
		return $this->andLike(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloNotLike($integer) {
		return $this->andNotLike(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloGreater($integer) {
		return $this->andGreater(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloGreaterEqual($integer) {
		return $this->andGreaterEqual(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloLess($integer) {
		return $this->andLess(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloLessEqual($integer) {
		return $this->andLessEqual(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloNull() {
		return $this->andNull(Submodulo::IDMODULO);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloNotNull() {
		return $this->andNotNull(Submodulo::IDMODULO);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloBetween($integer, $from, $to) {
		return $this->andBetween(Submodulo::IDMODULO, $integer, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloBeginsWith($integer) {
		return $this->andBeginsWith(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloEndsWith($integer) {
		return $this->andEndsWith(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function andIdmoduloContains($integer) {
		return $this->andContains(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmodulo($integer) {
		return $this->or(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloNot($integer) {
		return $this->orNot(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloLike($integer) {
		return $this->orLike(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloNotLike($integer) {
		return $this->orNotLike(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloGreater($integer) {
		return $this->orGreater(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloGreaterEqual($integer) {
		return $this->orGreaterEqual(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloLess($integer) {
		return $this->orLess(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloLessEqual($integer) {
		return $this->orLessEqual(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloNull() {
		return $this->orNull(Submodulo::IDMODULO);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloNotNull() {
		return $this->orNotNull(Submodulo::IDMODULO);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloBetween($integer, $from, $to) {
		return $this->orBetween(Submodulo::IDMODULO, $integer, $from, $to);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloBeginsWith($integer) {
		return $this->orBeginsWith(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloEndsWith($integer) {
		return $this->orEndsWith(Submodulo::IDMODULO, $integer);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orIdmoduloContains($integer) {
		return $this->orContains(Submodulo::IDMODULO, $integer);
	}


	/**
	 * @return SubmoduloQuery
	 */
	function orderByIdmoduloAsc() {
		return $this->orderBy(Submodulo::IDMODULO, self::ASC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function orderByIdmoduloDesc() {
		return $this->orderBy(Submodulo::IDMODULO, self::DESC);
	}

	/**
	 * @return SubmoduloQuery
	 */
	function groupByIdmodulo() {
		return $this->groupBy(Submodulo::IDMODULO);
	}


}