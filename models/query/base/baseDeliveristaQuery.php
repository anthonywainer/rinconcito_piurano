<?php

use Dabl\Query\Query;

abstract class baseDeliveristaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Deliverista::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return DeliveristaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new DeliveristaQuery($table_name, $alias);
	}

	/**
	 * @return Deliverista[]
	 */
	function select() {
		return Deliverista::doSelect($this);
	}

	/**
	 * @return Deliverista
	 */
	function selectOne() {
		return Deliverista::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Deliverista::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Deliverista::doCount($this);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Deliverista::isTemporalType($type)) {
			$value = Deliverista::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Deliverista::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Deliverista::isTemporalType($type)) {
			$value = Deliverista::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Deliverista::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andId($integer) {
		return $this->addAnd(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdNull() {
		return $this->andNull(Deliverista::ID);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Deliverista::ID);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Deliverista::ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orId($integer) {
		return $this->or(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdNull() {
		return $this->orNull(Deliverista::ID);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Deliverista::ID);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Deliverista::ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Deliverista::ID, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Deliverista::ID, $integer);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Deliverista::ID, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Deliverista::ID, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupById() {
		return $this->groupBy(Deliverista::ID);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombres($varchar) {
		return $this->addAnd(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresNot($varchar) {
		return $this->andNot(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresLike($varchar) {
		return $this->andLike(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresNotLike($varchar) {
		return $this->andNotLike(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresGreater($varchar) {
		return $this->andGreater(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresLess($varchar) {
		return $this->andLess(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresLessEqual($varchar) {
		return $this->andLessEqual(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresNull() {
		return $this->andNull(Deliverista::NOMBRES);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresNotNull() {
		return $this->andNotNull(Deliverista::NOMBRES);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresBetween($varchar, $from, $to) {
		return $this->andBetween(Deliverista::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresBeginsWith($varchar) {
		return $this->andBeginsWith(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresEndsWith($varchar) {
		return $this->andEndsWith(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andNombresContains($varchar) {
		return $this->andContains(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombres($varchar) {
		return $this->or(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresNot($varchar) {
		return $this->orNot(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresLike($varchar) {
		return $this->orLike(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresNotLike($varchar) {
		return $this->orNotLike(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresGreater($varchar) {
		return $this->orGreater(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresLess($varchar) {
		return $this->orLess(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresLessEqual($varchar) {
		return $this->orLessEqual(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresNull() {
		return $this->orNull(Deliverista::NOMBRES);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresNotNull() {
		return $this->orNotNull(Deliverista::NOMBRES);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresBetween($varchar, $from, $to) {
		return $this->orBetween(Deliverista::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresBeginsWith($varchar) {
		return $this->orBeginsWith(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresEndsWith($varchar) {
		return $this->orEndsWith(Deliverista::NOMBRES, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orNombresContains($varchar) {
		return $this->orContains(Deliverista::NOMBRES, $varchar);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByNombresAsc() {
		return $this->orderBy(Deliverista::NOMBRES, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByNombresDesc() {
		return $this->orderBy(Deliverista::NOMBRES, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByNombres() {
		return $this->groupBy(Deliverista::NOMBRES);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidos($varchar) {
		return $this->addAnd(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosNot($varchar) {
		return $this->andNot(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosLike($varchar) {
		return $this->andLike(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosNotLike($varchar) {
		return $this->andNotLike(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosGreater($varchar) {
		return $this->andGreater(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosGreaterEqual($varchar) {
		return $this->andGreaterEqual(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosLess($varchar) {
		return $this->andLess(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosLessEqual($varchar) {
		return $this->andLessEqual(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosNull() {
		return $this->andNull(Deliverista::APELLIDOS);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosNotNull() {
		return $this->andNotNull(Deliverista::APELLIDOS);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosBetween($varchar, $from, $to) {
		return $this->andBetween(Deliverista::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosBeginsWith($varchar) {
		return $this->andBeginsWith(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosEndsWith($varchar) {
		return $this->andEndsWith(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andApellidosContains($varchar) {
		return $this->andContains(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidos($varchar) {
		return $this->or(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosNot($varchar) {
		return $this->orNot(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosLike($varchar) {
		return $this->orLike(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosNotLike($varchar) {
		return $this->orNotLike(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosGreater($varchar) {
		return $this->orGreater(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosGreaterEqual($varchar) {
		return $this->orGreaterEqual(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosLess($varchar) {
		return $this->orLess(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosLessEqual($varchar) {
		return $this->orLessEqual(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosNull() {
		return $this->orNull(Deliverista::APELLIDOS);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosNotNull() {
		return $this->orNotNull(Deliverista::APELLIDOS);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosBetween($varchar, $from, $to) {
		return $this->orBetween(Deliverista::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosBeginsWith($varchar) {
		return $this->orBeginsWith(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosEndsWith($varchar) {
		return $this->orEndsWith(Deliverista::APELLIDOS, $varchar);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orApellidosContains($varchar) {
		return $this->orContains(Deliverista::APELLIDOS, $varchar);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByApellidosAsc() {
		return $this->orderBy(Deliverista::APELLIDOS, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByApellidosDesc() {
		return $this->orderBy(Deliverista::APELLIDOS, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByApellidos() {
		return $this->groupBy(Deliverista::APELLIDOS);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefono($integer) {
		return $this->addAnd(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoNot($integer) {
		return $this->andNot(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoLike($integer) {
		return $this->andLike(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoNotLike($integer) {
		return $this->andNotLike(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoGreater($integer) {
		return $this->andGreater(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoLess($integer) {
		return $this->andLess(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoLessEqual($integer) {
		return $this->andLessEqual(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoNull() {
		return $this->andNull(Deliverista::TELEFONO);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoNotNull() {
		return $this->andNotNull(Deliverista::TELEFONO);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoBetween($integer, $from, $to) {
		return $this->andBetween(Deliverista::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoBeginsWith($integer) {
		return $this->andBeginsWith(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoEndsWith($integer) {
		return $this->andEndsWith(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andTelefonoContains($integer) {
		return $this->andContains(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefono($integer) {
		return $this->or(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoNot($integer) {
		return $this->orNot(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoLike($integer) {
		return $this->orLike(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoNotLike($integer) {
		return $this->orNotLike(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoGreater($integer) {
		return $this->orGreater(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoLess($integer) {
		return $this->orLess(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoLessEqual($integer) {
		return $this->orLessEqual(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoNull() {
		return $this->orNull(Deliverista::TELEFONO);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoNotNull() {
		return $this->orNotNull(Deliverista::TELEFONO);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoBetween($integer, $from, $to) {
		return $this->orBetween(Deliverista::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoBeginsWith($integer) {
		return $this->orBeginsWith(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoEndsWith($integer) {
		return $this->orEndsWith(Deliverista::TELEFONO, $integer);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orTelefonoContains($integer) {
		return $this->orContains(Deliverista::TELEFONO, $integer);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByTelefonoAsc() {
		return $this->orderBy(Deliverista::TELEFONO, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByTelefonoDesc() {
		return $this->orderBy(Deliverista::TELEFONO, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByTelefono() {
		return $this->groupBy(Deliverista::TELEFONO);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Deliverista::CREATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Deliverista::CREATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliverista::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Deliverista::CREATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Deliverista::CREATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliverista::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliverista::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Deliverista::CREATED_AT, $timestamp);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Deliverista::CREATED_AT, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Deliverista::CREATED_AT, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Deliverista::CREATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Deliverista::UPDATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Deliverista::UPDATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliverista::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Deliverista::UPDATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Deliverista::UPDATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliverista::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliverista::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Deliverista::UPDATED_AT, $timestamp);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Deliverista::UPDATED_AT, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Deliverista::UPDATED_AT, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Deliverista::UPDATED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Deliverista::DELETED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Deliverista::DELETED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliverista::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Deliverista::DELETED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Deliverista::DELETED_AT);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliverista::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliverista::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Deliverista::DELETED_AT, $timestamp);
	}


	/**
	 * @return DeliveristaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Deliverista::DELETED_AT, self::ASC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Deliverista::DELETED_AT, self::DESC);
	}

	/**
	 * @return DeliveristaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Deliverista::DELETED_AT);
	}


}