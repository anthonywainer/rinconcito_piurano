<?php

use Dabl\Query\Query;

abstract class baseMeserosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Meseros::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MeserosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MeserosQuery($table_name, $alias);
	}

	/**
	 * @return Meseros[]
	 */
	function select() {
		return Meseros::doSelect($this);
	}

	/**
	 * @return Meseros
	 */
	function selectOne() {
		return Meseros::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Meseros::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Meseros::doCount($this);
	}

	/**
	 * @return MeserosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Meseros::isTemporalType($type)) {
			$value = Meseros::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Meseros::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MeserosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Meseros::isTemporalType($type)) {
			$value = Meseros::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Meseros::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MeserosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdNull() {
		return $this->andNull(Meseros::ID);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Meseros::ID);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Meseros::ID, $integer, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orId($integer) {
		return $this->or(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdNull() {
		return $this->orNull(Meseros::ID);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Meseros::ID);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Meseros::ID, $integer, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Meseros::ID, $integer);
	}

	/**
	 * @return MeserosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Meseros::ID, $integer);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Meseros::ID, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Meseros::ID, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupById() {
		return $this->groupBy(Meseros::ID);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombres($varchar) {
		return $this->addAnd(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresNot($varchar) {
		return $this->andNot(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresLike($varchar) {
		return $this->andLike(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresNotLike($varchar) {
		return $this->andNotLike(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresGreater($varchar) {
		return $this->andGreater(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresLess($varchar) {
		return $this->andLess(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresLessEqual($varchar) {
		return $this->andLessEqual(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresNull() {
		return $this->andNull(Meseros::NOMBRES);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresNotNull() {
		return $this->andNotNull(Meseros::NOMBRES);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresBetween($varchar, $from, $to) {
		return $this->andBetween(Meseros::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresBeginsWith($varchar) {
		return $this->andBeginsWith(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresEndsWith($varchar) {
		return $this->andEndsWith(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andNombresContains($varchar) {
		return $this->andContains(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombres($varchar) {
		return $this->or(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresNot($varchar) {
		return $this->orNot(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresLike($varchar) {
		return $this->orLike(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresNotLike($varchar) {
		return $this->orNotLike(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresGreater($varchar) {
		return $this->orGreater(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresLess($varchar) {
		return $this->orLess(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresLessEqual($varchar) {
		return $this->orLessEqual(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresNull() {
		return $this->orNull(Meseros::NOMBRES);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresNotNull() {
		return $this->orNotNull(Meseros::NOMBRES);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresBetween($varchar, $from, $to) {
		return $this->orBetween(Meseros::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresBeginsWith($varchar) {
		return $this->orBeginsWith(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresEndsWith($varchar) {
		return $this->orEndsWith(Meseros::NOMBRES, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orNombresContains($varchar) {
		return $this->orContains(Meseros::NOMBRES, $varchar);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByNombresAsc() {
		return $this->orderBy(Meseros::NOMBRES, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByNombresDesc() {
		return $this->orderBy(Meseros::NOMBRES, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupByNombres() {
		return $this->groupBy(Meseros::NOMBRES);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidos($varchar) {
		return $this->addAnd(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosNot($varchar) {
		return $this->andNot(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosLike($varchar) {
		return $this->andLike(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosNotLike($varchar) {
		return $this->andNotLike(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosGreater($varchar) {
		return $this->andGreater(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosGreaterEqual($varchar) {
		return $this->andGreaterEqual(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosLess($varchar) {
		return $this->andLess(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosLessEqual($varchar) {
		return $this->andLessEqual(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosNull() {
		return $this->andNull(Meseros::APELLIDOS);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosNotNull() {
		return $this->andNotNull(Meseros::APELLIDOS);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosBetween($varchar, $from, $to) {
		return $this->andBetween(Meseros::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosBeginsWith($varchar) {
		return $this->andBeginsWith(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosEndsWith($varchar) {
		return $this->andEndsWith(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andApellidosContains($varchar) {
		return $this->andContains(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidos($varchar) {
		return $this->or(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosNot($varchar) {
		return $this->orNot(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosLike($varchar) {
		return $this->orLike(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosNotLike($varchar) {
		return $this->orNotLike(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosGreater($varchar) {
		return $this->orGreater(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosGreaterEqual($varchar) {
		return $this->orGreaterEqual(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosLess($varchar) {
		return $this->orLess(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosLessEqual($varchar) {
		return $this->orLessEqual(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosNull() {
		return $this->orNull(Meseros::APELLIDOS);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosNotNull() {
		return $this->orNotNull(Meseros::APELLIDOS);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosBetween($varchar, $from, $to) {
		return $this->orBetween(Meseros::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosBeginsWith($varchar) {
		return $this->orBeginsWith(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosEndsWith($varchar) {
		return $this->orEndsWith(Meseros::APELLIDOS, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orApellidosContains($varchar) {
		return $this->orContains(Meseros::APELLIDOS, $varchar);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByApellidosAsc() {
		return $this->orderBy(Meseros::APELLIDOS, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByApellidosDesc() {
		return $this->orderBy(Meseros::APELLIDOS, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupByApellidos() {
		return $this->groupBy(Meseros::APELLIDOS);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexo($varchar) {
		return $this->addAnd(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoNot($varchar) {
		return $this->andNot(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoLike($varchar) {
		return $this->andLike(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoNotLike($varchar) {
		return $this->andNotLike(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoGreater($varchar) {
		return $this->andGreater(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoLess($varchar) {
		return $this->andLess(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoLessEqual($varchar) {
		return $this->andLessEqual(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoNull() {
		return $this->andNull(Meseros::SEXO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoNotNull() {
		return $this->andNotNull(Meseros::SEXO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoBetween($varchar, $from, $to) {
		return $this->andBetween(Meseros::SEXO, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoBeginsWith($varchar) {
		return $this->andBeginsWith(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoEndsWith($varchar) {
		return $this->andEndsWith(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSexoContains($varchar) {
		return $this->andContains(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexo($varchar) {
		return $this->or(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoNot($varchar) {
		return $this->orNot(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoLike($varchar) {
		return $this->orLike(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoNotLike($varchar) {
		return $this->orNotLike(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoGreater($varchar) {
		return $this->orGreater(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoLess($varchar) {
		return $this->orLess(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoLessEqual($varchar) {
		return $this->orLessEqual(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoNull() {
		return $this->orNull(Meseros::SEXO);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoNotNull() {
		return $this->orNotNull(Meseros::SEXO);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoBetween($varchar, $from, $to) {
		return $this->orBetween(Meseros::SEXO, $varchar, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoBeginsWith($varchar) {
		return $this->orBeginsWith(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoEndsWith($varchar) {
		return $this->orEndsWith(Meseros::SEXO, $varchar);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSexoContains($varchar) {
		return $this->orContains(Meseros::SEXO, $varchar);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderBySexoAsc() {
		return $this->orderBy(Meseros::SEXO, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderBySexoDesc() {
		return $this->orderBy(Meseros::SEXO, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupBySexo() {
		return $this->groupBy(Meseros::SEXO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasico($float) {
		return $this->addAnd(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoNot($float) {
		return $this->andNot(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoLike($float) {
		return $this->andLike(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoNotLike($float) {
		return $this->andNotLike(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoGreater($float) {
		return $this->andGreater(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoGreaterEqual($float) {
		return $this->andGreaterEqual(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoLess($float) {
		return $this->andLess(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoLessEqual($float) {
		return $this->andLessEqual(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoNull() {
		return $this->andNull(Meseros::SUELDO_BASICO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoNotNull() {
		return $this->andNotNull(Meseros::SUELDO_BASICO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoBetween($float, $from, $to) {
		return $this->andBetween(Meseros::SUELDO_BASICO, $float, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoBeginsWith($float) {
		return $this->andBeginsWith(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoEndsWith($float) {
		return $this->andEndsWith(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function andSueldoBasicoContains($float) {
		return $this->andContains(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasico($float) {
		return $this->or(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoNot($float) {
		return $this->orNot(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoLike($float) {
		return $this->orLike(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoNotLike($float) {
		return $this->orNotLike(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoGreater($float) {
		return $this->orGreater(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoGreaterEqual($float) {
		return $this->orGreaterEqual(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoLess($float) {
		return $this->orLess(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoLessEqual($float) {
		return $this->orLessEqual(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoNull() {
		return $this->orNull(Meseros::SUELDO_BASICO);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoNotNull() {
		return $this->orNotNull(Meseros::SUELDO_BASICO);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoBetween($float, $from, $to) {
		return $this->orBetween(Meseros::SUELDO_BASICO, $float, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoBeginsWith($float) {
		return $this->orBeginsWith(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoEndsWith($float) {
		return $this->orEndsWith(Meseros::SUELDO_BASICO, $float);
	}

	/**
	 * @return MeserosQuery
	 */
	function orSueldoBasicoContains($float) {
		return $this->orContains(Meseros::SUELDO_BASICO, $float);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderBySueldoBasicoAsc() {
		return $this->orderBy(Meseros::SUELDO_BASICO, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderBySueldoBasicoDesc() {
		return $this->orderBy(Meseros::SUELDO_BASICO, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupBySueldoBasico() {
		return $this->groupBy(Meseros::SUELDO_BASICO);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Meseros::CREATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Meseros::CREATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Meseros::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Meseros::CREATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Meseros::CREATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Meseros::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Meseros::CREATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Meseros::CREATED_AT, $timestamp);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Meseros::CREATED_AT, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Meseros::CREATED_AT, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Meseros::CREATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Meseros::UPDATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Meseros::UPDATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Meseros::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Meseros::UPDATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Meseros::UPDATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Meseros::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Meseros::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Meseros::UPDATED_AT, $timestamp);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Meseros::UPDATED_AT, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Meseros::UPDATED_AT, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Meseros::UPDATED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Meseros::DELETED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Meseros::DELETED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Meseros::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Meseros::DELETED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Meseros::DELETED_AT);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Meseros::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Meseros::DELETED_AT, $timestamp);
	}

	/**
	 * @return MeserosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Meseros::DELETED_AT, $timestamp);
	}


	/**
	 * @return MeserosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Meseros::DELETED_AT, self::ASC);
	}

	/**
	 * @return MeserosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Meseros::DELETED_AT, self::DESC);
	}

	/**
	 * @return MeserosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Meseros::DELETED_AT);
	}


}