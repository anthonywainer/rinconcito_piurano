<?php

use Dabl\Query\Query;

abstract class baseVentasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Ventas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VentasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VentasQuery($table_name, $alias);
	}

	/**
	 * @return Ventas[]
	 */
	function select() {
		return Ventas::doSelect($this);
	}

	/**
	 * @return Ventas
	 */
	function selectOne() {
		return Ventas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Ventas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Ventas::doCount($this);
	}

	/**
	 * @return VentasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Ventas::isTemporalType($type)) {
			$value = Ventas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Ventas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Ventas::isTemporalType($type)) {
			$value = Ventas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Ventas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdNull() {
		return $this->andNull(Ventas::ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Ventas::ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Ventas::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orId($integer) {
		return $this->or(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdNull() {
		return $this->orNull(Ventas::ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Ventas::ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Ventas::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Ventas::ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Ventas::ID, $integer);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Ventas::ID, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Ventas::ID, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupById() {
		return $this->groupBy(Ventas::ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroId($integer) {
		return $this->addAnd(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdNot($integer) {
		return $this->andNot(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdLike($integer) {
		return $this->andLike(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdNotLike($integer) {
		return $this->andNotLike(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdGreater($integer) {
		return $this->andGreater(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdLess($integer) {
		return $this->andLess(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdLessEqual($integer) {
		return $this->andLessEqual(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdNull() {
		return $this->andNull(Ventas::MESERO_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdNotNull() {
		return $this->andNotNull(Ventas::MESERO_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdBetween($integer, $from, $to) {
		return $this->andBetween(Ventas::MESERO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdBeginsWith($integer) {
		return $this->andBeginsWith(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdEndsWith($integer) {
		return $this->andEndsWith(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andMeseroIdContains($integer) {
		return $this->andContains(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroId($integer) {
		return $this->or(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdNot($integer) {
		return $this->orNot(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdLike($integer) {
		return $this->orLike(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdNotLike($integer) {
		return $this->orNotLike(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdGreater($integer) {
		return $this->orGreater(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdLess($integer) {
		return $this->orLess(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdLessEqual($integer) {
		return $this->orLessEqual(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdNull() {
		return $this->orNull(Ventas::MESERO_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdNotNull() {
		return $this->orNotNull(Ventas::MESERO_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdBetween($integer, $from, $to) {
		return $this->orBetween(Ventas::MESERO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdBeginsWith($integer) {
		return $this->orBeginsWith(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdEndsWith($integer) {
		return $this->orEndsWith(Ventas::MESERO_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orMeseroIdContains($integer) {
		return $this->orContains(Ventas::MESERO_ID, $integer);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByMeseroIdAsc() {
		return $this->orderBy(Ventas::MESERO_ID, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByMeseroIdDesc() {
		return $this->orderBy(Ventas::MESERO_ID, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByMeseroId() {
		return $this->groupBy(Ventas::MESERO_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(Ventas::CLIENTE_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(Ventas::CLIENTE_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(Ventas::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteId($integer) {
		return $this->or(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(Ventas::CLIENTE_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(Ventas::CLIENTE_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(Ventas::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(Ventas::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(Ventas::CLIENTE_ID, $integer);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(Ventas::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(Ventas::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(Ventas::CLIENTE_ID);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Ventas::CREATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Ventas::CREATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Ventas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Ventas::CREATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Ventas::CREATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Ventas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Ventas::CREATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Ventas::CREATED_AT, $timestamp);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Ventas::CREATED_AT, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Ventas::CREATED_AT, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Ventas::CREATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Ventas::UPDATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Ventas::UPDATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Ventas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Ventas::UPDATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Ventas::UPDATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Ventas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Ventas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Ventas::UPDATED_AT, $timestamp);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Ventas::UPDATED_AT, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Ventas::UPDATED_AT, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Ventas::UPDATED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Ventas::DELETED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Ventas::DELETED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Ventas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Ventas::DELETED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Ventas::DELETED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Ventas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Ventas::DELETED_AT, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Ventas::DELETED_AT, $timestamp);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Ventas::DELETED_AT, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Ventas::DELETED_AT, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Ventas::DELETED_AT);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVenta($boolean) {
		return $this->addAnd(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaNot($boolean) {
		return $this->andNot(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaLike($boolean) {
		return $this->andLike(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaNotLike($boolean) {
		return $this->andNotLike(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaGreater($boolean) {
		return $this->andGreater(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaGreaterEqual($boolean) {
		return $this->andGreaterEqual(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaLess($boolean) {
		return $this->andLess(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaLessEqual($boolean) {
		return $this->andLessEqual(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaNull() {
		return $this->andNull(Ventas::ESTADO_VENTA);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaNotNull() {
		return $this->andNotNull(Ventas::ESTADO_VENTA);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaBetween($boolean, $from, $to) {
		return $this->andBetween(Ventas::ESTADO_VENTA, $boolean, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaBeginsWith($boolean) {
		return $this->andBeginsWith(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaEndsWith($boolean) {
		return $this->andEndsWith(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function andEstadoVentaContains($boolean) {
		return $this->andContains(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVenta($boolean) {
		return $this->or(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaNot($boolean) {
		return $this->orNot(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaLike($boolean) {
		return $this->orLike(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaNotLike($boolean) {
		return $this->orNotLike(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaGreater($boolean) {
		return $this->orGreater(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaGreaterEqual($boolean) {
		return $this->orGreaterEqual(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaLess($boolean) {
		return $this->orLess(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaLessEqual($boolean) {
		return $this->orLessEqual(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaNull() {
		return $this->orNull(Ventas::ESTADO_VENTA);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaNotNull() {
		return $this->orNotNull(Ventas::ESTADO_VENTA);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaBetween($boolean, $from, $to) {
		return $this->orBetween(Ventas::ESTADO_VENTA, $boolean, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaBeginsWith($boolean) {
		return $this->orBeginsWith(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaEndsWith($boolean) {
		return $this->orEndsWith(Ventas::ESTADO_VENTA, $boolean);
	}

	/**
	 * @return VentasQuery
	 */
	function orEstadoVentaContains($boolean) {
		return $this->orContains(Ventas::ESTADO_VENTA, $boolean);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByEstadoVentaAsc() {
		return $this->orderBy(Ventas::ESTADO_VENTA, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByEstadoVentaDesc() {
		return $this->orderBy(Ventas::ESTADO_VENTA, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByEstadoVenta() {
		return $this->groupBy(Ventas::ESTADO_VENTA);
	}

	/**
	 * @return VentasQuery
	 */
	function andFecha($timestamp) {
		return $this->addAnd(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaNot($timestamp) {
		return $this->andNot(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaLike($timestamp) {
		return $this->andLike(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaNotLike($timestamp) {
		return $this->andNotLike(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaGreater($timestamp) {
		return $this->andGreater(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaLess($timestamp) {
		return $this->andLess(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaLessEqual($timestamp) {
		return $this->andLessEqual(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaNull() {
		return $this->andNull(Ventas::FECHA);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(Ventas::FECHA);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaBetween($timestamp, $from, $to) {
		return $this->andBetween(Ventas::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaBeginsWith($timestamp) {
		return $this->andBeginsWith(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaEndsWith($timestamp) {
		return $this->andEndsWith(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function andFechaContains($timestamp) {
		return $this->andContains(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFecha($timestamp) {
		return $this->or(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaNot($timestamp) {
		return $this->orNot(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaLike($timestamp) {
		return $this->orLike(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaNotLike($timestamp) {
		return $this->orNotLike(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaGreater($timestamp) {
		return $this->orGreater(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaLess($timestamp) {
		return $this->orLess(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaLessEqual($timestamp) {
		return $this->orLessEqual(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaNull() {
		return $this->orNull(Ventas::FECHA);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(Ventas::FECHA);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaBetween($timestamp, $from, $to) {
		return $this->orBetween(Ventas::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaBeginsWith($timestamp) {
		return $this->orBeginsWith(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaEndsWith($timestamp) {
		return $this->orEndsWith(Ventas::FECHA, $timestamp);
	}

	/**
	 * @return VentasQuery
	 */
	function orFechaContains($timestamp) {
		return $this->orContains(Ventas::FECHA, $timestamp);
	}


	/**
	 * @return VentasQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(Ventas::FECHA, self::ASC);
	}

	/**
	 * @return VentasQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(Ventas::FECHA, self::DESC);
	}

	/**
	 * @return VentasQuery
	 */
	function groupByFecha() {
		return $this->groupBy(Ventas::FECHA);
	}


}