<?php

use Dabl\Query\Query;

abstract class baseModulosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Modulos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ModulosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ModulosQuery($table_name, $alias);
	}

	/**
	 * @return Modulos[]
	 */
	function select() {
		return Modulos::doSelect($this);
	}

	/**
	 * @return Modulos
	 */
	function selectOne() {
		return Modulos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Modulos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Modulos::doCount($this);
	}

	/**
	 * @return ModulosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Modulos::isTemporalType($type)) {
			$value = Modulos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Modulos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ModulosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Modulos::isTemporalType($type)) {
			$value = Modulos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Modulos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ModulosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdNull() {
		return $this->andNull(Modulos::ID);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Modulos::ID);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Modulos::ID, $integer, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orId($integer) {
		return $this->or(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdNull() {
		return $this->orNull(Modulos::ID);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Modulos::ID);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Modulos::ID, $integer, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Modulos::ID, $integer);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Modulos::ID, $integer);
	}


	/**
	 * @return ModulosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Modulos::ID, self::ASC);
	}

	/**
	 * @return ModulosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Modulos::ID, self::DESC);
	}

	/**
	 * @return ModulosQuery
	 */
	function groupById() {
		return $this->groupBy(Modulos::ID);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreNull() {
		return $this->andNull(Modulos::NOMBRE);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Modulos::NOMBRE);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Modulos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombre($varchar) {
		return $this->or(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreNull() {
		return $this->orNull(Modulos::NOMBRE);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Modulos::NOMBRE);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Modulos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Modulos::NOMBRE, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Modulos::NOMBRE, $varchar);
	}


	/**
	 * @return ModulosQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Modulos::NOMBRE, self::ASC);
	}

	/**
	 * @return ModulosQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Modulos::NOMBRE, self::DESC);
	}

	/**
	 * @return ModulosQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Modulos::NOMBRE);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrl($varchar) {
		return $this->addAnd(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlNot($varchar) {
		return $this->andNot(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlLike($varchar) {
		return $this->andLike(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlNotLike($varchar) {
		return $this->andNotLike(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlGreater($varchar) {
		return $this->andGreater(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlGreaterEqual($varchar) {
		return $this->andGreaterEqual(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlLess($varchar) {
		return $this->andLess(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlLessEqual($varchar) {
		return $this->andLessEqual(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlNull() {
		return $this->andNull(Modulos::URL);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlNotNull() {
		return $this->andNotNull(Modulos::URL);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlBetween($varchar, $from, $to) {
		return $this->andBetween(Modulos::URL, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlBeginsWith($varchar) {
		return $this->andBeginsWith(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlEndsWith($varchar) {
		return $this->andEndsWith(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andUrlContains($varchar) {
		return $this->andContains(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrl($varchar) {
		return $this->or(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlNot($varchar) {
		return $this->orNot(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlLike($varchar) {
		return $this->orLike(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlNotLike($varchar) {
		return $this->orNotLike(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlGreater($varchar) {
		return $this->orGreater(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlGreaterEqual($varchar) {
		return $this->orGreaterEqual(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlLess($varchar) {
		return $this->orLess(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlLessEqual($varchar) {
		return $this->orLessEqual(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlNull() {
		return $this->orNull(Modulos::URL);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlNotNull() {
		return $this->orNotNull(Modulos::URL);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlBetween($varchar, $from, $to) {
		return $this->orBetween(Modulos::URL, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlBeginsWith($varchar) {
		return $this->orBeginsWith(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlEndsWith($varchar) {
		return $this->orEndsWith(Modulos::URL, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orUrlContains($varchar) {
		return $this->orContains(Modulos::URL, $varchar);
	}


	/**
	 * @return ModulosQuery
	 */
	function orderByUrlAsc() {
		return $this->orderBy(Modulos::URL, self::ASC);
	}

	/**
	 * @return ModulosQuery
	 */
	function orderByUrlDesc() {
		return $this->orderBy(Modulos::URL, self::DESC);
	}

	/**
	 * @return ModulosQuery
	 */
	function groupByUrl() {
		return $this->groupBy(Modulos::URL);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIcon($varchar) {
		return $this->addAnd(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconNot($varchar) {
		return $this->andNot(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconLike($varchar) {
		return $this->andLike(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconNotLike($varchar) {
		return $this->andNotLike(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconGreater($varchar) {
		return $this->andGreater(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconGreaterEqual($varchar) {
		return $this->andGreaterEqual(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconLess($varchar) {
		return $this->andLess(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconLessEqual($varchar) {
		return $this->andLessEqual(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconNull() {
		return $this->andNull(Modulos::ICON);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconNotNull() {
		return $this->andNotNull(Modulos::ICON);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconBetween($varchar, $from, $to) {
		return $this->andBetween(Modulos::ICON, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconBeginsWith($varchar) {
		return $this->andBeginsWith(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconEndsWith($varchar) {
		return $this->andEndsWith(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function andIconContains($varchar) {
		return $this->andContains(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIcon($varchar) {
		return $this->or(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconNot($varchar) {
		return $this->orNot(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconLike($varchar) {
		return $this->orLike(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconNotLike($varchar) {
		return $this->orNotLike(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconGreater($varchar) {
		return $this->orGreater(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconGreaterEqual($varchar) {
		return $this->orGreaterEqual(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconLess($varchar) {
		return $this->orLess(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconLessEqual($varchar) {
		return $this->orLessEqual(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconNull() {
		return $this->orNull(Modulos::ICON);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconNotNull() {
		return $this->orNotNull(Modulos::ICON);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconBetween($varchar, $from, $to) {
		return $this->orBetween(Modulos::ICON, $varchar, $from, $to);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconBeginsWith($varchar) {
		return $this->orBeginsWith(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconEndsWith($varchar) {
		return $this->orEndsWith(Modulos::ICON, $varchar);
	}

	/**
	 * @return ModulosQuery
	 */
	function orIconContains($varchar) {
		return $this->orContains(Modulos::ICON, $varchar);
	}


	/**
	 * @return ModulosQuery
	 */
	function orderByIconAsc() {
		return $this->orderBy(Modulos::ICON, self::ASC);
	}

	/**
	 * @return ModulosQuery
	 */
	function orderByIconDesc() {
		return $this->orderBy(Modulos::ICON, self::DESC);
	}

	/**
	 * @return ModulosQuery
	 */
	function groupByIcon() {
		return $this->groupBy(Modulos::ICON);
	}


}