<?php

use Dabl\Query\Query;

abstract class baseUsuariosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Usuarios::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UsuariosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UsuariosQuery($table_name, $alias);
	}

	/**
	 * @return Usuarios[]
	 */
	function select() {
		return Usuarios::doSelect($this);
	}

	/**
	 * @return Usuarios
	 */
	function selectOne() {
		return Usuarios::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Usuarios::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Usuarios::doCount($this);
	}

	/**
	 * @return UsuariosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Usuarios::isTemporalType($type)) {
			$value = Usuarios::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Usuarios::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UsuariosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Usuarios::isTemporalType($type)) {
			$value = Usuarios::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Usuarios::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdNull() {
		return $this->andNull(Usuarios::ID);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Usuarios::ID);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Usuarios::ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orId($integer) {
		return $this->or(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdNull() {
		return $this->orNull(Usuarios::ID);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Usuarios::ID);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Usuarios::ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Usuarios::ID, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Usuarios::ID, $integer);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Usuarios::ID, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Usuarios::ID, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupById() {
		return $this->groupBy(Usuarios::ID);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombres($varchar) {
		return $this->addAnd(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresNot($varchar) {
		return $this->andNot(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresLike($varchar) {
		return $this->andLike(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresNotLike($varchar) {
		return $this->andNotLike(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresGreater($varchar) {
		return $this->andGreater(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresLess($varchar) {
		return $this->andLess(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresNull() {
		return $this->andNull(Usuarios::NOMBRES);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresNotNull() {
		return $this->andNotNull(Usuarios::NOMBRES);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andNombresContains($varchar) {
		return $this->andContains(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombres($varchar) {
		return $this->or(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresNot($varchar) {
		return $this->orNot(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresLike($varchar) {
		return $this->orLike(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresNotLike($varchar) {
		return $this->orNotLike(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresGreater($varchar) {
		return $this->orGreater(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresLess($varchar) {
		return $this->orLess(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresNull() {
		return $this->orNull(Usuarios::NOMBRES);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresNotNull() {
		return $this->orNotNull(Usuarios::NOMBRES);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::NOMBRES, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orNombresContains($varchar) {
		return $this->orContains(Usuarios::NOMBRES, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByNombresAsc() {
		return $this->orderBy(Usuarios::NOMBRES, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByNombresDesc() {
		return $this->orderBy(Usuarios::NOMBRES, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByNombres() {
		return $this->groupBy(Usuarios::NOMBRES);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidos($varchar) {
		return $this->addAnd(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosNot($varchar) {
		return $this->andNot(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosLike($varchar) {
		return $this->andLike(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosNotLike($varchar) {
		return $this->andNotLike(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosGreater($varchar) {
		return $this->andGreater(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosLess($varchar) {
		return $this->andLess(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosNull() {
		return $this->andNull(Usuarios::APELLIDOS);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosNotNull() {
		return $this->andNotNull(Usuarios::APELLIDOS);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andApellidosContains($varchar) {
		return $this->andContains(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidos($varchar) {
		return $this->or(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosNot($varchar) {
		return $this->orNot(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosLike($varchar) {
		return $this->orLike(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosNotLike($varchar) {
		return $this->orNotLike(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosGreater($varchar) {
		return $this->orGreater(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosLess($varchar) {
		return $this->orLess(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosNull() {
		return $this->orNull(Usuarios::APELLIDOS);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosNotNull() {
		return $this->orNotNull(Usuarios::APELLIDOS);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::APELLIDOS, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orApellidosContains($varchar) {
		return $this->orContains(Usuarios::APELLIDOS, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByApellidosAsc() {
		return $this->orderBy(Usuarios::APELLIDOS, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByApellidosDesc() {
		return $this->orderBy(Usuarios::APELLIDOS, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByApellidos() {
		return $this->groupBy(Usuarios::APELLIDOS);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClave($varchar) {
		return $this->addAnd(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveNot($varchar) {
		return $this->andNot(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveLike($varchar) {
		return $this->andLike(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveNotLike($varchar) {
		return $this->andNotLike(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveGreater($varchar) {
		return $this->andGreater(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveLess($varchar) {
		return $this->andLess(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveNull() {
		return $this->andNull(Usuarios::CLAVE);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveNotNull() {
		return $this->andNotNull(Usuarios::CLAVE);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::CLAVE, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andClaveContains($varchar) {
		return $this->andContains(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClave($varchar) {
		return $this->or(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveNot($varchar) {
		return $this->orNot(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveLike($varchar) {
		return $this->orLike(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveNotLike($varchar) {
		return $this->orNotLike(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveGreater($varchar) {
		return $this->orGreater(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveLess($varchar) {
		return $this->orLess(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveNull() {
		return $this->orNull(Usuarios::CLAVE);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveNotNull() {
		return $this->orNotNull(Usuarios::CLAVE);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::CLAVE, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::CLAVE, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orClaveContains($varchar) {
		return $this->orContains(Usuarios::CLAVE, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByClaveAsc() {
		return $this->orderBy(Usuarios::CLAVE, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByClaveDesc() {
		return $this->orderBy(Usuarios::CLAVE, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByClave() {
		return $this->groupBy(Usuarios::CLAVE);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngreso($date) {
		return $this->addAnd(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoNot($date) {
		return $this->andNot(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoLike($date) {
		return $this->andLike(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoNotLike($date) {
		return $this->andNotLike(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoGreater($date) {
		return $this->andGreater(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoGreaterEqual($date) {
		return $this->andGreaterEqual(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoLess($date) {
		return $this->andLess(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoLessEqual($date) {
		return $this->andLessEqual(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoNull() {
		return $this->andNull(Usuarios::FECHA_INGRESO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoNotNull() {
		return $this->andNotNull(Usuarios::FECHA_INGRESO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoBetween($date, $from, $to) {
		return $this->andBetween(Usuarios::FECHA_INGRESO, $date, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoBeginsWith($date) {
		return $this->andBeginsWith(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoEndsWith($date) {
		return $this->andEndsWith(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaIngresoContains($date) {
		return $this->andContains(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngreso($date) {
		return $this->or(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoNot($date) {
		return $this->orNot(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoLike($date) {
		return $this->orLike(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoNotLike($date) {
		return $this->orNotLike(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoGreater($date) {
		return $this->orGreater(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoGreaterEqual($date) {
		return $this->orGreaterEqual(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoLess($date) {
		return $this->orLess(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoLessEqual($date) {
		return $this->orLessEqual(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoNull() {
		return $this->orNull(Usuarios::FECHA_INGRESO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoNotNull() {
		return $this->orNotNull(Usuarios::FECHA_INGRESO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoBetween($date, $from, $to) {
		return $this->orBetween(Usuarios::FECHA_INGRESO, $date, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoBeginsWith($date) {
		return $this->orBeginsWith(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoEndsWith($date) {
		return $this->orEndsWith(Usuarios::FECHA_INGRESO, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaIngresoContains($date) {
		return $this->orContains(Usuarios::FECHA_INGRESO, $date);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByFechaIngresoAsc() {
		return $this->orderBy(Usuarios::FECHA_INGRESO, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByFechaIngresoDesc() {
		return $this->orderBy(Usuarios::FECHA_INGRESO, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByFechaIngreso() {
		return $this->groupBy(Usuarios::FECHA_INGRESO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalida($date) {
		return $this->addAnd(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaNot($date) {
		return $this->andNot(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaLike($date) {
		return $this->andLike(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaNotLike($date) {
		return $this->andNotLike(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaGreater($date) {
		return $this->andGreater(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaGreaterEqual($date) {
		return $this->andGreaterEqual(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaLess($date) {
		return $this->andLess(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaLessEqual($date) {
		return $this->andLessEqual(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaNull() {
		return $this->andNull(Usuarios::FECHA_SALIDA);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaNotNull() {
		return $this->andNotNull(Usuarios::FECHA_SALIDA);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaBetween($date, $from, $to) {
		return $this->andBetween(Usuarios::FECHA_SALIDA, $date, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaBeginsWith($date) {
		return $this->andBeginsWith(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaEndsWith($date) {
		return $this->andEndsWith(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andFechaSalidaContains($date) {
		return $this->andContains(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalida($date) {
		return $this->or(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaNot($date) {
		return $this->orNot(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaLike($date) {
		return $this->orLike(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaNotLike($date) {
		return $this->orNotLike(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaGreater($date) {
		return $this->orGreater(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaGreaterEqual($date) {
		return $this->orGreaterEqual(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaLess($date) {
		return $this->orLess(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaLessEqual($date) {
		return $this->orLessEqual(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaNull() {
		return $this->orNull(Usuarios::FECHA_SALIDA);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaNotNull() {
		return $this->orNotNull(Usuarios::FECHA_SALIDA);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaBetween($date, $from, $to) {
		return $this->orBetween(Usuarios::FECHA_SALIDA, $date, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaBeginsWith($date) {
		return $this->orBeginsWith(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaEndsWith($date) {
		return $this->orEndsWith(Usuarios::FECHA_SALIDA, $date);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orFechaSalidaContains($date) {
		return $this->orContains(Usuarios::FECHA_SALIDA, $date);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByFechaSalidaAsc() {
		return $this->orderBy(Usuarios::FECHA_SALIDA, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByFechaSalidaDesc() {
		return $this->orderBy(Usuarios::FECHA_SALIDA, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByFechaSalida() {
		return $this->groupBy(Usuarios::FECHA_SALIDA);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivo($varchar) {
		return $this->addAnd(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoNot($varchar) {
		return $this->andNot(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoLike($varchar) {
		return $this->andLike(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoNotLike($varchar) {
		return $this->andNotLike(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoGreater($varchar) {
		return $this->andGreater(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoLess($varchar) {
		return $this->andLess(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoNull() {
		return $this->andNull(Usuarios::ESTA_ACTIVO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoNotNull() {
		return $this->andNotNull(Usuarios::ESTA_ACTIVO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::ESTA_ACTIVO, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andEstaActivoContains($varchar) {
		return $this->andContains(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivo($varchar) {
		return $this->or(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoNot($varchar) {
		return $this->orNot(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoLike($varchar) {
		return $this->orLike(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoNotLike($varchar) {
		return $this->orNotLike(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoGreater($varchar) {
		return $this->orGreater(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoLess($varchar) {
		return $this->orLess(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoNull() {
		return $this->orNull(Usuarios::ESTA_ACTIVO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoNotNull() {
		return $this->orNotNull(Usuarios::ESTA_ACTIVO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::ESTA_ACTIVO, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::ESTA_ACTIVO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orEstaActivoContains($varchar) {
		return $this->orContains(Usuarios::ESTA_ACTIVO, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByEstaActivoAsc() {
		return $this->orderBy(Usuarios::ESTA_ACTIVO, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByEstaActivoDesc() {
		return $this->orderBy(Usuarios::ESTA_ACTIVO, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByEstaActivo() {
		return $this->groupBy(Usuarios::ESTA_ACTIVO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreo($varchar) {
		return $this->addAnd(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoNot($varchar) {
		return $this->andNot(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoLike($varchar) {
		return $this->andLike(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoNotLike($varchar) {
		return $this->andNotLike(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoGreater($varchar) {
		return $this->andGreater(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoLess($varchar) {
		return $this->andLess(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoNull() {
		return $this->andNull(Usuarios::CORREO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoNotNull() {
		return $this->andNotNull(Usuarios::CORREO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::CORREO, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCorreoContains($varchar) {
		return $this->andContains(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreo($varchar) {
		return $this->or(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoNot($varchar) {
		return $this->orNot(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoLike($varchar) {
		return $this->orLike(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoNotLike($varchar) {
		return $this->orNotLike(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoGreater($varchar) {
		return $this->orGreater(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoLess($varchar) {
		return $this->orLess(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoNull() {
		return $this->orNull(Usuarios::CORREO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoNotNull() {
		return $this->orNotNull(Usuarios::CORREO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::CORREO, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::CORREO, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCorreoContains($varchar) {
		return $this->orContains(Usuarios::CORREO, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByCorreoAsc() {
		return $this->orderBy(Usuarios::CORREO, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByCorreoDesc() {
		return $this->orderBy(Usuarios::CORREO, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByCorreo() {
		return $this->groupBy(Usuarios::CORREO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefono($integer) {
		return $this->addAnd(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoNot($integer) {
		return $this->andNot(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoLike($integer) {
		return $this->andLike(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoNotLike($integer) {
		return $this->andNotLike(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoGreater($integer) {
		return $this->andGreater(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoGreaterEqual($integer) {
		return $this->andGreaterEqual(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoLess($integer) {
		return $this->andLess(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoLessEqual($integer) {
		return $this->andLessEqual(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoNull() {
		return $this->andNull(Usuarios::TELEFONO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoNotNull() {
		return $this->andNotNull(Usuarios::TELEFONO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoBetween($integer, $from, $to) {
		return $this->andBetween(Usuarios::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoBeginsWith($integer) {
		return $this->andBeginsWith(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoEndsWith($integer) {
		return $this->andEndsWith(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andTelefonoContains($integer) {
		return $this->andContains(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefono($integer) {
		return $this->or(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoNot($integer) {
		return $this->orNot(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoLike($integer) {
		return $this->orLike(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoNotLike($integer) {
		return $this->orNotLike(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoGreater($integer) {
		return $this->orGreater(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoGreaterEqual($integer) {
		return $this->orGreaterEqual(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoLess($integer) {
		return $this->orLess(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoLessEqual($integer) {
		return $this->orLessEqual(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoNull() {
		return $this->orNull(Usuarios::TELEFONO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoNotNull() {
		return $this->orNotNull(Usuarios::TELEFONO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoBetween($integer, $from, $to) {
		return $this->orBetween(Usuarios::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoBeginsWith($integer) {
		return $this->orBeginsWith(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoEndsWith($integer) {
		return $this->orEndsWith(Usuarios::TELEFONO, $integer);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orTelefonoContains($integer) {
		return $this->orContains(Usuarios::TELEFONO, $integer);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByTelefonoAsc() {
		return $this->orderBy(Usuarios::TELEFONO, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByTelefonoDesc() {
		return $this->orderBy(Usuarios::TELEFONO, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByTelefono() {
		return $this->groupBy(Usuarios::TELEFONO);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Usuarios::DIRECCION);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Usuarios::DIRECCION);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Usuarios::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Usuarios::DIRECCION);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Usuarios::DIRECCION);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Usuarios::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Usuarios::DIRECCION, $varchar);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Usuarios::DIRECCION, $varchar);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Usuarios::DIRECCION, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Usuarios::DIRECCION, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Usuarios::DIRECCION);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Usuarios::CREATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Usuarios::CREATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Usuarios::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Usuarios::CREATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Usuarios::CREATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Usuarios::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Usuarios::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Usuarios::CREATED_AT, $timestamp);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Usuarios::CREATED_AT, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Usuarios::CREATED_AT, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Usuarios::CREATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Usuarios::UPDATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Usuarios::UPDATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Usuarios::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Usuarios::UPDATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Usuarios::UPDATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Usuarios::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Usuarios::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Usuarios::UPDATED_AT, $timestamp);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Usuarios::UPDATED_AT, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Usuarios::UPDATED_AT, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Usuarios::UPDATED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Usuarios::DELETED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Usuarios::DELETED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Usuarios::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Usuarios::DELETED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Usuarios::DELETED_AT);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Usuarios::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Usuarios::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Usuarios::DELETED_AT, $timestamp);
	}


	/**
	 * @return UsuariosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Usuarios::DELETED_AT, self::ASC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Usuarios::DELETED_AT, self::DESC);
	}

	/**
	 * @return UsuariosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Usuarios::DELETED_AT);
	}


}