<?php

use Dabl\Query\Query;

abstract class baseAccionesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Acciones::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return AccionesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new AccionesQuery($table_name, $alias);
	}

	/**
	 * @return Acciones[]
	 */
	function select() {
		return Acciones::doSelect($this);
	}

	/**
	 * @return Acciones
	 */
	function selectOne() {
		return Acciones::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Acciones::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Acciones::doCount($this);
	}

	/**
	 * @return AccionesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Acciones::isTemporalType($type)) {
			$value = Acciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Acciones::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return AccionesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Acciones::isTemporalType($type)) {
			$value = Acciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Acciones::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return AccionesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdNull() {
		return $this->andNull(Acciones::ID);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Acciones::ID);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Acciones::ID, $integer, $from, $to);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orId($integer) {
		return $this->or(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdNull() {
		return $this->orNull(Acciones::ID);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Acciones::ID);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Acciones::ID, $integer, $from, $to);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Acciones::ID, $integer);
	}

	/**
	 * @return AccionesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Acciones::ID, $integer);
	}


	/**
	 * @return AccionesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Acciones::ID, self::ASC);
	}

	/**
	 * @return AccionesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Acciones::ID, self::DESC);
	}

	/**
	 * @return AccionesQuery
	 */
	function groupById() {
		return $this->groupBy(Acciones::ID);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccion($varchar) {
		return $this->addAnd(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionNot($varchar) {
		return $this->andNot(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionLike($varchar) {
		return $this->andLike(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionNotLike($varchar) {
		return $this->andNotLike(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionGreater($varchar) {
		return $this->andGreater(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionLess($varchar) {
		return $this->andLess(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionLessEqual($varchar) {
		return $this->andLessEqual(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionNull() {
		return $this->andNull(Acciones::ACCION);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionNotNull() {
		return $this->andNotNull(Acciones::ACCION);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionBetween($varchar, $from, $to) {
		return $this->andBetween(Acciones::ACCION, $varchar, $from, $to);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionBeginsWith($varchar) {
		return $this->andBeginsWith(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionEndsWith($varchar) {
		return $this->andEndsWith(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function andAccionContains($varchar) {
		return $this->andContains(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccion($varchar) {
		return $this->or(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionNot($varchar) {
		return $this->orNot(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionLike($varchar) {
		return $this->orLike(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionNotLike($varchar) {
		return $this->orNotLike(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionGreater($varchar) {
		return $this->orGreater(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionLess($varchar) {
		return $this->orLess(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionLessEqual($varchar) {
		return $this->orLessEqual(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionNull() {
		return $this->orNull(Acciones::ACCION);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionNotNull() {
		return $this->orNotNull(Acciones::ACCION);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionBetween($varchar, $from, $to) {
		return $this->orBetween(Acciones::ACCION, $varchar, $from, $to);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionBeginsWith($varchar) {
		return $this->orBeginsWith(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionEndsWith($varchar) {
		return $this->orEndsWith(Acciones::ACCION, $varchar);
	}

	/**
	 * @return AccionesQuery
	 */
	function orAccionContains($varchar) {
		return $this->orContains(Acciones::ACCION, $varchar);
	}


	/**
	 * @return AccionesQuery
	 */
	function orderByAccionAsc() {
		return $this->orderBy(Acciones::ACCION, self::ASC);
	}

	/**
	 * @return AccionesQuery
	 */
	function orderByAccionDesc() {
		return $this->orderBy(Acciones::ACCION, self::DESC);
	}

	/**
	 * @return AccionesQuery
	 */
	function groupByAccion() {
		return $this->groupBy(Acciones::ACCION);
	}


}