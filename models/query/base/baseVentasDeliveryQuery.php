<?php

use Dabl\Query\Query;

abstract class baseVentasDeliveryQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = VentasDelivery::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VentasDeliveryQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VentasDeliveryQuery($table_name, $alias);
	}

	/**
	 * @return VentasDelivery[]
	 */
	function select() {
		return VentasDelivery::doSelect($this);
	}

	/**
	 * @return VentasDelivery
	 */
	function selectOne() {
		return VentasDelivery::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return VentasDelivery::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return VentasDelivery::doCount($this);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasDelivery::isTemporalType($type)) {
			$value = VentasDelivery::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasDelivery::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasDelivery::isTemporalType($type)) {
			$value = VentasDelivery::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasDelivery::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andId($integer) {
		return $this->addAnd(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdNull() {
		return $this->andNull(VentasDelivery::ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(VentasDelivery::ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orId($integer) {
		return $this->or(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdNull() {
		return $this->orNull(VentasDelivery::ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(VentasDelivery::ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(VentasDelivery::ID, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(VentasDelivery::ID, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(VentasDelivery::ID, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupById() {
		return $this->groupBy(VentasDelivery::ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoId($integer) {
		return $this->addAnd(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdNot($integer) {
		return $this->andNot(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdLike($integer) {
		return $this->andLike(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdNotLike($integer) {
		return $this->andNotLike(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdGreater($integer) {
		return $this->andGreater(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdLess($integer) {
		return $this->andLess(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdNull() {
		return $this->andNull(VentasDelivery::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdNotNull() {
		return $this->andNotNull(VentasDelivery::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andMovimientoIdContains($integer) {
		return $this->andContains(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoId($integer) {
		return $this->or(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdNot($integer) {
		return $this->orNot(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdLike($integer) {
		return $this->orLike(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdNotLike($integer) {
		return $this->orNotLike(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdGreater($integer) {
		return $this->orGreater(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdLess($integer) {
		return $this->orLess(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdNull() {
		return $this->orNull(VentasDelivery::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdNotNull() {
		return $this->orNotNull(VentasDelivery::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orMovimientoIdContains($integer) {
		return $this->orContains(VentasDelivery::MOVIMIENTO_ID, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByMovimientoIdAsc() {
		return $this->orderBy(VentasDelivery::MOVIMIENTO_ID, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByMovimientoIdDesc() {
		return $this->orderBy(VentasDelivery::MOVIMIENTO_ID, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByMovimientoId() {
		return $this->groupBy(VentasDelivery::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryId($integer) {
		return $this->addAnd(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdNot($integer) {
		return $this->andNot(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdLike($integer) {
		return $this->andLike(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdNotLike($integer) {
		return $this->andNotLike(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdGreater($integer) {
		return $this->andGreater(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdLess($integer) {
		return $this->andLess(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdNull() {
		return $this->andNull(VentasDelivery::DELIVERY_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdNotNull() {
		return $this->andNotNull(VentasDelivery::DELIVERY_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::DELIVERY_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDeliveryIdContains($integer) {
		return $this->andContains(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryId($integer) {
		return $this->or(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdNot($integer) {
		return $this->orNot(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdLike($integer) {
		return $this->orLike(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdNotLike($integer) {
		return $this->orNotLike(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdGreater($integer) {
		return $this->orGreater(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdLess($integer) {
		return $this->orLess(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdNull() {
		return $this->orNull(VentasDelivery::DELIVERY_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdNotNull() {
		return $this->orNotNull(VentasDelivery::DELIVERY_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::DELIVERY_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::DELIVERY_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDeliveryIdContains($integer) {
		return $this->orContains(VentasDelivery::DELIVERY_ID, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByDeliveryIdAsc() {
		return $this->orderBy(VentasDelivery::DELIVERY_ID, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByDeliveryIdDesc() {
		return $this->orderBy(VentasDelivery::DELIVERY_ID, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByDeliveryId() {
		return $this->groupBy(VentasDelivery::DELIVERY_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotal($float) {
		return $this->addAnd(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalNot($float) {
		return $this->andNot(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalLike($float) {
		return $this->andLike(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalNotLike($float) {
		return $this->andNotLike(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalGreater($float) {
		return $this->andGreater(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalGreaterEqual($float) {
		return $this->andGreaterEqual(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalLess($float) {
		return $this->andLess(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalLessEqual($float) {
		return $this->andLessEqual(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalNull() {
		return $this->andNull(VentasDelivery::SUBTOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalNotNull() {
		return $this->andNotNull(VentasDelivery::SUBTOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalBetween($float, $from, $to) {
		return $this->andBetween(VentasDelivery::SUBTOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalBeginsWith($float) {
		return $this->andBeginsWith(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalEndsWith($float) {
		return $this->andEndsWith(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andSubtotalContains($float) {
		return $this->andContains(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotal($float) {
		return $this->or(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalNot($float) {
		return $this->orNot(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalLike($float) {
		return $this->orLike(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalNotLike($float) {
		return $this->orNotLike(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalGreater($float) {
		return $this->orGreater(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalGreaterEqual($float) {
		return $this->orGreaterEqual(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalLess($float) {
		return $this->orLess(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalLessEqual($float) {
		return $this->orLessEqual(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalNull() {
		return $this->orNull(VentasDelivery::SUBTOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalNotNull() {
		return $this->orNotNull(VentasDelivery::SUBTOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalBetween($float, $from, $to) {
		return $this->orBetween(VentasDelivery::SUBTOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalBeginsWith($float) {
		return $this->orBeginsWith(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalEndsWith($float) {
		return $this->orEndsWith(VentasDelivery::SUBTOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orSubtotalContains($float) {
		return $this->orContains(VentasDelivery::SUBTOTAL, $float);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderBySubtotalAsc() {
		return $this->orderBy(VentasDelivery::SUBTOTAL, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderBySubtotalDesc() {
		return $this->orderBy(VentasDelivery::SUBTOTAL, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupBySubtotal() {
		return $this->groupBy(VentasDelivery::SUBTOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFecha($timestamp) {
		return $this->addAnd(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaNot($timestamp) {
		return $this->andNot(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaLike($timestamp) {
		return $this->andLike(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaNotLike($timestamp) {
		return $this->andNotLike(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaGreater($timestamp) {
		return $this->andGreater(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaLess($timestamp) {
		return $this->andLess(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaLessEqual($timestamp) {
		return $this->andLessEqual(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaNull() {
		return $this->andNull(VentasDelivery::FECHA);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(VentasDelivery::FECHA);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaBetween($timestamp, $from, $to) {
		return $this->andBetween(VentasDelivery::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaBeginsWith($timestamp) {
		return $this->andBeginsWith(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaEndsWith($timestamp) {
		return $this->andEndsWith(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andFechaContains($timestamp) {
		return $this->andContains(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFecha($timestamp) {
		return $this->or(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaNot($timestamp) {
		return $this->orNot(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaLike($timestamp) {
		return $this->orLike(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaNotLike($timestamp) {
		return $this->orNotLike(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaGreater($timestamp) {
		return $this->orGreater(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaLess($timestamp) {
		return $this->orLess(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaLessEqual($timestamp) {
		return $this->orLessEqual(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaNull() {
		return $this->orNull(VentasDelivery::FECHA);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(VentasDelivery::FECHA);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaBetween($timestamp, $from, $to) {
		return $this->orBetween(VentasDelivery::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaBeginsWith($timestamp) {
		return $this->orBeginsWith(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaEndsWith($timestamp) {
		return $this->orEndsWith(VentasDelivery::FECHA, $timestamp);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orFechaContains($timestamp) {
		return $this->orContains(VentasDelivery::FECHA, $timestamp);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(VentasDelivery::FECHA, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(VentasDelivery::FECHA, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByFecha() {
		return $this->groupBy(VentasDelivery::FECHA);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuento($float) {
		return $this->addAnd(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoNot($float) {
		return $this->andNot(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoLike($float) {
		return $this->andLike(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoNotLike($float) {
		return $this->andNotLike(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoGreater($float) {
		return $this->andGreater(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoGreaterEqual($float) {
		return $this->andGreaterEqual(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoLess($float) {
		return $this->andLess(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoLessEqual($float) {
		return $this->andLessEqual(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoNull() {
		return $this->andNull(VentasDelivery::DESCUENTO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoNotNull() {
		return $this->andNotNull(VentasDelivery::DESCUENTO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoBetween($float, $from, $to) {
		return $this->andBetween(VentasDelivery::DESCUENTO, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoBeginsWith($float) {
		return $this->andBeginsWith(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoEndsWith($float) {
		return $this->andEndsWith(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andDescuentoContains($float) {
		return $this->andContains(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuento($float) {
		return $this->or(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoNot($float) {
		return $this->orNot(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoLike($float) {
		return $this->orLike(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoNotLike($float) {
		return $this->orNotLike(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoGreater($float) {
		return $this->orGreater(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoGreaterEqual($float) {
		return $this->orGreaterEqual(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoLess($float) {
		return $this->orLess(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoLessEqual($float) {
		return $this->orLessEqual(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoNull() {
		return $this->orNull(VentasDelivery::DESCUENTO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoNotNull() {
		return $this->orNotNull(VentasDelivery::DESCUENTO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoBetween($float, $from, $to) {
		return $this->orBetween(VentasDelivery::DESCUENTO, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoBeginsWith($float) {
		return $this->orBeginsWith(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoEndsWith($float) {
		return $this->orEndsWith(VentasDelivery::DESCUENTO, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orDescuentoContains($float) {
		return $this->orContains(VentasDelivery::DESCUENTO, $float);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByDescuentoAsc() {
		return $this->orderBy(VentasDelivery::DESCUENTO, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByDescuentoDesc() {
		return $this->orderBy(VentasDelivery::DESCUENTO, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByDescuento() {
		return $this->groupBy(VentasDelivery::DESCUENTO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentaje($integer) {
		return $this->addAnd(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeNot($integer) {
		return $this->andNot(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeLike($integer) {
		return $this->andLike(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeNotLike($integer) {
		return $this->andNotLike(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeGreater($integer) {
		return $this->andGreater(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeLess($integer) {
		return $this->andLess(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeNull() {
		return $this->andNull(VentasDelivery::PORCENTAJE);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeNotNull() {
		return $this->andNotNull(VentasDelivery::PORCENTAJE);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::PORCENTAJE, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andPorcentajeContains($integer) {
		return $this->andContains(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentaje($integer) {
		return $this->or(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeNot($integer) {
		return $this->orNot(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeLike($integer) {
		return $this->orLike(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeNotLike($integer) {
		return $this->orNotLike(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeGreater($integer) {
		return $this->orGreater(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeLess($integer) {
		return $this->orLess(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeNull() {
		return $this->orNull(VentasDelivery::PORCENTAJE);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeNotNull() {
		return $this->orNotNull(VentasDelivery::PORCENTAJE);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::PORCENTAJE, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orPorcentajeContains($integer) {
		return $this->orContains(VentasDelivery::PORCENTAJE, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByPorcentajeAsc() {
		return $this->orderBy(VentasDelivery::PORCENTAJE, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByPorcentajeDesc() {
		return $this->orderBy(VentasDelivery::PORCENTAJE, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByPorcentaje() {
		return $this->groupBy(VentasDelivery::PORCENTAJE);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotal($float) {
		return $this->addAnd(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalNot($float) {
		return $this->andNot(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalLike($float) {
		return $this->andLike(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalNotLike($float) {
		return $this->andNotLike(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalGreater($float) {
		return $this->andGreater(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalGreaterEqual($float) {
		return $this->andGreaterEqual(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalLess($float) {
		return $this->andLess(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalLessEqual($float) {
		return $this->andLessEqual(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalNull() {
		return $this->andNull(VentasDelivery::TOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalNotNull() {
		return $this->andNotNull(VentasDelivery::TOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalBetween($float, $from, $to) {
		return $this->andBetween(VentasDelivery::TOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalBeginsWith($float) {
		return $this->andBeginsWith(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalEndsWith($float) {
		return $this->andEndsWith(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andTotalContains($float) {
		return $this->andContains(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotal($float) {
		return $this->or(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalNot($float) {
		return $this->orNot(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalLike($float) {
		return $this->orLike(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalNotLike($float) {
		return $this->orNotLike(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalGreater($float) {
		return $this->orGreater(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalGreaterEqual($float) {
		return $this->orGreaterEqual(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalLess($float) {
		return $this->orLess(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalLessEqual($float) {
		return $this->orLessEqual(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalNull() {
		return $this->orNull(VentasDelivery::TOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalNotNull() {
		return $this->orNotNull(VentasDelivery::TOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalBetween($float, $from, $to) {
		return $this->orBetween(VentasDelivery::TOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalBeginsWith($float) {
		return $this->orBeginsWith(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalEndsWith($float) {
		return $this->orEndsWith(VentasDelivery::TOTAL, $float);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orTotalContains($float) {
		return $this->orContains(VentasDelivery::TOTAL, $float);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByTotalAsc() {
		return $this->orderBy(VentasDelivery::TOTAL, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByTotalDesc() {
		return $this->orderBy(VentasDelivery::TOTAL, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByTotal() {
		return $this->groupBy(VentasDelivery::TOTAL);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivo($integer) {
		return $this->addAnd(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoNot($integer) {
		return $this->andNot(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoLike($integer) {
		return $this->andLike(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoNotLike($integer) {
		return $this->andNotLike(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoGreater($integer) {
		return $this->andGreater(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoLess($integer) {
		return $this->andLess(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoNull() {
		return $this->andNull(VentasDelivery::EFECTIVO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoNotNull() {
		return $this->andNotNull(VentasDelivery::EFECTIVO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::EFECTIVO, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andEfectivoContains($integer) {
		return $this->andContains(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivo($integer) {
		return $this->or(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoNot($integer) {
		return $this->orNot(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoLike($integer) {
		return $this->orLike(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoNotLike($integer) {
		return $this->orNotLike(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoGreater($integer) {
		return $this->orGreater(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoLess($integer) {
		return $this->orLess(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoNull() {
		return $this->orNull(VentasDelivery::EFECTIVO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoNotNull() {
		return $this->orNotNull(VentasDelivery::EFECTIVO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::EFECTIVO, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::EFECTIVO, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orEfectivoContains($integer) {
		return $this->orContains(VentasDelivery::EFECTIVO, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByEfectivoAsc() {
		return $this->orderBy(VentasDelivery::EFECTIVO, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByEfectivoDesc() {
		return $this->orderBy(VentasDelivery::EFECTIVO, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByEfectivo() {
		return $this->groupBy(VentasDelivery::EFECTIVO);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(VentasDelivery::CLIENTE_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(VentasDelivery::CLIENTE_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasDelivery::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteId($integer) {
		return $this->or(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(VentasDelivery::CLIENTE_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(VentasDelivery::CLIENTE_ID);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasDelivery::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(VentasDelivery::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(VentasDelivery::CLIENTE_ID, $integer);
	}


	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(VentasDelivery::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(VentasDelivery::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return VentasDeliveryQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(VentasDelivery::CLIENTE_ID);
	}


}