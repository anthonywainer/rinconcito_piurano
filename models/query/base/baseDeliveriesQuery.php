<?php

use Dabl\Query\Query;

abstract class baseDeliveriesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Deliveries::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return DeliveriesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new DeliveriesQuery($table_name, $alias);
	}

	/**
	 * @return Deliveries[]
	 */
	function select() {
		return Deliveries::doSelect($this);
	}

	/**
	 * @return Deliveries
	 */
	function selectOne() {
		return Deliveries::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Deliveries::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Deliveries::doCount($this);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Deliveries::isTemporalType($type)) {
			$value = Deliveries::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Deliveries::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Deliveries::isTemporalType($type)) {
			$value = Deliveries::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Deliveries::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdNull() {
		return $this->andNull(Deliveries::ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Deliveries::ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Deliveries::ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orId($integer) {
		return $this->or(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdNull() {
		return $this->orNull(Deliveries::ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Deliveries::ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Deliveries::ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Deliveries::ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Deliveries::ID, $integer);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Deliveries::ID, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Deliveries::ID, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupById() {
		return $this->groupBy(Deliveries::ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFecha($date) {
		return $this->addAnd(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaNot($date) {
		return $this->andNot(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaLike($date) {
		return $this->andLike(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaNotLike($date) {
		return $this->andNotLike(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaGreater($date) {
		return $this->andGreater(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaGreaterEqual($date) {
		return $this->andGreaterEqual(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaLess($date) {
		return $this->andLess(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaLessEqual($date) {
		return $this->andLessEqual(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaNull() {
		return $this->andNull(Deliveries::FECHA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(Deliveries::FECHA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaBetween($date, $from, $to) {
		return $this->andBetween(Deliveries::FECHA, $date, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaBeginsWith($date) {
		return $this->andBeginsWith(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaEndsWith($date) {
		return $this->andEndsWith(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andFechaContains($date) {
		return $this->andContains(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFecha($date) {
		return $this->or(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaNot($date) {
		return $this->orNot(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaLike($date) {
		return $this->orLike(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaNotLike($date) {
		return $this->orNotLike(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaGreater($date) {
		return $this->orGreater(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaGreaterEqual($date) {
		return $this->orGreaterEqual(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaLess($date) {
		return $this->orLess(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaLessEqual($date) {
		return $this->orLessEqual(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaNull() {
		return $this->orNull(Deliveries::FECHA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(Deliveries::FECHA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaBetween($date, $from, $to) {
		return $this->orBetween(Deliveries::FECHA, $date, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaBeginsWith($date) {
		return $this->orBeginsWith(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaEndsWith($date) {
		return $this->orEndsWith(Deliveries::FECHA, $date);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orFechaContains($date) {
		return $this->orContains(Deliveries::FECHA, $date);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(Deliveries::FECHA, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(Deliveries::FECHA, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByFecha() {
		return $this->groupBy(Deliveries::FECHA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Deliveries::DIRECCION);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Deliveries::DIRECCION);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Deliveries::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Deliveries::DIRECCION);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Deliveries::DIRECCION);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Deliveries::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Deliveries::DIRECCION, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Deliveries::DIRECCION, $varchar);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Deliveries::DIRECCION, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Deliveries::DIRECCION, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Deliveries::DIRECCION);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferencia($varchar) {
		return $this->addAnd(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaNot($varchar) {
		return $this->andNot(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaLike($varchar) {
		return $this->andLike(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaNotLike($varchar) {
		return $this->andNotLike(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaGreater($varchar) {
		return $this->andGreater(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaLess($varchar) {
		return $this->andLess(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaLessEqual($varchar) {
		return $this->andLessEqual(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaNull() {
		return $this->andNull(Deliveries::REFERENCIA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaNotNull() {
		return $this->andNotNull(Deliveries::REFERENCIA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaBetween($varchar, $from, $to) {
		return $this->andBetween(Deliveries::REFERENCIA, $varchar, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaBeginsWith($varchar) {
		return $this->andBeginsWith(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaEndsWith($varchar) {
		return $this->andEndsWith(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andReferenciaContains($varchar) {
		return $this->andContains(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferencia($varchar) {
		return $this->or(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaNot($varchar) {
		return $this->orNot(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaLike($varchar) {
		return $this->orLike(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaNotLike($varchar) {
		return $this->orNotLike(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaGreater($varchar) {
		return $this->orGreater(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaLess($varchar) {
		return $this->orLess(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaLessEqual($varchar) {
		return $this->orLessEqual(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaNull() {
		return $this->orNull(Deliveries::REFERENCIA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaNotNull() {
		return $this->orNotNull(Deliveries::REFERENCIA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaBetween($varchar, $from, $to) {
		return $this->orBetween(Deliveries::REFERENCIA, $varchar, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaBeginsWith($varchar) {
		return $this->orBeginsWith(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaEndsWith($varchar) {
		return $this->orEndsWith(Deliveries::REFERENCIA, $varchar);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orReferenciaContains($varchar) {
		return $this->orContains(Deliveries::REFERENCIA, $varchar);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByReferenciaAsc() {
		return $this->orderBy(Deliveries::REFERENCIA, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByReferenciaDesc() {
		return $this->orderBy(Deliveries::REFERENCIA, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByReferencia() {
		return $this->groupBy(Deliveries::REFERENCIA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaId($integer) {
		return $this->addAnd(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdNot($integer) {
		return $this->andNot(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdLike($integer) {
		return $this->andLike(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdNotLike($integer) {
		return $this->andNotLike(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdGreater($integer) {
		return $this->andGreater(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdLess($integer) {
		return $this->andLess(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdLessEqual($integer) {
		return $this->andLessEqual(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdNull() {
		return $this->andNull(Deliveries::DELIVERISTA_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdNotNull() {
		return $this->andNotNull(Deliveries::DELIVERISTA_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdBetween($integer, $from, $to) {
		return $this->andBetween(Deliveries::DELIVERISTA_ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdBeginsWith($integer) {
		return $this->andBeginsWith(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdEndsWith($integer) {
		return $this->andEndsWith(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeliveristaIdContains($integer) {
		return $this->andContains(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaId($integer) {
		return $this->or(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdNot($integer) {
		return $this->orNot(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdLike($integer) {
		return $this->orLike(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdNotLike($integer) {
		return $this->orNotLike(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdGreater($integer) {
		return $this->orGreater(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdLess($integer) {
		return $this->orLess(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdLessEqual($integer) {
		return $this->orLessEqual(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdNull() {
		return $this->orNull(Deliveries::DELIVERISTA_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdNotNull() {
		return $this->orNotNull(Deliveries::DELIVERISTA_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdBetween($integer, $from, $to) {
		return $this->orBetween(Deliveries::DELIVERISTA_ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdBeginsWith($integer) {
		return $this->orBeginsWith(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdEndsWith($integer) {
		return $this->orEndsWith(Deliveries::DELIVERISTA_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeliveristaIdContains($integer) {
		return $this->orContains(Deliveries::DELIVERISTA_ID, $integer);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByDeliveristaIdAsc() {
		return $this->orderBy(Deliveries::DELIVERISTA_ID, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByDeliveristaIdDesc() {
		return $this->orderBy(Deliveries::DELIVERISTA_ID, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByDeliveristaId() {
		return $this->groupBy(Deliveries::DELIVERISTA_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Deliveries::CREATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Deliveries::CREATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliveries::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Deliveries::CREATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Deliveries::CREATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliveries::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliveries::CREATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Deliveries::CREATED_AT, $timestamp);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Deliveries::CREATED_AT, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Deliveries::CREATED_AT, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Deliveries::CREATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Deliveries::UPDATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Deliveries::UPDATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliveries::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Deliveries::UPDATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Deliveries::UPDATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliveries::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliveries::UPDATED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Deliveries::UPDATED_AT, $timestamp);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Deliveries::UPDATED_AT, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Deliveries::UPDATED_AT, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Deliveries::UPDATED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Deliveries::DELETED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Deliveries::DELETED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Deliveries::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Deliveries::DELETED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Deliveries::DELETED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Deliveries::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Deliveries::DELETED_AT, $timestamp);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Deliveries::DELETED_AT, $timestamp);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Deliveries::DELETED_AT, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Deliveries::DELETED_AT, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Deliveries::DELETED_AT);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(Deliveries::CLIENTE_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(Deliveries::CLIENTE_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(Deliveries::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteId($integer) {
		return $this->or(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(Deliveries::CLIENTE_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(Deliveries::CLIENTE_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(Deliveries::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(Deliveries::CLIENTE_ID, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(Deliveries::CLIENTE_ID, $integer);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(Deliveries::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(Deliveries::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(Deliveries::CLIENTE_ID);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemora($integer) {
		return $this->addAnd(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraNot($integer) {
		return $this->andNot(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraLike($integer) {
		return $this->andLike(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraNotLike($integer) {
		return $this->andNotLike(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraGreater($integer) {
		return $this->andGreater(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraLess($integer) {
		return $this->andLess(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraLessEqual($integer) {
		return $this->andLessEqual(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraNull() {
		return $this->andNull(Deliveries::TIEMPO_DEMORA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraNotNull() {
		return $this->andNotNull(Deliveries::TIEMPO_DEMORA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraBetween($integer, $from, $to) {
		return $this->andBetween(Deliveries::TIEMPO_DEMORA, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraBeginsWith($integer) {
		return $this->andBeginsWith(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraEndsWith($integer) {
		return $this->andEndsWith(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andTiempoDemoraContains($integer) {
		return $this->andContains(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemora($integer) {
		return $this->or(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraNot($integer) {
		return $this->orNot(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraLike($integer) {
		return $this->orLike(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraNotLike($integer) {
		return $this->orNotLike(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraGreater($integer) {
		return $this->orGreater(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraLess($integer) {
		return $this->orLess(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraLessEqual($integer) {
		return $this->orLessEqual(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraNull() {
		return $this->orNull(Deliveries::TIEMPO_DEMORA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraNotNull() {
		return $this->orNotNull(Deliveries::TIEMPO_DEMORA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraBetween($integer, $from, $to) {
		return $this->orBetween(Deliveries::TIEMPO_DEMORA, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraBeginsWith($integer) {
		return $this->orBeginsWith(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraEndsWith($integer) {
		return $this->orEndsWith(Deliveries::TIEMPO_DEMORA, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orTiempoDemoraContains($integer) {
		return $this->orContains(Deliveries::TIEMPO_DEMORA, $integer);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByTiempoDemoraAsc() {
		return $this->orderBy(Deliveries::TIEMPO_DEMORA, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByTiempoDemoraDesc() {
		return $this->orderBy(Deliveries::TIEMPO_DEMORA, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByTiempoDemora() {
		return $this->groupBy(Deliveries::TIEMPO_DEMORA);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDelivery($integer) {
		return $this->addAnd(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryNot($integer) {
		return $this->andNot(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryLike($integer) {
		return $this->andLike(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryNotLike($integer) {
		return $this->andNotLike(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryGreater($integer) {
		return $this->andGreater(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryGreaterEqual($integer) {
		return $this->andGreaterEqual(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryLess($integer) {
		return $this->andLess(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryLessEqual($integer) {
		return $this->andLessEqual(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryNull() {
		return $this->andNull(Deliveries::ESTADO_DELIVERY);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryNotNull() {
		return $this->andNotNull(Deliveries::ESTADO_DELIVERY);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryBetween($integer, $from, $to) {
		return $this->andBetween(Deliveries::ESTADO_DELIVERY, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryBeginsWith($integer) {
		return $this->andBeginsWith(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryEndsWith($integer) {
		return $this->andEndsWith(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function andEstadoDeliveryContains($integer) {
		return $this->andContains(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDelivery($integer) {
		return $this->or(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryNot($integer) {
		return $this->orNot(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryLike($integer) {
		return $this->orLike(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryNotLike($integer) {
		return $this->orNotLike(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryGreater($integer) {
		return $this->orGreater(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryGreaterEqual($integer) {
		return $this->orGreaterEqual(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryLess($integer) {
		return $this->orLess(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryLessEqual($integer) {
		return $this->orLessEqual(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryNull() {
		return $this->orNull(Deliveries::ESTADO_DELIVERY);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryNotNull() {
		return $this->orNotNull(Deliveries::ESTADO_DELIVERY);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryBetween($integer, $from, $to) {
		return $this->orBetween(Deliveries::ESTADO_DELIVERY, $integer, $from, $to);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryBeginsWith($integer) {
		return $this->orBeginsWith(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryEndsWith($integer) {
		return $this->orEndsWith(Deliveries::ESTADO_DELIVERY, $integer);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orEstadoDeliveryContains($integer) {
		return $this->orContains(Deliveries::ESTADO_DELIVERY, $integer);
	}


	/**
	 * @return DeliveriesQuery
	 */
	function orderByEstadoDeliveryAsc() {
		return $this->orderBy(Deliveries::ESTADO_DELIVERY, self::ASC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function orderByEstadoDeliveryDesc() {
		return $this->orderBy(Deliveries::ESTADO_DELIVERY, self::DESC);
	}

	/**
	 * @return DeliveriesQuery
	 */
	function groupByEstadoDelivery() {
		return $this->groupBy(Deliveries::ESTADO_DELIVERY);
	}


}