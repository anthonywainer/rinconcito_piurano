<?php

use Dabl\Query\Query;

abstract class basePermisosUsuarioQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = PermisosUsuario::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return PermisosUsuarioQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new PermisosUsuarioQuery($table_name, $alias);
	}

	/**
	 * @return PermisosUsuario[]
	 */
	function select() {
		return PermisosUsuario::doSelect($this);
	}

	/**
	 * @return PermisosUsuario
	 */
	function selectOne() {
		return PermisosUsuario::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return PermisosUsuario::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return PermisosUsuario::doCount($this);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PermisosUsuario::isTemporalType($type)) {
			$value = PermisosUsuario::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PermisosUsuario::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PermisosUsuario::isTemporalType($type)) {
			$value = PermisosUsuario::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PermisosUsuario::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andId($integer) {
		return $this->addAnd(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdNull() {
		return $this->andNull(PermisosUsuario::ID);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(PermisosUsuario::ID);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(PermisosUsuario::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orId($integer) {
		return $this->or(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdNull() {
		return $this->orNull(PermisosUsuario::ID);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(PermisosUsuario::ID);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(PermisosUsuario::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(PermisosUsuario::ID, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(PermisosUsuario::ID, $integer);
	}


	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(PermisosUsuario::ID, self::ASC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(PermisosUsuario::ID, self::DESC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function groupById() {
		return $this->groupBy(PermisosUsuario::ID);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuario($integer) {
		return $this->addAnd(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioNot($integer) {
		return $this->andNot(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioLike($integer) {
		return $this->andLike(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioNotLike($integer) {
		return $this->andNotLike(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioGreater($integer) {
		return $this->andGreater(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioLess($integer) {
		return $this->andLess(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioLessEqual($integer) {
		return $this->andLessEqual(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioNull() {
		return $this->andNull(PermisosUsuario::IDUSUARIO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioNotNull() {
		return $this->andNotNull(PermisosUsuario::IDUSUARIO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioBetween($integer, $from, $to) {
		return $this->andBetween(PermisosUsuario::IDUSUARIO, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioBeginsWith($integer) {
		return $this->andBeginsWith(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioEndsWith($integer) {
		return $this->andEndsWith(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdusuarioContains($integer) {
		return $this->andContains(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuario($integer) {
		return $this->or(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioNot($integer) {
		return $this->orNot(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioLike($integer) {
		return $this->orLike(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioNotLike($integer) {
		return $this->orNotLike(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioGreater($integer) {
		return $this->orGreater(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioLess($integer) {
		return $this->orLess(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioLessEqual($integer) {
		return $this->orLessEqual(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioNull() {
		return $this->orNull(PermisosUsuario::IDUSUARIO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioNotNull() {
		return $this->orNotNull(PermisosUsuario::IDUSUARIO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioBetween($integer, $from, $to) {
		return $this->orBetween(PermisosUsuario::IDUSUARIO, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioBeginsWith($integer) {
		return $this->orBeginsWith(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioEndsWith($integer) {
		return $this->orEndsWith(PermisosUsuario::IDUSUARIO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdusuarioContains($integer) {
		return $this->orContains(PermisosUsuario::IDUSUARIO, $integer);
	}


	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdusuarioAsc() {
		return $this->orderBy(PermisosUsuario::IDUSUARIO, self::ASC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdusuarioDesc() {
		return $this->orderBy(PermisosUsuario::IDUSUARIO, self::DESC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function groupByIdusuario() {
		return $this->groupBy(PermisosUsuario::IDUSUARIO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermiso($integer) {
		return $this->addAnd(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoNot($integer) {
		return $this->andNot(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoLike($integer) {
		return $this->andLike(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoNotLike($integer) {
		return $this->andNotLike(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoGreater($integer) {
		return $this->andGreater(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoLess($integer) {
		return $this->andLess(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoLessEqual($integer) {
		return $this->andLessEqual(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoNull() {
		return $this->andNull(PermisosUsuario::IDPERMISO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoNotNull() {
		return $this->andNotNull(PermisosUsuario::IDPERMISO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoBetween($integer, $from, $to) {
		return $this->andBetween(PermisosUsuario::IDPERMISO, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoBeginsWith($integer) {
		return $this->andBeginsWith(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoEndsWith($integer) {
		return $this->andEndsWith(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function andIdpermisoContains($integer) {
		return $this->andContains(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermiso($integer) {
		return $this->or(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoNot($integer) {
		return $this->orNot(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoLike($integer) {
		return $this->orLike(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoNotLike($integer) {
		return $this->orNotLike(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoGreater($integer) {
		return $this->orGreater(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoLess($integer) {
		return $this->orLess(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoLessEqual($integer) {
		return $this->orLessEqual(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoNull() {
		return $this->orNull(PermisosUsuario::IDPERMISO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoNotNull() {
		return $this->orNotNull(PermisosUsuario::IDPERMISO);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoBetween($integer, $from, $to) {
		return $this->orBetween(PermisosUsuario::IDPERMISO, $integer, $from, $to);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoBeginsWith($integer) {
		return $this->orBeginsWith(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoEndsWith($integer) {
		return $this->orEndsWith(PermisosUsuario::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orIdpermisoContains($integer) {
		return $this->orContains(PermisosUsuario::IDPERMISO, $integer);
	}


	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdpermisoAsc() {
		return $this->orderBy(PermisosUsuario::IDPERMISO, self::ASC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function orderByIdpermisoDesc() {
		return $this->orderBy(PermisosUsuario::IDPERMISO, self::DESC);
	}

	/**
	 * @return PermisosUsuarioQuery
	 */
	function groupByIdpermiso() {
		return $this->groupBy(PermisosUsuario::IDPERMISO);
	}


}