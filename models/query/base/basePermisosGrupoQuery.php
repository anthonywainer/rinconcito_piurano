<?php

use Dabl\Query\Query;

abstract class basePermisosGrupoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = PermisosGrupo::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return PermisosGrupoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new PermisosGrupoQuery($table_name, $alias);
	}

	/**
	 * @return PermisosGrupo[]
	 */
	function select() {
		return PermisosGrupo::doSelect($this);
	}

	/**
	 * @return PermisosGrupo
	 */
	function selectOne() {
		return PermisosGrupo::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return PermisosGrupo::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return PermisosGrupo::doCount($this);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PermisosGrupo::isTemporalType($type)) {
			$value = PermisosGrupo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PermisosGrupo::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PermisosGrupo::isTemporalType($type)) {
			$value = PermisosGrupo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PermisosGrupo::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andId($integer) {
		return $this->addAnd(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdNull() {
		return $this->andNull(PermisosGrupo::ID);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(PermisosGrupo::ID);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(PermisosGrupo::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orId($integer) {
		return $this->or(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdNull() {
		return $this->orNull(PermisosGrupo::ID);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(PermisosGrupo::ID);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(PermisosGrupo::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(PermisosGrupo::ID, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(PermisosGrupo::ID, $integer);
	}


	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(PermisosGrupo::ID, self::ASC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(PermisosGrupo::ID, self::DESC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function groupById() {
		return $this->groupBy(PermisosGrupo::ID);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermiso($integer) {
		return $this->addAnd(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoNot($integer) {
		return $this->andNot(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoLike($integer) {
		return $this->andLike(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoNotLike($integer) {
		return $this->andNotLike(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoGreater($integer) {
		return $this->andGreater(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoLess($integer) {
		return $this->andLess(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoLessEqual($integer) {
		return $this->andLessEqual(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoNull() {
		return $this->andNull(PermisosGrupo::IDPERMISO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoNotNull() {
		return $this->andNotNull(PermisosGrupo::IDPERMISO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoBetween($integer, $from, $to) {
		return $this->andBetween(PermisosGrupo::IDPERMISO, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoBeginsWith($integer) {
		return $this->andBeginsWith(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoEndsWith($integer) {
		return $this->andEndsWith(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdpermisoContains($integer) {
		return $this->andContains(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermiso($integer) {
		return $this->or(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoNot($integer) {
		return $this->orNot(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoLike($integer) {
		return $this->orLike(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoNotLike($integer) {
		return $this->orNotLike(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoGreater($integer) {
		return $this->orGreater(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoLess($integer) {
		return $this->orLess(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoLessEqual($integer) {
		return $this->orLessEqual(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoNull() {
		return $this->orNull(PermisosGrupo::IDPERMISO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoNotNull() {
		return $this->orNotNull(PermisosGrupo::IDPERMISO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoBetween($integer, $from, $to) {
		return $this->orBetween(PermisosGrupo::IDPERMISO, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoBeginsWith($integer) {
		return $this->orBeginsWith(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoEndsWith($integer) {
		return $this->orEndsWith(PermisosGrupo::IDPERMISO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdpermisoContains($integer) {
		return $this->orContains(PermisosGrupo::IDPERMISO, $integer);
	}


	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdpermisoAsc() {
		return $this->orderBy(PermisosGrupo::IDPERMISO, self::ASC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdpermisoDesc() {
		return $this->orderBy(PermisosGrupo::IDPERMISO, self::DESC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function groupByIdpermiso() {
		return $this->groupBy(PermisosGrupo::IDPERMISO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupo($integer) {
		return $this->addAnd(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoNot($integer) {
		return $this->andNot(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoLike($integer) {
		return $this->andLike(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoNotLike($integer) {
		return $this->andNotLike(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoGreater($integer) {
		return $this->andGreater(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoGreaterEqual($integer) {
		return $this->andGreaterEqual(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoLess($integer) {
		return $this->andLess(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoLessEqual($integer) {
		return $this->andLessEqual(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoNull() {
		return $this->andNull(PermisosGrupo::IDGRUPO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoNotNull() {
		return $this->andNotNull(PermisosGrupo::IDGRUPO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoBetween($integer, $from, $to) {
		return $this->andBetween(PermisosGrupo::IDGRUPO, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoBeginsWith($integer) {
		return $this->andBeginsWith(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoEndsWith($integer) {
		return $this->andEndsWith(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function andIdgrupoContains($integer) {
		return $this->andContains(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupo($integer) {
		return $this->or(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoNot($integer) {
		return $this->orNot(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoLike($integer) {
		return $this->orLike(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoNotLike($integer) {
		return $this->orNotLike(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoGreater($integer) {
		return $this->orGreater(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoGreaterEqual($integer) {
		return $this->orGreaterEqual(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoLess($integer) {
		return $this->orLess(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoLessEqual($integer) {
		return $this->orLessEqual(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoNull() {
		return $this->orNull(PermisosGrupo::IDGRUPO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoNotNull() {
		return $this->orNotNull(PermisosGrupo::IDGRUPO);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoBetween($integer, $from, $to) {
		return $this->orBetween(PermisosGrupo::IDGRUPO, $integer, $from, $to);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoBeginsWith($integer) {
		return $this->orBeginsWith(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoEndsWith($integer) {
		return $this->orEndsWith(PermisosGrupo::IDGRUPO, $integer);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orIdgrupoContains($integer) {
		return $this->orContains(PermisosGrupo::IDGRUPO, $integer);
	}


	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdgrupoAsc() {
		return $this->orderBy(PermisosGrupo::IDGRUPO, self::ASC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function orderByIdgrupoDesc() {
		return $this->orderBy(PermisosGrupo::IDGRUPO, self::DESC);
	}

	/**
	 * @return PermisosGrupoQuery
	 */
	function groupByIdgrupo() {
		return $this->groupBy(PermisosGrupo::IDGRUPO);
	}


}