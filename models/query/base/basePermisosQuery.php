<?php

use Dabl\Query\Query;

abstract class basePermisosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Permisos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return PermisosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new PermisosQuery($table_name, $alias);
	}

	/**
	 * @return Permisos[]
	 */
	function select() {
		return Permisos::doSelect($this);
	}

	/**
	 * @return Permisos
	 */
	function selectOne() {
		return Permisos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Permisos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Permisos::doCount($this);
	}

	/**
	 * @return PermisosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Permisos::isTemporalType($type)) {
			$value = Permisos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Permisos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Permisos::isTemporalType($type)) {
			$value = Permisos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Permisos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return PermisosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdNull() {
		return $this->andNull(Permisos::ID);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Permisos::ID);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Permisos::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orId($integer) {
		return $this->or(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdNull() {
		return $this->orNull(Permisos::ID);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Permisos::ID);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Permisos::ID, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Permisos::ID, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Permisos::ID, $integer);
	}


	/**
	 * @return PermisosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Permisos::ID, self::ASC);
	}

	/**
	 * @return PermisosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Permisos::ID, self::DESC);
	}

	/**
	 * @return PermisosQuery
	 */
	function groupById() {
		return $this->groupBy(Permisos::ID);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccion($integer) {
		return $this->addAnd(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionNot($integer) {
		return $this->andNot(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionLike($integer) {
		return $this->andLike(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionNotLike($integer) {
		return $this->andNotLike(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionGreater($integer) {
		return $this->andGreater(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionGreaterEqual($integer) {
		return $this->andGreaterEqual(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionLess($integer) {
		return $this->andLess(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionLessEqual($integer) {
		return $this->andLessEqual(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionNull() {
		return $this->andNull(Permisos::IDACCION);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionNotNull() {
		return $this->andNotNull(Permisos::IDACCION);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionBetween($integer, $from, $to) {
		return $this->andBetween(Permisos::IDACCION, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionBeginsWith($integer) {
		return $this->andBeginsWith(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionEndsWith($integer) {
		return $this->andEndsWith(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdaccionContains($integer) {
		return $this->andContains(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccion($integer) {
		return $this->or(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionNot($integer) {
		return $this->orNot(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionLike($integer) {
		return $this->orLike(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionNotLike($integer) {
		return $this->orNotLike(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionGreater($integer) {
		return $this->orGreater(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionGreaterEqual($integer) {
		return $this->orGreaterEqual(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionLess($integer) {
		return $this->orLess(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionLessEqual($integer) {
		return $this->orLessEqual(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionNull() {
		return $this->orNull(Permisos::IDACCION);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionNotNull() {
		return $this->orNotNull(Permisos::IDACCION);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionBetween($integer, $from, $to) {
		return $this->orBetween(Permisos::IDACCION, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionBeginsWith($integer) {
		return $this->orBeginsWith(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionEndsWith($integer) {
		return $this->orEndsWith(Permisos::IDACCION, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdaccionContains($integer) {
		return $this->orContains(Permisos::IDACCION, $integer);
	}


	/**
	 * @return PermisosQuery
	 */
	function orderByIdaccionAsc() {
		return $this->orderBy(Permisos::IDACCION, self::ASC);
	}

	/**
	 * @return PermisosQuery
	 */
	function orderByIdaccionDesc() {
		return $this->orderBy(Permisos::IDACCION, self::DESC);
	}

	/**
	 * @return PermisosQuery
	 */
	function groupByIdaccion() {
		return $this->groupBy(Permisos::IDACCION);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmodulo($integer) {
		return $this->addAnd(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloNot($integer) {
		return $this->andNot(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloLike($integer) {
		return $this->andLike(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloNotLike($integer) {
		return $this->andNotLike(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloGreater($integer) {
		return $this->andGreater(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloGreaterEqual($integer) {
		return $this->andGreaterEqual(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloLess($integer) {
		return $this->andLess(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloLessEqual($integer) {
		return $this->andLessEqual(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloNull() {
		return $this->andNull(Permisos::IDSUBMODULO);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloNotNull() {
		return $this->andNotNull(Permisos::IDSUBMODULO);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloBetween($integer, $from, $to) {
		return $this->andBetween(Permisos::IDSUBMODULO, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloBeginsWith($integer) {
		return $this->andBeginsWith(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloEndsWith($integer) {
		return $this->andEndsWith(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function andIdsubmoduloContains($integer) {
		return $this->andContains(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmodulo($integer) {
		return $this->or(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloNot($integer) {
		return $this->orNot(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloLike($integer) {
		return $this->orLike(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloNotLike($integer) {
		return $this->orNotLike(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloGreater($integer) {
		return $this->orGreater(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloGreaterEqual($integer) {
		return $this->orGreaterEqual(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloLess($integer) {
		return $this->orLess(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloLessEqual($integer) {
		return $this->orLessEqual(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloNull() {
		return $this->orNull(Permisos::IDSUBMODULO);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloNotNull() {
		return $this->orNotNull(Permisos::IDSUBMODULO);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloBetween($integer, $from, $to) {
		return $this->orBetween(Permisos::IDSUBMODULO, $integer, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloBeginsWith($integer) {
		return $this->orBeginsWith(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloEndsWith($integer) {
		return $this->orEndsWith(Permisos::IDSUBMODULO, $integer);
	}

	/**
	 * @return PermisosQuery
	 */
	function orIdsubmoduloContains($integer) {
		return $this->orContains(Permisos::IDSUBMODULO, $integer);
	}


	/**
	 * @return PermisosQuery
	 */
	function orderByIdsubmoduloAsc() {
		return $this->orderBy(Permisos::IDSUBMODULO, self::ASC);
	}

	/**
	 * @return PermisosQuery
	 */
	function orderByIdsubmoduloDesc() {
		return $this->orderBy(Permisos::IDSUBMODULO, self::DESC);
	}

	/**
	 * @return PermisosQuery
	 */
	function groupByIdsubmodulo() {
		return $this->groupBy(Permisos::IDSUBMODULO);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreNull() {
		return $this->andNull(Permisos::NOMBRE);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Permisos::NOMBRE);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Permisos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombre($varchar) {
		return $this->or(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreNull() {
		return $this->orNull(Permisos::NOMBRE);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Permisos::NOMBRE);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Permisos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Permisos::NOMBRE, $varchar);
	}

	/**
	 * @return PermisosQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Permisos::NOMBRE, $varchar);
	}


	/**
	 * @return PermisosQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Permisos::NOMBRE, self::ASC);
	}

	/**
	 * @return PermisosQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Permisos::NOMBRE, self::DESC);
	}

	/**
	 * @return PermisosQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Permisos::NOMBRE);
	}


}