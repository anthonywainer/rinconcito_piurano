<?php

use Dabl\Query\Query;

abstract class baseMesasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Mesas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MesasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MesasQuery($table_name, $alias);
	}

	/**
	 * @return Mesas[]
	 */
	function select() {
		return Mesas::doSelect($this);
	}

	/**
	 * @return Mesas
	 */
	function selectOne() {
		return Mesas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Mesas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Mesas::doCount($this);
	}

	/**
	 * @return MesasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Mesas::isTemporalType($type)) {
			$value = Mesas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Mesas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MesasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Mesas::isTemporalType($type)) {
			$value = Mesas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Mesas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MesasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdNull() {
		return $this->andNull(Mesas::ID);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Mesas::ID);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Mesas::ID, $integer, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orId($integer) {
		return $this->or(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdNull() {
		return $this->orNull(Mesas::ID);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Mesas::ID);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Mesas::ID, $integer, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Mesas::ID, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Mesas::ID, $integer);
	}


	/**
	 * @return MesasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Mesas::ID, self::ASC);
	}

	/**
	 * @return MesasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Mesas::ID, self::DESC);
	}

	/**
	 * @return MesasQuery
	 */
	function groupById() {
		return $this->groupBy(Mesas::ID);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumero($integer) {
		return $this->addAnd(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroNot($integer) {
		return $this->andNot(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroLike($integer) {
		return $this->andLike(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroNotLike($integer) {
		return $this->andNotLike(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroGreater($integer) {
		return $this->andGreater(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroGreaterEqual($integer) {
		return $this->andGreaterEqual(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroLess($integer) {
		return $this->andLess(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroLessEqual($integer) {
		return $this->andLessEqual(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroNull() {
		return $this->andNull(Mesas::NUMERO);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroNotNull() {
		return $this->andNotNull(Mesas::NUMERO);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroBetween($integer, $from, $to) {
		return $this->andBetween(Mesas::NUMERO, $integer, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroBeginsWith($integer) {
		return $this->andBeginsWith(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroEndsWith($integer) {
		return $this->andEndsWith(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function andNumeroContains($integer) {
		return $this->andContains(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumero($integer) {
		return $this->or(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroNot($integer) {
		return $this->orNot(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroLike($integer) {
		return $this->orLike(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroNotLike($integer) {
		return $this->orNotLike(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroGreater($integer) {
		return $this->orGreater(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroGreaterEqual($integer) {
		return $this->orGreaterEqual(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroLess($integer) {
		return $this->orLess(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroLessEqual($integer) {
		return $this->orLessEqual(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroNull() {
		return $this->orNull(Mesas::NUMERO);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroNotNull() {
		return $this->orNotNull(Mesas::NUMERO);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroBetween($integer, $from, $to) {
		return $this->orBetween(Mesas::NUMERO, $integer, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroBeginsWith($integer) {
		return $this->orBeginsWith(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroEndsWith($integer) {
		return $this->orEndsWith(Mesas::NUMERO, $integer);
	}

	/**
	 * @return MesasQuery
	 */
	function orNumeroContains($integer) {
		return $this->orContains(Mesas::NUMERO, $integer);
	}


	/**
	 * @return MesasQuery
	 */
	function orderByNumeroAsc() {
		return $this->orderBy(Mesas::NUMERO, self::ASC);
	}

	/**
	 * @return MesasQuery
	 */
	function orderByNumeroDesc() {
		return $this->orderBy(Mesas::NUMERO, self::DESC);
	}

	/**
	 * @return MesasQuery
	 */
	function groupByNumero() {
		return $this->groupBy(Mesas::NUMERO);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesa($varchar) {
		return $this->addAnd(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaNot($varchar) {
		return $this->andNot(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaLike($varchar) {
		return $this->andLike(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaNotLike($varchar) {
		return $this->andNotLike(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaGreater($varchar) {
		return $this->andGreater(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaLess($varchar) {
		return $this->andLess(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaLessEqual($varchar) {
		return $this->andLessEqual(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaNull() {
		return $this->andNull(Mesas::ESTADO_MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaNotNull() {
		return $this->andNotNull(Mesas::ESTADO_MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaBetween($varchar, $from, $to) {
		return $this->andBetween(Mesas::ESTADO_MESA, $varchar, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaBeginsWith($varchar) {
		return $this->andBeginsWith(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaEndsWith($varchar) {
		return $this->andEndsWith(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andEstadoMesaContains($varchar) {
		return $this->andContains(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesa($varchar) {
		return $this->or(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaNot($varchar) {
		return $this->orNot(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaLike($varchar) {
		return $this->orLike(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaNotLike($varchar) {
		return $this->orNotLike(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaGreater($varchar) {
		return $this->orGreater(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaLess($varchar) {
		return $this->orLess(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaLessEqual($varchar) {
		return $this->orLessEqual(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaNull() {
		return $this->orNull(Mesas::ESTADO_MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaNotNull() {
		return $this->orNotNull(Mesas::ESTADO_MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaBetween($varchar, $from, $to) {
		return $this->orBetween(Mesas::ESTADO_MESA, $varchar, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaBeginsWith($varchar) {
		return $this->orBeginsWith(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaEndsWith($varchar) {
		return $this->orEndsWith(Mesas::ESTADO_MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orEstadoMesaContains($varchar) {
		return $this->orContains(Mesas::ESTADO_MESA, $varchar);
	}


	/**
	 * @return MesasQuery
	 */
	function orderByEstadoMesaAsc() {
		return $this->orderBy(Mesas::ESTADO_MESA, self::ASC);
	}

	/**
	 * @return MesasQuery
	 */
	function orderByEstadoMesaDesc() {
		return $this->orderBy(Mesas::ESTADO_MESA, self::DESC);
	}

	/**
	 * @return MesasQuery
	 */
	function groupByEstadoMesa() {
		return $this->groupBy(Mesas::ESTADO_MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesa($varchar) {
		return $this->addAnd(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaNot($varchar) {
		return $this->andNot(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaLike($varchar) {
		return $this->andLike(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaNotLike($varchar) {
		return $this->andNotLike(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaGreater($varchar) {
		return $this->andGreater(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaLess($varchar) {
		return $this->andLess(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaLessEqual($varchar) {
		return $this->andLessEqual(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaNull() {
		return $this->andNull(Mesas::MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaNotNull() {
		return $this->andNotNull(Mesas::MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaBetween($varchar, $from, $to) {
		return $this->andBetween(Mesas::MESA, $varchar, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaBeginsWith($varchar) {
		return $this->andBeginsWith(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaEndsWith($varchar) {
		return $this->andEndsWith(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function andMesaContains($varchar) {
		return $this->andContains(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesa($varchar) {
		return $this->or(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaNot($varchar) {
		return $this->orNot(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaLike($varchar) {
		return $this->orLike(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaNotLike($varchar) {
		return $this->orNotLike(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaGreater($varchar) {
		return $this->orGreater(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaLess($varchar) {
		return $this->orLess(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaLessEqual($varchar) {
		return $this->orLessEqual(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaNull() {
		return $this->orNull(Mesas::MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaNotNull() {
		return $this->orNotNull(Mesas::MESA);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaBetween($varchar, $from, $to) {
		return $this->orBetween(Mesas::MESA, $varchar, $from, $to);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaBeginsWith($varchar) {
		return $this->orBeginsWith(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaEndsWith($varchar) {
		return $this->orEndsWith(Mesas::MESA, $varchar);
	}

	/**
	 * @return MesasQuery
	 */
	function orMesaContains($varchar) {
		return $this->orContains(Mesas::MESA, $varchar);
	}


	/**
	 * @return MesasQuery
	 */
	function orderByMesaAsc() {
		return $this->orderBy(Mesas::MESA, self::ASC);
	}

	/**
	 * @return MesasQuery
	 */
	function orderByMesaDesc() {
		return $this->orderBy(Mesas::MESA, self::DESC);
	}

	/**
	 * @return MesasQuery
	 */
	function groupByMesa() {
		return $this->groupBy(Mesas::MESA);
	}


}