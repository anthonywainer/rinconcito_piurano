<?php

use Dabl\Query\Query;

abstract class basePedidoProductoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = PedidoProducto::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return PedidoProductoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new PedidoProductoQuery($table_name, $alias);
	}

	/**
	 * @return PedidoProducto[]
	 */
	function select() {
		return PedidoProducto::doSelect($this);
	}

	/**
	 * @return PedidoProducto
	 */
	function selectOne() {
		return PedidoProducto::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return PedidoProducto::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return PedidoProducto::doCount($this);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PedidoProducto::isTemporalType($type)) {
			$value = PedidoProducto::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PedidoProducto::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && PedidoProducto::isTemporalType($type)) {
			$value = PedidoProducto::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = PedidoProducto::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andId($integer) {
		return $this->addAnd(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdNull() {
		return $this->andNull(PedidoProducto::ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(PedidoProducto::ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(PedidoProducto::ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orId($integer) {
		return $this->or(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdNull() {
		return $this->orNull(PedidoProducto::ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(PedidoProducto::ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(PedidoProducto::ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(PedidoProducto::ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(PedidoProducto::ID, $integer);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(PedidoProducto::ID, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(PedidoProducto::ID, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupById() {
		return $this->groupBy(PedidoProducto::ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoId($integer) {
		return $this->addAnd(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdNot($integer) {
		return $this->andNot(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdLike($integer) {
		return $this->andLike(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdNotLike($integer) {
		return $this->andNotLike(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdGreater($integer) {
		return $this->andGreater(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdLess($integer) {
		return $this->andLess(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdLessEqual($integer) {
		return $this->andLessEqual(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdNull() {
		return $this->andNull(PedidoProducto::PRODUCTO_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdNotNull() {
		return $this->andNotNull(PedidoProducto::PRODUCTO_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(PedidoProducto::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdEndsWith($integer) {
		return $this->andEndsWith(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andProductoIdContains($integer) {
		return $this->andContains(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoId($integer) {
		return $this->or(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdNot($integer) {
		return $this->orNot(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdLike($integer) {
		return $this->orLike(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdNotLike($integer) {
		return $this->orNotLike(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdGreater($integer) {
		return $this->orGreater(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdLess($integer) {
		return $this->orLess(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdLessEqual($integer) {
		return $this->orLessEqual(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdNull() {
		return $this->orNull(PedidoProducto::PRODUCTO_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdNotNull() {
		return $this->orNotNull(PedidoProducto::PRODUCTO_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(PedidoProducto::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdEndsWith($integer) {
		return $this->orEndsWith(PedidoProducto::PRODUCTO_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orProductoIdContains($integer) {
		return $this->orContains(PedidoProducto::PRODUCTO_ID, $integer);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByProductoIdAsc() {
		return $this->orderBy(PedidoProducto::PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByProductoIdDesc() {
		return $this->orderBy(PedidoProducto::PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByProductoId() {
		return $this->groupBy(PedidoProducto::PRODUCTO_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaId($integer) {
		return $this->addAnd(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdNot($integer) {
		return $this->andNot(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdLike($integer) {
		return $this->andLike(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdNotLike($integer) {
		return $this->andNotLike(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdGreater($integer) {
		return $this->andGreater(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdLess($integer) {
		return $this->andLess(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdLessEqual($integer) {
		return $this->andLessEqual(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdNull() {
		return $this->andNull(PedidoProducto::VENTA_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdNotNull() {
		return $this->andNotNull(PedidoProducto::VENTA_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdBetween($integer, $from, $to) {
		return $this->andBetween(PedidoProducto::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdBeginsWith($integer) {
		return $this->andBeginsWith(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdEndsWith($integer) {
		return $this->andEndsWith(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andVentaIdContains($integer) {
		return $this->andContains(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaId($integer) {
		return $this->or(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdNot($integer) {
		return $this->orNot(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdLike($integer) {
		return $this->orLike(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdNotLike($integer) {
		return $this->orNotLike(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdGreater($integer) {
		return $this->orGreater(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdLess($integer) {
		return $this->orLess(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdLessEqual($integer) {
		return $this->orLessEqual(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdNull() {
		return $this->orNull(PedidoProducto::VENTA_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdNotNull() {
		return $this->orNotNull(PedidoProducto::VENTA_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdBetween($integer, $from, $to) {
		return $this->orBetween(PedidoProducto::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdBeginsWith($integer) {
		return $this->orBeginsWith(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdEndsWith($integer) {
		return $this->orEndsWith(PedidoProducto::VENTA_ID, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orVentaIdContains($integer) {
		return $this->orContains(PedidoProducto::VENTA_ID, $integer);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByVentaIdAsc() {
		return $this->orderBy(PedidoProducto::VENTA_ID, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByVentaIdDesc() {
		return $this->orderBy(PedidoProducto::VENTA_ID, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByVentaId() {
		return $this->groupBy(PedidoProducto::VENTA_ID);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidad($integer) {
		return $this->addAnd(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadNot($integer) {
		return $this->andNot(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadLike($integer) {
		return $this->andLike(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadNotLike($integer) {
		return $this->andNotLike(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadGreater($integer) {
		return $this->andGreater(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadGreaterEqual($integer) {
		return $this->andGreaterEqual(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadLess($integer) {
		return $this->andLess(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadLessEqual($integer) {
		return $this->andLessEqual(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadNull() {
		return $this->andNull(PedidoProducto::CANTIDAD);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadNotNull() {
		return $this->andNotNull(PedidoProducto::CANTIDAD);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadBetween($integer, $from, $to) {
		return $this->andBetween(PedidoProducto::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadBeginsWith($integer) {
		return $this->andBeginsWith(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadEndsWith($integer) {
		return $this->andEndsWith(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCantidadContains($integer) {
		return $this->andContains(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidad($integer) {
		return $this->or(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadNot($integer) {
		return $this->orNot(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadLike($integer) {
		return $this->orLike(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadNotLike($integer) {
		return $this->orNotLike(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadGreater($integer) {
		return $this->orGreater(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadGreaterEqual($integer) {
		return $this->orGreaterEqual(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadLess($integer) {
		return $this->orLess(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadLessEqual($integer) {
		return $this->orLessEqual(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadNull() {
		return $this->orNull(PedidoProducto::CANTIDAD);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadNotNull() {
		return $this->orNotNull(PedidoProducto::CANTIDAD);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadBetween($integer, $from, $to) {
		return $this->orBetween(PedidoProducto::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadBeginsWith($integer) {
		return $this->orBeginsWith(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadEndsWith($integer) {
		return $this->orEndsWith(PedidoProducto::CANTIDAD, $integer);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCantidadContains($integer) {
		return $this->orContains(PedidoProducto::CANTIDAD, $integer);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByCantidadAsc() {
		return $this->orderBy(PedidoProducto::CANTIDAD, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByCantidadDesc() {
		return $this->orderBy(PedidoProducto::CANTIDAD, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByCantidad() {
		return $this->groupBy(PedidoProducto::CANTIDAD);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecio($float) {
		return $this->addAnd(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioNot($float) {
		return $this->andNot(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioLike($float) {
		return $this->andLike(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioNotLike($float) {
		return $this->andNotLike(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioGreater($float) {
		return $this->andGreater(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioGreaterEqual($float) {
		return $this->andGreaterEqual(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioLess($float) {
		return $this->andLess(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioLessEqual($float) {
		return $this->andLessEqual(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioNull() {
		return $this->andNull(PedidoProducto::PRECIO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(PedidoProducto::PRECIO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioBetween($float, $from, $to) {
		return $this->andBetween(PedidoProducto::PRECIO, $float, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioBeginsWith($float) {
		return $this->andBeginsWith(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioEndsWith($float) {
		return $this->andEndsWith(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andPrecioContains($float) {
		return $this->andContains(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecio($float) {
		return $this->or(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioNot($float) {
		return $this->orNot(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioLike($float) {
		return $this->orLike(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioNotLike($float) {
		return $this->orNotLike(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioGreater($float) {
		return $this->orGreater(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioGreaterEqual($float) {
		return $this->orGreaterEqual(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioLess($float) {
		return $this->orLess(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioLessEqual($float) {
		return $this->orLessEqual(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioNull() {
		return $this->orNull(PedidoProducto::PRECIO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(PedidoProducto::PRECIO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioBetween($float, $from, $to) {
		return $this->orBetween(PedidoProducto::PRECIO, $float, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioBeginsWith($float) {
		return $this->orBeginsWith(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioEndsWith($float) {
		return $this->orEndsWith(PedidoProducto::PRECIO, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orPrecioContains($float) {
		return $this->orContains(PedidoProducto::PRECIO, $float);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(PedidoProducto::PRECIO, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(PedidoProducto::PRECIO, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(PedidoProducto::PRECIO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotal($float) {
		return $this->addAnd(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalNot($float) {
		return $this->andNot(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalLike($float) {
		return $this->andLike(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalNotLike($float) {
		return $this->andNotLike(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalGreater($float) {
		return $this->andGreater(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalGreaterEqual($float) {
		return $this->andGreaterEqual(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalLess($float) {
		return $this->andLess(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalLessEqual($float) {
		return $this->andLessEqual(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalNull() {
		return $this->andNull(PedidoProducto::TOTAL);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalNotNull() {
		return $this->andNotNull(PedidoProducto::TOTAL);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalBetween($float, $from, $to) {
		return $this->andBetween(PedidoProducto::TOTAL, $float, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalBeginsWith($float) {
		return $this->andBeginsWith(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalEndsWith($float) {
		return $this->andEndsWith(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTotalContains($float) {
		return $this->andContains(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotal($float) {
		return $this->or(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalNot($float) {
		return $this->orNot(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalLike($float) {
		return $this->orLike(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalNotLike($float) {
		return $this->orNotLike(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalGreater($float) {
		return $this->orGreater(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalGreaterEqual($float) {
		return $this->orGreaterEqual(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalLess($float) {
		return $this->orLess(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalLessEqual($float) {
		return $this->orLessEqual(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalNull() {
		return $this->orNull(PedidoProducto::TOTAL);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalNotNull() {
		return $this->orNotNull(PedidoProducto::TOTAL);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalBetween($float, $from, $to) {
		return $this->orBetween(PedidoProducto::TOTAL, $float, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalBeginsWith($float) {
		return $this->orBeginsWith(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalEndsWith($float) {
		return $this->orEndsWith(PedidoProducto::TOTAL, $float);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTotalContains($float) {
		return $this->orContains(PedidoProducto::TOTAL, $float);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByTotalAsc() {
		return $this->orderBy(PedidoProducto::TOTAL, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByTotalDesc() {
		return $this->orderBy(PedidoProducto::TOTAL, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByTotal() {
		return $this->groupBy(PedidoProducto::TOTAL);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(PedidoProducto::CREATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(PedidoProducto::CREATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(PedidoProducto::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(PedidoProducto::CREATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(PedidoProducto::CREATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(PedidoProducto::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(PedidoProducto::CREATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(PedidoProducto::CREATED_AT, $timestamp);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(PedidoProducto::CREATED_AT, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(PedidoProducto::CREATED_AT, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(PedidoProducto::CREATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(PedidoProducto::UPDATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(PedidoProducto::UPDATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(PedidoProducto::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(PedidoProducto::UPDATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(PedidoProducto::UPDATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(PedidoProducto::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(PedidoProducto::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(PedidoProducto::UPDATED_AT, $timestamp);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(PedidoProducto::UPDATED_AT, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(PedidoProducto::UPDATED_AT, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(PedidoProducto::UPDATED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(PedidoProducto::DELETED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(PedidoProducto::DELETED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(PedidoProducto::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(PedidoProducto::DELETED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(PedidoProducto::DELETED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(PedidoProducto::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(PedidoProducto::DELETED_AT, $timestamp);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(PedidoProducto::DELETED_AT, $timestamp);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(PedidoProducto::DELETED_AT, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(PedidoProducto::DELETED_AT, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(PedidoProducto::DELETED_AT);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEspera($varchar) {
		return $this->addAnd(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaNot($varchar) {
		return $this->andNot(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaLike($varchar) {
		return $this->andLike(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaNotLike($varchar) {
		return $this->andNotLike(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaGreater($varchar) {
		return $this->andGreater(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaGreaterEqual($varchar) {
		return $this->andGreaterEqual(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaLess($varchar) {
		return $this->andLess(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaLessEqual($varchar) {
		return $this->andLessEqual(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaNull() {
		return $this->andNull(PedidoProducto::TIEMPO_ESPERA);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaNotNull() {
		return $this->andNotNull(PedidoProducto::TIEMPO_ESPERA);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaBetween($varchar, $from, $to) {
		return $this->andBetween(PedidoProducto::TIEMPO_ESPERA, $varchar, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaBeginsWith($varchar) {
		return $this->andBeginsWith(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaEndsWith($varchar) {
		return $this->andEndsWith(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andTiempoEsperaContains($varchar) {
		return $this->andContains(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEspera($varchar) {
		return $this->or(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaNot($varchar) {
		return $this->orNot(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaLike($varchar) {
		return $this->orLike(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaNotLike($varchar) {
		return $this->orNotLike(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaGreater($varchar) {
		return $this->orGreater(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaGreaterEqual($varchar) {
		return $this->orGreaterEqual(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaLess($varchar) {
		return $this->orLess(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaLessEqual($varchar) {
		return $this->orLessEqual(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaNull() {
		return $this->orNull(PedidoProducto::TIEMPO_ESPERA);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaNotNull() {
		return $this->orNotNull(PedidoProducto::TIEMPO_ESPERA);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaBetween($varchar, $from, $to) {
		return $this->orBetween(PedidoProducto::TIEMPO_ESPERA, $varchar, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaBeginsWith($varchar) {
		return $this->orBeginsWith(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaEndsWith($varchar) {
		return $this->orEndsWith(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orTiempoEsperaContains($varchar) {
		return $this->orContains(PedidoProducto::TIEMPO_ESPERA, $varchar);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByTiempoEsperaAsc() {
		return $this->orderBy(PedidoProducto::TIEMPO_ESPERA, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByTiempoEsperaDesc() {
		return $this->orderBy(PedidoProducto::TIEMPO_ESPERA, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByTiempoEspera() {
		return $this->groupBy(PedidoProducto::TIEMPO_ESPERA);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedido($boolean) {
		return $this->addAnd(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoNot($boolean) {
		return $this->andNot(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoLike($boolean) {
		return $this->andLike(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoNotLike($boolean) {
		return $this->andNotLike(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoGreater($boolean) {
		return $this->andGreater(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoGreaterEqual($boolean) {
		return $this->andGreaterEqual(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoLess($boolean) {
		return $this->andLess(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoLessEqual($boolean) {
		return $this->andLessEqual(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoNull() {
		return $this->andNull(PedidoProducto::ESTADO_PEDIDO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoNotNull() {
		return $this->andNotNull(PedidoProducto::ESTADO_PEDIDO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoBetween($boolean, $from, $to) {
		return $this->andBetween(PedidoProducto::ESTADO_PEDIDO, $boolean, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoBeginsWith($boolean) {
		return $this->andBeginsWith(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoEndsWith($boolean) {
		return $this->andEndsWith(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function andEstadoPedidoContains($boolean) {
		return $this->andContains(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedido($boolean) {
		return $this->or(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoNot($boolean) {
		return $this->orNot(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoLike($boolean) {
		return $this->orLike(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoNotLike($boolean) {
		return $this->orNotLike(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoGreater($boolean) {
		return $this->orGreater(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoGreaterEqual($boolean) {
		return $this->orGreaterEqual(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoLess($boolean) {
		return $this->orLess(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoLessEqual($boolean) {
		return $this->orLessEqual(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoNull() {
		return $this->orNull(PedidoProducto::ESTADO_PEDIDO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoNotNull() {
		return $this->orNotNull(PedidoProducto::ESTADO_PEDIDO);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoBetween($boolean, $from, $to) {
		return $this->orBetween(PedidoProducto::ESTADO_PEDIDO, $boolean, $from, $to);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoBeginsWith($boolean) {
		return $this->orBeginsWith(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoEndsWith($boolean) {
		return $this->orEndsWith(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orEstadoPedidoContains($boolean) {
		return $this->orContains(PedidoProducto::ESTADO_PEDIDO, $boolean);
	}


	/**
	 * @return PedidoProductoQuery
	 */
	function orderByEstadoPedidoAsc() {
		return $this->orderBy(PedidoProducto::ESTADO_PEDIDO, self::ASC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function orderByEstadoPedidoDesc() {
		return $this->orderBy(PedidoProducto::ESTADO_PEDIDO, self::DESC);
	}

	/**
	 * @return PedidoProductoQuery
	 */
	function groupByEstadoPedido() {
		return $this->groupBy(PedidoProducto::ESTADO_PEDIDO);
	}


}