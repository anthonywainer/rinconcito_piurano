<?php

use Dabl\Query\Query;

abstract class baseVouchersQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Vouchers::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VouchersQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VouchersQuery($table_name, $alias);
	}

	/**
	 * @return Vouchers[]
	 */
	function select() {
		return Vouchers::doSelect($this);
	}

	/**
	 * @return Vouchers
	 */
	function selectOne() {
		return Vouchers::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Vouchers::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Vouchers::doCount($this);
	}

	/**
	 * @return VouchersQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Vouchers::isTemporalType($type)) {
			$value = Vouchers::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Vouchers::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VouchersQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Vouchers::isTemporalType($type)) {
			$value = Vouchers::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Vouchers::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VouchersQuery
	 */
	function andId($integer) {
		return $this->addAnd(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdNull() {
		return $this->andNull(Vouchers::ID);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Vouchers::ID);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Vouchers::ID, $integer, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orId($integer) {
		return $this->or(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdNull() {
		return $this->orNull(Vouchers::ID);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Vouchers::ID);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Vouchers::ID, $integer, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Vouchers::ID, $integer);
	}

	/**
	 * @return VouchersQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Vouchers::ID, $integer);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Vouchers::ID, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Vouchers::ID, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupById() {
		return $this->groupBy(Vouchers::ID);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobante($varchar) {
		return $this->addAnd(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteNot($varchar) {
		return $this->andNot(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteLike($varchar) {
		return $this->andLike(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteNotLike($varchar) {
		return $this->andNotLike(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteGreater($varchar) {
		return $this->andGreater(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteGreaterEqual($varchar) {
		return $this->andGreaterEqual(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteLess($varchar) {
		return $this->andLess(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteLessEqual($varchar) {
		return $this->andLessEqual(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteNull() {
		return $this->andNull(Vouchers::TIPO_COMPROBANTE);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteNotNull() {
		return $this->andNotNull(Vouchers::TIPO_COMPROBANTE);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteBetween($varchar, $from, $to) {
		return $this->andBetween(Vouchers::TIPO_COMPROBANTE, $varchar, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteBeginsWith($varchar) {
		return $this->andBeginsWith(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteEndsWith($varchar) {
		return $this->andEndsWith(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function andTipoComprobanteContains($varchar) {
		return $this->andContains(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobante($varchar) {
		return $this->or(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteNot($varchar) {
		return $this->orNot(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteLike($varchar) {
		return $this->orLike(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteNotLike($varchar) {
		return $this->orNotLike(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteGreater($varchar) {
		return $this->orGreater(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteGreaterEqual($varchar) {
		return $this->orGreaterEqual(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteLess($varchar) {
		return $this->orLess(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteLessEqual($varchar) {
		return $this->orLessEqual(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteNull() {
		return $this->orNull(Vouchers::TIPO_COMPROBANTE);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteNotNull() {
		return $this->orNotNull(Vouchers::TIPO_COMPROBANTE);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteBetween($varchar, $from, $to) {
		return $this->orBetween(Vouchers::TIPO_COMPROBANTE, $varchar, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteBeginsWith($varchar) {
		return $this->orBeginsWith(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteEndsWith($varchar) {
		return $this->orEndsWith(Vouchers::TIPO_COMPROBANTE, $varchar);
	}

	/**
	 * @return VouchersQuery
	 */
	function orTipoComprobanteContains($varchar) {
		return $this->orContains(Vouchers::TIPO_COMPROBANTE, $varchar);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByTipoComprobanteAsc() {
		return $this->orderBy(Vouchers::TIPO_COMPROBANTE, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByTipoComprobanteDesc() {
		return $this->orderBy(Vouchers::TIPO_COMPROBANTE, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupByTipoComprobante() {
		return $this->groupBy(Vouchers::TIPO_COMPROBANTE);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFecha($date) {
		return $this->addAnd(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaNot($date) {
		return $this->andNot(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaLike($date) {
		return $this->andLike(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaNotLike($date) {
		return $this->andNotLike(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaGreater($date) {
		return $this->andGreater(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaGreaterEqual($date) {
		return $this->andGreaterEqual(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaLess($date) {
		return $this->andLess(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaLessEqual($date) {
		return $this->andLessEqual(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaNull() {
		return $this->andNull(Vouchers::FECHA);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(Vouchers::FECHA);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaBetween($date, $from, $to) {
		return $this->andBetween(Vouchers::FECHA, $date, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaBeginsWith($date) {
		return $this->andBeginsWith(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaEndsWith($date) {
		return $this->andEndsWith(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function andFechaContains($date) {
		return $this->andContains(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFecha($date) {
		return $this->or(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaNot($date) {
		return $this->orNot(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaLike($date) {
		return $this->orLike(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaNotLike($date) {
		return $this->orNotLike(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaGreater($date) {
		return $this->orGreater(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaGreaterEqual($date) {
		return $this->orGreaterEqual(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaLess($date) {
		return $this->orLess(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaLessEqual($date) {
		return $this->orLessEqual(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaNull() {
		return $this->orNull(Vouchers::FECHA);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(Vouchers::FECHA);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaBetween($date, $from, $to) {
		return $this->orBetween(Vouchers::FECHA, $date, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaBeginsWith($date) {
		return $this->orBeginsWith(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaEndsWith($date) {
		return $this->orEndsWith(Vouchers::FECHA, $date);
	}

	/**
	 * @return VouchersQuery
	 */
	function orFechaContains($date) {
		return $this->orContains(Vouchers::FECHA, $date);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(Vouchers::FECHA, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(Vouchers::FECHA, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupByFecha() {
		return $this->groupBy(Vouchers::FECHA);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Vouchers::CREATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Vouchers::CREATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Vouchers::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Vouchers::CREATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Vouchers::CREATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Vouchers::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Vouchers::CREATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Vouchers::CREATED_AT, $timestamp);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Vouchers::CREATED_AT, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Vouchers::CREATED_AT, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Vouchers::CREATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Vouchers::UPDATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Vouchers::UPDATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Vouchers::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Vouchers::UPDATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Vouchers::UPDATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Vouchers::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Vouchers::UPDATED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Vouchers::UPDATED_AT, $timestamp);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Vouchers::UPDATED_AT, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Vouchers::UPDATED_AT, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Vouchers::UPDATED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Vouchers::DELETED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Vouchers::DELETED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Vouchers::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Vouchers::DELETED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Vouchers::DELETED_AT);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Vouchers::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Vouchers::DELETED_AT, $timestamp);
	}

	/**
	 * @return VouchersQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Vouchers::DELETED_AT, $timestamp);
	}


	/**
	 * @return VouchersQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Vouchers::DELETED_AT, self::ASC);
	}

	/**
	 * @return VouchersQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Vouchers::DELETED_AT, self::DESC);
	}

	/**
	 * @return VouchersQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Vouchers::DELETED_AT);
	}


}