<?php

use Dabl\Query\Query;

abstract class baseVentasMesasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = VentasMesas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VentasMesasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VentasMesasQuery($table_name, $alias);
	}

	/**
	 * @return VentasMesas[]
	 */
	function select() {
		return VentasMesas::doSelect($this);
	}

	/**
	 * @return VentasMesas
	 */
	function selectOne() {
		return VentasMesas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return VentasMesas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return VentasMesas::doCount($this);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasMesas::isTemporalType($type)) {
			$value = VentasMesas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasMesas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasMesas::isTemporalType($type)) {
			$value = VentasMesas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasMesas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andId($integer) {
		return $this->addAnd(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdNull() {
		return $this->andNull(VentasMesas::ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(VentasMesas::ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMesas::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orId($integer) {
		return $this->or(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdNull() {
		return $this->orNull(VentasMesas::ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(VentasMesas::ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMesas::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(VentasMesas::ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(VentasMesas::ID, $integer);
	}


	/**
	 * @return VentasMesasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(VentasMesas::ID, self::ASC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(VentasMesas::ID, self::DESC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function groupById() {
		return $this->groupBy(VentasMesas::ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaId($integer) {
		return $this->addAnd(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdNot($integer) {
		return $this->andNot(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdLike($integer) {
		return $this->andLike(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdNotLike($integer) {
		return $this->andNotLike(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdGreater($integer) {
		return $this->andGreater(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdLess($integer) {
		return $this->andLess(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdLessEqual($integer) {
		return $this->andLessEqual(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdNull() {
		return $this->andNull(VentasMesas::VENTA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdNotNull() {
		return $this->andNotNull(VentasMesas::VENTA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMesas::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdEndsWith($integer) {
		return $this->andEndsWith(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andVentaIdContains($integer) {
		return $this->andContains(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaId($integer) {
		return $this->or(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdNot($integer) {
		return $this->orNot(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdLike($integer) {
		return $this->orLike(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdNotLike($integer) {
		return $this->orNotLike(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdGreater($integer) {
		return $this->orGreater(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdLess($integer) {
		return $this->orLess(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdLessEqual($integer) {
		return $this->orLessEqual(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdNull() {
		return $this->orNull(VentasMesas::VENTA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdNotNull() {
		return $this->orNotNull(VentasMesas::VENTA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMesas::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdEndsWith($integer) {
		return $this->orEndsWith(VentasMesas::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orVentaIdContains($integer) {
		return $this->orContains(VentasMesas::VENTA_ID, $integer);
	}


	/**
	 * @return VentasMesasQuery
	 */
	function orderByVentaIdAsc() {
		return $this->orderBy(VentasMesas::VENTA_ID, self::ASC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orderByVentaIdDesc() {
		return $this->orderBy(VentasMesas::VENTA_ID, self::DESC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function groupByVentaId() {
		return $this->groupBy(VentasMesas::VENTA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaId($integer) {
		return $this->addAnd(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdNot($integer) {
		return $this->andNot(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdLike($integer) {
		return $this->andLike(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdNotLike($integer) {
		return $this->andNotLike(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdGreater($integer) {
		return $this->andGreater(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdLess($integer) {
		return $this->andLess(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdLessEqual($integer) {
		return $this->andLessEqual(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdNull() {
		return $this->andNull(VentasMesas::MESA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdNotNull() {
		return $this->andNotNull(VentasMesas::MESA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMesas::MESA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdEndsWith($integer) {
		return $this->andEndsWith(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andMesaIdContains($integer) {
		return $this->andContains(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaId($integer) {
		return $this->or(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdNot($integer) {
		return $this->orNot(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdLike($integer) {
		return $this->orLike(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdNotLike($integer) {
		return $this->orNotLike(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdGreater($integer) {
		return $this->orGreater(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdLess($integer) {
		return $this->orLess(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdLessEqual($integer) {
		return $this->orLessEqual(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdNull() {
		return $this->orNull(VentasMesas::MESA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdNotNull() {
		return $this->orNotNull(VentasMesas::MESA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMesas::MESA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdEndsWith($integer) {
		return $this->orEndsWith(VentasMesas::MESA_ID, $integer);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orMesaIdContains($integer) {
		return $this->orContains(VentasMesas::MESA_ID, $integer);
	}


	/**
	 * @return VentasMesasQuery
	 */
	function orderByMesaIdAsc() {
		return $this->orderBy(VentasMesas::MESA_ID, self::ASC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orderByMesaIdDesc() {
		return $this->orderBy(VentasMesas::MESA_ID, self::DESC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function groupByMesaId() {
		return $this->groupBy(VentasMesas::MESA_ID);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesa($boolean) {
		return $this->addAnd(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaNot($boolean) {
		return $this->andNot(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaLike($boolean) {
		return $this->andLike(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaNotLike($boolean) {
		return $this->andNotLike(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaGreater($boolean) {
		return $this->andGreater(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaGreaterEqual($boolean) {
		return $this->andGreaterEqual(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaLess($boolean) {
		return $this->andLess(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaLessEqual($boolean) {
		return $this->andLessEqual(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaNull() {
		return $this->andNull(VentasMesas::ESTADO_VENTA_MESA);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaNotNull() {
		return $this->andNotNull(VentasMesas::ESTADO_VENTA_MESA);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaBetween($boolean, $from, $to) {
		return $this->andBetween(VentasMesas::ESTADO_VENTA_MESA, $boolean, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaBeginsWith($boolean) {
		return $this->andBeginsWith(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaEndsWith($boolean) {
		return $this->andEndsWith(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function andEstadoVentaMesaContains($boolean) {
		return $this->andContains(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesa($boolean) {
		return $this->or(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaNot($boolean) {
		return $this->orNot(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaLike($boolean) {
		return $this->orLike(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaNotLike($boolean) {
		return $this->orNotLike(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaGreater($boolean) {
		return $this->orGreater(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaGreaterEqual($boolean) {
		return $this->orGreaterEqual(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaLess($boolean) {
		return $this->orLess(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaLessEqual($boolean) {
		return $this->orLessEqual(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaNull() {
		return $this->orNull(VentasMesas::ESTADO_VENTA_MESA);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaNotNull() {
		return $this->orNotNull(VentasMesas::ESTADO_VENTA_MESA);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaBetween($boolean, $from, $to) {
		return $this->orBetween(VentasMesas::ESTADO_VENTA_MESA, $boolean, $from, $to);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaBeginsWith($boolean) {
		return $this->orBeginsWith(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaEndsWith($boolean) {
		return $this->orEndsWith(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orEstadoVentaMesaContains($boolean) {
		return $this->orContains(VentasMesas::ESTADO_VENTA_MESA, $boolean);
	}


	/**
	 * @return VentasMesasQuery
	 */
	function orderByEstadoVentaMesaAsc() {
		return $this->orderBy(VentasMesas::ESTADO_VENTA_MESA, self::ASC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function orderByEstadoVentaMesaDesc() {
		return $this->orderBy(VentasMesas::ESTADO_VENTA_MESA, self::DESC);
	}

	/**
	 * @return VentasMesasQuery
	 */
	function groupByEstadoVentaMesa() {
		return $this->groupBy(VentasMesas::ESTADO_VENTA_MESA);
	}


}