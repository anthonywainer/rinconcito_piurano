<?php

use Dabl\Query\Query;

abstract class baseSeriesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Series::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return SeriesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new SeriesQuery($table_name, $alias);
	}

	/**
	 * @return Series[]
	 */
	function select() {
		return Series::doSelect($this);
	}

	/**
	 * @return Series
	 */
	function selectOne() {
		return Series::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Series::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Series::doCount($this);
	}

	/**
	 * @return SeriesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Series::isTemporalType($type)) {
			$value = Series::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Series::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return SeriesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Series::isTemporalType($type)) {
			$value = Series::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Series::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return SeriesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdNull() {
		return $this->andNull(Series::ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Series::ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Series::ID, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orId($integer) {
		return $this->or(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdNull() {
		return $this->orNull(Series::ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Series::ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Series::ID, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Series::ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Series::ID, $integer);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Series::ID, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Series::ID, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupById() {
		return $this->groupBy(Series::ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerie($integer) {
		return $this->addAnd(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieNot($integer) {
		return $this->andNot(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieLike($integer) {
		return $this->andLike(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieNotLike($integer) {
		return $this->andNotLike(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieGreater($integer) {
		return $this->andGreater(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieGreaterEqual($integer) {
		return $this->andGreaterEqual(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieLess($integer) {
		return $this->andLess(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieLessEqual($integer) {
		return $this->andLessEqual(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieNull() {
		return $this->andNull(Series::SERIE);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieNotNull() {
		return $this->andNotNull(Series::SERIE);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieBetween($integer, $from, $to) {
		return $this->andBetween(Series::SERIE, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieBeginsWith($integer) {
		return $this->andBeginsWith(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieEndsWith($integer) {
		return $this->andEndsWith(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andSerieContains($integer) {
		return $this->andContains(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerie($integer) {
		return $this->or(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieNot($integer) {
		return $this->orNot(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieLike($integer) {
		return $this->orLike(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieNotLike($integer) {
		return $this->orNotLike(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieGreater($integer) {
		return $this->orGreater(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieGreaterEqual($integer) {
		return $this->orGreaterEqual(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieLess($integer) {
		return $this->orLess(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieLessEqual($integer) {
		return $this->orLessEqual(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieNull() {
		return $this->orNull(Series::SERIE);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieNotNull() {
		return $this->orNotNull(Series::SERIE);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieBetween($integer, $from, $to) {
		return $this->orBetween(Series::SERIE, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieBeginsWith($integer) {
		return $this->orBeginsWith(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieEndsWith($integer) {
		return $this->orEndsWith(Series::SERIE, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orSerieContains($integer) {
		return $this->orContains(Series::SERIE, $integer);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderBySerieAsc() {
		return $this->orderBy(Series::SERIE, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderBySerieDesc() {
		return $this->orderBy(Series::SERIE, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupBySerie() {
		return $this->groupBy(Series::SERIE);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativo($integer) {
		return $this->addAnd(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoNot($integer) {
		return $this->andNot(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoLike($integer) {
		return $this->andLike(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoNotLike($integer) {
		return $this->andNotLike(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoGreater($integer) {
		return $this->andGreater(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoGreaterEqual($integer) {
		return $this->andGreaterEqual(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoLess($integer) {
		return $this->andLess(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoLessEqual($integer) {
		return $this->andLessEqual(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoNull() {
		return $this->andNull(Series::CORRELATIVO);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoNotNull() {
		return $this->andNotNull(Series::CORRELATIVO);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoBetween($integer, $from, $to) {
		return $this->andBetween(Series::CORRELATIVO, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoBeginsWith($integer) {
		return $this->andBeginsWith(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoEndsWith($integer) {
		return $this->andEndsWith(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCorrelativoContains($integer) {
		return $this->andContains(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativo($integer) {
		return $this->or(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoNot($integer) {
		return $this->orNot(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoLike($integer) {
		return $this->orLike(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoNotLike($integer) {
		return $this->orNotLike(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoGreater($integer) {
		return $this->orGreater(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoGreaterEqual($integer) {
		return $this->orGreaterEqual(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoLess($integer) {
		return $this->orLess(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoLessEqual($integer) {
		return $this->orLessEqual(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoNull() {
		return $this->orNull(Series::CORRELATIVO);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoNotNull() {
		return $this->orNotNull(Series::CORRELATIVO);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoBetween($integer, $from, $to) {
		return $this->orBetween(Series::CORRELATIVO, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoBeginsWith($integer) {
		return $this->orBeginsWith(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoEndsWith($integer) {
		return $this->orEndsWith(Series::CORRELATIVO, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCorrelativoContains($integer) {
		return $this->orContains(Series::CORRELATIVO, $integer);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByCorrelativoAsc() {
		return $this->orderBy(Series::CORRELATIVO, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByCorrelativoDesc() {
		return $this->orderBy(Series::CORRELATIVO, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupByCorrelativo() {
		return $this->groupBy(Series::CORRELATIVO);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherId($integer) {
		return $this->addAnd(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdNot($integer) {
		return $this->andNot(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdLike($integer) {
		return $this->andLike(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdNotLike($integer) {
		return $this->andNotLike(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdGreater($integer) {
		return $this->andGreater(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdLess($integer) {
		return $this->andLess(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdLessEqual($integer) {
		return $this->andLessEqual(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdNull() {
		return $this->andNull(Series::VOUCHER_ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdNotNull() {
		return $this->andNotNull(Series::VOUCHER_ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdBetween($integer, $from, $to) {
		return $this->andBetween(Series::VOUCHER_ID, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdBeginsWith($integer) {
		return $this->andBeginsWith(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdEndsWith($integer) {
		return $this->andEndsWith(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function andVoucherIdContains($integer) {
		return $this->andContains(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherId($integer) {
		return $this->or(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdNot($integer) {
		return $this->orNot(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdLike($integer) {
		return $this->orLike(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdNotLike($integer) {
		return $this->orNotLike(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdGreater($integer) {
		return $this->orGreater(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdLess($integer) {
		return $this->orLess(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdLessEqual($integer) {
		return $this->orLessEqual(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdNull() {
		return $this->orNull(Series::VOUCHER_ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdNotNull() {
		return $this->orNotNull(Series::VOUCHER_ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdBetween($integer, $from, $to) {
		return $this->orBetween(Series::VOUCHER_ID, $integer, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdBeginsWith($integer) {
		return $this->orBeginsWith(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdEndsWith($integer) {
		return $this->orEndsWith(Series::VOUCHER_ID, $integer);
	}

	/**
	 * @return SeriesQuery
	 */
	function orVoucherIdContains($integer) {
		return $this->orContains(Series::VOUCHER_ID, $integer);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByVoucherIdAsc() {
		return $this->orderBy(Series::VOUCHER_ID, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByVoucherIdDesc() {
		return $this->orderBy(Series::VOUCHER_ID, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupByVoucherId() {
		return $this->groupBy(Series::VOUCHER_ID);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Series::CREATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Series::CREATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Series::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Series::CREATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Series::CREATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Series::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Series::CREATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Series::CREATED_AT, $timestamp);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Series::CREATED_AT, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Series::CREATED_AT, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Series::CREATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Series::UPDATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Series::UPDATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Series::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Series::UPDATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Series::UPDATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Series::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Series::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Series::UPDATED_AT, $timestamp);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Series::UPDATED_AT, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Series::UPDATED_AT, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Series::UPDATED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Series::DELETED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Series::DELETED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Series::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Series::DELETED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Series::DELETED_AT);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Series::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Series::DELETED_AT, $timestamp);
	}

	/**
	 * @return SeriesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Series::DELETED_AT, $timestamp);
	}


	/**
	 * @return SeriesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Series::DELETED_AT, self::ASC);
	}

	/**
	 * @return SeriesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Series::DELETED_AT, self::DESC);
	}

	/**
	 * @return SeriesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Series::DELETED_AT);
	}


}