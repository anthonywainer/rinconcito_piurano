<?php

use Dabl\Query\Query;

abstract class baseMovimientosDeDineroQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = MovimientosDeDinero::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MovimientosDeDineroQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MovimientosDeDineroQuery($table_name, $alias);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	function select() {
		return MovimientosDeDinero::doSelect($this);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function selectOne() {
		return MovimientosDeDinero::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return MovimientosDeDinero::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return MovimientosDeDinero::doCount($this);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MovimientosDeDinero::isTemporalType($type)) {
			$value = MovimientosDeDinero::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MovimientosDeDinero::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MovimientosDeDinero::isTemporalType($type)) {
			$value = MovimientosDeDinero::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MovimientosDeDinero::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andId($integer) {
		return $this->addAnd(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNull() {
		return $this->andNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orId($integer) {
		return $this->or(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNull() {
		return $this->orNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(MovimientosDeDinero::ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(MovimientosDeDinero::ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupById() {
		return $this->groupBy(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucher($integer) {
		return $this->addAnd(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherNot($integer) {
		return $this->andNot(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherLike($integer) {
		return $this->andLike(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherLess($integer) {
		return $this->andLess(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherNull() {
		return $this->andNull(MovimientosDeDinero::NUMERO_VOUCHER);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherNotNull() {
		return $this->andNotNull(MovimientosDeDinero::NUMERO_VOUCHER);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::NUMERO_VOUCHER, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroVoucherContains($integer) {
		return $this->andContains(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucher($integer) {
		return $this->or(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherNot($integer) {
		return $this->orNot(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherLike($integer) {
		return $this->orLike(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherLess($integer) {
		return $this->orLess(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherNull() {
		return $this->orNull(MovimientosDeDinero::NUMERO_VOUCHER);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherNotNull() {
		return $this->orNotNull(MovimientosDeDinero::NUMERO_VOUCHER);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::NUMERO_VOUCHER, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroVoucherContains($integer) {
		return $this->orContains(MovimientosDeDinero::NUMERO_VOUCHER, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByNumeroVoucherAsc() {
		return $this->orderBy(MovimientosDeDinero::NUMERO_VOUCHER, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByNumeroVoucherDesc() {
		return $this->orderBy(MovimientosDeDinero::NUMERO_VOUCHER, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByNumeroVoucher() {
		return $this->groupBy(MovimientosDeDinero::NUMERO_VOUCHER);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherId($integer) {
		return $this->addAnd(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdNull() {
		return $this->andNull(MovimientosDeDinero::VOUCHER_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::VOUCHER_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::VOUCHER_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andVoucherIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherId($integer) {
		return $this->or(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdNull() {
		return $this->orNull(MovimientosDeDinero::VOUCHER_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::VOUCHER_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::VOUCHER_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::VOUCHER_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orVoucherIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::VOUCHER_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByVoucherIdAsc() {
		return $this->orderBy(MovimientosDeDinero::VOUCHER_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByVoucherIdDesc() {
		return $this->orderBy(MovimientosDeDinero::VOUCHER_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByVoucherId() {
		return $this->groupBy(MovimientosDeDinero::VOUCHER_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoId($integer) {
		return $this->addAnd(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdNull() {
		return $this->andNull(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoDePagoIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoId($integer) {
		return $this->or(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdNull() {
		return $this->orNull(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoDePagoIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByConceptoDePagoIdAsc() {
		return $this->orderBy(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByConceptoDePagoIdDesc() {
		return $this->orderBy(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByConceptoDePagoId() {
		return $this->groupBy(MovimientosDeDinero::CONCEPTO_DE_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaId($integer) {
		return $this->addAnd(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNull() {
		return $this->andNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CAJA_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaId($integer) {
		return $this->or(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNull() {
		return $this->orNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CAJA_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::CAJA_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCajaIdAsc() {
		return $this->orderBy(MovimientosDeDinero::CAJA_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCajaIdDesc() {
		return $this->orderBy(MovimientosDeDinero::CAJA_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByCajaId() {
		return $this->groupBy(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioId($integer) {
		return $this->addAnd(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNull() {
		return $this->andNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioId($integer) {
		return $this->or(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNull() {
		return $this->orNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::USUARIO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUsuarioIdAsc() {
		return $this->orderBy(MovimientosDeDinero::USUARIO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUsuarioIdDesc() {
		return $this->orderBy(MovimientosDeDinero::USUARIO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByUsuarioId() {
		return $this->groupBy(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoId($integer) {
		return $this->addAnd(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNull() {
		return $this->andNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::FORMA_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoId($integer) {
		return $this->or(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNull() {
		return $this->orNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::FORMA_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFormaPagoIdAsc() {
		return $this->orderBy(MovimientosDeDinero::FORMA_PAGO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFormaPagoIdDesc() {
		return $this->orderBy(MovimientosDeDinero::FORMA_PAGO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByFormaPagoId() {
		return $this->groupBy(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::CREATED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::CREATED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::CREATED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::UPDATED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::UPDATED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::DELETED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::DELETED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::DELETED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(MovimientosDeDinero::DELETED_AT);
	}


}