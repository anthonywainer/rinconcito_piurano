<?php

use Dabl\Query\Query;

abstract class baseTiposConceptoPagoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = TiposConceptoPago::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TiposConceptoPagoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TiposConceptoPagoQuery($table_name, $alias);
	}

	/**
	 * @return TiposConceptoPago[]
	 */
	function select() {
		return TiposConceptoPago::doSelect($this);
	}

	/**
	 * @return TiposConceptoPago
	 */
	function selectOne() {
		return TiposConceptoPago::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return TiposConceptoPago::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return TiposConceptoPago::doCount($this);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposConceptoPago::isTemporalType($type)) {
			$value = TiposConceptoPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposConceptoPago::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposConceptoPago::isTemporalType($type)) {
			$value = TiposConceptoPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposConceptoPago::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andId($integer) {
		return $this->addAnd(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdNull() {
		return $this->andNull(TiposConceptoPago::ID);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(TiposConceptoPago::ID);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(TiposConceptoPago::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orId($integer) {
		return $this->or(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdNull() {
		return $this->orNull(TiposConceptoPago::ID);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(TiposConceptoPago::ID);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(TiposConceptoPago::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(TiposConceptoPago::ID, $integer);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(TiposConceptoPago::ID, $integer);
	}


	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(TiposConceptoPago::ID, self::ASC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(TiposConceptoPago::ID, self::DESC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function groupById() {
		return $this->groupBy(TiposConceptoPago::ID);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(TiposConceptoPago::DESCRIPCION);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(TiposConceptoPago::DESCRIPCION);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(TiposConceptoPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(TiposConceptoPago::DESCRIPCION);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(TiposConceptoPago::DESCRIPCION);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(TiposConceptoPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(TiposConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(TiposConceptoPago::DESCRIPCION, $varchar);
	}


	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(TiposConceptoPago::DESCRIPCION, self::ASC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(TiposConceptoPago::DESCRIPCION, self::DESC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(TiposConceptoPago::DESCRIPCION);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(TiposConceptoPago::CREATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(TiposConceptoPago::CREATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposConceptoPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(TiposConceptoPago::CREATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(TiposConceptoPago::CREATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposConceptoPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(TiposConceptoPago::CREATED_AT, $timestamp);
	}


	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(TiposConceptoPago::CREATED_AT, self::ASC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(TiposConceptoPago::CREATED_AT, self::DESC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(TiposConceptoPago::CREATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(TiposConceptoPago::UPDATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(TiposConceptoPago::UPDATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposConceptoPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(TiposConceptoPago::UPDATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(TiposConceptoPago::UPDATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposConceptoPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(TiposConceptoPago::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(TiposConceptoPago::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(TiposConceptoPago::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(TiposConceptoPago::UPDATED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(TiposConceptoPago::DELETED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(TiposConceptoPago::DELETED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposConceptoPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(TiposConceptoPago::DELETED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(TiposConceptoPago::DELETED_AT);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposConceptoPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(TiposConceptoPago::DELETED_AT, $timestamp);
	}


	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(TiposConceptoPago::DELETED_AT, self::ASC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(TiposConceptoPago::DELETED_AT, self::DESC);
	}

	/**
	 * @return TiposConceptoPagoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(TiposConceptoPago::DELETED_AT);
	}


}