<?php

use Dabl\Query\Query;

abstract class baseFormasPagoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = FormasPago::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return FormasPagoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new FormasPagoQuery($table_name, $alias);
	}

	/**
	 * @return FormasPago[]
	 */
	function select() {
		return FormasPago::doSelect($this);
	}

	/**
	 * @return FormasPago
	 */
	function selectOne() {
		return FormasPago::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return FormasPago::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return FormasPago::doCount($this);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && FormasPago::isTemporalType($type)) {
			$value = FormasPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = FormasPago::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && FormasPago::isTemporalType($type)) {
			$value = FormasPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = FormasPago::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andId($integer) {
		return $this->addAnd(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdNull() {
		return $this->andNull(FormasPago::ID);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(FormasPago::ID);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(FormasPago::ID, $integer, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orId($integer) {
		return $this->or(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdNull() {
		return $this->orNull(FormasPago::ID);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(FormasPago::ID);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(FormasPago::ID, $integer, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(FormasPago::ID, $integer);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(FormasPago::ID, $integer);
	}


	/**
	 * @return FormasPagoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(FormasPago::ID, self::ASC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(FormasPago::ID, self::DESC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function groupById() {
		return $this->groupBy(FormasPago::ID);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(FormasPago::DESCRIPCION);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(FormasPago::DESCRIPCION);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(FormasPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(FormasPago::DESCRIPCION);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(FormasPago::DESCRIPCION);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(FormasPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(FormasPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(FormasPago::DESCRIPCION, $varchar);
	}


	/**
	 * @return FormasPagoQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(FormasPago::DESCRIPCION, self::ASC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(FormasPago::DESCRIPCION, self::DESC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(FormasPago::DESCRIPCION);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(FormasPago::CREATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(FormasPago::CREATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(FormasPago::CREATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(FormasPago::CREATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(FormasPago::CREATED_AT, $timestamp);
	}


	/**
	 * @return FormasPagoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(FormasPago::CREATED_AT, self::ASC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(FormasPago::CREATED_AT, self::DESC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(FormasPago::CREATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(FormasPago::UPDATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(FormasPago::UPDATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(FormasPago::UPDATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(FormasPago::UPDATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(FormasPago::UPDATED_AT, $timestamp);
	}


	/**
	 * @return FormasPagoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(FormasPago::UPDATED_AT, self::ASC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(FormasPago::UPDATED_AT, self::DESC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(FormasPago::UPDATED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(FormasPago::DELETED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(FormasPago::DELETED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(FormasPago::DELETED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(FormasPago::DELETED_AT);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(FormasPago::DELETED_AT, $timestamp);
	}


	/**
	 * @return FormasPagoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(FormasPago::DELETED_AT, self::ASC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(FormasPago::DELETED_AT, self::DESC);
	}

	/**
	 * @return FormasPagoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(FormasPago::DELETED_AT);
	}


}