<?php

use Dabl\Query\Query;

abstract class baseVentasMovimientoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = VentasMovimiento::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VentasMovimientoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VentasMovimientoQuery($table_name, $alias);
	}

	/**
	 * @return VentasMovimiento[]
	 */
	function select() {
		return VentasMovimiento::doSelect($this);
	}

	/**
	 * @return VentasMovimiento
	 */
	function selectOne() {
		return VentasMovimiento::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return VentasMovimiento::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return VentasMovimiento::doCount($this);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasMovimiento::isTemporalType($type)) {
			$value = VentasMovimiento::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasMovimiento::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasMovimiento::isTemporalType($type)) {
			$value = VentasMovimiento::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasMovimiento::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andId($integer) {
		return $this->addAnd(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdNull() {
		return $this->andNull(VentasMovimiento::ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(VentasMovimiento::ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orId($integer) {
		return $this->or(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdNull() {
		return $this->orNull(VentasMovimiento::ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(VentasMovimiento::ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(VentasMovimiento::ID, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(VentasMovimiento::ID, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(VentasMovimiento::ID, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupById() {
		return $this->groupBy(VentasMovimiento::ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaId($integer) {
		return $this->addAnd(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdNot($integer) {
		return $this->andNot(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdLike($integer) {
		return $this->andLike(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdGreater($integer) {
		return $this->andGreater(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdLess($integer) {
		return $this->andLess(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdNull() {
		return $this->andNull(VentasMovimiento::VENTA_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdNotNull() {
		return $this->andNotNull(VentasMovimiento::VENTA_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andVentaIdContains($integer) {
		return $this->andContains(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaId($integer) {
		return $this->or(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdNot($integer) {
		return $this->orNot(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdLike($integer) {
		return $this->orLike(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdGreater($integer) {
		return $this->orGreater(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdLess($integer) {
		return $this->orLess(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdNull() {
		return $this->orNull(VentasMovimiento::VENTA_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdNotNull() {
		return $this->orNotNull(VentasMovimiento::VENTA_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::VENTA_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::VENTA_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orVentaIdContains($integer) {
		return $this->orContains(VentasMovimiento::VENTA_ID, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByVentaIdAsc() {
		return $this->orderBy(VentasMovimiento::VENTA_ID, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByVentaIdDesc() {
		return $this->orderBy(VentasMovimiento::VENTA_ID, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByVentaId() {
		return $this->groupBy(VentasMovimiento::VENTA_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotal($float) {
		return $this->addAnd(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalNot($float) {
		return $this->andNot(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalLike($float) {
		return $this->andLike(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalNotLike($float) {
		return $this->andNotLike(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalGreater($float) {
		return $this->andGreater(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalGreaterEqual($float) {
		return $this->andGreaterEqual(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalLess($float) {
		return $this->andLess(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalLessEqual($float) {
		return $this->andLessEqual(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalNull() {
		return $this->andNull(VentasMovimiento::SUBTOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalNotNull() {
		return $this->andNotNull(VentasMovimiento::SUBTOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalBetween($float, $from, $to) {
		return $this->andBetween(VentasMovimiento::SUBTOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalBeginsWith($float) {
		return $this->andBeginsWith(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalEndsWith($float) {
		return $this->andEndsWith(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andSubtotalContains($float) {
		return $this->andContains(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotal($float) {
		return $this->or(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalNot($float) {
		return $this->orNot(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalLike($float) {
		return $this->orLike(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalNotLike($float) {
		return $this->orNotLike(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalGreater($float) {
		return $this->orGreater(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalGreaterEqual($float) {
		return $this->orGreaterEqual(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalLess($float) {
		return $this->orLess(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalLessEqual($float) {
		return $this->orLessEqual(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalNull() {
		return $this->orNull(VentasMovimiento::SUBTOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalNotNull() {
		return $this->orNotNull(VentasMovimiento::SUBTOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalBetween($float, $from, $to) {
		return $this->orBetween(VentasMovimiento::SUBTOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalBeginsWith($float) {
		return $this->orBeginsWith(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalEndsWith($float) {
		return $this->orEndsWith(VentasMovimiento::SUBTOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orSubtotalContains($float) {
		return $this->orContains(VentasMovimiento::SUBTOTAL, $float);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderBySubtotalAsc() {
		return $this->orderBy(VentasMovimiento::SUBTOTAL, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderBySubtotalDesc() {
		return $this->orderBy(VentasMovimiento::SUBTOTAL, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupBySubtotal() {
		return $this->groupBy(VentasMovimiento::SUBTOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoId($integer) {
		return $this->addAnd(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdNot($integer) {
		return $this->andNot(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdLike($integer) {
		return $this->andLike(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdGreater($integer) {
		return $this->andGreater(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdLess($integer) {
		return $this->andLess(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdNull() {
		return $this->andNull(VentasMovimiento::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdNotNull() {
		return $this->andNotNull(VentasMovimiento::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andMovimientoIdContains($integer) {
		return $this->andContains(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoId($integer) {
		return $this->or(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdNot($integer) {
		return $this->orNot(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdLike($integer) {
		return $this->orLike(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdGreater($integer) {
		return $this->orGreater(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdLess($integer) {
		return $this->orLess(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdNull() {
		return $this->orNull(VentasMovimiento::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdNotNull() {
		return $this->orNotNull(VentasMovimiento::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orMovimientoIdContains($integer) {
		return $this->orContains(VentasMovimiento::MOVIMIENTO_ID, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByMovimientoIdAsc() {
		return $this->orderBy(VentasMovimiento::MOVIMIENTO_ID, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByMovimientoIdDesc() {
		return $this->orderBy(VentasMovimiento::MOVIMIENTO_ID, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByMovimientoId() {
		return $this->groupBy(VentasMovimiento::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFecha($timestamp) {
		return $this->addAnd(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaNot($timestamp) {
		return $this->andNot(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaLike($timestamp) {
		return $this->andLike(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaNotLike($timestamp) {
		return $this->andNotLike(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaGreater($timestamp) {
		return $this->andGreater(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaLess($timestamp) {
		return $this->andLess(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaLessEqual($timestamp) {
		return $this->andLessEqual(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaNull() {
		return $this->andNull(VentasMovimiento::FECHA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(VentasMovimiento::FECHA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaBetween($timestamp, $from, $to) {
		return $this->andBetween(VentasMovimiento::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaBeginsWith($timestamp) {
		return $this->andBeginsWith(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaEndsWith($timestamp) {
		return $this->andEndsWith(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andFechaContains($timestamp) {
		return $this->andContains(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFecha($timestamp) {
		return $this->or(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaNot($timestamp) {
		return $this->orNot(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaLike($timestamp) {
		return $this->orLike(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaNotLike($timestamp) {
		return $this->orNotLike(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaGreater($timestamp) {
		return $this->orGreater(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaLess($timestamp) {
		return $this->orLess(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaLessEqual($timestamp) {
		return $this->orLessEqual(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaNull() {
		return $this->orNull(VentasMovimiento::FECHA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(VentasMovimiento::FECHA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaBetween($timestamp, $from, $to) {
		return $this->orBetween(VentasMovimiento::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaBeginsWith($timestamp) {
		return $this->orBeginsWith(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaEndsWith($timestamp) {
		return $this->orEndsWith(VentasMovimiento::FECHA, $timestamp);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orFechaContains($timestamp) {
		return $this->orContains(VentasMovimiento::FECHA, $timestamp);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(VentasMovimiento::FECHA, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(VentasMovimiento::FECHA, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByFecha() {
		return $this->groupBy(VentasMovimiento::FECHA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(VentasMovimiento::CLIENTE_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(VentasMovimiento::CLIENTE_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteId($integer) {
		return $this->or(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(VentasMovimiento::CLIENTE_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(VentasMovimiento::CLIENTE_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::CLIENTE_ID, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(VentasMovimiento::CLIENTE_ID, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(VentasMovimiento::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(VentasMovimiento::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(VentasMovimiento::CLIENTE_ID);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuento($float) {
		return $this->addAnd(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoNot($float) {
		return $this->andNot(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoLike($float) {
		return $this->andLike(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoNotLike($float) {
		return $this->andNotLike(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoGreater($float) {
		return $this->andGreater(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoGreaterEqual($float) {
		return $this->andGreaterEqual(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoLess($float) {
		return $this->andLess(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoLessEqual($float) {
		return $this->andLessEqual(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoNull() {
		return $this->andNull(VentasMovimiento::DESCUENTO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoNotNull() {
		return $this->andNotNull(VentasMovimiento::DESCUENTO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoBetween($float, $from, $to) {
		return $this->andBetween(VentasMovimiento::DESCUENTO, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoBeginsWith($float) {
		return $this->andBeginsWith(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoEndsWith($float) {
		return $this->andEndsWith(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andDescuentoContains($float) {
		return $this->andContains(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuento($float) {
		return $this->or(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoNot($float) {
		return $this->orNot(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoLike($float) {
		return $this->orLike(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoNotLike($float) {
		return $this->orNotLike(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoGreater($float) {
		return $this->orGreater(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoGreaterEqual($float) {
		return $this->orGreaterEqual(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoLess($float) {
		return $this->orLess(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoLessEqual($float) {
		return $this->orLessEqual(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoNull() {
		return $this->orNull(VentasMovimiento::DESCUENTO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoNotNull() {
		return $this->orNotNull(VentasMovimiento::DESCUENTO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoBetween($float, $from, $to) {
		return $this->orBetween(VentasMovimiento::DESCUENTO, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoBeginsWith($float) {
		return $this->orBeginsWith(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoEndsWith($float) {
		return $this->orEndsWith(VentasMovimiento::DESCUENTO, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orDescuentoContains($float) {
		return $this->orContains(VentasMovimiento::DESCUENTO, $float);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByDescuentoAsc() {
		return $this->orderBy(VentasMovimiento::DESCUENTO, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByDescuentoDesc() {
		return $this->orderBy(VentasMovimiento::DESCUENTO, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByDescuento() {
		return $this->groupBy(VentasMovimiento::DESCUENTO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentaje($integer) {
		return $this->addAnd(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeNot($integer) {
		return $this->andNot(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeLike($integer) {
		return $this->andLike(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeGreater($integer) {
		return $this->andGreater(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeLess($integer) {
		return $this->andLess(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeNull() {
		return $this->andNull(VentasMovimiento::PORCENTAJE);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeNotNull() {
		return $this->andNotNull(VentasMovimiento::PORCENTAJE);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::PORCENTAJE, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andPorcentajeContains($integer) {
		return $this->andContains(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentaje($integer) {
		return $this->or(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeNot($integer) {
		return $this->orNot(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeLike($integer) {
		return $this->orLike(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeGreater($integer) {
		return $this->orGreater(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeLess($integer) {
		return $this->orLess(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeNull() {
		return $this->orNull(VentasMovimiento::PORCENTAJE);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeNotNull() {
		return $this->orNotNull(VentasMovimiento::PORCENTAJE);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::PORCENTAJE, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::PORCENTAJE, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orPorcentajeContains($integer) {
		return $this->orContains(VentasMovimiento::PORCENTAJE, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByPorcentajeAsc() {
		return $this->orderBy(VentasMovimiento::PORCENTAJE, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByPorcentajeDesc() {
		return $this->orderBy(VentasMovimiento::PORCENTAJE, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByPorcentaje() {
		return $this->groupBy(VentasMovimiento::PORCENTAJE);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotal($float) {
		return $this->addAnd(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalNot($float) {
		return $this->andNot(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalLike($float) {
		return $this->andLike(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalNotLike($float) {
		return $this->andNotLike(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalGreater($float) {
		return $this->andGreater(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalGreaterEqual($float) {
		return $this->andGreaterEqual(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalLess($float) {
		return $this->andLess(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalLessEqual($float) {
		return $this->andLessEqual(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalNull() {
		return $this->andNull(VentasMovimiento::TOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalNotNull() {
		return $this->andNotNull(VentasMovimiento::TOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalBetween($float, $from, $to) {
		return $this->andBetween(VentasMovimiento::TOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalBeginsWith($float) {
		return $this->andBeginsWith(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalEndsWith($float) {
		return $this->andEndsWith(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andTotalContains($float) {
		return $this->andContains(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotal($float) {
		return $this->or(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalNot($float) {
		return $this->orNot(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalLike($float) {
		return $this->orLike(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalNotLike($float) {
		return $this->orNotLike(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalGreater($float) {
		return $this->orGreater(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalGreaterEqual($float) {
		return $this->orGreaterEqual(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalLess($float) {
		return $this->orLess(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalLessEqual($float) {
		return $this->orLessEqual(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalNull() {
		return $this->orNull(VentasMovimiento::TOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalNotNull() {
		return $this->orNotNull(VentasMovimiento::TOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalBetween($float, $from, $to) {
		return $this->orBetween(VentasMovimiento::TOTAL, $float, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalBeginsWith($float) {
		return $this->orBeginsWith(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalEndsWith($float) {
		return $this->orEndsWith(VentasMovimiento::TOTAL, $float);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orTotalContains($float) {
		return $this->orContains(VentasMovimiento::TOTAL, $float);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByTotalAsc() {
		return $this->orderBy(VentasMovimiento::TOTAL, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByTotalDesc() {
		return $this->orderBy(VentasMovimiento::TOTAL, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByTotal() {
		return $this->groupBy(VentasMovimiento::TOTAL);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivo($integer) {
		return $this->addAnd(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoNot($integer) {
		return $this->andNot(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoLike($integer) {
		return $this->andLike(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoNotLike($integer) {
		return $this->andNotLike(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoGreater($integer) {
		return $this->andGreater(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoLess($integer) {
		return $this->andLess(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoLessEqual($integer) {
		return $this->andLessEqual(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoNull() {
		return $this->andNull(VentasMovimiento::EFECTIVO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoNotNull() {
		return $this->andNotNull(VentasMovimiento::EFECTIVO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoBetween($integer, $from, $to) {
		return $this->andBetween(VentasMovimiento::EFECTIVO, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoBeginsWith($integer) {
		return $this->andBeginsWith(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoEndsWith($integer) {
		return $this->andEndsWith(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andEfectivoContains($integer) {
		return $this->andContains(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivo($integer) {
		return $this->or(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoNot($integer) {
		return $this->orNot(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoLike($integer) {
		return $this->orLike(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoNotLike($integer) {
		return $this->orNotLike(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoGreater($integer) {
		return $this->orGreater(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoLess($integer) {
		return $this->orLess(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoLessEqual($integer) {
		return $this->orLessEqual(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoNull() {
		return $this->orNull(VentasMovimiento::EFECTIVO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoNotNull() {
		return $this->orNotNull(VentasMovimiento::EFECTIVO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoBetween($integer, $from, $to) {
		return $this->orBetween(VentasMovimiento::EFECTIVO, $integer, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoBeginsWith($integer) {
		return $this->orBeginsWith(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoEndsWith($integer) {
		return $this->orEndsWith(VentasMovimiento::EFECTIVO, $integer);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orEfectivoContains($integer) {
		return $this->orContains(VentasMovimiento::EFECTIVO, $integer);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByEfectivoAsc() {
		return $this->orderBy(VentasMovimiento::EFECTIVO, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByEfectivoDesc() {
		return $this->orderBy(VentasMovimiento::EFECTIVO, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByEfectivo() {
		return $this->groupBy(VentasMovimiento::EFECTIVO);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andResta($decimal) {
		return $this->addAnd(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaNot($decimal) {
		return $this->andNot(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaLike($decimal) {
		return $this->andLike(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaNotLike($decimal) {
		return $this->andNotLike(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaGreater($decimal) {
		return $this->andGreater(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaGreaterEqual($decimal) {
		return $this->andGreaterEqual(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaLess($decimal) {
		return $this->andLess(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaLessEqual($decimal) {
		return $this->andLessEqual(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaNull() {
		return $this->andNull(VentasMovimiento::RESTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaNotNull() {
		return $this->andNotNull(VentasMovimiento::RESTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaBetween($decimal, $from, $to) {
		return $this->andBetween(VentasMovimiento::RESTA, $decimal, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaBeginsWith($decimal) {
		return $this->andBeginsWith(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaEndsWith($decimal) {
		return $this->andEndsWith(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andRestaContains($decimal) {
		return $this->andContains(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orResta($decimal) {
		return $this->or(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaNot($decimal) {
		return $this->orNot(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaLike($decimal) {
		return $this->orLike(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaNotLike($decimal) {
		return $this->orNotLike(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaGreater($decimal) {
		return $this->orGreater(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaGreaterEqual($decimal) {
		return $this->orGreaterEqual(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaLess($decimal) {
		return $this->orLess(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaLessEqual($decimal) {
		return $this->orLessEqual(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaNull() {
		return $this->orNull(VentasMovimiento::RESTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaNotNull() {
		return $this->orNotNull(VentasMovimiento::RESTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaBetween($decimal, $from, $to) {
		return $this->orBetween(VentasMovimiento::RESTA, $decimal, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaBeginsWith($decimal) {
		return $this->orBeginsWith(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaEndsWith($decimal) {
		return $this->orEndsWith(VentasMovimiento::RESTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orRestaContains($decimal) {
		return $this->orContains(VentasMovimiento::RESTA, $decimal);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByRestaAsc() {
		return $this->orderBy(VentasMovimiento::RESTA, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByRestaDesc() {
		return $this->orderBy(VentasMovimiento::RESTA, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByResta() {
		return $this->groupBy(VentasMovimiento::RESTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuota($decimal) {
		return $this->addAnd(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaNot($decimal) {
		return $this->andNot(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaLike($decimal) {
		return $this->andLike(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaNotLike($decimal) {
		return $this->andNotLike(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaGreater($decimal) {
		return $this->andGreater(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaGreaterEqual($decimal) {
		return $this->andGreaterEqual(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaLess($decimal) {
		return $this->andLess(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaLessEqual($decimal) {
		return $this->andLessEqual(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaNull() {
		return $this->andNull(VentasMovimiento::CUOTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaNotNull() {
		return $this->andNotNull(VentasMovimiento::CUOTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaBetween($decimal, $from, $to) {
		return $this->andBetween(VentasMovimiento::CUOTA, $decimal, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaBeginsWith($decimal) {
		return $this->andBeginsWith(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaEndsWith($decimal) {
		return $this->andEndsWith(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function andCuotaContains($decimal) {
		return $this->andContains(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuota($decimal) {
		return $this->or(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaNot($decimal) {
		return $this->orNot(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaLike($decimal) {
		return $this->orLike(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaNotLike($decimal) {
		return $this->orNotLike(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaGreater($decimal) {
		return $this->orGreater(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaGreaterEqual($decimal) {
		return $this->orGreaterEqual(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaLess($decimal) {
		return $this->orLess(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaLessEqual($decimal) {
		return $this->orLessEqual(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaNull() {
		return $this->orNull(VentasMovimiento::CUOTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaNotNull() {
		return $this->orNotNull(VentasMovimiento::CUOTA);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaBetween($decimal, $from, $to) {
		return $this->orBetween(VentasMovimiento::CUOTA, $decimal, $from, $to);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaBeginsWith($decimal) {
		return $this->orBeginsWith(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaEndsWith($decimal) {
		return $this->orEndsWith(VentasMovimiento::CUOTA, $decimal);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orCuotaContains($decimal) {
		return $this->orContains(VentasMovimiento::CUOTA, $decimal);
	}


	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByCuotaAsc() {
		return $this->orderBy(VentasMovimiento::CUOTA, self::ASC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function orderByCuotaDesc() {
		return $this->orderBy(VentasMovimiento::CUOTA, self::DESC);
	}

	/**
	 * @return VentasMovimientoQuery
	 */
	function groupByCuota() {
		return $this->groupBy(VentasMovimiento::CUOTA);
	}


}