
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- acciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `acciones`;

CREATE TABLE `acciones`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`accion` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- amortizaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `amortizaciones`;

CREATE TABLE `amortizaciones`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`monto` FLOAT NOT NULL,
	`cronograma_de_pago_id` INTEGER NOT NULL,
	`movimiento_de_dinero_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `cronograma_de_pago_id` (`cronograma_de_pago_id`),
	INDEX `movimiento_de_dinero_id` (`movimiento_de_dinero_id`),
	CONSTRAINT `amortizaciones_ibfk_1`
		FOREIGN KEY (`cronograma_de_pago_id`)
		REFERENCES `cronogramas_de_pago` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `amortizaciones_ibfk_2`
		FOREIGN KEY (`movimiento_de_dinero_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cajas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cajas`;

CREATE TABLE `cajas`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`monto_abierto` FLOAT NOT NULL,
	`monto_cerrado` FLOAT NOT NULL,
	`fecha_cerrada` DATETIME NOT NULL,
	`fecha_abierta` DATETIME NOT NULL,
	`usuario_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	`estado` FLOAT DEFAULT 1 NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `usuario_id` (`usuario_id`),
	CONSTRAINT `cajas_ibfk_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- clientes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(40) NOT NULL,
	`apellidos` VARCHAR(40),
	`telefono` VARCHAR(15),
	`direccion` VARCHAR(40) NOT NULL,
	`correo` VARCHAR(25),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`dni` VARCHAR(8),
	`ruc` VARCHAR(11),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- conceptos_de_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `conceptos_de_pago`;

CREATE TABLE `conceptos_de_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(60) NOT NULL,
	`movimiento_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`monto` DECIMAL DEFAULT 0.00,
	PRIMARY KEY (`id`),
	INDEX `tipo_de_pago_id` (`movimiento_id`),
	CONSTRAINT `conceptos_de_pago_ibfk_1`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cronogramas_de_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cronogramas_de_pago`;

CREATE TABLE `cronogramas_de_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`fecha_pagar` DATE NOT NULL,
	`cuota` FLOAT NOT NULL,
	`monto_pagado` FLOAT NOT NULL,
	`monto_total` FLOAT NOT NULL,
	`monto_pagar` FLOAT NOT NULL,
	`venta_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `venta_id` (`venta_id`),
	CONSTRAINT `cronogramas_de_pago_ibfk_1`
		FOREIGN KEY (`venta_id`)
		REFERENCES `ventas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- deliveries
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `deliveries`;

CREATE TABLE `deliveries`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`Fecha` DATE NOT NULL,
	`direccion` VARCHAR(40),
	`referencia` VARCHAR(40),
	`deliverista_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`cliente_id` INTEGER,
	`tiempo_demora` INTEGER,
	`estado_delivery` INTEGER(1) DEFAULT 0,
	PRIMARY KEY (`id`),
	INDEX `empleado_id` (`deliverista_id`),
	INDEX `cliente_id` (`cliente_id`),
	CONSTRAINT `deliveries_ibfk_1`
		FOREIGN KEY (`deliverista_id`)
		REFERENCES `deliverista` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `deliveries_ibfk_2`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- deliverista
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `deliverista`;

CREATE TABLE `deliverista`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(40) NOT NULL,
	`apellidos` VARCHAR(40) NOT NULL,
	`telefono` INTEGER(15) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- formas_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pago`;

CREATE TABLE `formas_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(100) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- grupos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `grupos`;

CREATE TABLE `grupos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(50) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- mesas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mesas`;

CREATE TABLE `mesas`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`numero` INTEGER(255) NOT NULL,
	`estado_mesa` VARCHAR(255) DEFAULT 'disponible' NOT NULL,
	`mesa` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- meseros
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `meseros`;

CREATE TABLE `meseros`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(40) NOT NULL,
	`apellidos` VARCHAR(40) NOT NULL,
	`sexo` VARCHAR(20) NOT NULL,
	`sueldo_basico` FLOAT NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- modulos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `modulos`;

CREATE TABLE `modulos`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- movimientos_de_dinero
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movimientos_de_dinero`;

CREATE TABLE `movimientos_de_dinero`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`numero_voucher` INTEGER,
	`voucher_id` INTEGER,
	`concepto_de_pago_id` INTEGER,
	`caja_id` INTEGER NOT NULL,
	`usuario_id` INTEGER NOT NULL,
	`forma_pago_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `voucher_id` (`voucher_id`),
	INDEX `concepto_de_pago_id` (`concepto_de_pago_id`),
	INDEX `caja_id` (`caja_id`),
	INDEX `usuario_id` (`usuario_id`),
	INDEX `forma_pago_id` (`forma_pago_id`),
	CONSTRAINT `movimientos_de_dinero_ibfk_1`
		FOREIGN KEY (`voucher_id`)
		REFERENCES `vouchers` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_3`
		FOREIGN KEY (`caja_id`)
		REFERENCES `cajas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_4`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_5`
		FOREIGN KEY (`forma_pago_id`)
		REFERENCES `formas_pago` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_6`
		FOREIGN KEY (`concepto_de_pago_id`)
		REFERENCES `tipos_concepto_pago` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- pedido_producto
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pedido_producto`;

CREATE TABLE `pedido_producto`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`producto_id` INTEGER NOT NULL,
	`venta_id` INTEGER NOT NULL,
	`cantidad` INTEGER DEFAULT 1,
	`precio` FLOAT,
	`total` FLOAT DEFAULT 0,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`tiempo_espera` VARCHAR(255) DEFAULT '0',
	`estado_pedido` TINYINT(1) DEFAULT 0,
	PRIMARY KEY (`id`),
	INDEX `producto_id` (`producto_id`),
	INDEX `pedidos_id` (`venta_id`),
	CONSTRAINT `pedido_producto_ibfk_2`
		FOREIGN KEY (`venta_id`)
		REFERENCES `ventas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `pk_producto`
		FOREIGN KEY (`producto_id`)
		REFERENCES `productos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos`;

CREATE TABLE `permisos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idaccion` INTEGER NOT NULL,
	`idsubmodulo` INTEGER,
	`nombre` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `idaccion` (`idaccion`),
	INDEX `idmodulo` (`idsubmodulo`),
	INDEX `permisos_ibfk_3` (`idmodulo`),
	CONSTRAINT `permisos_ibfk_2`
		FOREIGN KEY (`idaccion`)
		REFERENCES `acciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_3`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_4`
		FOREIGN KEY (`idsubmodulo`)
		REFERENCES `submodulo` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_grupo`;

CREATE TABLE `permisos_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idpermiso` INTEGER NOT NULL,
	`idgrupo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `permisos_grupo_ibfk_1` (`idpermiso`),
	INDEX `permisos_grupo_ibfk_2` (`idgrupo`),
	CONSTRAINT `permisos_grupo_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_grupo_ibfk_2`
		FOREIGN KEY (`idgrupo`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_usuario
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_usuario`;

CREATE TABLE `permisos_usuario`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idusuario` INTEGER,
	`idpermiso` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `permisos_usuario_ibfk_1` (`idpermiso`),
	INDEX `permisos_usuario_ibfk_2` (`idusuario`),
	CONSTRAINT `permisos_usuario_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_usuario_ibfk_2`
		FOREIGN KEY (`idusuario`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- producto_delivery
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `producto_delivery`;

CREATE TABLE `producto_delivery`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`producto_id` INTEGER NOT NULL,
	`delivery_id` INTEGER NOT NULL,
	`cantidad` INTEGER DEFAULT 1 NOT NULL,
	`total` FLOAT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`precio` DECIMAL,
	PRIMARY KEY (`id`),
	INDEX `producto_id` (`producto_id`),
	INDEX `delivery_id` (`delivery_id`),
	CONSTRAINT `producto_delivery_ibfk_1`
		FOREIGN KEY (`producto_id`)
		REFERENCES `productos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `producto_delivery_ibfk_2`
		FOREIGN KEY (`delivery_id`)
		REFERENCES `deliveries` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- productos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(40) NOT NULL,
	`precio` FLOAT NOT NULL,
	`tipo_producto_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	`descripcion` VARCHAR(150),
	PRIMARY KEY (`id`),
	INDEX `tipo_producto_id` (`tipo_producto_id`),
	CONSTRAINT `productos_ibfk_1`
		FOREIGN KEY (`tipo_producto_id`)
		REFERENCES `tipos_producto` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- series
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `series`;

CREATE TABLE `series`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`serie` INTEGER NOT NULL,
	`correlativo` INTEGER NOT NULL,
	`voucher_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `voucher_id` (`voucher_id`),
	CONSTRAINT `series_ibfk_1`
		FOREIGN KEY (`voucher_id`)
		REFERENCES `vouchers` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- submodulo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `submodulo`;

CREATE TABLE `submodulo`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `pk_mod` (`idmodulo`),
	CONSTRAINT `submodulo_ibfk_1`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos_concepto_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_concepto_pago`;

CREATE TABLE `tipos_concepto_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(50) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos_producto
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_producto`;

CREATE TABLE `tipos_producto`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(40) NOT NULL,
	`descripcion` TEXT NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- urls_permitidas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urls_permitidas`;

CREATE TABLE `urls_permitidas`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`url` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(50) NOT NULL,
	`apellidos` VARCHAR(50) NOT NULL,
	`clave` VARCHAR(50) NOT NULL,
	`fecha_ingreso` DATE NOT NULL,
	`fecha_salida` DATE NOT NULL,
	`esta_activo` VARCHAR(20) NOT NULL,
	`correo` VARCHAR(50) NOT NULL,
	`telefono` INTEGER NOT NULL,
	`direccion` VARCHAR(60) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios_grupo`;

CREATE TABLE `usuarios_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER NOT NULL,
	`grupo_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `usuario_id` (`usuario_id`),
	INDEX `grupo_id` (`grupo_id`),
	CONSTRAINT `usuarios_grupo_ibfk_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `usuarios_grupo_ibfk_2`
		FOREIGN KEY (`grupo_id`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`mesero_id` INTEGER,
	`cliente_id` INTEGER,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`estado_venta` TINYINT(1) DEFAULT 0 NOT NULL,
	`fecha` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `mesero_id` (`mesero_id`),
	INDEX `cliente_id` (`cliente_id`),
	CONSTRAINT `ventas_ibfk_1`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_ibfk_3`
		FOREIGN KEY (`mesero_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas_delivery
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas_delivery`;

CREATE TABLE `ventas_delivery`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`movimiento_id` INTEGER NOT NULL,
	`delivery_id` INTEGER NOT NULL,
	`subtotal` FLOAT NOT NULL,
	`fecha` DATETIME NOT NULL,
	`descuento` FLOAT,
	`porcentaje` INTEGER,
	`total` FLOAT NOT NULL,
	`efectivo` INTEGER,
	`cliente_id` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `movimiento_id` (`movimiento_id`),
	INDEX `delivery` (`delivery_id`),
	INDEX `ventas_delivery_ibfk_3` (`cliente_id`),
	CONSTRAINT `ventas_delivery_ibfk_1`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_delivery_ibfk_2`
		FOREIGN KEY (`delivery_id`)
		REFERENCES `deliveries` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_delivery_ibfk_3`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas_mesas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas_mesas`;

CREATE TABLE `ventas_mesas`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`venta_id` INTEGER NOT NULL,
	`mesa_id` INTEGER NOT NULL,
	`estado_venta_mesa` TINYINT(1) DEFAULT 0 NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `pedido_id` (`venta_id`),
	INDEX `mesas_id` (`mesa_id`),
	CONSTRAINT `ventas_mesas_ibfk_1`
		FOREIGN KEY (`mesa_id`)
		REFERENCES `mesas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_mesas_ibfk_2`
		FOREIGN KEY (`venta_id`)
		REFERENCES `ventas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas_movimiento
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas_movimiento`;

CREATE TABLE `ventas_movimiento`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`venta_id` INTEGER NOT NULL,
	`subtotal` FLOAT NOT NULL,
	`movimiento_id` INTEGER NOT NULL,
	`fecha` DATETIME NOT NULL,
	`cliente_id` INTEGER NOT NULL,
	`descuento` FLOAT,
	`porcentaje` INTEGER,
	`total` FLOAT NOT NULL,
	`efectivo` INTEGER DEFAULT 0,
	`resta` DECIMAL(8,2),
	`cuota` DECIMAL(8,2),
	PRIMARY KEY (`id`),
	INDEX `venta_id` (`venta_id`),
	INDEX `movimiento_de_dinero_id` (`movimiento_id`),
	INDEX `cliente_id` (`cliente_id`),
	CONSTRAINT `ventas_movimiento_ibfk_2`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_movimiento_ibfk_3`
		FOREIGN KEY (`venta_id`)
		REFERENCES `ventas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_movimiento_ibfk_4`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- vouchers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vouchers`;

CREATE TABLE `vouchers`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`tipo_comprobante` VARCHAR(40) NOT NULL,
	`fecha` DATE NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
